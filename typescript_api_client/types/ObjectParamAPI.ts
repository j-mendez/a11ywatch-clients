import { ResponseContext, RequestContext, HttpFile } from '../http/http';
import * as models from '../models/all';
import { Configuration} from '../configuration'

import { Analytics } from '../models/Analytics';
import { AuthInput } from '../models/AuthInput';
import { CrawlInput } from '../models/CrawlInput';
import { GenericInput } from '../models/GenericInput';
import { History } from '../models/History';
import { Issues } from '../models/Issues';
import { IssuesInfo } from '../models/IssuesInfo';
import { PageIssue } from '../models/PageIssue';
import { PageLoadTime } from '../models/PageLoadTime';
import { Pages } from '../models/Pages';
import { Report } from '../models/Report';
import { Users } from '../models/Users';
import { WebsiteInput } from '../models/WebsiteInput';
import { Websites } from '../models/Websites';
import { WebsitesInsight } from '../models/WebsitesInsight';

import { ObservableCollectionApi } from "./ObservableAPI";
import { CollectionApiRequestFactory, CollectionApiResponseProcessor} from "../apis/CollectionApi";

export interface CollectionApiGetAnalyticsRequest {
    /**
     * The page offset for the next set
     * @type string
     * @memberof CollectionApigetAnalytics
     */
    offset?: string
    /**
     * Domain of website that needs to be fetched
     * @type string
     * @memberof CollectionApigetAnalytics
     */
    domain?: string
}

export interface CollectionApiGetIssuesRequest {
    /**
     * The page offset for the next set
     * @type string
     * @memberof CollectionApigetIssues
     */
    offset?: string
    /**
     * Domain of website that needs to be fetched
     * @type string
     * @memberof CollectionApigetIssues
     */
    domain?: string
}

export interface CollectionApiGetPageSpeedRequest {
    /**
     * The page offset for the next set
     * @type string
     * @memberof CollectionApigetPageSpeed
     */
    offset?: string
    /**
     * Domain of website that needs to be fetched
     * @type string
     * @memberof CollectionApigetPageSpeed
     */
    domain?: string
}

export interface CollectionApiGetPagesRequest {
    /**
     * The page offset for the next set
     * @type string
     * @memberof CollectionApigetPages
     */
    offset?: string
    /**
     * Domain of website that needs to be fetched
     * @type string
     * @memberof CollectionApigetPages
     */
    domain?: string
}

export interface CollectionApiGetWebsitesRequest {
    /**
     * The page offset for the next set
     * @type string
     * @memberof CollectionApigetWebsites
     */
    offset?: string
}

export class ObjectCollectionApi {
    private api: ObservableCollectionApi

    public constructor(configuration: Configuration, requestFactory?: CollectionApiRequestFactory, responseProcessor?: CollectionApiResponseProcessor) {
        this.api = new ObservableCollectionApi(configuration, requestFactory, responseProcessor);
    }

    /**
     * Get the analytics for a website
     * @param param the request object
     */
    public getAnalytics(param: CollectionApiGetAnalyticsRequest, options?: Configuration): Promise<any> {
        return this.api.getAnalytics(param.offset, param.domain,  options).toPromise();
    }

    /**
     * List the issues for a website
     * @param param the request object
     */
    public getIssues(param: CollectionApiGetIssuesRequest, options?: Configuration): Promise<any> {
        return this.api.getIssues(param.offset, param.domain,  options).toPromise();
    }

    /**
     * Get the pagespeed for a website
     * @param param the request object
     */
    public getPageSpeed(param: CollectionApiGetPageSpeedRequest, options?: Configuration): Promise<any> {
        return this.api.getPageSpeed(param.offset, param.domain,  options).toPromise();
    }

    /**
     * List the pages in order for a website
     * @param param the request object
     */
    public getPages(param: CollectionApiGetPagesRequest, options?: Configuration): Promise<any> {
        return this.api.getPages(param.offset, param.domain,  options).toPromise();
    }

    /**
     * Returns a map of websites
     * Returns websites for the user in alphabetical order
     * @param param the request object
     */
    public getWebsites(param: CollectionApiGetWebsitesRequest, options?: Configuration): Promise<any> {
        return this.api.getWebsites(param.offset,  options).toPromise();
    }

}

import { ObservableReportsApi } from "./ObservableAPI";
import { ReportsApiRequestFactory, ReportsApiResponseProcessor} from "../apis/ReportsApi";

export interface ReportsApiCrawlWebsiteStreamRequest {
    /**
     * 
     * @type string
     * @memberof ReportsApicrawlWebsiteStream
     */
    transferEncoding: string
    /**
     * The website standard body
     * @type WebsiteInput
     * @memberof ReportsApicrawlWebsiteStream
     */
    websiteInput: WebsiteInput
}

export interface ReportsApiCrawlWebsitesSyncRequest {
}

export interface ReportsApiGetReportRequest {
    /**
     * The page url or domain for the report
     * @type string
     * @memberof ReportsApigetReport
     */
    url?: string
    /**
     * Domain of website that needs to be fetched
     * @type string
     * @memberof ReportsApigetReport
     */
    domain?: string
}

export interface ReportsApiScanWebsiteRequest {
    /**
     * The website standard body
     * @type WebsiteInput
     * @memberof ReportsApiscanWebsite
     */
    websiteInput: WebsiteInput
}

export interface ReportsApiScanWebsiteSimpleRequest {
    /**
     * The website standard body
     * @type WebsiteInput
     * @memberof ReportsApiscanWebsiteSimple
     */
    websiteInput: WebsiteInput
}

export class ObjectReportsApi {
    private api: ObservableReportsApi

    public constructor(configuration: Configuration, requestFactory?: ReportsApiRequestFactory, responseProcessor?: ReportsApiResponseProcessor) {
        this.api = new ObservableReportsApi(configuration, requestFactory, responseProcessor);
    }

    /**
     * Multi-page crawl a website streaming issues on found
     * @param param the request object
     */
    public crawlWebsiteStream(param: ReportsApiCrawlWebsiteStreamRequest, options?: Configuration): Promise<any> {
        return this.api.crawlWebsiteStream(param.transferEncoding, param.websiteInput,  options).toPromise();
    }

    /**
     * Multi-page crawl all websites attached to account
     * @param param the request object
     */
    public crawlWebsitesSync(param: ReportsApiCrawlWebsitesSyncRequest, options?: Configuration): Promise<any> {
        return this.api.crawlWebsitesSync( options).toPromise();
    }

    /**
     * Get the report from a previus scan
     * @param param the request object
     */
    public getReport(param: ReportsApiGetReportRequest, options?: Configuration): Promise<any> {
        return this.api.getReport(param.url, param.domain,  options).toPromise();
    }

    /**
     * Scan a website for issues
     * @param param the request object
     */
    public scanWebsite(param: ReportsApiScanWebsiteRequest, options?: Configuration): Promise<any> {
        return this.api.scanWebsite(param.websiteInput,  options).toPromise();
    }

    /**
     * Scan a website for issues without storing data and limited responses.
     * @param param the request object
     */
    public scanWebsiteSimple(param: ReportsApiScanWebsiteSimpleRequest, options?: Configuration): Promise<any> {
        return this.api.scanWebsiteSimple(param.websiteInput,  options).toPromise();
    }

}

import { ObservableUserApi } from "./ObservableAPI";
import { UserApiRequestFactory, UserApiResponseProcessor} from "../apis/UserApi";

export interface UserApiCreateUserRequest {
    /**
     * The auth standard body
     * @type AuthInput
     * @memberof UserApicreateUser
     */
    authInput: AuthInput
}

export interface UserApiGetUsersRequest {
}

export interface UserApiLoginUserRequest {
    /**
     * The auth standard body
     * @type AuthInput
     * @memberof UserApiloginUser
     */
    authInput: AuthInput
}

export interface UserApiLogoutUserRequest {
}

export class ObjectUserApi {
    private api: ObservableUserApi

    public constructor(configuration: Configuration, requestFactory?: UserApiRequestFactory, responseProcessor?: UserApiResponseProcessor) {
        this.api = new ObservableUserApi(configuration, requestFactory, responseProcessor);
    }

    /**
     * Adds a new user not created yet into the system
     * Register user into the system
     * @param param the request object
     */
    public createUser(param: UserApiCreateUserRequest, options?: Configuration): Promise<Users> {
        return this.api.createUser(param.authInput,  options).toPromise();
    }

    /**
     * Retrieve the current user.
     * Get user
     * @param param the request object
     */
    public getUsers(param: UserApiGetUsersRequest, options?: Configuration): Promise<void> {
        return this.api.getUsers( options).toPromise();
    }

    /**
     * Logs user into the system
     * @param param the request object
     */
    public loginUser(param: UserApiLoginUserRequest, options?: Configuration): Promise<Users> {
        return this.api.loginUser(param.authInput,  options).toPromise();
    }

    /**
     * Logs out current logged in user session
     * @param param the request object
     */
    public logoutUser(param: UserApiLogoutUserRequest, options?: Configuration): Promise<void> {
        return this.api.logoutUser( options).toPromise();
    }

}

import { ObservableWebsitesApi } from "./ObservableAPI";
import { WebsitesApiRequestFactory, WebsitesApiResponseProcessor} from "../apis/WebsitesApi";

export interface WebsitesApiAddWebsiteRequest {
    /**
     * The website standard body
     * @type WebsiteInput
     * @memberof WebsitesApiaddWebsite
     */
    websiteInput: WebsiteInput
}

export interface WebsitesApiDeleteWebsiteRequest {
    /**
     * Websites domain to delete
     * @type string
     * @memberof WebsitesApideleteWebsite
     */
    domain: string
}

export interface WebsitesApiGetWebsiteByDomainRequest {
    /**
     * Domain of website that needs to be fetched
     * @type string
     * @memberof WebsitesApigetWebsiteByDomain
     */
    domain: string
}

export class ObjectWebsitesApi {
    private api: ObservableWebsitesApi

    public constructor(configuration: Configuration, requestFactory?: WebsitesApiRequestFactory, responseProcessor?: WebsitesApiResponseProcessor) {
        this.api = new ObservableWebsitesApi(configuration, requestFactory, responseProcessor);
    }

    /**
     * Add a website in the collection with form data
     * @param param the request object
     */
    public addWebsite(param: WebsitesApiAddWebsiteRequest, options?: Configuration): Promise<any> {
        return this.api.addWebsite(param.websiteInput,  options).toPromise();
    }

    /**
     * Deletes a website
     * @param param the request object
     */
    public deleteWebsite(param: WebsitesApiDeleteWebsiteRequest, options?: Configuration): Promise<any> {
        return this.api.deleteWebsite(param.domain,  options).toPromise();
    }

    /**
     * Returns a website when DOMAIN == website.domain.  Empty strings will simulate API error conditions
     * Find website by Domain
     * @param param the request object
     */
    public getWebsiteByDomain(param: WebsitesApiGetWebsiteByDomainRequest, options?: Configuration): Promise<any> {
        return this.api.getWebsiteByDomain(param.domain,  options).toPromise();
    }

}
