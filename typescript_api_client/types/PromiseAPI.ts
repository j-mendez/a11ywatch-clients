import { ResponseContext, RequestContext, HttpFile } from '../http/http';
import * as models from '../models/all';
import { Configuration} from '../configuration'

import { Analytics } from '../models/Analytics';
import { AuthInput } from '../models/AuthInput';
import { CrawlInput } from '../models/CrawlInput';
import { GenericInput } from '../models/GenericInput';
import { History } from '../models/History';
import { Issues } from '../models/Issues';
import { IssuesInfo } from '../models/IssuesInfo';
import { PageIssue } from '../models/PageIssue';
import { PageLoadTime } from '../models/PageLoadTime';
import { Pages } from '../models/Pages';
import { Report } from '../models/Report';
import { Users } from '../models/Users';
import { WebsiteInput } from '../models/WebsiteInput';
import { Websites } from '../models/Websites';
import { WebsitesInsight } from '../models/WebsitesInsight';
import { ObservableCollectionApi } from './ObservableAPI';

import { CollectionApiRequestFactory, CollectionApiResponseProcessor} from "../apis/CollectionApi";
export class PromiseCollectionApi {
    private api: ObservableCollectionApi

    public constructor(
        configuration: Configuration,
        requestFactory?: CollectionApiRequestFactory,
        responseProcessor?: CollectionApiResponseProcessor
    ) {
        this.api = new ObservableCollectionApi(configuration, requestFactory, responseProcessor);
    }

    /**
     * Get the analytics for a website
     * @param offset The page offset for the next set
     * @param domain Domain of website that needs to be fetched
     */
    public getAnalytics(offset?: string, domain?: string, _options?: Configuration): Promise<any> {
        const result = this.api.getAnalytics(offset, domain, _options);
        return result.toPromise();
    }

    /**
     * List the issues for a website
     * @param offset The page offset for the next set
     * @param domain Domain of website that needs to be fetched
     */
    public getIssues(offset?: string, domain?: string, _options?: Configuration): Promise<any> {
        const result = this.api.getIssues(offset, domain, _options);
        return result.toPromise();
    }

    /**
     * Get the pagespeed for a website
     * @param offset The page offset for the next set
     * @param domain Domain of website that needs to be fetched
     */
    public getPageSpeed(offset?: string, domain?: string, _options?: Configuration): Promise<any> {
        const result = this.api.getPageSpeed(offset, domain, _options);
        return result.toPromise();
    }

    /**
     * List the pages in order for a website
     * @param offset The page offset for the next set
     * @param domain Domain of website that needs to be fetched
     */
    public getPages(offset?: string, domain?: string, _options?: Configuration): Promise<any> {
        const result = this.api.getPages(offset, domain, _options);
        return result.toPromise();
    }

    /**
     * Returns a map of websites
     * Returns websites for the user in alphabetical order
     * @param offset The page offset for the next set
     */
    public getWebsites(offset?: string, _options?: Configuration): Promise<any> {
        const result = this.api.getWebsites(offset, _options);
        return result.toPromise();
    }


}



import { ObservableReportsApi } from './ObservableAPI';

import { ReportsApiRequestFactory, ReportsApiResponseProcessor} from "../apis/ReportsApi";
export class PromiseReportsApi {
    private api: ObservableReportsApi

    public constructor(
        configuration: Configuration,
        requestFactory?: ReportsApiRequestFactory,
        responseProcessor?: ReportsApiResponseProcessor
    ) {
        this.api = new ObservableReportsApi(configuration, requestFactory, responseProcessor);
    }

    /**
     * Multi-page crawl a website streaming issues on found
     * @param transferEncoding 
     * @param websiteInput The website standard body
     */
    public crawlWebsiteStream(transferEncoding: string, websiteInput: WebsiteInput, _options?: Configuration): Promise<any> {
        const result = this.api.crawlWebsiteStream(transferEncoding, websiteInput, _options);
        return result.toPromise();
    }

    /**
     * Multi-page crawl all websites attached to account
     */
    public crawlWebsitesSync(_options?: Configuration): Promise<any> {
        const result = this.api.crawlWebsitesSync(_options);
        return result.toPromise();
    }

    /**
     * Get the report from a previus scan
     * @param url The page url or domain for the report
     * @param domain Domain of website that needs to be fetched
     */
    public getReport(url?: string, domain?: string, _options?: Configuration): Promise<any> {
        const result = this.api.getReport(url, domain, _options);
        return result.toPromise();
    }

    /**
     * Scan a website for issues
     * @param websiteInput The website standard body
     */
    public scanWebsite(websiteInput: WebsiteInput, _options?: Configuration): Promise<any> {
        const result = this.api.scanWebsite(websiteInput, _options);
        return result.toPromise();
    }

    /**
     * Scan a website for issues without storing data and limited responses.
     * @param websiteInput The website standard body
     */
    public scanWebsiteSimple(websiteInput: WebsiteInput, _options?: Configuration): Promise<any> {
        const result = this.api.scanWebsiteSimple(websiteInput, _options);
        return result.toPromise();
    }


}



import { ObservableUserApi } from './ObservableAPI';

import { UserApiRequestFactory, UserApiResponseProcessor} from "../apis/UserApi";
export class PromiseUserApi {
    private api: ObservableUserApi

    public constructor(
        configuration: Configuration,
        requestFactory?: UserApiRequestFactory,
        responseProcessor?: UserApiResponseProcessor
    ) {
        this.api = new ObservableUserApi(configuration, requestFactory, responseProcessor);
    }

    /**
     * Adds a new user not created yet into the system
     * Register user into the system
     * @param authInput The auth standard body
     */
    public createUser(authInput: AuthInput, _options?: Configuration): Promise<Users> {
        const result = this.api.createUser(authInput, _options);
        return result.toPromise();
    }

    /**
     * Retrieve the current user.
     * Get user
     */
    public getUsers(_options?: Configuration): Promise<void> {
        const result = this.api.getUsers(_options);
        return result.toPromise();
    }

    /**
     * Logs user into the system
     * @param authInput The auth standard body
     */
    public loginUser(authInput: AuthInput, _options?: Configuration): Promise<Users> {
        const result = this.api.loginUser(authInput, _options);
        return result.toPromise();
    }

    /**
     * Logs out current logged in user session
     */
    public logoutUser(_options?: Configuration): Promise<void> {
        const result = this.api.logoutUser(_options);
        return result.toPromise();
    }


}



import { ObservableWebsitesApi } from './ObservableAPI';

import { WebsitesApiRequestFactory, WebsitesApiResponseProcessor} from "../apis/WebsitesApi";
export class PromiseWebsitesApi {
    private api: ObservableWebsitesApi

    public constructor(
        configuration: Configuration,
        requestFactory?: WebsitesApiRequestFactory,
        responseProcessor?: WebsitesApiResponseProcessor
    ) {
        this.api = new ObservableWebsitesApi(configuration, requestFactory, responseProcessor);
    }

    /**
     * Add a website in the collection with form data
     * @param websiteInput The website standard body
     */
    public addWebsite(websiteInput: WebsiteInput, _options?: Configuration): Promise<any> {
        const result = this.api.addWebsite(websiteInput, _options);
        return result.toPromise();
    }

    /**
     * Deletes a website
     * @param domain Websites domain to delete
     */
    public deleteWebsite(domain: string, _options?: Configuration): Promise<any> {
        const result = this.api.deleteWebsite(domain, _options);
        return result.toPromise();
    }

    /**
     * Returns a website when DOMAIN == website.domain.  Empty strings will simulate API error conditions
     * Find website by Domain
     * @param domain Domain of website that needs to be fetched
     */
    public getWebsiteByDomain(domain: string, _options?: Configuration): Promise<any> {
        const result = this.api.getWebsiteByDomain(domain, _options);
        return result.toPromise();
    }


}



