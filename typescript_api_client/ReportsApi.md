# .ReportsApi

All URIs are relative to *https://api.a11ywatch.com/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**crawlWebsiteStream**](ReportsApi.md#crawlWebsiteStream) | **POST** /crawl | Multi-page crawl a website streaming issues on found
[**crawlWebsitesSync**](ReportsApi.md#crawlWebsitesSync) | **POST** /websites-sync | Multi-page crawl all websites attached to account
[**getReport**](ReportsApi.md#getReport) | **GET** /report | Get the report from a previus scan
[**scanWebsite**](ReportsApi.md#scanWebsite) | **POST** /scan | Scan a website for issues
[**scanWebsiteSimple**](ReportsApi.md#scanWebsiteSimple) | **POST** /scan-simple | Scan a website for issues without storing data and limited responses.


# **crawlWebsiteStream**
> any crawlWebsiteStream(websiteInput)


### Example


```typescript
import {  } from '';
import * as fs from 'fs';

const configuration = .createConfiguration();
const apiInstance = new .ReportsApi(configuration);

let body:.ReportsApiCrawlWebsiteStreamRequest = {
  // string
  transferEncoding: "Chunked",
  // WebsiteInput | The website standard body
  websiteInput: {
    url: "url_example",
    mobile: true,
    pageInsights: true,
    ua: "ua_example",
    standard: "standard_example",
    robots: "robots_example",
    subdomains: true,
    tld: true,
    customHeaders: [
      "customHeaders_example",
    ],
  },
};

apiInstance.crawlWebsiteStream(body).then((data:any) => {
  console.log('API called successfully. Returned data: ' + data);
}).catch((error:any) => console.error(error));
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **websiteInput** | **WebsiteInput**| The website standard body |
 **transferEncoding** | [**string**] |  | defaults to 'Chunked'


### Return type

**any**

### Authorization

[bearerAuth](README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**405** | Invalid input |  -  |

[[Back to top]](#) [[Back to API list]](README.md#documentation-for-api-endpoints) [[Back to Model list]](README.md#documentation-for-models) [[Back to README]](README.md)

# **crawlWebsitesSync**
> any crawlWebsitesSync()


### Example


```typescript
import {  } from '';
import * as fs from 'fs';

const configuration = .createConfiguration();
const apiInstance = new .ReportsApi(configuration);

let body:any = {};

apiInstance.crawlWebsitesSync(body).then((data:any) => {
  console.log('API called successfully. Returned data: ' + data);
}).catch((error:any) => console.error(error));
```


### Parameters
This endpoint does not need any parameter.


### Return type

**any**

### Authorization

[bearerAuth](README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**405** | Invalid input |  -  |

[[Back to top]](#) [[Back to API list]](README.md#documentation-for-api-endpoints) [[Back to Model list]](README.md#documentation-for-models) [[Back to README]](README.md)

# **getReport**
> any getReport()


### Example


```typescript
import {  } from '';
import * as fs from 'fs';

const configuration = .createConfiguration();
const apiInstance = new .ReportsApi(configuration);

let body:.ReportsApiGetReportRequest = {
  // string | The page url or domain for the report (optional)
  url: "url_example",
  // string | Domain of website that needs to be fetched (optional)
  domain: "domain_example",
};

apiInstance.getReport(body).then((data:any) => {
  console.log('API called successfully. Returned data: ' + data);
}).catch((error:any) => console.error(error));
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **url** | [**string**] | The page url or domain for the report | (optional) defaults to undefined
 **domain** | [**string**] | Domain of website that needs to be fetched | (optional) defaults to undefined


### Return type

**any**

### Authorization

[bearerAuth](README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**405** | Invalid input |  -  |

[[Back to top]](#) [[Back to API list]](README.md#documentation-for-api-endpoints) [[Back to Model list]](README.md#documentation-for-models) [[Back to README]](README.md)

# **scanWebsite**
> any scanWebsite(websiteInput)


### Example


```typescript
import {  } from '';
import * as fs from 'fs';

const configuration = .createConfiguration();
const apiInstance = new .ReportsApi(configuration);

let body:.ReportsApiScanWebsiteRequest = {
  // WebsiteInput | The website standard body
  websiteInput: {
    url: "url_example",
    mobile: true,
    pageInsights: true,
    ua: "ua_example",
    standard: "standard_example",
    robots: "robots_example",
    subdomains: true,
    tld: true,
    customHeaders: [
      "customHeaders_example",
    ],
  },
};

apiInstance.scanWebsite(body).then((data:any) => {
  console.log('API called successfully. Returned data: ' + data);
}).catch((error:any) => console.error(error));
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **websiteInput** | **WebsiteInput**| The website standard body |


### Return type

**any**

### Authorization

[bearerAuth](README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**405** | Invalid input |  -  |

[[Back to top]](#) [[Back to API list]](README.md#documentation-for-api-endpoints) [[Back to Model list]](README.md#documentation-for-models) [[Back to README]](README.md)

# **scanWebsiteSimple**
> any scanWebsiteSimple(websiteInput)


### Example


```typescript
import {  } from '';
import * as fs from 'fs';

const configuration = .createConfiguration();
const apiInstance = new .ReportsApi(configuration);

let body:.ReportsApiScanWebsiteSimpleRequest = {
  // WebsiteInput | The website standard body
  websiteInput: {
    url: "url_example",
    mobile: true,
    pageInsights: true,
    ua: "ua_example",
    standard: "standard_example",
    robots: "robots_example",
    subdomains: true,
    tld: true,
    customHeaders: [
      "customHeaders_example",
    ],
  },
};

apiInstance.scanWebsiteSimple(body).then((data:any) => {
  console.log('API called successfully. Returned data: ' + data);
}).catch((error:any) => console.error(error));
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **websiteInput** | **WebsiteInput**| The website standard body |


### Return type

**any**

### Authorization

[bearerAuth](README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**405** | Invalid input |  -  |

[[Back to top]](#) [[Back to API list]](README.md#documentation-for-api-endpoints) [[Back to Model list]](README.md#documentation-for-models) [[Back to README]](README.md)


