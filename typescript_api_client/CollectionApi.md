# .CollectionApi

All URIs are relative to *https://api.a11ywatch.com/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getAnalytics**](CollectionApi.md#getAnalytics) | **GET** /list/analytics | Get the analytics for a website
[**getIssues**](CollectionApi.md#getIssues) | **GET** /list/issue | List the issues for a website
[**getPageSpeed**](CollectionApi.md#getPageSpeed) | **GET** /list/pagespeed | Get the pagespeed for a website
[**getPages**](CollectionApi.md#getPages) | **GET** /list/pages | List the pages in order for a website
[**getWebsites**](CollectionApi.md#getWebsites) | **GET** /list/website | Returns websites for the user in alphabetical order


# **getAnalytics**
> any getAnalytics()


### Example


```typescript
import {  } from '';
import * as fs from 'fs';

const configuration = .createConfiguration();
const apiInstance = new .CollectionApi(configuration);

let body:.CollectionApiGetAnalyticsRequest = {
  // string | The page offset for the next set (optional)
  offset: "offset_example",
  // string | Domain of website that needs to be fetched (optional)
  domain: "domain_example",
};

apiInstance.getAnalytics(body).then((data:any) => {
  console.log('API called successfully. Returned data: ' + data);
}).catch((error:any) => console.error(error));
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **offset** | [**string**] | The page offset for the next set | (optional) defaults to undefined
 **domain** | [**string**] | Domain of website that needs to be fetched | (optional) defaults to undefined


### Return type

**any**

### Authorization

[bearerAuth](README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**400** | Invalid Analytics |  -  |

[[Back to top]](#) [[Back to API list]](README.md#documentation-for-api-endpoints) [[Back to Model list]](README.md#documentation-for-models) [[Back to README]](README.md)

# **getIssues**
> any getIssues()


### Example


```typescript
import {  } from '';
import * as fs from 'fs';

const configuration = .createConfiguration();
const apiInstance = new .CollectionApi(configuration);

let body:.CollectionApiGetIssuesRequest = {
  // string | The page offset for the next set (optional)
  offset: "offset_example",
  // string | Domain of website that needs to be fetched (optional)
  domain: "domain_example",
};

apiInstance.getIssues(body).then((data:any) => {
  console.log('API called successfully. Returned data: ' + data);
}).catch((error:any) => console.error(error));
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **offset** | [**string**] | The page offset for the next set | (optional) defaults to undefined
 **domain** | [**string**] | Domain of website that needs to be fetched | (optional) defaults to undefined


### Return type

**any**

### Authorization

[bearerAuth](README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**400** | Invalid Issues |  -  |

[[Back to top]](#) [[Back to API list]](README.md#documentation-for-api-endpoints) [[Back to Model list]](README.md#documentation-for-models) [[Back to README]](README.md)

# **getPageSpeed**
> any getPageSpeed()


### Example


```typescript
import {  } from '';
import * as fs from 'fs';

const configuration = .createConfiguration();
const apiInstance = new .CollectionApi(configuration);

let body:.CollectionApiGetPageSpeedRequest = {
  // string | The page offset for the next set (optional)
  offset: "offset_example",
  // string | Domain of website that needs to be fetched (optional)
  domain: "domain_example",
};

apiInstance.getPageSpeed(body).then((data:any) => {
  console.log('API called successfully. Returned data: ' + data);
}).catch((error:any) => console.error(error));
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **offset** | [**string**] | The page offset for the next set | (optional) defaults to undefined
 **domain** | [**string**] | Domain of website that needs to be fetched | (optional) defaults to undefined


### Return type

**any**

### Authorization

[bearerAuth](README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**400** | Invalid Analytics |  -  |

[[Back to top]](#) [[Back to API list]](README.md#documentation-for-api-endpoints) [[Back to Model list]](README.md#documentation-for-models) [[Back to README]](README.md)

# **getPages**
> any getPages()


### Example


```typescript
import {  } from '';
import * as fs from 'fs';

const configuration = .createConfiguration();
const apiInstance = new .CollectionApi(configuration);

let body:.CollectionApiGetPagesRequest = {
  // string | The page offset for the next set (optional)
  offset: "offset_example",
  // string | Domain of website that needs to be fetched (optional)
  domain: "domain_example",
};

apiInstance.getPages(body).then((data:any) => {
  console.log('API called successfully. Returned data: ' + data);
}).catch((error:any) => console.error(error));
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **offset** | [**string**] | The page offset for the next set | (optional) defaults to undefined
 **domain** | [**string**] | Domain of website that needs to be fetched | (optional) defaults to undefined


### Return type

**any**

### Authorization

[bearerAuth](README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**400** | Invalid Pages |  -  |

[[Back to top]](#) [[Back to API list]](README.md#documentation-for-api-endpoints) [[Back to Model list]](README.md#documentation-for-models) [[Back to README]](README.md)

# **getWebsites**
> any getWebsites()

Returns a map of websites

### Example


```typescript
import {  } from '';
import * as fs from 'fs';

const configuration = .createConfiguration();
const apiInstance = new .CollectionApi(configuration);

let body:.CollectionApiGetWebsitesRequest = {
  // string | The page offset for the next set (optional)
  offset: "offset_example",
};

apiInstance.getWebsites(body).then((data:any) => {
  console.log('API called successfully. Returned data: ' + data);
}).catch((error:any) => console.error(error));
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **offset** | [**string**] | The page offset for the next set | (optional) defaults to undefined


### Return type

**any**

### Authorization

[bearerAuth](README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](README.md#documentation-for-api-endpoints) [[Back to Model list]](README.md#documentation-for-models) [[Back to README]](README.md)


