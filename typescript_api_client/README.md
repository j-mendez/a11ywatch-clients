## @a11ywatch/client

This generator creates TypeScript/JavaScript client that utilizes fetch-api.

Example:

```ts
import {
  createConfiguration,
  ReportsApi,
  ServerConfiguration,
} from '@a11ywatch/client'

const configuration = createConfiguration({
    // replace localhost with api.a11ywatch.com for production
    baseServer: new ServerConfiguration('http://localhost:3280/api', {}),
    authMethods: {
        bearerAuth: {
        tokenProvider: {
            getToken: () => {
                // replace value with JWT
                return ""
            },
        },
        },
    },
})

const api = new ReportsApi(configuration)

// audit an entire website chunked
const report = await api.crawlWebsiteStream('chunked', {
    url: 'https://jeffmendez.com',
})

console.log(report) 
```

### Building

To build and compile the typescript sources to javascript use:
```
npm install
npm run build
```

### Publishing

First build the package then run ```npm publish```

### Consuming

navigate to the folder of your consuming project and run one of the following commands.

_published:_

```
npm install @ --save
```

_unPublished (not recommended):_

```
npm install PATH_TO_GENERATED_PACKAGE --save
