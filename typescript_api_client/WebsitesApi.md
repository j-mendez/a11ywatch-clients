# .WebsitesApi

All URIs are relative to *https://api.a11ywatch.com/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**addWebsite**](WebsitesApi.md#addWebsite) | **POST** /website | Add a website in the collection with form data
[**deleteWebsite**](WebsitesApi.md#deleteWebsite) | **DELETE** /website | Deletes a website
[**getWebsiteByDomain**](WebsitesApi.md#getWebsiteByDomain) | **GET** /website | Find website by Domain


# **addWebsite**
> any addWebsite(websiteInput)


### Example


```typescript
import {  } from '';
import * as fs from 'fs';

const configuration = .createConfiguration();
const apiInstance = new .WebsitesApi(configuration);

let body:.WebsitesApiAddWebsiteRequest = {
  // WebsiteInput | The website standard body
  websiteInput: {
    url: "url_example",
    mobile: true,
    pageInsights: true,
    ua: "ua_example",
    standard: "standard_example",
    robots: "robots_example",
    subdomains: true,
    tld: true,
    customHeaders: [
      "customHeaders_example",
    ],
  },
};

apiInstance.addWebsite(body).then((data:any) => {
  console.log('API called successfully. Returned data: ' + data);
}).catch((error:any) => console.error(error));
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **websiteInput** | **WebsiteInput**| The website standard body |


### Return type

**any**

### Authorization

[bearerAuth](README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**405** | Invalid input |  -  |

[[Back to top]](#) [[Back to API list]](README.md#documentation-for-api-endpoints) [[Back to Model list]](README.md#documentation-for-models) [[Back to README]](README.md)

# **deleteWebsite**
> any deleteWebsite()


### Example


```typescript
import {  } from '';
import * as fs from 'fs';

const configuration = .createConfiguration();
const apiInstance = new .WebsitesApi(configuration);

let body:.WebsitesApiDeleteWebsiteRequest = {
  // string | Websites domain to delete
  domain: "domain_example",
};

apiInstance.deleteWebsite(body).then((data:any) => {
  console.log('API called successfully. Returned data: ' + data);
}).catch((error:any) => console.error(error));
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **domain** | [**string**] | Websites domain to delete | defaults to undefined


### Return type

**any**

### Authorization

[bearerAuth](README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**400** | Invalid website value |  -  |

[[Back to top]](#) [[Back to API list]](README.md#documentation-for-api-endpoints) [[Back to Model list]](README.md#documentation-for-models) [[Back to README]](README.md)

# **getWebsiteByDomain**
> any getWebsiteByDomain()

Returns a website when DOMAIN == website.domain.  Empty strings will simulate API error conditions

### Example


```typescript
import {  } from '';
import * as fs from 'fs';

const configuration = .createConfiguration();
const apiInstance = new .WebsitesApi(configuration);

let body:.WebsitesApiGetWebsiteByDomainRequest = {
  // string | Domain of website that needs to be fetched
  domain: "domain_example",
};

apiInstance.getWebsiteByDomain(body).then((data:any) => {
  console.log('API called successfully. Returned data: ' + data);
}).catch((error:any) => console.error(error));
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **domain** | [**string**] | Domain of website that needs to be fetched | defaults to undefined


### Return type

**any**

### Authorization

[bearerAuth](README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**404** | Websites not found |  -  |
**200** | successful operation |  -  |
**400** | Invalid domain supplied |  -  |

[[Back to top]](#) [[Back to API list]](README.md#documentation-for-api-endpoints) [[Back to Model list]](README.md#documentation-for-models) [[Back to README]](README.md)


