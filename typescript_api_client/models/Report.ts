/**
 * A11ywatch Client
 * The web accessibility tool built for scale.
 *
 * OpenAPI spec version: 0.7.66
 * Contact: support@a11ywatch.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { IssuesInfo } from './IssuesInfo';
import { PageIssue } from './PageIssue';
import { HttpFile } from '../http/http';

export class Report {
    'id'?: number;
    'domain'?: string;
    'pageUrl'?: string;
    'issues'?: Array<PageIssue>;
    'issuesInfo'?: IssuesInfo;

    static readonly discriminator: string | undefined = undefined;

    static readonly attributeTypeMap: Array<{name: string, baseName: string, type: string, format: string}> = [
        {
            "name": "id",
            "baseName": "_id",
            "type": "number",
            "format": "int64"
        },
        {
            "name": "domain",
            "baseName": "domain",
            "type": "string",
            "format": ""
        },
        {
            "name": "pageUrl",
            "baseName": "pageUrl",
            "type": "string",
            "format": ""
        },
        {
            "name": "issues",
            "baseName": "issues",
            "type": "Array<PageIssue>",
            "format": ""
        },
        {
            "name": "issuesInfo",
            "baseName": "issuesInfo",
            "type": "IssuesInfo",
            "format": ""
        }    ];

    static getAttributeTypeMap() {
        return Report.attributeTypeMap;
    }

    public constructor() {
    }
}

