/**
 * A11ywatch Client
 * The web accessibility tool built for scale.
 *
 * OpenAPI spec version: 0.7.66
 * Contact: support@a11ywatch.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { Pages } from './Pages';
import { HttpFile } from '../http/http';

export class GetPages200Response {
    'data'?: Array<Pages>;

    static readonly discriminator: string | undefined = undefined;

    static readonly attributeTypeMap: Array<{name: string, baseName: string, type: string, format: string}> = [
        {
            "name": "data",
            "baseName": "data",
            "type": "Array<Pages>",
            "format": ""
        }    ];

    static getAttributeTypeMap() {
        return GetPages200Response.attributeTypeMap;
    }

    public constructor() {
    }
}

