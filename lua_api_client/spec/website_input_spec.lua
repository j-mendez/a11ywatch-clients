--[[
  A11ywatch Client

  The web accessibility tool built for scale.

  The version of the OpenAPI document: 0.7.66
  Contact: support@a11ywatch.com
  Generated by: https://openapi-generator.tech
]]

--[[
Unit tests for openapiclient.model.website_input
Automatically generated by openapi-generator (https://openapi-generator.tech)
Please update as you see appropriate
]]
describe("website_input", function()
  local openapiclient_website_input = require "openapiclient.model.website_input"

  -- unit tests for the property 'url'
  describe("property url test", function()
    it("should work", function()
      -- TODO assertion here: http://olivinelabs.com/busted/#asserts
    end)
  end)

  -- unit tests for the property 'mobile'
  describe("property mobile test", function()
    it("should work", function()
      -- TODO assertion here: http://olivinelabs.com/busted/#asserts
    end)
  end)

  -- unit tests for the property 'page_insights'
  describe("property page_insights test", function()
    it("should work", function()
      -- TODO assertion here: http://olivinelabs.com/busted/#asserts
    end)
  end)

  -- unit tests for the property 'ua'
  describe("property ua test", function()
    it("should work", function()
      -- TODO assertion here: http://olivinelabs.com/busted/#asserts
    end)
  end)

  -- unit tests for the property 'standard'
  describe("property standard test", function()
    it("should work", function()
      -- TODO assertion here: http://olivinelabs.com/busted/#asserts
    end)
  end)

  -- unit tests for the property 'robots'
  describe("property robots test", function()
    it("should work", function()
      -- TODO assertion here: http://olivinelabs.com/busted/#asserts
    end)
  end)

  -- unit tests for the property 'subdomains'
  describe("property subdomains test", function()
    it("should work", function()
      -- TODO assertion here: http://olivinelabs.com/busted/#asserts
    end)
  end)

  -- unit tests for the property 'tld'
  describe("property tld test", function()
    it("should work", function()
      -- TODO assertion here: http://olivinelabs.com/busted/#asserts
    end)
  end)

  -- unit tests for the property 'custom_headers'
  describe("property custom_headers test", function()
    it("should work", function()
      -- TODO assertion here: http://olivinelabs.com/busted/#asserts
    end)
  end)

end)
