--[[
  A11ywatch Client

  The web accessibility tool built for scale.

  The version of the OpenAPI document: 0.7.66
  Contact: support@a11ywatch.com
  Generated by: https://openapi-generator.tech
]]

--[[
Unit tests for openapiclient.api.reports_api
Automatically generated by openapi-generator (https://openapi-generator.tech)
Please update as you see appropriate
]]
describe("reports_api", function()
  local openapiclient_reports_api = require "openapiclient.api.reports_api"
  -- unit tests for crawl_website_stream
  describe("crawl_website_stream test", function()
    it("should work", function()
      -- TODO assertion here: http://olivinelabs.com/busted/#asserts
    end)
  end)

  -- unit tests for crawl_websites_sync
  describe("crawl_websites_sync test", function()
    it("should work", function()
      -- TODO assertion here: http://olivinelabs.com/busted/#asserts
    end)
  end)

  -- unit tests for get_report
  describe("get_report test", function()
    it("should work", function()
      -- TODO assertion here: http://olivinelabs.com/busted/#asserts
    end)
  end)

  -- unit tests for scan_website
  describe("scan_website test", function()
    it("should work", function()
      -- TODO assertion here: http://olivinelabs.com/busted/#asserts
    end)
  end)

  -- unit tests for scan_website_simple
  describe("scan_website_simple test", function()
    it("should work", function()
      -- TODO assertion here: http://olivinelabs.com/busted/#asserts
    end)
  end)

end)
