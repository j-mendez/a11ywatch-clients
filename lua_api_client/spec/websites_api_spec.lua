--[[
  A11ywatch Client

  The web accessibility tool built for scale.

  The version of the OpenAPI document: 0.7.66
  Contact: support@a11ywatch.com
  Generated by: https://openapi-generator.tech
]]

--[[
Unit tests for openapiclient.api.websites_api
Automatically generated by openapi-generator (https://openapi-generator.tech)
Please update as you see appropriate
]]
describe("websites_api", function()
  local openapiclient_websites_api = require "openapiclient.api.websites_api"
  -- unit tests for add_website
  describe("add_website test", function()
    it("should work", function()
      -- TODO assertion here: http://olivinelabs.com/busted/#asserts
    end)
  end)

  -- unit tests for delete_website
  describe("delete_website test", function()
    it("should work", function()
      -- TODO assertion here: http://olivinelabs.com/busted/#asserts
    end)
  end)

  -- unit tests for get_website_by_domain
  describe("get_website_by_domain test", function()
    it("should work", function()
      -- TODO assertion here: http://olivinelabs.com/busted/#asserts
    end)
  end)

end)
