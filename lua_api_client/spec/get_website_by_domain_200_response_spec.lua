--[[
  A11ywatch Client

  The web accessibility tool built for scale.

  The version of the OpenAPI document: 0.7.66
  Contact: support@a11ywatch.com
  Generated by: https://openapi-generator.tech
]]

--[[
Unit tests for openapiclient.model.get_website_by_domain_200_response
Automatically generated by openapi-generator (https://openapi-generator.tech)
Please update as you see appropriate
]]
describe("get_website_by_domain_200_response", function()
  local openapiclient_get_website_by_domain_200_response = require "openapiclient.model.get_website_by_domain_200_response"

  -- unit tests for the property 'data'
  describe("property data test", function()
    it("should work", function()
      -- TODO assertion here: http://olivinelabs.com/busted/#asserts
    end)
  end)

end)
