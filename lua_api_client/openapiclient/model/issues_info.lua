--[[
  A11ywatch Client

  The web accessibility tool built for scale.

  The version of the OpenAPI document: 0.7.66
  Contact: support@a11ywatch.com
  Generated by: https://openapi-generator.tech
]]

-- issues_info class
local issues_info = {}
local issues_info_mt = {
	__name = "issues_info";
	__index = issues_info;
}

local function cast_issues_info(t)
	return setmetatable(t, issues_info_mt)
end

local function new_issues_info(access_score_average, possible_issues_fixed_by_cdn, total_issues, issues_fixed_by_cdn, error_count, warning_count, notice_count, page_count)
	return cast_issues_info({
		["accessScoreAverage"] = access_score_average;
		["possibleIssuesFixedByCdn"] = possible_issues_fixed_by_cdn;
		["totalIssues"] = total_issues;
		["issuesFixedByCdn"] = issues_fixed_by_cdn;
		["errorCount"] = error_count;
		["warningCount"] = warning_count;
		["noticeCount"] = notice_count;
		["pageCount"] = page_count;
	})
end

return {
	cast = cast_issues_info;
	new = new_issues_info;
}
