--[[
  A11ywatch Client

  The web accessibility tool built for scale.

  The version of the OpenAPI document: 0.7.66
  Contact: support@a11ywatch.com
  Generated by: https://openapi-generator.tech
]]

-- page_load_time class
local page_load_time = {}
local page_load_time_mt = {
	__name = "page_load_time";
	__index = page_load_time;
}

local function cast_page_load_time(t)
	return setmetatable(t, page_load_time_mt)
end

local function new_page_load_time(duration, duration_formated, color)
	return cast_page_load_time({
		["duration"] = duration;
		["durationFormated"] = duration_formated;
		["color"] = color;
	})
end

return {
	cast = cast_page_load_time;
	new = new_page_load_time;
}
