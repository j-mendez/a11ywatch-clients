package = "openapiclient"
version = "1.0.0-1"
source = {
	url = "https://github.com/GIT_USER_ID/GIT_REPO_ID.git"
}

description = {
	summary = "API client generated by OpenAPI Generator",
	detailed = [[
The web accessibility tool built for scale.]],
	homepage = "https://openapi-generator.tech",
	license = "Unlicense",
	maintainer = "OpenAPI Generator contributors",
}

dependencies = {
	"lua >= 5.2",
	"http",
	"dkjson",
	"basexx"
}

build = {
	type = "builtin",
	modules = {
		["openapiclient.api.collection_api"] = "openapiclient/api/collection_api.lua";
		["openapiclient.api.reports_api"] = "openapiclient/api/reports_api.lua";
		["openapiclient.api.user_api"] = "openapiclient/api/user_api.lua";
		["openapiclient.api.websites_api"] = "openapiclient/api/websites_api.lua";
		["openapiclient.model.analytics"] = "openapiclient/model/analytics.lua";
		["openapiclient.model.auth_input"] = "openapiclient/model/auth_input.lua";
		["openapiclient.model.crawl_input"] = "openapiclient/model/crawl_input.lua";
		["openapiclient.model.generic_input"] = "openapiclient/model/generic_input.lua";
		["openapiclient.model.history"] = "openapiclient/model/history.lua";
		["openapiclient.model.issues"] = "openapiclient/model/issues.lua";
		["openapiclient.model.issues_info"] = "openapiclient/model/issues_info.lua";
		["openapiclient.model.page_issue"] = "openapiclient/model/page_issue.lua";
		["openapiclient.model.page_load_time"] = "openapiclient/model/page_load_time.lua";
		["openapiclient.model.pages"] = "openapiclient/model/pages.lua";
		["openapiclient.model.report"] = "openapiclient/model/report.lua";
		["openapiclient.model.users"] = "openapiclient/model/users.lua";
		["openapiclient.model.website_input"] = "openapiclient/model/website_input.lua";
		["openapiclient.model.websites"] = "openapiclient/model/websites.lua";
		["openapiclient.model.websites_insight"] = "openapiclient/model/websites_insight.lua";
	}
}
