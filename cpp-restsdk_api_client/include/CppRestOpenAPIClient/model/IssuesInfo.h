/**
 * A11ywatch Client
 * The web accessibility tool built for scale.
 *
 * The version of the OpenAPI document: 0.7.66
 * Contact: support@a11ywatch.com
 *
 * NOTE: This class is auto generated by OpenAPI-Generator 6.0.1.
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

/*
 * IssuesInfo.h
 *
 * 
 */

#ifndef ORG_OPENAPITOOLS_CLIENT_MODEL_IssuesInfo_H_
#define ORG_OPENAPITOOLS_CLIENT_MODEL_IssuesInfo_H_


#include "CppRestOpenAPIClient/ModelBase.h"


namespace org {
namespace openapitools {
namespace client {
namespace model {


/// <summary>
/// 
/// </summary>
class  IssuesInfo
    : public ModelBase
{
public:
    IssuesInfo();
    virtual ~IssuesInfo();

    /////////////////////////////////////////////
    /// ModelBase overrides

    void validate() override;

    web::json::value toJson() const override;
    bool fromJson(const web::json::value& json) override;

    void toMultipart(std::shared_ptr<MultipartFormData> multipart, const utility::string_t& namePrefix) const override;
    bool fromMultiPart(std::shared_ptr<MultipartFormData> multipart, const utility::string_t& namePrefix) override;

    /////////////////////////////////////////////
    /// IssuesInfo members

    /// <summary>
    /// 
    /// </summary>
    int64_t getAccessScoreAverage() const;
    bool accessScoreAverageIsSet() const;
    void unsetAccessScoreAverage();

    void setAccessScoreAverage(int64_t value);

    /// <summary>
    /// 
    /// </summary>
    int64_t getPossibleIssuesFixedByCdn() const;
    bool possibleIssuesFixedByCdnIsSet() const;
    void unsetPossibleIssuesFixedByCdn();

    void setPossibleIssuesFixedByCdn(int64_t value);

    /// <summary>
    /// 
    /// </summary>
    int64_t getTotalIssues() const;
    bool totalIssuesIsSet() const;
    void unsetTotalIssues();

    void setTotalIssues(int64_t value);

    /// <summary>
    /// 
    /// </summary>
    int64_t getIssuesFixedByCdn() const;
    bool issuesFixedByCdnIsSet() const;
    void unsetIssuesFixedByCdn();

    void setIssuesFixedByCdn(int64_t value);

    /// <summary>
    /// 
    /// </summary>
    int64_t getErrorCount() const;
    bool errorCountIsSet() const;
    void unsetErrorCount();

    void setErrorCount(int64_t value);

    /// <summary>
    /// 
    /// </summary>
    int64_t getWarningCount() const;
    bool warningCountIsSet() const;
    void unsetWarningCount();

    void setWarningCount(int64_t value);

    /// <summary>
    /// 
    /// </summary>
    int64_t getNoticeCount() const;
    bool noticeCountIsSet() const;
    void unsetNoticeCount();

    void setNoticeCount(int64_t value);

    /// <summary>
    /// 
    /// </summary>
    int64_t getPageCount() const;
    bool pageCountIsSet() const;
    void unsetPageCount();

    void setPageCount(int64_t value);


protected:
    int64_t m_AccessScoreAverage;
    bool m_AccessScoreAverageIsSet;
    int64_t m_PossibleIssuesFixedByCdn;
    bool m_PossibleIssuesFixedByCdnIsSet;
    int64_t m_TotalIssues;
    bool m_TotalIssuesIsSet;
    int64_t m_IssuesFixedByCdn;
    bool m_IssuesFixedByCdnIsSet;
    int64_t m_ErrorCount;
    bool m_ErrorCountIsSet;
    int64_t m_WarningCount;
    bool m_WarningCountIsSet;
    int64_t m_NoticeCount;
    bool m_NoticeCountIsSet;
    int64_t m_PageCount;
    bool m_PageCountIsSet;
};


}
}
}
}

#endif /* ORG_OPENAPITOOLS_CLIENT_MODEL_IssuesInfo_H_ */
