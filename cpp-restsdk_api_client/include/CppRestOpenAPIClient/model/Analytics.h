/**
 * A11ywatch Client
 * The web accessibility tool built for scale.
 *
 * The version of the OpenAPI document: 0.7.66
 * Contact: support@a11ywatch.com
 *
 * NOTE: This class is auto generated by OpenAPI-Generator 6.0.1.
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

/*
 * Analytics.h
 *
 * 
 */

#ifndef ORG_OPENAPITOOLS_CLIENT_MODEL_Analytics_H_
#define ORG_OPENAPITOOLS_CLIENT_MODEL_Analytics_H_


#include "CppRestOpenAPIClient/ModelBase.h"

#include <cpprest/details/basic_types.h>

namespace org {
namespace openapitools {
namespace client {
namespace model {


/// <summary>
/// 
/// </summary>
class  Analytics
    : public ModelBase
{
public:
    Analytics();
    virtual ~Analytics();

    /////////////////////////////////////////////
    /// ModelBase overrides

    void validate() override;

    web::json::value toJson() const override;
    bool fromJson(const web::json::value& json) override;

    void toMultipart(std::shared_ptr<MultipartFormData> multipart, const utility::string_t& namePrefix) const override;
    bool fromMultiPart(std::shared_ptr<MultipartFormData> multipart, const utility::string_t& namePrefix) override;

    /////////////////////////////////////////////
    /// Analytics members

    /// <summary>
    /// 
    /// </summary>
    int64_t getId() const;
    bool idIsSet() const;
    void unset_id();

    void setId(int64_t value);

    /// <summary>
    /// 
    /// </summary>
    utility::string_t getDomain() const;
    bool domainIsSet() const;
    void unsetDomain();

    void setDomain(const utility::string_t& value);

    /// <summary>
    /// 
    /// </summary>
    utility::string_t getPageUrl() const;
    bool pageUrlIsSet() const;
    void unsetPageUrl();

    void setPageUrl(const utility::string_t& value);

    /// <summary>
    /// 
    /// </summary>
    int64_t getUserId() const;
    bool userIdIsSet() const;
    void unsetUserId();

    void setUserId(int64_t value);

    /// <summary>
    /// 
    /// </summary>
    int64_t getAccessScore() const;
    bool accessScoreIsSet() const;
    void unsetAccessScore();

    void setAccessScore(int64_t value);

    /// <summary>
    /// 
    /// </summary>
    int64_t getPossibleIssuesFixedByCdn() const;
    bool possibleIssuesFixedByCdnIsSet() const;
    void unsetPossibleIssuesFixedByCdn();

    void setPossibleIssuesFixedByCdn(int64_t value);

    /// <summary>
    /// 
    /// </summary>
    int64_t getTotalIssues() const;
    bool totalIssuesIsSet() const;
    void unsetTotalIssues();

    void setTotalIssues(int64_t value);

    /// <summary>
    /// 
    /// </summary>
    int64_t getIssuesFixedByCdn() const;
    bool issuesFixedByCdnIsSet() const;
    void unsetIssuesFixedByCdn();

    void setIssuesFixedByCdn(int64_t value);

    /// <summary>
    /// 
    /// </summary>
    int64_t getErrorCount() const;
    bool errorCountIsSet() const;
    void unsetErrorCount();

    void setErrorCount(int64_t value);

    /// <summary>
    /// 
    /// </summary>
    int64_t getWarningCount() const;
    bool warningCountIsSet() const;
    void unsetWarningCount();

    void setWarningCount(int64_t value);

    /// <summary>
    /// 
    /// </summary>
    int64_t getNoticeCount() const;
    bool noticeCountIsSet() const;
    void unsetNoticeCount();

    void setNoticeCount(int64_t value);


protected:
    int64_t m__id;
    bool m__idIsSet;
    utility::string_t m_Domain;
    bool m_DomainIsSet;
    utility::string_t m_PageUrl;
    bool m_PageUrlIsSet;
    int64_t m_UserId;
    bool m_UserIdIsSet;
    int64_t m_AccessScore;
    bool m_AccessScoreIsSet;
    int64_t m_PossibleIssuesFixedByCdn;
    bool m_PossibleIssuesFixedByCdnIsSet;
    int64_t m_TotalIssues;
    bool m_TotalIssuesIsSet;
    int64_t m_IssuesFixedByCdn;
    bool m_IssuesFixedByCdnIsSet;
    int64_t m_ErrorCount;
    bool m_ErrorCountIsSet;
    int64_t m_WarningCount;
    bool m_WarningCountIsSet;
    int64_t m_NoticeCount;
    bool m_NoticeCountIsSet;
};


}
}
}
}

#endif /* ORG_OPENAPITOOLS_CLIENT_MODEL_Analytics_H_ */
