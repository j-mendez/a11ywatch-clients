/**
 * A11ywatch Client
 * The web accessibility tool built for scale.
 *
 * The version of the OpenAPI document: 0.7.66
 * Contact: support@a11ywatch.com
 *
 * NOTE: This class is auto generated by OpenAPI-Generator 6.0.1.
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

/*
 * Pages.h
 *
 * 
 */

#ifndef ORG_OPENAPITOOLS_CLIENT_MODEL_Pages_H_
#define ORG_OPENAPITOOLS_CLIENT_MODEL_Pages_H_


#include "CppRestOpenAPIClient/ModelBase.h"

#include "CppRestOpenAPIClient/model/IssuesInfo.h"
#include "CppRestOpenAPIClient/model/PageLoadTime.h"
#include "CppRestOpenAPIClient/model/Websites_insight.h"
#include <cpprest/details/basic_types.h>

namespace org {
namespace openapitools {
namespace client {
namespace model {

class PageLoadTime;
class Websites_insight;
class IssuesInfo;

/// <summary>
/// 
/// </summary>
class  Pages
    : public ModelBase
{
public:
    Pages();
    virtual ~Pages();

    /////////////////////////////////////////////
    /// ModelBase overrides

    void validate() override;

    web::json::value toJson() const override;
    bool fromJson(const web::json::value& json) override;

    void toMultipart(std::shared_ptr<MultipartFormData> multipart, const utility::string_t& namePrefix) const override;
    bool fromMultiPart(std::shared_ptr<MultipartFormData> multipart, const utility::string_t& namePrefix) override;

    /////////////////////////////////////////////
    /// Pages members

    /// <summary>
    /// 
    /// </summary>
    int64_t getId() const;
    bool idIsSet() const;
    void unset_id();

    void setId(int64_t value);

    /// <summary>
    /// 
    /// </summary>
    int64_t getUserId() const;
    bool userIdIsSet() const;
    void unsetUserId();

    void setUserId(int64_t value);

    /// <summary>
    /// 
    /// </summary>
    utility::string_t getDomain() const;
    bool domainIsSet() const;
    void unsetDomain();

    void setDomain(const utility::string_t& value);

    /// <summary>
    /// 
    /// </summary>
    utility::string_t getUrl() const;
    bool urlIsSet() const;
    void unsetUrl();

    void setUrl(const utility::string_t& value);

    /// <summary>
    /// 
    /// </summary>
    bool isCdnConnected() const;
    bool cdnConnectedIsSet() const;
    void unsetCdnConnected();

    void setCdnConnected(bool value);

    /// <summary>
    /// 
    /// </summary>
    bool isOnline() const;
    bool onlineIsSet() const;
    void unsetOnline();

    void setOnline(bool value);

    /// <summary>
    /// 
    /// </summary>
    std::shared_ptr<PageLoadTime> getPageLoadTime() const;
    bool pageLoadTimeIsSet() const;
    void unsetPageLoadTime();

    void setPageLoadTime(const std::shared_ptr<PageLoadTime>& value);

    /// <summary>
    /// 
    /// </summary>
    std::shared_ptr<Websites_insight> getInsight() const;
    bool insightIsSet() const;
    void unsetInsight();

    void setInsight(const std::shared_ptr<Websites_insight>& value);

    /// <summary>
    /// 
    /// </summary>
    std::shared_ptr<IssuesInfo> getIssuesInfo() const;
    bool issuesInfoIsSet() const;
    void unsetIssuesInfo();

    void setIssuesInfo(const std::shared_ptr<IssuesInfo>& value);

    /// <summary>
    /// 
    /// </summary>
    utility::string_t getLastScanDate() const;
    bool lastScanDateIsSet() const;
    void unsetLastScanDate();

    void setLastScanDate(const utility::string_t& value);


protected:
    int64_t m__id;
    bool m__idIsSet;
    int64_t m_UserId;
    bool m_UserIdIsSet;
    utility::string_t m_Domain;
    bool m_DomainIsSet;
    utility::string_t m_Url;
    bool m_UrlIsSet;
    bool m_CdnConnected;
    bool m_CdnConnectedIsSet;
    bool m_Online;
    bool m_OnlineIsSet;
    std::shared_ptr<PageLoadTime> m_PageLoadTime;
    bool m_PageLoadTimeIsSet;
    std::shared_ptr<Websites_insight> m_Insight;
    bool m_InsightIsSet;
    std::shared_ptr<IssuesInfo> m_IssuesInfo;
    bool m_IssuesInfoIsSet;
    utility::string_t m_LastScanDate;
    bool m_LastScanDateIsSet;
};


}
}
}
}

#endif /* ORG_OPENAPITOOLS_CLIENT_MODEL_Pages_H_ */
