/**
 * A11ywatch Client
 * The web accessibility tool built for scale.
 *
 * The version of the OpenAPI document: 0.7.66
 * Contact: support@a11ywatch.com
 *
 * NOTE: This class is auto generated by OpenAPI-Generator 5.3.0.
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */



#include "Analytics.h"

namespace org {
namespace openapitools {
namespace client {
namespace model {




Analytics::Analytics()
{
    m__id = 0L;
    m__idIsSet = false;
    m_Domain = utility::conversions::to_string_t("");
    m_DomainIsSet = false;
    m_PageUrl = utility::conversions::to_string_t("");
    m_PageUrlIsSet = false;
    m_UserId = 0L;
    m_UserIdIsSet = false;
    m_AccessScore = 0L;
    m_AccessScoreIsSet = false;
    m_PossibleIssuesFixedByCdn = 0L;
    m_PossibleIssuesFixedByCdnIsSet = false;
    m_TotalIssues = 0L;
    m_TotalIssuesIsSet = false;
    m_IssuesFixedByCdn = 0L;
    m_IssuesFixedByCdnIsSet = false;
    m_ErrorCount = 0L;
    m_ErrorCountIsSet = false;
    m_WarningCount = 0L;
    m_WarningCountIsSet = false;
    m_NoticeCount = 0L;
    m_NoticeCountIsSet = false;
}

Analytics::~Analytics()
{
}

void Analytics::validate()
{
    // TODO: implement validation
}

web::json::value Analytics::toJson() const
{

    web::json::value val = web::json::value::object();
    
    if(m__idIsSet)
    {
        val[utility::conversions::to_string_t(U("_id"))] = ModelBase::toJson(m__id);
    }
    if(m_DomainIsSet)
    {
        val[utility::conversions::to_string_t(U("domain"))] = ModelBase::toJson(m_Domain);
    }
    if(m_PageUrlIsSet)
    {
        val[utility::conversions::to_string_t(U("pageUrl"))] = ModelBase::toJson(m_PageUrl);
    }
    if(m_UserIdIsSet)
    {
        val[utility::conversions::to_string_t(U("userId"))] = ModelBase::toJson(m_UserId);
    }
    if(m_AccessScoreIsSet)
    {
        val[utility::conversions::to_string_t(U("accessScore"))] = ModelBase::toJson(m_AccessScore);
    }
    if(m_PossibleIssuesFixedByCdnIsSet)
    {
        val[utility::conversions::to_string_t(U("possibleIssuesFixedByCdn"))] = ModelBase::toJson(m_PossibleIssuesFixedByCdn);
    }
    if(m_TotalIssuesIsSet)
    {
        val[utility::conversions::to_string_t(U("totalIssues"))] = ModelBase::toJson(m_TotalIssues);
    }
    if(m_IssuesFixedByCdnIsSet)
    {
        val[utility::conversions::to_string_t(U("issuesFixedByCdn"))] = ModelBase::toJson(m_IssuesFixedByCdn);
    }
    if(m_ErrorCountIsSet)
    {
        val[utility::conversions::to_string_t(U("errorCount"))] = ModelBase::toJson(m_ErrorCount);
    }
    if(m_WarningCountIsSet)
    {
        val[utility::conversions::to_string_t(U("warningCount"))] = ModelBase::toJson(m_WarningCount);
    }
    if(m_NoticeCountIsSet)
    {
        val[utility::conversions::to_string_t(U("noticeCount"))] = ModelBase::toJson(m_NoticeCount);
    }

    return val;
}

bool Analytics::fromJson(const web::json::value& val)
{
    bool ok = true;
    
    if(val.has_field(utility::conversions::to_string_t(U("_id"))))
    {
        const web::json::value& fieldValue = val.at(utility::conversions::to_string_t(U("_id")));
        if(!fieldValue.is_null())
        {
            int64_t refVal__id;
            ok &= ModelBase::fromJson(fieldValue, refVal__id);
            setId(refVal__id);
        }
    }
    if(val.has_field(utility::conversions::to_string_t(U("domain"))))
    {
        const web::json::value& fieldValue = val.at(utility::conversions::to_string_t(U("domain")));
        if(!fieldValue.is_null())
        {
            utility::string_t refVal_domain;
            ok &= ModelBase::fromJson(fieldValue, refVal_domain);
            setDomain(refVal_domain);
        }
    }
    if(val.has_field(utility::conversions::to_string_t(U("pageUrl"))))
    {
        const web::json::value& fieldValue = val.at(utility::conversions::to_string_t(U("pageUrl")));
        if(!fieldValue.is_null())
        {
            utility::string_t refVal_pageUrl;
            ok &= ModelBase::fromJson(fieldValue, refVal_pageUrl);
            setPageUrl(refVal_pageUrl);
        }
    }
    if(val.has_field(utility::conversions::to_string_t(U("userId"))))
    {
        const web::json::value& fieldValue = val.at(utility::conversions::to_string_t(U("userId")));
        if(!fieldValue.is_null())
        {
            int64_t refVal_userId;
            ok &= ModelBase::fromJson(fieldValue, refVal_userId);
            setUserId(refVal_userId);
        }
    }
    if(val.has_field(utility::conversions::to_string_t(U("accessScore"))))
    {
        const web::json::value& fieldValue = val.at(utility::conversions::to_string_t(U("accessScore")));
        if(!fieldValue.is_null())
        {
            int64_t refVal_accessScore;
            ok &= ModelBase::fromJson(fieldValue, refVal_accessScore);
            setAccessScore(refVal_accessScore);
        }
    }
    if(val.has_field(utility::conversions::to_string_t(U("possibleIssuesFixedByCdn"))))
    {
        const web::json::value& fieldValue = val.at(utility::conversions::to_string_t(U("possibleIssuesFixedByCdn")));
        if(!fieldValue.is_null())
        {
            int64_t refVal_possibleIssuesFixedByCdn;
            ok &= ModelBase::fromJson(fieldValue, refVal_possibleIssuesFixedByCdn);
            setPossibleIssuesFixedByCdn(refVal_possibleIssuesFixedByCdn);
        }
    }
    if(val.has_field(utility::conversions::to_string_t(U("totalIssues"))))
    {
        const web::json::value& fieldValue = val.at(utility::conversions::to_string_t(U("totalIssues")));
        if(!fieldValue.is_null())
        {
            int64_t refVal_totalIssues;
            ok &= ModelBase::fromJson(fieldValue, refVal_totalIssues);
            setTotalIssues(refVal_totalIssues);
        }
    }
    if(val.has_field(utility::conversions::to_string_t(U("issuesFixedByCdn"))))
    {
        const web::json::value& fieldValue = val.at(utility::conversions::to_string_t(U("issuesFixedByCdn")));
        if(!fieldValue.is_null())
        {
            int64_t refVal_issuesFixedByCdn;
            ok &= ModelBase::fromJson(fieldValue, refVal_issuesFixedByCdn);
            setIssuesFixedByCdn(refVal_issuesFixedByCdn);
        }
    }
    if(val.has_field(utility::conversions::to_string_t(U("errorCount"))))
    {
        const web::json::value& fieldValue = val.at(utility::conversions::to_string_t(U("errorCount")));
        if(!fieldValue.is_null())
        {
            int64_t refVal_errorCount;
            ok &= ModelBase::fromJson(fieldValue, refVal_errorCount);
            setErrorCount(refVal_errorCount);
        }
    }
    if(val.has_field(utility::conversions::to_string_t(U("warningCount"))))
    {
        const web::json::value& fieldValue = val.at(utility::conversions::to_string_t(U("warningCount")));
        if(!fieldValue.is_null())
        {
            int64_t refVal_warningCount;
            ok &= ModelBase::fromJson(fieldValue, refVal_warningCount);
            setWarningCount(refVal_warningCount);
        }
    }
    if(val.has_field(utility::conversions::to_string_t(U("noticeCount"))))
    {
        const web::json::value& fieldValue = val.at(utility::conversions::to_string_t(U("noticeCount")));
        if(!fieldValue.is_null())
        {
            int64_t refVal_noticeCount;
            ok &= ModelBase::fromJson(fieldValue, refVal_noticeCount);
            setNoticeCount(refVal_noticeCount);
        }
    }
    return ok;
}

void Analytics::toMultipart(std::shared_ptr<MultipartFormData> multipart, const utility::string_t& prefix) const
{
    utility::string_t namePrefix = prefix;
    if(namePrefix.size() > 0 && namePrefix.substr(namePrefix.size() - 1) != utility::conversions::to_string_t(U(".")))
    {
        namePrefix += utility::conversions::to_string_t(U("."));
    }
    if(m__idIsSet)
    {
        multipart->add(ModelBase::toHttpContent(namePrefix + utility::conversions::to_string_t(U("_id")), m__id));
    }
    if(m_DomainIsSet)
    {
        multipart->add(ModelBase::toHttpContent(namePrefix + utility::conversions::to_string_t(U("domain")), m_Domain));
    }
    if(m_PageUrlIsSet)
    {
        multipart->add(ModelBase::toHttpContent(namePrefix + utility::conversions::to_string_t(U("pageUrl")), m_PageUrl));
    }
    if(m_UserIdIsSet)
    {
        multipart->add(ModelBase::toHttpContent(namePrefix + utility::conversions::to_string_t(U("userId")), m_UserId));
    }
    if(m_AccessScoreIsSet)
    {
        multipart->add(ModelBase::toHttpContent(namePrefix + utility::conversions::to_string_t(U("accessScore")), m_AccessScore));
    }
    if(m_PossibleIssuesFixedByCdnIsSet)
    {
        multipart->add(ModelBase::toHttpContent(namePrefix + utility::conversions::to_string_t(U("possibleIssuesFixedByCdn")), m_PossibleIssuesFixedByCdn));
    }
    if(m_TotalIssuesIsSet)
    {
        multipart->add(ModelBase::toHttpContent(namePrefix + utility::conversions::to_string_t(U("totalIssues")), m_TotalIssues));
    }
    if(m_IssuesFixedByCdnIsSet)
    {
        multipart->add(ModelBase::toHttpContent(namePrefix + utility::conversions::to_string_t(U("issuesFixedByCdn")), m_IssuesFixedByCdn));
    }
    if(m_ErrorCountIsSet)
    {
        multipart->add(ModelBase::toHttpContent(namePrefix + utility::conversions::to_string_t(U("errorCount")), m_ErrorCount));
    }
    if(m_WarningCountIsSet)
    {
        multipart->add(ModelBase::toHttpContent(namePrefix + utility::conversions::to_string_t(U("warningCount")), m_WarningCount));
    }
    if(m_NoticeCountIsSet)
    {
        multipart->add(ModelBase::toHttpContent(namePrefix + utility::conversions::to_string_t(U("noticeCount")), m_NoticeCount));
    }
}

bool Analytics::fromMultiPart(std::shared_ptr<MultipartFormData> multipart, const utility::string_t& prefix)
{
    bool ok = true;
    utility::string_t namePrefix = prefix;
    if(namePrefix.size() > 0 && namePrefix.substr(namePrefix.size() - 1) != utility::conversions::to_string_t(U(".")))
    {
        namePrefix += utility::conversions::to_string_t(U("."));
    }

    if(multipart->hasContent(utility::conversions::to_string_t(U("_id"))))
    {
        int64_t refVal__id;
        ok &= ModelBase::fromHttpContent(multipart->getContent(utility::conversions::to_string_t(U("_id"))), refVal__id );
        setId(refVal__id);
    }
    if(multipart->hasContent(utility::conversions::to_string_t(U("domain"))))
    {
        utility::string_t refVal_domain;
        ok &= ModelBase::fromHttpContent(multipart->getContent(utility::conversions::to_string_t(U("domain"))), refVal_domain );
        setDomain(refVal_domain);
    }
    if(multipart->hasContent(utility::conversions::to_string_t(U("pageUrl"))))
    {
        utility::string_t refVal_pageUrl;
        ok &= ModelBase::fromHttpContent(multipart->getContent(utility::conversions::to_string_t(U("pageUrl"))), refVal_pageUrl );
        setPageUrl(refVal_pageUrl);
    }
    if(multipart->hasContent(utility::conversions::to_string_t(U("userId"))))
    {
        int64_t refVal_userId;
        ok &= ModelBase::fromHttpContent(multipart->getContent(utility::conversions::to_string_t(U("userId"))), refVal_userId );
        setUserId(refVal_userId);
    }
    if(multipart->hasContent(utility::conversions::to_string_t(U("accessScore"))))
    {
        int64_t refVal_accessScore;
        ok &= ModelBase::fromHttpContent(multipart->getContent(utility::conversions::to_string_t(U("accessScore"))), refVal_accessScore );
        setAccessScore(refVal_accessScore);
    }
    if(multipart->hasContent(utility::conversions::to_string_t(U("possibleIssuesFixedByCdn"))))
    {
        int64_t refVal_possibleIssuesFixedByCdn;
        ok &= ModelBase::fromHttpContent(multipart->getContent(utility::conversions::to_string_t(U("possibleIssuesFixedByCdn"))), refVal_possibleIssuesFixedByCdn );
        setPossibleIssuesFixedByCdn(refVal_possibleIssuesFixedByCdn);
    }
    if(multipart->hasContent(utility::conversions::to_string_t(U("totalIssues"))))
    {
        int64_t refVal_totalIssues;
        ok &= ModelBase::fromHttpContent(multipart->getContent(utility::conversions::to_string_t(U("totalIssues"))), refVal_totalIssues );
        setTotalIssues(refVal_totalIssues);
    }
    if(multipart->hasContent(utility::conversions::to_string_t(U("issuesFixedByCdn"))))
    {
        int64_t refVal_issuesFixedByCdn;
        ok &= ModelBase::fromHttpContent(multipart->getContent(utility::conversions::to_string_t(U("issuesFixedByCdn"))), refVal_issuesFixedByCdn );
        setIssuesFixedByCdn(refVal_issuesFixedByCdn);
    }
    if(multipart->hasContent(utility::conversions::to_string_t(U("errorCount"))))
    {
        int64_t refVal_errorCount;
        ok &= ModelBase::fromHttpContent(multipart->getContent(utility::conversions::to_string_t(U("errorCount"))), refVal_errorCount );
        setErrorCount(refVal_errorCount);
    }
    if(multipart->hasContent(utility::conversions::to_string_t(U("warningCount"))))
    {
        int64_t refVal_warningCount;
        ok &= ModelBase::fromHttpContent(multipart->getContent(utility::conversions::to_string_t(U("warningCount"))), refVal_warningCount );
        setWarningCount(refVal_warningCount);
    }
    if(multipart->hasContent(utility::conversions::to_string_t(U("noticeCount"))))
    {
        int64_t refVal_noticeCount;
        ok &= ModelBase::fromHttpContent(multipart->getContent(utility::conversions::to_string_t(U("noticeCount"))), refVal_noticeCount );
        setNoticeCount(refVal_noticeCount);
    }
    return ok;
}

int64_t Analytics::getId() const
{
    return m__id;
}

void Analytics::setId(int64_t value)
{
    m__id = value;
    m__idIsSet = true;
}

bool Analytics::idIsSet() const
{
    return m__idIsSet;
}

void Analytics::unset_id()
{
    m__idIsSet = false;
}
utility::string_t Analytics::getDomain() const
{
    return m_Domain;
}

void Analytics::setDomain(const utility::string_t& value)
{
    m_Domain = value;
    m_DomainIsSet = true;
}

bool Analytics::domainIsSet() const
{
    return m_DomainIsSet;
}

void Analytics::unsetDomain()
{
    m_DomainIsSet = false;
}
utility::string_t Analytics::getPageUrl() const
{
    return m_PageUrl;
}

void Analytics::setPageUrl(const utility::string_t& value)
{
    m_PageUrl = value;
    m_PageUrlIsSet = true;
}

bool Analytics::pageUrlIsSet() const
{
    return m_PageUrlIsSet;
}

void Analytics::unsetPageUrl()
{
    m_PageUrlIsSet = false;
}
int64_t Analytics::getUserId() const
{
    return m_UserId;
}

void Analytics::setUserId(int64_t value)
{
    m_UserId = value;
    m_UserIdIsSet = true;
}

bool Analytics::userIdIsSet() const
{
    return m_UserIdIsSet;
}

void Analytics::unsetUserId()
{
    m_UserIdIsSet = false;
}
int64_t Analytics::getAccessScore() const
{
    return m_AccessScore;
}

void Analytics::setAccessScore(int64_t value)
{
    m_AccessScore = value;
    m_AccessScoreIsSet = true;
}

bool Analytics::accessScoreIsSet() const
{
    return m_AccessScoreIsSet;
}

void Analytics::unsetAccessScore()
{
    m_AccessScoreIsSet = false;
}
int64_t Analytics::getPossibleIssuesFixedByCdn() const
{
    return m_PossibleIssuesFixedByCdn;
}

void Analytics::setPossibleIssuesFixedByCdn(int64_t value)
{
    m_PossibleIssuesFixedByCdn = value;
    m_PossibleIssuesFixedByCdnIsSet = true;
}

bool Analytics::possibleIssuesFixedByCdnIsSet() const
{
    return m_PossibleIssuesFixedByCdnIsSet;
}

void Analytics::unsetPossibleIssuesFixedByCdn()
{
    m_PossibleIssuesFixedByCdnIsSet = false;
}
int64_t Analytics::getTotalIssues() const
{
    return m_TotalIssues;
}

void Analytics::setTotalIssues(int64_t value)
{
    m_TotalIssues = value;
    m_TotalIssuesIsSet = true;
}

bool Analytics::totalIssuesIsSet() const
{
    return m_TotalIssuesIsSet;
}

void Analytics::unsetTotalIssues()
{
    m_TotalIssuesIsSet = false;
}
int64_t Analytics::getIssuesFixedByCdn() const
{
    return m_IssuesFixedByCdn;
}

void Analytics::setIssuesFixedByCdn(int64_t value)
{
    m_IssuesFixedByCdn = value;
    m_IssuesFixedByCdnIsSet = true;
}

bool Analytics::issuesFixedByCdnIsSet() const
{
    return m_IssuesFixedByCdnIsSet;
}

void Analytics::unsetIssuesFixedByCdn()
{
    m_IssuesFixedByCdnIsSet = false;
}
int64_t Analytics::getErrorCount() const
{
    return m_ErrorCount;
}

void Analytics::setErrorCount(int64_t value)
{
    m_ErrorCount = value;
    m_ErrorCountIsSet = true;
}

bool Analytics::errorCountIsSet() const
{
    return m_ErrorCountIsSet;
}

void Analytics::unsetErrorCount()
{
    m_ErrorCountIsSet = false;
}
int64_t Analytics::getWarningCount() const
{
    return m_WarningCount;
}

void Analytics::setWarningCount(int64_t value)
{
    m_WarningCount = value;
    m_WarningCountIsSet = true;
}

bool Analytics::warningCountIsSet() const
{
    return m_WarningCountIsSet;
}

void Analytics::unsetWarningCount()
{
    m_WarningCountIsSet = false;
}
int64_t Analytics::getNoticeCount() const
{
    return m_NoticeCount;
}

void Analytics::setNoticeCount(int64_t value)
{
    m_NoticeCount = value;
    m_NoticeCountIsSet = true;
}

bool Analytics::noticeCountIsSet() const
{
    return m_NoticeCountIsSet;
}

void Analytics::unsetNoticeCount()
{
    m_NoticeCountIsSet = false;
}
}
}
}
}


