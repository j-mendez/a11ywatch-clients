/**
 * A11ywatch Client
 * The web accessibility tool built for scale.
 *
 * The version of the OpenAPI document: 0.7.66
 * Contact: support@a11ywatch.com
 *
 * NOTE: This class is auto generated by OpenAPI-Generator 6.0.1.
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */



#include "CppRestOpenAPIClient/model/Issues.h"

namespace org {
namespace openapitools {
namespace client {
namespace model {



Issues::Issues()
{
    m__id = 0L;
    m__idIsSet = false;
    m_UserId = 0L;
    m_UserIdIsSet = false;
    m_Domain = utility::conversions::to_string_t("");
    m_DomainIsSet = false;
    m_PageUrl = utility::conversions::to_string_t("");
    m_PageUrlIsSet = false;
    m_IssuesIsSet = false;
}

Issues::~Issues()
{
}

void Issues::validate()
{
    // TODO: implement validation
}

web::json::value Issues::toJson() const
{

    web::json::value val = web::json::value::object();
    
    if(m__idIsSet)
    {
        val[utility::conversions::to_string_t(U("_id"))] = ModelBase::toJson(m__id);
    }
    if(m_UserIdIsSet)
    {
        val[utility::conversions::to_string_t(U("userId"))] = ModelBase::toJson(m_UserId);
    }
    if(m_DomainIsSet)
    {
        val[utility::conversions::to_string_t(U("domain"))] = ModelBase::toJson(m_Domain);
    }
    if(m_PageUrlIsSet)
    {
        val[utility::conversions::to_string_t(U("pageUrl"))] = ModelBase::toJson(m_PageUrl);
    }
    if(m_IssuesIsSet)
    {
        val[utility::conversions::to_string_t(U("issues"))] = ModelBase::toJson(m_Issues);
    }

    return val;
}

bool Issues::fromJson(const web::json::value& val)
{
    bool ok = true;
    
    if(val.has_field(utility::conversions::to_string_t(U("_id"))))
    {
        const web::json::value& fieldValue = val.at(utility::conversions::to_string_t(U("_id")));
        if(!fieldValue.is_null())
        {
            int64_t refVal_setId;
            ok &= ModelBase::fromJson(fieldValue, refVal_setId);
            setId(refVal_setId);
        }
    }
    if(val.has_field(utility::conversions::to_string_t(U("userId"))))
    {
        const web::json::value& fieldValue = val.at(utility::conversions::to_string_t(U("userId")));
        if(!fieldValue.is_null())
        {
            int64_t refVal_setUserId;
            ok &= ModelBase::fromJson(fieldValue, refVal_setUserId);
            setUserId(refVal_setUserId);
        }
    }
    if(val.has_field(utility::conversions::to_string_t(U("domain"))))
    {
        const web::json::value& fieldValue = val.at(utility::conversions::to_string_t(U("domain")));
        if(!fieldValue.is_null())
        {
            utility::string_t refVal_setDomain;
            ok &= ModelBase::fromJson(fieldValue, refVal_setDomain);
            setDomain(refVal_setDomain);
        }
    }
    if(val.has_field(utility::conversions::to_string_t(U("pageUrl"))))
    {
        const web::json::value& fieldValue = val.at(utility::conversions::to_string_t(U("pageUrl")));
        if(!fieldValue.is_null())
        {
            utility::string_t refVal_setPageUrl;
            ok &= ModelBase::fromJson(fieldValue, refVal_setPageUrl);
            setPageUrl(refVal_setPageUrl);
        }
    }
    if(val.has_field(utility::conversions::to_string_t(U("issues"))))
    {
        const web::json::value& fieldValue = val.at(utility::conversions::to_string_t(U("issues")));
        if(!fieldValue.is_null())
        {
            std::shared_ptr<PageIssue> refVal_setIssues;
            ok &= ModelBase::fromJson(fieldValue, refVal_setIssues);
            setIssues(refVal_setIssues);
        }
    }
    return ok;
}

void Issues::toMultipart(std::shared_ptr<MultipartFormData> multipart, const utility::string_t& prefix) const
{
    utility::string_t namePrefix = prefix;
    if(namePrefix.size() > 0 && namePrefix.substr(namePrefix.size() - 1) != utility::conversions::to_string_t(U(".")))
    {
        namePrefix += utility::conversions::to_string_t(U("."));
    }
    if(m__idIsSet)
    {
        multipart->add(ModelBase::toHttpContent(namePrefix + utility::conversions::to_string_t(U("_id")), m__id));
    }
    if(m_UserIdIsSet)
    {
        multipart->add(ModelBase::toHttpContent(namePrefix + utility::conversions::to_string_t(U("userId")), m_UserId));
    }
    if(m_DomainIsSet)
    {
        multipart->add(ModelBase::toHttpContent(namePrefix + utility::conversions::to_string_t(U("domain")), m_Domain));
    }
    if(m_PageUrlIsSet)
    {
        multipart->add(ModelBase::toHttpContent(namePrefix + utility::conversions::to_string_t(U("pageUrl")), m_PageUrl));
    }
    if(m_IssuesIsSet)
    {
        multipart->add(ModelBase::toHttpContent(namePrefix + utility::conversions::to_string_t(U("issues")), m_Issues));
    }
}

bool Issues::fromMultiPart(std::shared_ptr<MultipartFormData> multipart, const utility::string_t& prefix)
{
    bool ok = true;
    utility::string_t namePrefix = prefix;
    if(namePrefix.size() > 0 && namePrefix.substr(namePrefix.size() - 1) != utility::conversions::to_string_t(U(".")))
    {
        namePrefix += utility::conversions::to_string_t(U("."));
    }

    if(multipart->hasContent(utility::conversions::to_string_t(U("_id"))))
    {
        int64_t refVal_setId;
        ok &= ModelBase::fromHttpContent(multipart->getContent(utility::conversions::to_string_t(U("_id"))), refVal_setId );
        setId(refVal_setId);
    }
    if(multipart->hasContent(utility::conversions::to_string_t(U("userId"))))
    {
        int64_t refVal_setUserId;
        ok &= ModelBase::fromHttpContent(multipart->getContent(utility::conversions::to_string_t(U("userId"))), refVal_setUserId );
        setUserId(refVal_setUserId);
    }
    if(multipart->hasContent(utility::conversions::to_string_t(U("domain"))))
    {
        utility::string_t refVal_setDomain;
        ok &= ModelBase::fromHttpContent(multipart->getContent(utility::conversions::to_string_t(U("domain"))), refVal_setDomain );
        setDomain(refVal_setDomain);
    }
    if(multipart->hasContent(utility::conversions::to_string_t(U("pageUrl"))))
    {
        utility::string_t refVal_setPageUrl;
        ok &= ModelBase::fromHttpContent(multipart->getContent(utility::conversions::to_string_t(U("pageUrl"))), refVal_setPageUrl );
        setPageUrl(refVal_setPageUrl);
    }
    if(multipart->hasContent(utility::conversions::to_string_t(U("issues"))))
    {
        std::shared_ptr<PageIssue> refVal_setIssues;
        ok &= ModelBase::fromHttpContent(multipart->getContent(utility::conversions::to_string_t(U("issues"))), refVal_setIssues );
        setIssues(refVal_setIssues);
    }
    return ok;
}

int64_t Issues::getId() const
{
    return m__id;
}

void Issues::setId(int64_t value)
{
    m__id = value;
    m__idIsSet = true;
}

bool Issues::idIsSet() const
{
    return m__idIsSet;
}

void Issues::unset_id()
{
    m__idIsSet = false;
}
int64_t Issues::getUserId() const
{
    return m_UserId;
}

void Issues::setUserId(int64_t value)
{
    m_UserId = value;
    m_UserIdIsSet = true;
}

bool Issues::userIdIsSet() const
{
    return m_UserIdIsSet;
}

void Issues::unsetUserId()
{
    m_UserIdIsSet = false;
}
utility::string_t Issues::getDomain() const
{
    return m_Domain;
}

void Issues::setDomain(const utility::string_t& value)
{
    m_Domain = value;
    m_DomainIsSet = true;
}

bool Issues::domainIsSet() const
{
    return m_DomainIsSet;
}

void Issues::unsetDomain()
{
    m_DomainIsSet = false;
}
utility::string_t Issues::getPageUrl() const
{
    return m_PageUrl;
}

void Issues::setPageUrl(const utility::string_t& value)
{
    m_PageUrl = value;
    m_PageUrlIsSet = true;
}

bool Issues::pageUrlIsSet() const
{
    return m_PageUrlIsSet;
}

void Issues::unsetPageUrl()
{
    m_PageUrlIsSet = false;
}
std::shared_ptr<PageIssue> Issues::getIssues() const
{
    return m_Issues;
}

void Issues::setIssues(const std::shared_ptr<PageIssue>& value)
{
    m_Issues = value;
    m_IssuesIsSet = true;
}

bool Issues::issuesIsSet() const
{
    return m_IssuesIsSet;
}

void Issues::unsetIssues()
{
    m_IssuesIsSet = false;
}
}
}
}
}


