/*
 * A11ywatch Client
 *
 * The web accessibility tool built for scale.
 *
 * The version of the OpenAPI document: 0.7.66
 * Contact: support@a11ywatch.com
 * Generated by: https://github.com/openapitools/openapi-generator.git
 */

using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.ComponentModel.DataAnnotations;
using OpenAPIDateConverter = Org.OpenAPITools.Client.OpenAPIDateConverter;

namespace Org.OpenAPITools.Model
{
    /// <summary>
    /// IssuesInfo
    /// </summary>
    [DataContract]
    public partial class IssuesInfo :  IEquatable<IssuesInfo>, IValidatableObject
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="IssuesInfo" /> class.
        /// </summary>
        /// <param name="accessScoreAverage">accessScoreAverage.</param>
        /// <param name="possibleIssuesFixedByCdn">possibleIssuesFixedByCdn.</param>
        /// <param name="totalIssues">totalIssues.</param>
        /// <param name="issuesFixedByCdn">issuesFixedByCdn.</param>
        /// <param name="errorCount">errorCount.</param>
        /// <param name="warningCount">warningCount.</param>
        /// <param name="noticeCount">noticeCount.</param>
        /// <param name="pageCount">pageCount.</param>
        public IssuesInfo(long accessScoreAverage = default(long), long possibleIssuesFixedByCdn = default(long), long totalIssues = default(long), long issuesFixedByCdn = default(long), long errorCount = default(long), long warningCount = default(long), long noticeCount = default(long), long pageCount = default(long))
        {
            this.AccessScoreAverage = accessScoreAverage;
            this.PossibleIssuesFixedByCdn = possibleIssuesFixedByCdn;
            this.TotalIssues = totalIssues;
            this.IssuesFixedByCdn = issuesFixedByCdn;
            this.ErrorCount = errorCount;
            this.WarningCount = warningCount;
            this.NoticeCount = noticeCount;
            this.PageCount = pageCount;
        }

        /// <summary>
        /// Gets or Sets AccessScoreAverage
        /// </summary>
        [DataMember(Name="accessScoreAverage", EmitDefaultValue=false)]
        public long AccessScoreAverage { get; set; }

        /// <summary>
        /// Gets or Sets PossibleIssuesFixedByCdn
        /// </summary>
        [DataMember(Name="possibleIssuesFixedByCdn", EmitDefaultValue=false)]
        public long PossibleIssuesFixedByCdn { get; set; }

        /// <summary>
        /// Gets or Sets TotalIssues
        /// </summary>
        [DataMember(Name="totalIssues", EmitDefaultValue=false)]
        public long TotalIssues { get; set; }

        /// <summary>
        /// Gets or Sets IssuesFixedByCdn
        /// </summary>
        [DataMember(Name="issuesFixedByCdn", EmitDefaultValue=false)]
        public long IssuesFixedByCdn { get; set; }

        /// <summary>
        /// Gets or Sets ErrorCount
        /// </summary>
        [DataMember(Name="errorCount", EmitDefaultValue=false)]
        public long ErrorCount { get; set; }

        /// <summary>
        /// Gets or Sets WarningCount
        /// </summary>
        [DataMember(Name="warningCount", EmitDefaultValue=false)]
        public long WarningCount { get; set; }

        /// <summary>
        /// Gets or Sets NoticeCount
        /// </summary>
        [DataMember(Name="noticeCount", EmitDefaultValue=false)]
        public long NoticeCount { get; set; }

        /// <summary>
        /// Gets or Sets PageCount
        /// </summary>
        [DataMember(Name="pageCount", EmitDefaultValue=false)]
        public long PageCount { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class IssuesInfo {\n");
            sb.Append("  AccessScoreAverage: ").Append(AccessScoreAverage).Append("\n");
            sb.Append("  PossibleIssuesFixedByCdn: ").Append(PossibleIssuesFixedByCdn).Append("\n");
            sb.Append("  TotalIssues: ").Append(TotalIssues).Append("\n");
            sb.Append("  IssuesFixedByCdn: ").Append(IssuesFixedByCdn).Append("\n");
            sb.Append("  ErrorCount: ").Append(ErrorCount).Append("\n");
            sb.Append("  WarningCount: ").Append(WarningCount).Append("\n");
            sb.Append("  NoticeCount: ").Append(NoticeCount).Append("\n");
            sb.Append("  PageCount: ").Append(PageCount).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public virtual string ToJson()
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(this, Newtonsoft.Json.Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="input">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object input)
        {
            return this.Equals(input as IssuesInfo);
        }

        /// <summary>
        /// Returns true if IssuesInfo instances are equal
        /// </summary>
        /// <param name="input">Instance of IssuesInfo to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(IssuesInfo input)
        {
            if (input == null)
                return false;

            return 
                (
                    this.AccessScoreAverage == input.AccessScoreAverage ||
                    (this.AccessScoreAverage != null &&
                    this.AccessScoreAverage.Equals(input.AccessScoreAverage))
                ) && 
                (
                    this.PossibleIssuesFixedByCdn == input.PossibleIssuesFixedByCdn ||
                    (this.PossibleIssuesFixedByCdn != null &&
                    this.PossibleIssuesFixedByCdn.Equals(input.PossibleIssuesFixedByCdn))
                ) && 
                (
                    this.TotalIssues == input.TotalIssues ||
                    (this.TotalIssues != null &&
                    this.TotalIssues.Equals(input.TotalIssues))
                ) && 
                (
                    this.IssuesFixedByCdn == input.IssuesFixedByCdn ||
                    (this.IssuesFixedByCdn != null &&
                    this.IssuesFixedByCdn.Equals(input.IssuesFixedByCdn))
                ) && 
                (
                    this.ErrorCount == input.ErrorCount ||
                    (this.ErrorCount != null &&
                    this.ErrorCount.Equals(input.ErrorCount))
                ) && 
                (
                    this.WarningCount == input.WarningCount ||
                    (this.WarningCount != null &&
                    this.WarningCount.Equals(input.WarningCount))
                ) && 
                (
                    this.NoticeCount == input.NoticeCount ||
                    (this.NoticeCount != null &&
                    this.NoticeCount.Equals(input.NoticeCount))
                ) && 
                (
                    this.PageCount == input.PageCount ||
                    (this.PageCount != null &&
                    this.PageCount.Equals(input.PageCount))
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hashCode = 41;
                if (this.AccessScoreAverage != null)
                    hashCode = hashCode * 59 + this.AccessScoreAverage.GetHashCode();
                if (this.PossibleIssuesFixedByCdn != null)
                    hashCode = hashCode * 59 + this.PossibleIssuesFixedByCdn.GetHashCode();
                if (this.TotalIssues != null)
                    hashCode = hashCode * 59 + this.TotalIssues.GetHashCode();
                if (this.IssuesFixedByCdn != null)
                    hashCode = hashCode * 59 + this.IssuesFixedByCdn.GetHashCode();
                if (this.ErrorCount != null)
                    hashCode = hashCode * 59 + this.ErrorCount.GetHashCode();
                if (this.WarningCount != null)
                    hashCode = hashCode * 59 + this.WarningCount.GetHashCode();
                if (this.NoticeCount != null)
                    hashCode = hashCode * 59 + this.NoticeCount.GetHashCode();
                if (this.PageCount != null)
                    hashCode = hashCode * 59 + this.PageCount.GetHashCode();
                return hashCode;
            }
        }

        /// <summary>
        /// To validate all properties of the instance
        /// </summary>
        /// <param name="validationContext">Validation context</param>
        /// <returns>Validation Result</returns>
        IEnumerable<System.ComponentModel.DataAnnotations.ValidationResult> IValidatableObject.Validate(ValidationContext validationContext)
        {
            yield break;
        }
    }

}
