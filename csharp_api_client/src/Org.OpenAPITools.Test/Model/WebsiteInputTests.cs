/*
 * A11ywatch Client
 *
 * The web accessibility tool built for scale.
 *
 * The version of the OpenAPI document: 0.7.66
 * Contact: support@a11ywatch.com
 * Generated by: https://github.com/openapitools/openapi-generator.git
 */


using NUnit.Framework;

using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using Org.OpenAPITools.Api;
using Org.OpenAPITools.Model;
using Org.OpenAPITools.Client;
using System.Reflection;
using Newtonsoft.Json;

namespace Org.OpenAPITools.Test
{
    /// <summary>
    ///  Class for testing WebsiteInput
    /// </summary>
    /// <remarks>
    /// This file is automatically generated by OpenAPI Generator (https://openapi-generator.tech).
    /// Please update the test case below to test the model.
    /// </remarks>
    public class WebsiteInputTests
    {
        // TODO uncomment below to declare an instance variable for WebsiteInput
        //private WebsiteInput instance;

        /// <summary>
        /// Setup before each test
        /// </summary>
        [SetUp]
        public void Init()
        {
            // TODO uncomment below to create an instance of WebsiteInput
            //instance = new WebsiteInput();
        }

        /// <summary>
        /// Clean up after each test
        /// </summary>
        [TearDown]
        public void Cleanup()
        {

        }

        /// <summary>
        /// Test an instance of WebsiteInput
        /// </summary>
        [Test]
        public void WebsiteInputInstanceTest()
        {
            // TODO uncomment below to test "IsInstanceOf" WebsiteInput
            //Assert.IsInstanceOf(typeof(WebsiteInput), instance);
        }


        /// <summary>
        /// Test the property 'Url'
        /// </summary>
        [Test]
        public void UrlTest()
        {
            // TODO unit test for the property 'Url'
        }
        /// <summary>
        /// Test the property 'Mobile'
        /// </summary>
        [Test]
        public void MobileTest()
        {
            // TODO unit test for the property 'Mobile'
        }
        /// <summary>
        /// Test the property 'PageInsights'
        /// </summary>
        [Test]
        public void PageInsightsTest()
        {
            // TODO unit test for the property 'PageInsights'
        }
        /// <summary>
        /// Test the property 'Ua'
        /// </summary>
        [Test]
        public void UaTest()
        {
            // TODO unit test for the property 'Ua'
        }
        /// <summary>
        /// Test the property 'Standard'
        /// </summary>
        [Test]
        public void StandardTest()
        {
            // TODO unit test for the property 'Standard'
        }
        /// <summary>
        /// Test the property 'Robots'
        /// </summary>
        [Test]
        public void RobotsTest()
        {
            // TODO unit test for the property 'Robots'
        }
        /// <summary>
        /// Test the property 'Subdomains'
        /// </summary>
        [Test]
        public void SubdomainsTest()
        {
            // TODO unit test for the property 'Subdomains'
        }
        /// <summary>
        /// Test the property 'Tld'
        /// </summary>
        [Test]
        public void TldTest()
        {
            // TODO unit test for the property 'Tld'
        }
        /// <summary>
        /// Test the property 'CustomHeaders'
        /// </summary>
        [Test]
        public void CustomHeadersTest()
        {
            // TODO unit test for the property 'CustomHeaders'
        }

    }

}
