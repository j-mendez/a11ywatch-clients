/*
 * A11ywatch Client
 *
 * The web accessibility tool built for scale.
 *
 * The version of the OpenAPI document: 0.7.66
 * Contact: support@a11ywatch.com
 * Generated by: https://github.com/openapitools/openapi-generator.git
 */

using System;
using System.IO;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using RestSharp;
using NUnit.Framework;

using Org.OpenAPITools.Client;
using Org.OpenAPITools.Api;
using Org.OpenAPITools.Model;

namespace Org.OpenAPITools.Test
{
    /// <summary>
    ///  Class for testing WebsitesApi
    /// </summary>
    /// <remarks>
    /// This file is automatically generated by OpenAPI Generator (https://openapi-generator.tech).
    /// Please update the test case below to test the API endpoint.
    /// </remarks>
    public class WebsitesApiTests
    {
        private WebsitesApi instance;

        /// <summary>
        /// Setup before each unit test
        /// </summary>
        [SetUp]
        public void Init()
        {
            instance = new WebsitesApi();
        }

        /// <summary>
        /// Clean up after each unit test
        /// </summary>
        [TearDown]
        public void Cleanup()
        {

        }

        /// <summary>
        /// Test an instance of WebsitesApi
        /// </summary>
        [Test]
        public void InstanceTest()
        {
            // TODO uncomment below to test 'IsInstanceOf' WebsitesApi
            //Assert.IsInstanceOf(typeof(WebsitesApi), instance);
        }

        
        /// <summary>
        /// Test AddWebsite
        /// </summary>
        [Test]
        public void AddWebsiteTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //WebsiteInput websiteInput = null;
            //var response = instance.AddWebsite(websiteInput);
            //Assert.IsInstanceOf(typeof(GetWebsiteByDomain200Response), response, "response is GetWebsiteByDomain200Response");
        }
        
        /// <summary>
        /// Test DeleteWebsite
        /// </summary>
        [Test]
        public void DeleteWebsiteTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //string domain = null;
            //var response = instance.DeleteWebsite(domain);
            //Assert.IsInstanceOf(typeof(GetWebsiteByDomain200Response), response, "response is GetWebsiteByDomain200Response");
        }
        
        /// <summary>
        /// Test GetWebsiteByDomain
        /// </summary>
        [Test]
        public void GetWebsiteByDomainTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //string domain = null;
            //var response = instance.GetWebsiteByDomain(domain);
            //Assert.IsInstanceOf(typeof(GetWebsiteByDomain200Response), response, "response is GetWebsiteByDomain200Response");
        }
        
    }

}
