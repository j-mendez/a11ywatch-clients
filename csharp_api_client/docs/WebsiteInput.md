
# Org.OpenAPITools.Model.WebsiteInput

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Url** | **string** |  | [optional] 
**Mobile** | **bool** |  | [optional] 
**PageInsights** | **bool** |  | [optional] 
**Ua** | **string** |  | [optional] 
**Standard** | **string** |  | [optional] 
**Robots** | **string** |  | [optional] 
**Subdomains** | **bool** |  | [optional] 
**Tld** | **bool** |  | [optional] 
**CustomHeaders** | **List&lt;string&gt;** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models)
[[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to README]](../README.md)

