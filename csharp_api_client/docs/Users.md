
# Org.OpenAPITools.Model.Users

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **long** |  | [optional] 
**Email** | **string** |  | [optional] 
**Jwt** | **string** |  | [optional] 
**Role** | **string** |  | [optional] 
**Password** | **string** |  | [optional] 
**AlertEnabled** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models)
[[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to README]](../README.md)

