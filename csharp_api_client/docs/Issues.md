
# Org.OpenAPITools.Model.Issues

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **long** |  | [optional] 
**UserId** | **long** |  | [optional] 
**Domain** | **string** |  | [optional] 
**PageUrl** | **string** |  | [optional] 
**_Issues** | [**PageIssue**](PageIssue.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models)
[[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to README]](../README.md)

