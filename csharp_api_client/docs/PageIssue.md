
# Org.OpenAPITools.Model.PageIssue

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Recurrence** | **long** |  | [optional] 
**TypeCode** | **long** |  | [optional] 
**Code** | **string** |  | [optional] 
**Message** | **string** |  | [optional] 
**Context** | **string** |  | [optional] 
**Selector** | **string** |  | [optional] 
**Runner** | **string** |  | [optional] 
**Type** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models)
[[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to README]](../README.md)

