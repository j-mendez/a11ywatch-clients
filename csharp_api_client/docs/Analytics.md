
# Org.OpenAPITools.Model.Analytics

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **long** |  | [optional] 
**Domain** | **string** |  | [optional] 
**PageUrl** | **string** |  | [optional] 
**UserId** | **long** |  | [optional] 
**AccessScore** | **long** |  | [optional] 
**PossibleIssuesFixedByCdn** | **long** |  | [optional] 
**TotalIssues** | **long** |  | [optional] 
**IssuesFixedByCdn** | **long** |  | [optional] 
**ErrorCount** | **long** |  | [optional] 
**WarningCount** | **long** |  | [optional] 
**NoticeCount** | **long** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models)
[[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to README]](../README.md)

