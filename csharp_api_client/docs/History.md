
# Org.OpenAPITools.Model.History

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **long** |  | [optional] 
**Domain** | **string** |  | [optional] 
**Insight** | [**WebsitesInsight**](WebsitesInsight.md) |  | [optional] 
**PageHeaders** | **List&lt;string&gt;** |  | [optional] 
**IssuesInfo** | [**IssuesInfo**](IssuesInfo.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models)
[[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to README]](../README.md)

