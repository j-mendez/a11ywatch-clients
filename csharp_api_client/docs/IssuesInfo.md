
# Org.OpenAPITools.Model.IssuesInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**AccessScoreAverage** | **long** |  | [optional] 
**PossibleIssuesFixedByCdn** | **long** |  | [optional] 
**TotalIssues** | **long** |  | [optional] 
**IssuesFixedByCdn** | **long** |  | [optional] 
**ErrorCount** | **long** |  | [optional] 
**WarningCount** | **long** |  | [optional] 
**NoticeCount** | **long** |  | [optional] 
**PageCount** | **long** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models)
[[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to README]](../README.md)

