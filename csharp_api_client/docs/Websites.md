
# Org.OpenAPITools.Model.Websites

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **long** |  | [optional] 
**UserId** | **long** |  | [optional] 
**Url** | **string** |  | [optional] 
**Domain** | **string** |  | [optional] 
**CrawlDuration** | **long** |  | [optional] 
**CdnConnected** | **bool** |  | [optional] 
**PageInsights** | **bool** |  | [optional] 
**Online** | **bool** |  | [optional] 
**Mobile** | **bool** |  | [optional] 
**Robots** | **bool** |  | [optional] 
**Insight** | [**WebsitesInsight**](WebsitesInsight.md) |  | [optional] 
**PageHeaders** | **List&lt;string&gt;** |  | [optional] 
**PageLoadTime** | [**PageLoadTime**](PageLoadTime.md) |  | [optional] 
**IssuesInfo** | [**IssuesInfo**](IssuesInfo.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models)
[[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to README]](../README.md)

