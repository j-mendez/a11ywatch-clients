
# Org.OpenAPITools.Model.Pages

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **long** |  | [optional] 
**UserId** | **long** |  | [optional] 
**Domain** | **string** |  | [optional] 
**Url** | **string** |  | [optional] 
**CdnConnected** | **bool** |  | [optional] 
**Online** | **bool** |  | [optional] 
**PageLoadTime** | [**PageLoadTime**](PageLoadTime.md) |  | [optional] 
**Insight** | [**WebsitesInsight**](WebsitesInsight.md) |  | [optional] 
**IssuesInfo** | [**IssuesInfo**](IssuesInfo.md) |  | [optional] 
**LastScanDate** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models)
[[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to README]](../README.md)

