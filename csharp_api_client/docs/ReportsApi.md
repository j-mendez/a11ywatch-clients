# Org.OpenAPITools.Api.ReportsApi

All URIs are relative to *https://api.a11ywatch.com/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**CrawlWebsiteStream**](ReportsApi.md#crawlwebsitestream) | **POST** /crawl | Multi-page crawl a website streaming issues on found
[**CrawlWebsitesSync**](ReportsApi.md#crawlwebsitessync) | **POST** /websites-sync | Multi-page crawl all websites attached to account
[**GetReport**](ReportsApi.md#getreport) | **GET** /report | Get the report from a previus scan
[**ScanWebsite**](ReportsApi.md#scanwebsite) | **POST** /scan | Scan a website for issues
[**ScanWebsiteSimple**](ReportsApi.md#scanwebsitesimple) | **POST** /scan-simple | Scan a website for issues without storing data and limited responses.



## CrawlWebsiteStream

> Object CrawlWebsiteStream (string transferEncoding, WebsiteInput websiteInput)

Multi-page crawl a website streaming issues on found

### Example

```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Org.OpenAPITools.Api;
using Org.OpenAPITools.Client;
using Org.OpenAPITools.Model;

namespace Example
{
    public class CrawlWebsiteStreamExample
    {
        public static void Main()
        {
            Configuration.Default.BasePath = "https://api.a11ywatch.com/api";
            // Configure HTTP bearer authorization: bearerAuth
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";

            var apiInstance = new ReportsApi(Configuration.Default);
            var transferEncoding = transferEncoding_example;  // string |  (default to "Chunked")
            var websiteInput = new WebsiteInput(); // WebsiteInput | The website standard body

            try
            {
                // Multi-page crawl a website streaming issues on found
                Object result = apiInstance.CrawlWebsiteStream(transferEncoding, websiteInput);
                Debug.WriteLine(result);
            }
            catch (ApiException e)
            {
                Debug.Print("Exception when calling ReportsApi.CrawlWebsiteStream: " + e.Message );
                Debug.Print("Status Code: "+ e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **transferEncoding** | **string**|  | [default to &quot;Chunked&quot;]
 **websiteInput** | [**WebsiteInput**](WebsiteInput.md)| The website standard body | 

### Return type

**Object**

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | successful operation |  -  |
| **405** | Invalid input |  -  |

[[Back to top]](#)
[[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## CrawlWebsitesSync

> Object CrawlWebsitesSync ()

Multi-page crawl all websites attached to account

### Example

```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Org.OpenAPITools.Api;
using Org.OpenAPITools.Client;
using Org.OpenAPITools.Model;

namespace Example
{
    public class CrawlWebsitesSyncExample
    {
        public static void Main()
        {
            Configuration.Default.BasePath = "https://api.a11ywatch.com/api";
            // Configure HTTP bearer authorization: bearerAuth
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";

            var apiInstance = new ReportsApi(Configuration.Default);

            try
            {
                // Multi-page crawl all websites attached to account
                Object result = apiInstance.CrawlWebsitesSync();
                Debug.WriteLine(result);
            }
            catch (ApiException e)
            {
                Debug.Print("Exception when calling ReportsApi.CrawlWebsitesSync: " + e.Message );
                Debug.Print("Status Code: "+ e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

### Parameters

This endpoint does not need any parameter.

### Return type

**Object**

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | successful operation |  -  |
| **405** | Invalid input |  -  |

[[Back to top]](#)
[[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetReport

> Object GetReport (string url = null, string domain = null)

Get the report from a previus scan

### Example

```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Org.OpenAPITools.Api;
using Org.OpenAPITools.Client;
using Org.OpenAPITools.Model;

namespace Example
{
    public class GetReportExample
    {
        public static void Main()
        {
            Configuration.Default.BasePath = "https://api.a11ywatch.com/api";
            // Configure HTTP bearer authorization: bearerAuth
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";

            var apiInstance = new ReportsApi(Configuration.Default);
            var url = url_example;  // string | The page url or domain for the report (optional) 
            var domain = domain_example;  // string | Domain of website that needs to be fetched (optional) 

            try
            {
                // Get the report from a previus scan
                Object result = apiInstance.GetReport(url, domain);
                Debug.WriteLine(result);
            }
            catch (ApiException e)
            {
                Debug.Print("Exception when calling ReportsApi.GetReport: " + e.Message );
                Debug.Print("Status Code: "+ e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **url** | **string**| The page url or domain for the report | [optional] 
 **domain** | **string**| Domain of website that needs to be fetched | [optional] 

### Return type

**Object**

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | successful operation |  -  |
| **405** | Invalid input |  -  |

[[Back to top]](#)
[[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ScanWebsite

> Object ScanWebsite (WebsiteInput websiteInput)

Scan a website for issues

### Example

```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Org.OpenAPITools.Api;
using Org.OpenAPITools.Client;
using Org.OpenAPITools.Model;

namespace Example
{
    public class ScanWebsiteExample
    {
        public static void Main()
        {
            Configuration.Default.BasePath = "https://api.a11ywatch.com/api";
            // Configure HTTP bearer authorization: bearerAuth
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";

            var apiInstance = new ReportsApi(Configuration.Default);
            var websiteInput = new WebsiteInput(); // WebsiteInput | The website standard body

            try
            {
                // Scan a website for issues
                Object result = apiInstance.ScanWebsite(websiteInput);
                Debug.WriteLine(result);
            }
            catch (ApiException e)
            {
                Debug.Print("Exception when calling ReportsApi.ScanWebsite: " + e.Message );
                Debug.Print("Status Code: "+ e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **websiteInput** | [**WebsiteInput**](WebsiteInput.md)| The website standard body | 

### Return type

**Object**

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | successful operation |  -  |
| **405** | Invalid input |  -  |

[[Back to top]](#)
[[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ScanWebsiteSimple

> Object ScanWebsiteSimple (WebsiteInput websiteInput)

Scan a website for issues without storing data and limited responses.

### Example

```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Org.OpenAPITools.Api;
using Org.OpenAPITools.Client;
using Org.OpenAPITools.Model;

namespace Example
{
    public class ScanWebsiteSimpleExample
    {
        public static void Main()
        {
            Configuration.Default.BasePath = "https://api.a11ywatch.com/api";
            // Configure HTTP bearer authorization: bearerAuth
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";

            var apiInstance = new ReportsApi(Configuration.Default);
            var websiteInput = new WebsiteInput(); // WebsiteInput | The website standard body

            try
            {
                // Scan a website for issues without storing data and limited responses.
                Object result = apiInstance.ScanWebsiteSimple(websiteInput);
                Debug.WriteLine(result);
            }
            catch (ApiException e)
            {
                Debug.Print("Exception when calling ReportsApi.ScanWebsiteSimple: " + e.Message );
                Debug.Print("Status Code: "+ e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **websiteInput** | [**WebsiteInput**](WebsiteInput.md)| The website standard body | 

### Return type

**Object**

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | successful operation |  -  |
| **405** | Invalid input |  -  |

[[Back to top]](#)
[[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

