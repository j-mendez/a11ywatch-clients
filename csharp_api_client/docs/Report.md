
# Org.OpenAPITools.Model.Report

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **long** |  | [optional] 
**Domain** | **string** |  | [optional] 
**PageUrl** | **string** |  | [optional] 
**Issues** | [**List&lt;PageIssue&gt;**](PageIssue.md) |  | [optional] 
**IssuesInfo** | [**IssuesInfo**](IssuesInfo.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models)
[[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to README]](../README.md)

