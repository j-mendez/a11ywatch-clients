# Org.OpenAPITools.Model.PageLoadTime

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Duration** | **decimal** |  | [optional] 
**DurationFormated** | **string** |  | [optional] 
**Color** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

