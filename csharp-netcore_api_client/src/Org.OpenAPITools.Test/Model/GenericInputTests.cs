/*
 * A11ywatch Client
 *
 * The web accessibility tool built for scale.
 *
 * The version of the OpenAPI document: 0.7.66
 * Contact: support@a11ywatch.com
 * Generated by: https://github.com/openapitools/openapi-generator.git
 */


using Xunit;

using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using Org.OpenAPITools.Api;
using Org.OpenAPITools.Model;
using Org.OpenAPITools.Client;
using System.Reflection;
using Newtonsoft.Json;

namespace Org.OpenAPITools.Test.Model
{
    /// <summary>
    ///  Class for testing GenericInput
    /// </summary>
    /// <remarks>
    /// This file is automatically generated by OpenAPI Generator (https://openapi-generator.tech).
    /// Please update the test case below to test the model.
    /// </remarks>
    public class GenericInputTests : IDisposable
    {
        // TODO uncomment below to declare an instance variable for GenericInput
        //private GenericInput instance;

        public GenericInputTests()
        {
            // TODO uncomment below to create an instance of GenericInput
            //instance = new GenericInput();
        }

        public void Dispose()
        {
            // Cleanup when everything is done.
        }

        /// <summary>
        /// Test an instance of GenericInput
        /// </summary>
        [Fact]
        public void GenericInputInstanceTest()
        {
            // TODO uncomment below to test "IsType" GenericInput
            //Assert.IsType<GenericInput>(instance);
        }


        /// <summary>
        /// Test the property 'Domain'
        /// </summary>
        [Fact]
        public void DomainTest()
        {
            // TODO unit test for the property 'Domain'
        }

    }

}
