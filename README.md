# a11ywatch-clients

The repo to manage all of the source code generated by OpenAPI.

## Clients

The main repo lives in [here](https://github.com/A11yWatch/a11ywatch/tree/main/clients) on github and has the source code ignored.
We use gitlab to host all of the code that is auto generated.

- [C](./c_api_client)

- [CLI](https://github.com/a11ywatch/a11ywatch/tree/main/cli)

- [C++](./cpp-restsdk_api_client)

- [C#](./csharp_api_client)

- [C#-NetCore](./csharp-netcore_api_client)

- [Elixir](./elixir_api_client)

- [Go](./go_api_client)

- [Java](./java_api_client)

- [Javascript](./javascript_api_client)

- [Lua](./lua_api_client)

- [Rust](./rust_api_client)

- [Ruby](./ruby_api_client)

- [Swift](./swift5_api_client)

- [Typescript](./typescript_api_client)

- [Obj-C](./objc_api_client)

- [PHP](./php_api_client)

- [Python](./python_api_client)
