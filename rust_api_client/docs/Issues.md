# Issues

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_id** | Option<**i64**> |  | [optional]
**user_id** | Option<**i64**> |  | [optional]
**domain** | Option<**String**> |  | [optional]
**page_url** | Option<**String**> |  | [optional]
**issues** | Option<[**crate::models::PageIssue**](PageIssue.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


