# \ReportsApi

All URIs are relative to *https://api.a11ywatch.com/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**crawl_website_stream**](ReportsApi.md#crawl_website_stream) | **POST** /crawl | Multi-page crawl a website streaming issues on found
[**crawl_websites_sync**](ReportsApi.md#crawl_websites_sync) | **POST** /websites-sync | Multi-page crawl all websites attached to account
[**get_report**](ReportsApi.md#get_report) | **GET** /report | Get the report from a previus scan
[**scan_website**](ReportsApi.md#scan_website) | **POST** /scan | Scan a website for issues
[**scan_website_simple**](ReportsApi.md#scan_website_simple) | **POST** /scan-simple | Scan a website for issues without storing data and limited responses.



## crawl_website_stream

> serde_json::Value crawl_website_stream(transfer_encoding, website_input)
Multi-page crawl a website streaming issues on found

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**transfer_encoding** | **String** |  | [required] |[default to Chunked]
**website_input** | [**WebsiteInput**](WebsiteInput.md) | The website standard body | [required] |

### Return type

[**serde_json::Value**](serde_json::Value.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## crawl_websites_sync

> serde_json::Value crawl_websites_sync()
Multi-page crawl all websites attached to account

### Parameters

This endpoint does not need any parameter.

### Return type

[**serde_json::Value**](serde_json::Value.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_report

> serde_json::Value get_report(url, domain)
Get the report from a previus scan

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**url** | Option<**String**> | The page url or domain for the report |  |
**domain** | Option<**String**> | Domain of website that needs to be fetched |  |

### Return type

[**serde_json::Value**](serde_json::Value.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## scan_website

> serde_json::Value scan_website(website_input)
Scan a website for issues

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**website_input** | [**WebsiteInput**](WebsiteInput.md) | The website standard body | [required] |

### Return type

[**serde_json::Value**](serde_json::Value.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## scan_website_simple

> serde_json::Value scan_website_simple(website_input)
Scan a website for issues without storing data and limited responses.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**website_input** | [**WebsiteInput**](WebsiteInput.md) | The website standard body | [required] |

### Return type

[**serde_json::Value**](serde_json::Value.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

