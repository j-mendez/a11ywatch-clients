# History

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_id** | Option<**i64**> |  | [optional]
**domain** | Option<**String**> |  | [optional]
**insight** | Option<[**crate::models::WebsitesInsight**](Websites_insight.md)> |  | [optional]
**page_headers** | Option<**Vec<String>**> |  | [optional]
**issues_info** | Option<[**crate::models::IssuesInfo**](IssuesInfo.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


