# Users

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_id** | Option<**i64**> |  | [optional]
**email** | Option<**String**> |  | [optional]
**jwt** | Option<**String**> |  | [optional]
**role** | Option<**String**> |  | [optional]
**password** | Option<**String**> |  | [optional]
**alert_enabled** | Option<**bool**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


