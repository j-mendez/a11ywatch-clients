# PageLoadTime

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**duration** | Option<**f32**> |  | [optional]
**duration_formated** | Option<**String**> |  | [optional]
**color** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


