# \CollectionApi

All URIs are relative to *https://api.a11ywatch.com/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_analytics**](CollectionApi.md#get_analytics) | **GET** /list/analytics | Get the analytics for a website
[**get_issues**](CollectionApi.md#get_issues) | **GET** /list/issue | List the issues for a website
[**get_page_speed**](CollectionApi.md#get_page_speed) | **GET** /list/pagespeed | Get the pagespeed for a website
[**get_pages**](CollectionApi.md#get_pages) | **GET** /list/pages | List the pages in order for a website
[**get_websites**](CollectionApi.md#get_websites) | **GET** /list/website | Returns websites for the user in alphabetical order



## get_analytics

> serde_json::Value get_analytics(offset, domain)
Get the analytics for a website

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**offset** | Option<**String**> | The page offset for the next set |  |
**domain** | Option<**String**> | Domain of website that needs to be fetched |  |

### Return type

[**serde_json::Value**](serde_json::Value.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_issues

> serde_json::Value get_issues(offset, domain)
List the issues for a website

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**offset** | Option<**String**> | The page offset for the next set |  |
**domain** | Option<**String**> | Domain of website that needs to be fetched |  |

### Return type

[**serde_json::Value**](serde_json::Value.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_page_speed

> serde_json::Value get_page_speed(offset, domain)
Get the pagespeed for a website

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**offset** | Option<**String**> | The page offset for the next set |  |
**domain** | Option<**String**> | Domain of website that needs to be fetched |  |

### Return type

[**serde_json::Value**](serde_json::Value.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_pages

> serde_json::Value get_pages(offset, domain)
List the pages in order for a website

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**offset** | Option<**String**> | The page offset for the next set |  |
**domain** | Option<**String**> | Domain of website that needs to be fetched |  |

### Return type

[**serde_json::Value**](serde_json::Value.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_websites

> serde_json::Value get_websites(offset)
Returns websites for the user in alphabetical order

Returns a map of websites

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**offset** | Option<**String**> | The page offset for the next set |  |

### Return type

[**serde_json::Value**](serde_json::Value.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

