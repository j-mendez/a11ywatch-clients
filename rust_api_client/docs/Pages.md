# Pages

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_id** | Option<**i64**> |  | [optional]
**user_id** | Option<**i64**> |  | [optional]
**domain** | Option<**String**> |  | [optional]
**url** | Option<**String**> |  | [optional]
**cdn_connected** | Option<**bool**> |  | [optional]
**online** | Option<**bool**> |  | [optional]
**page_load_time** | Option<[**crate::models::PageLoadTime**](PageLoadTime.md)> |  | [optional]
**insight** | Option<[**crate::models::WebsitesInsight**](Websites_insight.md)> |  | [optional]
**issues_info** | Option<[**crate::models::IssuesInfo**](IssuesInfo.md)> |  | [optional]
**last_scan_date** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


