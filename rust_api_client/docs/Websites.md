# Websites

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_id** | Option<**i64**> |  | [optional]
**user_id** | Option<**i64**> |  | [optional]
**url** | Option<**String**> |  | [optional]
**domain** | Option<**String**> |  | [optional]
**crawl_duration** | Option<**i64**> |  | [optional]
**cdn_connected** | Option<**bool**> |  | [optional]
**page_insights** | Option<**bool**> |  | [optional]
**online** | Option<**bool**> |  | [optional]
**mobile** | Option<**bool**> |  | [optional]
**robots** | Option<**bool**> |  | [optional]
**insight** | Option<[**crate::models::WebsitesInsight**](Websites_insight.md)> |  | [optional]
**page_headers** | Option<**Vec<String>**> |  | [optional]
**page_load_time** | Option<[**crate::models::PageLoadTime**](PageLoadTime.md)> |  | [optional]
**issues_info** | Option<[**crate::models::IssuesInfo**](IssuesInfo.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


