# PageIssue

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**recurrence** | Option<**i64**> |  | [optional]
**type_code** | Option<**i64**> |  | [optional]
**code** | Option<**String**> |  | [optional]
**message** | Option<**String**> |  | [optional]
**context** | Option<**String**> |  | [optional]
**selector** | Option<**String**> |  | [optional]
**runner** | Option<**String**> |  | [optional]
**_type** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


