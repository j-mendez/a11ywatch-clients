# Analytics

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_id** | Option<**i64**> |  | [optional]
**domain** | Option<**String**> |  | [optional]
**page_url** | Option<**String**> |  | [optional]
**user_id** | Option<**i64**> |  | [optional]
**access_score** | Option<**i64**> |  | [optional]
**possible_issues_fixed_by_cdn** | Option<**i64**> |  | [optional]
**total_issues** | Option<**i64**> |  | [optional]
**issues_fixed_by_cdn** | Option<**i64**> |  | [optional]
**error_count** | Option<**i64**> |  | [optional]
**warning_count** | Option<**i64**> |  | [optional]
**notice_count** | Option<**i64**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


