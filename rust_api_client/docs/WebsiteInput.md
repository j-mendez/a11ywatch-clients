# WebsiteInput

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**url** | Option<**String**> |  | [optional]
**mobile** | Option<**bool**> |  | [optional]
**page_insights** | Option<**bool**> |  | [optional]
**ua** | Option<**String**> |  | [optional]
**standard** | Option<**String**> |  | [optional]
**robots** | Option<**String**> |  | [optional]
**subdomains** | Option<**bool**> |  | [optional]
**tld** | Option<**bool**> |  | [optional]
**custom_headers** | Option<**Vec<String>**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


