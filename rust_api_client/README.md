# Rust API client for openapi

The web accessibility tool built for scale.

For more information, please visit [https://a11ywatch.com/contact](https://a11ywatch.com/contact)

## Overview

This API client was generated by the [OpenAPI Generator](https://openapi-generator.tech) project.  By using the [openapi-spec](https://openapis.org) from a remote server, you can easily generate an API client.

- API version: 0.7.66
- Package version: 0.7.66
- Build package: `org.openapitools.codegen.languages.RustClientCodegen`

## Installation

Put the package under your project folder in a directory named `openapi` and add the following to `Cargo.toml` under `[dependencies]`:

```
openapi = { path = "./openapi" }
```

## Documentation for API Endpoints

All URIs are relative to *https://api.a11ywatch.com/api*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*CollectionApi* | [**get_analytics**](docs/CollectionApi.md#get_analytics) | **GET** /list/analytics | Get the analytics for a website
*CollectionApi* | [**get_issues**](docs/CollectionApi.md#get_issues) | **GET** /list/issue | List the issues for a website
*CollectionApi* | [**get_page_speed**](docs/CollectionApi.md#get_page_speed) | **GET** /list/pagespeed | Get the pagespeed for a website
*CollectionApi* | [**get_pages**](docs/CollectionApi.md#get_pages) | **GET** /list/pages | List the pages in order for a website
*CollectionApi* | [**get_websites**](docs/CollectionApi.md#get_websites) | **GET** /list/website | Returns websites for the user in alphabetical order
*ReportsApi* | [**crawl_website_stream**](docs/ReportsApi.md#crawl_website_stream) | **POST** /crawl | Multi-page crawl a website streaming issues on found
*ReportsApi* | [**crawl_websites_sync**](docs/ReportsApi.md#crawl_websites_sync) | **POST** /websites-sync | Multi-page crawl all websites attached to account
*ReportsApi* | [**get_report**](docs/ReportsApi.md#get_report) | **GET** /report | Get the report from a previus scan
*ReportsApi* | [**scan_website**](docs/ReportsApi.md#scan_website) | **POST** /scan | Scan a website for issues
*ReportsApi* | [**scan_website_simple**](docs/ReportsApi.md#scan_website_simple) | **POST** /scan-simple | Scan a website for issues without storing data and limited responses.
*UserApi* | [**create_user**](docs/UserApi.md#create_user) | **POST** /register | Register user into the system
*UserApi* | [**get_users**](docs/UserApi.md#get_users) | **GET** /user | Get user
*UserApi* | [**login_user**](docs/UserApi.md#login_user) | **POST** /login | Logs user into the system
*UserApi* | [**logout_user**](docs/UserApi.md#logout_user) | **POST** /logout | Logs out current logged in user session
*WebsitesApi* | [**add_website**](docs/WebsitesApi.md#add_website) | **POST** /website | Add a website in the collection with form data
*WebsitesApi* | [**delete_website**](docs/WebsitesApi.md#delete_website) | **DELETE** /website | Deletes a website
*WebsitesApi* | [**get_website_by_domain**](docs/WebsitesApi.md#get_website_by_domain) | **GET** /website | Find website by Domain


## Documentation For Models

 - [Analytics](docs/Analytics.md)
 - [AuthInput](docs/AuthInput.md)
 - [CrawlInput](docs/CrawlInput.md)
 - [GenericInput](docs/GenericInput.md)
 - [History](docs/History.md)
 - [Issues](docs/Issues.md)
 - [IssuesInfo](docs/IssuesInfo.md)
 - [PageIssue](docs/PageIssue.md)
 - [PageLoadTime](docs/PageLoadTime.md)
 - [Pages](docs/Pages.md)
 - [Report](docs/Report.md)
 - [Users](docs/Users.md)
 - [WebsiteInput](docs/WebsiteInput.md)
 - [Websites](docs/Websites.md)
 - [WebsitesInsight](docs/WebsitesInsight.md)


To get access to the crate's generated documentation, use:

```
cargo doc --open
```

## Author

support@a11ywatch.com

