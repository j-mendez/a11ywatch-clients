/*
 * A11ywatch Client
 *
 * The web accessibility tool built for scale.
 *
 * The version of the OpenAPI document: 0.7.66
 * Contact: support@a11ywatch.com
 * Generated by: https://openapi-generator.tech
 */




#[derive(Clone, Debug, PartialEq, Default, Serialize, Deserialize)]
pub struct Pages {
    #[serde(rename = "_id", skip_serializing_if = "Option::is_none")]
    pub _id: Option<i64>,
    #[serde(rename = "userId", skip_serializing_if = "Option::is_none")]
    pub user_id: Option<i64>,
    #[serde(rename = "domain", skip_serializing_if = "Option::is_none")]
    pub domain: Option<String>,
    #[serde(rename = "url", skip_serializing_if = "Option::is_none")]
    pub url: Option<String>,
    #[serde(rename = "cdnConnected", skip_serializing_if = "Option::is_none")]
    pub cdn_connected: Option<bool>,
    #[serde(rename = "online", skip_serializing_if = "Option::is_none")]
    pub online: Option<bool>,
    #[serde(rename = "pageLoadTime", skip_serializing_if = "Option::is_none")]
    pub page_load_time: Option<Box<crate::models::PageLoadTime>>,
    #[serde(rename = "insight", skip_serializing_if = "Option::is_none")]
    pub insight: Option<Box<crate::models::WebsitesInsight>>,
    #[serde(rename = "issuesInfo", skip_serializing_if = "Option::is_none")]
    pub issues_info: Option<Box<crate::models::IssuesInfo>>,
    #[serde(rename = "lastScanDate", skip_serializing_if = "Option::is_none")]
    pub last_scan_date: Option<String>,
}

impl Pages {
    pub fn new() -> Pages {
        Pages {
            _id: None,
            user_id: None,
            domain: None,
            url: None,
            cdn_connected: None,
            online: None,
            page_load_time: None,
            insight: None,
            issues_info: None,
            last_scan_date: None,
        }
    }
}


