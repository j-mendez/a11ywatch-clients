/*
 * A11ywatch Client
 *
 * The web accessibility tool built for scale.
 *
 * The version of the OpenAPI document: 0.7.66
 * Contact: support@a11ywatch.com
 * Generated by: https://openapi-generator.tech
 */




#[derive(Clone, Debug, PartialEq, Default, Serialize, Deserialize)]
pub struct ScanWebsiteSimple200Response {
    #[serde(rename = "data", skip_serializing_if = "Option::is_none")]
    pub data: Option<Box<crate::models::Report>>,
}

impl ScanWebsiteSimple200Response {
    pub fn new() -> ScanWebsiteSimple200Response {
        ScanWebsiteSimple200Response {
            data: None,
        }
    }
}


