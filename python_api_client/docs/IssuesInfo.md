# IssuesInfo


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**access_score_average** | **int** |  | [optional] 
**possible_issues_fixed_by_cdn** | **int** |  | [optional] 
**total_issues** | **int** |  | [optional] 
**issues_fixed_by_cdn** | **int** |  | [optional] 
**error_count** | **int** |  | [optional] 
**warning_count** | **int** |  | [optional] 
**notice_count** | **int** |  | [optional] 
**page_count** | **int** |  | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


