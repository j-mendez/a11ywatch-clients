# WebsiteInput


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**url** | **str** |  | [optional] 
**mobile** | **bool** |  | [optional] 
**page_insights** | **bool** |  | [optional] 
**ua** | **str** |  | [optional] 
**standard** | **str** |  | [optional] 
**robots** | **str** |  | [optional] 
**subdomains** | **bool** |  | [optional] 
**tld** | **bool** |  | [optional] 
**custom_headers** | **[str]** |  | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


