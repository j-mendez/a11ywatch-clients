# openapi_client.ReportsApi

All URIs are relative to *https://api.a11ywatch.com/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**crawl_website_stream**](ReportsApi.md#crawl_website_stream) | **POST** /crawl | Multi-page crawl a website streaming issues on found
[**crawl_websites_sync**](ReportsApi.md#crawl_websites_sync) | **POST** /websites-sync | Multi-page crawl all websites attached to account
[**get_report**](ReportsApi.md#get_report) | **GET** /report | Get the report from a previus scan
[**scan_website**](ReportsApi.md#scan_website) | **POST** /scan | Scan a website for issues
[**scan_website_simple**](ReportsApi.md#scan_website_simple) | **POST** /scan-simple | Scan a website for issues without storing data and limited responses.


# **crawl_website_stream**
> bool, date, datetime, dict, float, int, list, str, none_type crawl_website_stream(website_input)

Multi-page crawl a website streaming issues on found

### Example

* Bearer (JWT) Authentication (bearerAuth):

```python
import time
import openapi_client
from openapi_client.api import reports_api
from openapi_client.model.website_input import WebsiteInput
from pprint import pprint
# Defining the host is optional and defaults to https://api.a11ywatch.com/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://api.a11ywatch.com/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): bearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = reports_api.ReportsApi(api_client)
    website_input = WebsiteInput(
        url="url_example",
        mobile=True,
        page_insights=True,
        ua="ua_example",
        standard="standard_example",
        robots="robots_example",
        subdomains=True,
        tld=True,
        custom_headers=[
            "custom_headers_example",
        ],
    ) # WebsiteInput | The website standard body

    # example passing only required values which don't have defaults set
    try:
        # Multi-page crawl a website streaming issues on found
        api_response = api_instance.crawl_website_stream(website_input)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling ReportsApi->crawl_website_stream: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **website_input** | [**WebsiteInput**](WebsiteInput.md)| The website standard body |
 **transfer_encoding** | **str**|  | defaults to "Chunked"

### Return type

**bool, date, datetime, dict, float, int, list, str, none_type**

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**405** | Invalid input |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **crawl_websites_sync**
> bool, date, datetime, dict, float, int, list, str, none_type crawl_websites_sync()

Multi-page crawl all websites attached to account

### Example

* Bearer (JWT) Authentication (bearerAuth):

```python
import time
import openapi_client
from openapi_client.api import reports_api
from pprint import pprint
# Defining the host is optional and defaults to https://api.a11ywatch.com/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://api.a11ywatch.com/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): bearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = reports_api.ReportsApi(api_client)

    # example, this endpoint has no required or optional parameters
    try:
        # Multi-page crawl all websites attached to account
        api_response = api_instance.crawl_websites_sync()
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling ReportsApi->crawl_websites_sync: %s\n" % e)
```


### Parameters
This endpoint does not need any parameter.

### Return type

**bool, date, datetime, dict, float, int, list, str, none_type**

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**405** | Invalid input |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_report**
> bool, date, datetime, dict, float, int, list, str, none_type get_report()

Get the report from a previus scan

### Example

* Bearer (JWT) Authentication (bearerAuth):

```python
import time
import openapi_client
from openapi_client.api import reports_api
from pprint import pprint
# Defining the host is optional and defaults to https://api.a11ywatch.com/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://api.a11ywatch.com/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): bearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = reports_api.ReportsApi(api_client)
    url = "url_example" # str | The page url or domain for the report (optional)
    domain = "domain_example" # str | Domain of website that needs to be fetched (optional)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Get the report from a previus scan
        api_response = api_instance.get_report(url=url, domain=domain)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling ReportsApi->get_report: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **url** | **str**| The page url or domain for the report | [optional]
 **domain** | **str**| Domain of website that needs to be fetched | [optional]

### Return type

**bool, date, datetime, dict, float, int, list, str, none_type**

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**405** | Invalid input |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **scan_website**
> bool, date, datetime, dict, float, int, list, str, none_type scan_website(website_input)

Scan a website for issues

### Example

* Bearer (JWT) Authentication (bearerAuth):

```python
import time
import openapi_client
from openapi_client.api import reports_api
from openapi_client.model.website_input import WebsiteInput
from pprint import pprint
# Defining the host is optional and defaults to https://api.a11ywatch.com/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://api.a11ywatch.com/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): bearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = reports_api.ReportsApi(api_client)
    website_input = WebsiteInput(
        url="url_example",
        mobile=True,
        page_insights=True,
        ua="ua_example",
        standard="standard_example",
        robots="robots_example",
        subdomains=True,
        tld=True,
        custom_headers=[
            "custom_headers_example",
        ],
    ) # WebsiteInput | The website standard body

    # example passing only required values which don't have defaults set
    try:
        # Scan a website for issues
        api_response = api_instance.scan_website(website_input)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling ReportsApi->scan_website: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **website_input** | [**WebsiteInput**](WebsiteInput.md)| The website standard body |

### Return type

**bool, date, datetime, dict, float, int, list, str, none_type**

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**405** | Invalid input |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **scan_website_simple**
> bool, date, datetime, dict, float, int, list, str, none_type scan_website_simple(website_input)

Scan a website for issues without storing data and limited responses.

### Example

* Bearer (JWT) Authentication (bearerAuth):

```python
import time
import openapi_client
from openapi_client.api import reports_api
from openapi_client.model.website_input import WebsiteInput
from pprint import pprint
# Defining the host is optional and defaults to https://api.a11ywatch.com/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://api.a11ywatch.com/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): bearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = reports_api.ReportsApi(api_client)
    website_input = WebsiteInput(
        url="url_example",
        mobile=True,
        page_insights=True,
        ua="ua_example",
        standard="standard_example",
        robots="robots_example",
        subdomains=True,
        tld=True,
        custom_headers=[
            "custom_headers_example",
        ],
    ) # WebsiteInput | The website standard body

    # example passing only required values which don't have defaults set
    try:
        # Scan a website for issues without storing data and limited responses.
        api_response = api_instance.scan_website_simple(website_input)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling ReportsApi->scan_website_simple: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **website_input** | [**WebsiteInput**](WebsiteInput.md)| The website standard body |

### Return type

**bool, date, datetime, dict, float, int, list, str, none_type**

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**405** | Invalid input |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

