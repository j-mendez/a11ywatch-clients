# Pages


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**user_id** | **int** |  | [optional] 
**domain** | **str** |  | [optional] 
**url** | **str** |  | [optional] 
**cdn_connected** | **bool** |  | [optional] 
**online** | **bool** |  | [optional] 
**page_load_time** | [**PageLoadTime**](PageLoadTime.md) |  | [optional] 
**insight** | [**WebsitesInsight**](WebsitesInsight.md) |  | [optional] 
**issues_info** | [**IssuesInfo**](IssuesInfo.md) |  | [optional] 
**last_scan_date** | **str** |  | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


