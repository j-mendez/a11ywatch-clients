# Report


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**domain** | **str** |  | [optional] 
**page_url** | **str** |  | [optional] 
**issues** | [**[PageIssue]**](PageIssue.md) |  | [optional] 
**issues_info** | [**IssuesInfo**](IssuesInfo.md) |  | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


