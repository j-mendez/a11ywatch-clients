# Websites


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**user_id** | **int** |  | [optional] 
**url** | **str** |  | [optional] 
**domain** | **str** |  | [optional] 
**crawl_duration** | **int** |  | [optional] 
**cdn_connected** | **bool** |  | [optional] 
**page_insights** | **bool** |  | [optional] 
**online** | **bool** |  | [optional] 
**mobile** | **bool** |  | [optional] 
**robots** | **bool** |  | [optional] 
**insight** | [**WebsitesInsight**](WebsitesInsight.md) |  | [optional] 
**page_headers** | **[str]** |  | [optional] 
**page_load_time** | [**PageLoadTime**](PageLoadTime.md) |  | [optional] 
**issues_info** | [**IssuesInfo**](IssuesInfo.md) |  | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


