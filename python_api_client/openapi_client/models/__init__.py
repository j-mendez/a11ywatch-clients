# flake8: noqa

# import all models into this package
# if you have many models here with many references from one model to another this may
# raise a RecursionError
# to avoid this, import only the models that you directly need like:
# from from openapi_client.model.pet import Pet
# or import this package, but before doing it, use:
# import sys
# sys.setrecursionlimit(n)

from openapi_client.model.analytics import Analytics
from openapi_client.model.auth_input import AuthInput
from openapi_client.model.crawl_input import CrawlInput
from openapi_client.model.generic_input import GenericInput
from openapi_client.model.history import History
from openapi_client.model.issues import Issues
from openapi_client.model.issues_info import IssuesInfo
from openapi_client.model.page_issue import PageIssue
from openapi_client.model.page_load_time import PageLoadTime
from openapi_client.model.pages import Pages
from openapi_client.model.report import Report
from openapi_client.model.users import Users
from openapi_client.model.website_input import WebsiteInput
from openapi_client.model.websites import Websites
from openapi_client.model.websites_insight import WebsitesInsight
