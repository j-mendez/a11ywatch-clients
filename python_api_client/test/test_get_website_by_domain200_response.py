"""
    A11ywatch Client

    The web accessibility tool built for scale.  # noqa: E501

    The version of the OpenAPI document: 0.7.66
    Contact: support@a11ywatch.com
    Generated by: https://openapi-generator.tech
"""


import sys
import unittest

import openapi_client
from openapi_client.model.websites import Websites
globals()['Websites'] = Websites
from openapi_client.model.get_website_by_domain200_response import GetWebsiteByDomain200Response


class TestGetWebsiteByDomain200Response(unittest.TestCase):
    """GetWebsiteByDomain200Response unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testGetWebsiteByDomain200Response(self):
        """Test GetWebsiteByDomain200Response"""
        # FIXME: construct object with mandatory attributes with example values
        # model = GetWebsiteByDomain200Response()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
