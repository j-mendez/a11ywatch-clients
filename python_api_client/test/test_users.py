"""
    A11ywatch Client

    The web accessibility tool built for scale.  # noqa: E501

    The version of the OpenAPI document: 0.7.66
    Contact: support@a11ywatch.com
    Generated by: https://openapi-generator.tech
"""


import sys
import unittest

import openapi_client
from openapi_client.model.users import Users


class TestUsers(unittest.TestCase):
    """Users unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testUsers(self):
        """Test Users"""
        # FIXME: construct object with mandatory attributes with example values
        # model = Users()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
