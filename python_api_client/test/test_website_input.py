"""
    A11ywatch Client

    The web accessibility tool built for scale.  # noqa: E501

    The version of the OpenAPI document: 0.7.66
    Contact: support@a11ywatch.com
    Generated by: https://openapi-generator.tech
"""


import sys
import unittest

import openapi_client
from openapi_client.model.website_input import WebsiteInput


class TestWebsiteInput(unittest.TestCase):
    """WebsiteInput unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testWebsiteInput(self):
        """Test WebsiteInput"""
        # FIXME: construct object with mandatory attributes with example values
        # model = WebsiteInput()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
