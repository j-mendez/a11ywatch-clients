"""
    A11ywatch Client

    The web accessibility tool built for scale.  # noqa: E501

    The version of the OpenAPI document: 0.7.66
    Contact: support@a11ywatch.com
    Generated by: https://openapi-generator.tech
"""


import sys
import unittest

import openapi_client
from openapi_client.model.report import Report
globals()['Report'] = Report
from openapi_client.model.scan_website_simple200_response import ScanWebsiteSimple200Response


class TestScanWebsiteSimple200Response(unittest.TestCase):
    """ScanWebsiteSimple200Response unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testScanWebsiteSimple200Response(self):
        """Test ScanWebsiteSimple200Response"""
        # FIXME: construct object with mandatory attributes with example values
        # model = ScanWebsiteSimple200Response()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
