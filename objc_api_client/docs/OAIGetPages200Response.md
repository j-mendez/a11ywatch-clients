# OAIGetPages200Response

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**NSArray&lt;OAIPages&gt;***](OAIPages.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


