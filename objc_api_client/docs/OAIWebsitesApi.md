# OAIWebsitesApi

All URIs are relative to *https://api.a11ywatch.com/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**addWebsite**](OAIWebsitesApi.md#addwebsite) | **POST** /website | Add a website in the collection with form data
[**deleteWebsite**](OAIWebsitesApi.md#deletewebsite) | **DELETE** /website | Deletes a website
[**getWebsiteByDomain**](OAIWebsitesApi.md#getwebsitebydomain) | **GET** /website | Find website by Domain


# **addWebsite**
```objc
-(NSURLSessionTask*) addWebsiteWithWebsiteInput: (OAIWebsiteInput*) websiteInput
        completionHandler: (void (^)(NSObject* output, NSError* error)) handler;
```

Add a website in the collection with form data

### Example
```objc
OAIDefaultConfiguration *apiConfig = [OAIDefaultConfiguration sharedConfig];
// Configure HTTP basic authorization (authentication scheme: bearerAuth)
[apiConfig setUsername:@"YOUR_USERNAME"];
[apiConfig setPassword:@"YOUR_PASSWORD"];


OAIWebsiteInput* websiteInput = [[OAIWebsiteInput alloc] init]; // The website standard body

OAIWebsitesApi*apiInstance = [[OAIWebsitesApi alloc] init];

// Add a website in the collection with form data
[apiInstance addWebsiteWithWebsiteInput:websiteInput
          completionHandler: ^(NSObject* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling OAIWebsitesApi->addWebsite: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **websiteInput** | [**OAIWebsiteInput***](OAIWebsiteInput.md)| The website standard body | 

### Return type

**NSObject***

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **deleteWebsite**
```objc
-(NSURLSessionTask*) deleteWebsiteWithDomain: (NSString*) domain
        completionHandler: (void (^)(NSObject* output, NSError* error)) handler;
```

Deletes a website

### Example
```objc
OAIDefaultConfiguration *apiConfig = [OAIDefaultConfiguration sharedConfig];
// Configure HTTP basic authorization (authentication scheme: bearerAuth)
[apiConfig setUsername:@"YOUR_USERNAME"];
[apiConfig setPassword:@"YOUR_PASSWORD"];


NSString* domain = @"domain_example"; // Websites domain to delete

OAIWebsitesApi*apiInstance = [[OAIWebsitesApi alloc] init];

// Deletes a website
[apiInstance deleteWebsiteWithDomain:domain
          completionHandler: ^(NSObject* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling OAIWebsitesApi->deleteWebsite: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **domain** | **NSString***| Websites domain to delete | 

### Return type

**NSObject***

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getWebsiteByDomain**
```objc
-(NSURLSessionTask*) getWebsiteByDomainWithDomain: (NSString*) domain
        completionHandler: (void (^)(NSObject* output, NSError* error)) handler;
```

Find website by Domain

Returns a website when DOMAIN == website.domain.  Empty strings will simulate API error conditions

### Example
```objc
OAIDefaultConfiguration *apiConfig = [OAIDefaultConfiguration sharedConfig];
// Configure HTTP basic authorization (authentication scheme: bearerAuth)
[apiConfig setUsername:@"YOUR_USERNAME"];
[apiConfig setPassword:@"YOUR_PASSWORD"];


NSString* domain = @"domain_example"; // Domain of website that needs to be fetched

OAIWebsitesApi*apiInstance = [[OAIWebsitesApi alloc] init];

// Find website by Domain
[apiInstance getWebsiteByDomainWithDomain:domain
          completionHandler: ^(NSObject* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling OAIWebsitesApi->getWebsiteByDomain: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **domain** | **NSString***| Domain of website that needs to be fetched | 

### Return type

**NSObject***

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

