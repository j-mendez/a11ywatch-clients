# OAIReportsApi

All URIs are relative to *https://api.a11ywatch.com/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**crawlWebsiteStream**](OAIReportsApi.md#crawlwebsitestream) | **POST** /crawl | Multi-page crawl a website streaming issues on found
[**crawlWebsitesSync**](OAIReportsApi.md#crawlwebsitessync) | **POST** /websites-sync | Multi-page crawl all websites attached to account
[**getReport**](OAIReportsApi.md#getreport) | **GET** /report | Get the report from a previus scan
[**scanWebsite**](OAIReportsApi.md#scanwebsite) | **POST** /scan | Scan a website for issues
[**scanWebsiteSimple**](OAIReportsApi.md#scanwebsitesimple) | **POST** /scan-simple | Scan a website for issues without storing data and limited responses.


# **crawlWebsiteStream**
```objc
-(NSURLSessionTask*) crawlWebsiteStreamWithTransferEncoding: (NSString*) transferEncoding
    websiteInput: (OAIWebsiteInput*) websiteInput
        completionHandler: (void (^)(NSObject* output, NSError* error)) handler;
```

Multi-page crawl a website streaming issues on found

### Example
```objc
OAIDefaultConfiguration *apiConfig = [OAIDefaultConfiguration sharedConfig];
// Configure HTTP basic authorization (authentication scheme: bearerAuth)
[apiConfig setUsername:@"YOUR_USERNAME"];
[apiConfig setPassword:@"YOUR_PASSWORD"];


NSString* transferEncoding = @"Chunked"; //  (default to @"Chunked")
OAIWebsiteInput* websiteInput = [[OAIWebsiteInput alloc] init]; // The website standard body

OAIReportsApi*apiInstance = [[OAIReportsApi alloc] init];

// Multi-page crawl a website streaming issues on found
[apiInstance crawlWebsiteStreamWithTransferEncoding:transferEncoding
              websiteInput:websiteInput
          completionHandler: ^(NSObject* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling OAIReportsApi->crawlWebsiteStream: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **transferEncoding** | **NSString***|  | [default to @&quot;Chunked&quot;]
 **websiteInput** | [**OAIWebsiteInput***](OAIWebsiteInput.md)| The website standard body | 

### Return type

**NSObject***

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **crawlWebsitesSync**
```objc
-(NSURLSessionTask*) crawlWebsitesSyncWithCompletionHandler: 
        (void (^)(NSObject* output, NSError* error)) handler;
```

Multi-page crawl all websites attached to account

### Example
```objc
OAIDefaultConfiguration *apiConfig = [OAIDefaultConfiguration sharedConfig];
// Configure HTTP basic authorization (authentication scheme: bearerAuth)
[apiConfig setUsername:@"YOUR_USERNAME"];
[apiConfig setPassword:@"YOUR_PASSWORD"];



OAIReportsApi*apiInstance = [[OAIReportsApi alloc] init];

// Multi-page crawl all websites attached to account
[apiInstance crawlWebsitesSyncWithCompletionHandler: 
          ^(NSObject* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling OAIReportsApi->crawlWebsitesSync: %@", error);
                        }
                    }];
```

### Parameters
This endpoint does not need any parameter.

### Return type

**NSObject***

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getReport**
```objc
-(NSURLSessionTask*) getReportWithUrl: (NSString*) url
    domain: (NSString*) domain
        completionHandler: (void (^)(NSObject* output, NSError* error)) handler;
```

Get the report from a previus scan

### Example
```objc
OAIDefaultConfiguration *apiConfig = [OAIDefaultConfiguration sharedConfig];
// Configure HTTP basic authorization (authentication scheme: bearerAuth)
[apiConfig setUsername:@"YOUR_USERNAME"];
[apiConfig setPassword:@"YOUR_PASSWORD"];


NSString* url = @"url_example"; // The page url or domain for the report (optional)
NSString* domain = @"domain_example"; // Domain of website that needs to be fetched (optional)

OAIReportsApi*apiInstance = [[OAIReportsApi alloc] init];

// Get the report from a previus scan
[apiInstance getReportWithUrl:url
              domain:domain
          completionHandler: ^(NSObject* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling OAIReportsApi->getReport: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **url** | **NSString***| The page url or domain for the report | [optional] 
 **domain** | **NSString***| Domain of website that needs to be fetched | [optional] 

### Return type

**NSObject***

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **scanWebsite**
```objc
-(NSURLSessionTask*) scanWebsiteWithWebsiteInput: (OAIWebsiteInput*) websiteInput
        completionHandler: (void (^)(NSObject* output, NSError* error)) handler;
```

Scan a website for issues

### Example
```objc
OAIDefaultConfiguration *apiConfig = [OAIDefaultConfiguration sharedConfig];
// Configure HTTP basic authorization (authentication scheme: bearerAuth)
[apiConfig setUsername:@"YOUR_USERNAME"];
[apiConfig setPassword:@"YOUR_PASSWORD"];


OAIWebsiteInput* websiteInput = [[OAIWebsiteInput alloc] init]; // The website standard body

OAIReportsApi*apiInstance = [[OAIReportsApi alloc] init];

// Scan a website for issues
[apiInstance scanWebsiteWithWebsiteInput:websiteInput
          completionHandler: ^(NSObject* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling OAIReportsApi->scanWebsite: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **websiteInput** | [**OAIWebsiteInput***](OAIWebsiteInput.md)| The website standard body | 

### Return type

**NSObject***

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **scanWebsiteSimple**
```objc
-(NSURLSessionTask*) scanWebsiteSimpleWithWebsiteInput: (OAIWebsiteInput*) websiteInput
        completionHandler: (void (^)(NSObject* output, NSError* error)) handler;
```

Scan a website for issues without storing data and limited responses.

### Example
```objc
OAIDefaultConfiguration *apiConfig = [OAIDefaultConfiguration sharedConfig];
// Configure HTTP basic authorization (authentication scheme: bearerAuth)
[apiConfig setUsername:@"YOUR_USERNAME"];
[apiConfig setPassword:@"YOUR_PASSWORD"];


OAIWebsiteInput* websiteInput = [[OAIWebsiteInput alloc] init]; // The website standard body

OAIReportsApi*apiInstance = [[OAIReportsApi alloc] init];

// Scan a website for issues without storing data and limited responses.
[apiInstance scanWebsiteSimpleWithWebsiteInput:websiteInput
          completionHandler: ^(NSObject* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling OAIReportsApi->scanWebsiteSimple: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **websiteInput** | [**OAIWebsiteInput***](OAIWebsiteInput.md)| The website standard body | 

### Return type

**NSObject***

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

