# OAIPageLoadTime

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**duration** | **NSNumber*** |  | [optional] 
**durationFormated** | **NSString*** |  | [optional] 
**color** | **NSString*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


