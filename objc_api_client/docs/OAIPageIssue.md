# OAIPageIssue

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**recurrence** | **NSNumber*** |  | [optional] 
**typeCode** | **NSNumber*** |  | [optional] 
**code** | **NSString*** |  | [optional] 
**message** | **NSString*** |  | [optional] 
**context** | **NSString*** |  | [optional] 
**selector** | **NSString*** |  | [optional] 
**runner** | **NSString*** |  | [optional] 
**type** | **NSString*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


