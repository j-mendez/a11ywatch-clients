# OAIPages

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_id** | **NSNumber*** |  | [optional] 
**userId** | **NSNumber*** |  | [optional] 
**domain** | **NSString*** |  | [optional] 
**url** | **NSString*** |  | [optional] 
**cdnConnected** | **NSNumber*** |  | [optional] 
**online** | **NSNumber*** |  | [optional] 
**pageLoadTime** | [**OAIPageLoadTime***](OAIPageLoadTime.md) |  | [optional] 
**insight** | [**OAIWebsitesInsight***](OAIWebsitesInsight.md) |  | [optional] 
**issuesInfo** | [**OAIIssuesInfo***](OAIIssuesInfo.md) |  | [optional] 
**lastScanDate** | **NSString*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


