# OAIWebsites

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_id** | **NSNumber*** |  | [optional] 
**userId** | **NSNumber*** |  | [optional] 
**url** | **NSString*** |  | [optional] 
**domain** | **NSString*** |  | [optional] 
**crawlDuration** | **NSNumber*** |  | [optional] 
**cdnConnected** | **NSNumber*** |  | [optional] 
**pageInsights** | **NSNumber*** |  | [optional] 
**online** | **NSNumber*** |  | [optional] 
**mobile** | **NSNumber*** |  | [optional] 
**robots** | **NSNumber*** |  | [optional] 
**insight** | [**OAIWebsitesInsight***](OAIWebsitesInsight.md) |  | [optional] 
**pageHeaders** | **NSArray&lt;NSString*&gt;*** |  | [optional] 
**pageLoadTime** | [**OAIPageLoadTime***](OAIPageLoadTime.md) |  | [optional] 
**issuesInfo** | [**OAIIssuesInfo***](OAIIssuesInfo.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


