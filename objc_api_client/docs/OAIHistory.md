# OAIHistory

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_id** | **NSNumber*** |  | [optional] 
**domain** | **NSString*** |  | [optional] 
**insight** | [**OAIWebsitesInsight***](OAIWebsitesInsight.md) |  | [optional] 
**pageHeaders** | **NSArray&lt;NSString*&gt;*** |  | [optional] 
**issuesInfo** | [**OAIIssuesInfo***](OAIIssuesInfo.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


