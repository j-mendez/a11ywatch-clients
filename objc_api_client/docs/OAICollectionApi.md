# OAICollectionApi

All URIs are relative to *https://api.a11ywatch.com/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getAnalytics**](OAICollectionApi.md#getanalytics) | **GET** /list/analytics | Get the analytics for a website
[**getIssues**](OAICollectionApi.md#getissues) | **GET** /list/issue | List the issues for a website
[**getPageSpeed**](OAICollectionApi.md#getpagespeed) | **GET** /list/pagespeed | Get the pagespeed for a website
[**getPages**](OAICollectionApi.md#getpages) | **GET** /list/pages | List the pages in order for a website
[**getWebsites**](OAICollectionApi.md#getwebsites) | **GET** /list/website | Returns websites for the user in alphabetical order


# **getAnalytics**
```objc
-(NSURLSessionTask*) getAnalyticsWithOffset: (NSString*) offset
    domain: (NSString*) domain
        completionHandler: (void (^)(NSObject* output, NSError* error)) handler;
```

Get the analytics for a website

### Example
```objc
OAIDefaultConfiguration *apiConfig = [OAIDefaultConfiguration sharedConfig];
// Configure HTTP basic authorization (authentication scheme: bearerAuth)
[apiConfig setUsername:@"YOUR_USERNAME"];
[apiConfig setPassword:@"YOUR_PASSWORD"];


NSString* offset = @"offset_example"; // The page offset for the next set (optional)
NSString* domain = @"domain_example"; // Domain of website that needs to be fetched (optional)

OAICollectionApi*apiInstance = [[OAICollectionApi alloc] init];

// Get the analytics for a website
[apiInstance getAnalyticsWithOffset:offset
              domain:domain
          completionHandler: ^(NSObject* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling OAICollectionApi->getAnalytics: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **offset** | **NSString***| The page offset for the next set | [optional] 
 **domain** | **NSString***| Domain of website that needs to be fetched | [optional] 

### Return type

**NSObject***

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getIssues**
```objc
-(NSURLSessionTask*) getIssuesWithOffset: (NSString*) offset
    domain: (NSString*) domain
        completionHandler: (void (^)(NSObject* output, NSError* error)) handler;
```

List the issues for a website

### Example
```objc
OAIDefaultConfiguration *apiConfig = [OAIDefaultConfiguration sharedConfig];
// Configure HTTP basic authorization (authentication scheme: bearerAuth)
[apiConfig setUsername:@"YOUR_USERNAME"];
[apiConfig setPassword:@"YOUR_PASSWORD"];


NSString* offset = @"offset_example"; // The page offset for the next set (optional)
NSString* domain = @"domain_example"; // Domain of website that needs to be fetched (optional)

OAICollectionApi*apiInstance = [[OAICollectionApi alloc] init];

// List the issues for a website
[apiInstance getIssuesWithOffset:offset
              domain:domain
          completionHandler: ^(NSObject* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling OAICollectionApi->getIssues: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **offset** | **NSString***| The page offset for the next set | [optional] 
 **domain** | **NSString***| Domain of website that needs to be fetched | [optional] 

### Return type

**NSObject***

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getPageSpeed**
```objc
-(NSURLSessionTask*) getPageSpeedWithOffset: (NSString*) offset
    domain: (NSString*) domain
        completionHandler: (void (^)(NSObject* output, NSError* error)) handler;
```

Get the pagespeed for a website

### Example
```objc
OAIDefaultConfiguration *apiConfig = [OAIDefaultConfiguration sharedConfig];
// Configure HTTP basic authorization (authentication scheme: bearerAuth)
[apiConfig setUsername:@"YOUR_USERNAME"];
[apiConfig setPassword:@"YOUR_PASSWORD"];


NSString* offset = @"offset_example"; // The page offset for the next set (optional)
NSString* domain = @"domain_example"; // Domain of website that needs to be fetched (optional)

OAICollectionApi*apiInstance = [[OAICollectionApi alloc] init];

// Get the pagespeed for a website
[apiInstance getPageSpeedWithOffset:offset
              domain:domain
          completionHandler: ^(NSObject* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling OAICollectionApi->getPageSpeed: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **offset** | **NSString***| The page offset for the next set | [optional] 
 **domain** | **NSString***| Domain of website that needs to be fetched | [optional] 

### Return type

**NSObject***

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getPages**
```objc
-(NSURLSessionTask*) getPagesWithOffset: (NSString*) offset
    domain: (NSString*) domain
        completionHandler: (void (^)(NSObject* output, NSError* error)) handler;
```

List the pages in order for a website

### Example
```objc
OAIDefaultConfiguration *apiConfig = [OAIDefaultConfiguration sharedConfig];
// Configure HTTP basic authorization (authentication scheme: bearerAuth)
[apiConfig setUsername:@"YOUR_USERNAME"];
[apiConfig setPassword:@"YOUR_PASSWORD"];


NSString* offset = @"offset_example"; // The page offset for the next set (optional)
NSString* domain = @"domain_example"; // Domain of website that needs to be fetched (optional)

OAICollectionApi*apiInstance = [[OAICollectionApi alloc] init];

// List the pages in order for a website
[apiInstance getPagesWithOffset:offset
              domain:domain
          completionHandler: ^(NSObject* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling OAICollectionApi->getPages: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **offset** | **NSString***| The page offset for the next set | [optional] 
 **domain** | **NSString***| Domain of website that needs to be fetched | [optional] 

### Return type

**NSObject***

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getWebsites**
```objc
-(NSURLSessionTask*) getWebsitesWithOffset: (NSString*) offset
        completionHandler: (void (^)(NSObject* output, NSError* error)) handler;
```

Returns websites for the user in alphabetical order

Returns a map of websites

### Example
```objc
OAIDefaultConfiguration *apiConfig = [OAIDefaultConfiguration sharedConfig];
// Configure HTTP basic authorization (authentication scheme: bearerAuth)
[apiConfig setUsername:@"YOUR_USERNAME"];
[apiConfig setPassword:@"YOUR_PASSWORD"];


NSString* offset = @"offset_example"; // The page offset for the next set (optional)

OAICollectionApi*apiInstance = [[OAICollectionApi alloc] init];

// Returns websites for the user in alphabetical order
[apiInstance getWebsitesWithOffset:offset
          completionHandler: ^(NSObject* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling OAICollectionApi->getWebsites: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **offset** | **NSString***| The page offset for the next set | [optional] 

### Return type

**NSObject***

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

