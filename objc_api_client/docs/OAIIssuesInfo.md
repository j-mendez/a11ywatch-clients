# OAIIssuesInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**accessScoreAverage** | **NSNumber*** |  | [optional] 
**possibleIssuesFixedByCdn** | **NSNumber*** |  | [optional] 
**totalIssues** | **NSNumber*** |  | [optional] 
**issuesFixedByCdn** | **NSNumber*** |  | [optional] 
**errorCount** | **NSNumber*** |  | [optional] 
**warningCount** | **NSNumber*** |  | [optional] 
**noticeCount** | **NSNumber*** |  | [optional] 
**pageCount** | **NSNumber*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


