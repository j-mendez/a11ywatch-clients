# OAIUsers

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_id** | **NSNumber*** |  | [optional] 
**email** | **NSString*** |  | [optional] 
**jwt** | **NSString*** |  | [optional] 
**role** | **NSString*** |  | [optional] 
**password** | **NSString*** |  | [optional] 
**alertEnabled** | **NSNumber*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


