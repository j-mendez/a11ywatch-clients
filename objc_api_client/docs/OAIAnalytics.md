# OAIAnalytics

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_id** | **NSNumber*** |  | [optional] 
**domain** | **NSString*** |  | [optional] 
**pageUrl** | **NSString*** |  | [optional] 
**userId** | **NSNumber*** |  | [optional] 
**accessScore** | **NSNumber*** |  | [optional] 
**possibleIssuesFixedByCdn** | **NSNumber*** |  | [optional] 
**totalIssues** | **NSNumber*** |  | [optional] 
**issuesFixedByCdn** | **NSNumber*** |  | [optional] 
**errorCount** | **NSNumber*** |  | [optional] 
**warningCount** | **NSNumber*** |  | [optional] 
**noticeCount** | **NSNumber*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


