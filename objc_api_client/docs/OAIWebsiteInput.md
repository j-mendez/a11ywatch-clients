# OAIWebsiteInput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**url** | **NSString*** |  | [optional] 
**mobile** | **NSNumber*** |  | [optional] 
**pageInsights** | **NSNumber*** |  | [optional] 
**ua** | **NSString*** |  | [optional] 
**standard** | **NSString*** |  | [optional] 
**robots** | **NSString*** |  | [optional] 
**subdomains** | **NSNumber*** |  | [optional] 
**tld** | **NSNumber*** |  | [optional] 
**customHeaders** | **NSArray&lt;NSString*&gt;*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


