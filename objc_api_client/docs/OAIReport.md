# OAIReport

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_id** | **NSNumber*** |  | [optional] 
**domain** | **NSString*** |  | [optional] 
**pageUrl** | **NSString*** |  | [optional] 
**issues** | [**NSArray&lt;OAIPageIssue&gt;***](OAIPageIssue.md) |  | [optional] 
**issuesInfo** | [**OAIIssuesInfo***](OAIIssuesInfo.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


