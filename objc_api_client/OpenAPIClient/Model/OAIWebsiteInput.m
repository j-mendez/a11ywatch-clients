#import "OAIWebsiteInput.h"

@implementation OAIWebsiteInput

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"url": @"url", @"mobile": @"mobile", @"pageInsights": @"pageInsights", @"ua": @"ua", @"standard": @"standard", @"robots": @"robots", @"subdomains": @"subdomains", @"tld": @"tld", @"customHeaders": @"customHeaders" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"url", @"mobile", @"pageInsights", @"ua", @"standard", @"robots", @"subdomains", @"tld", @"customHeaders"];
  return [optionalProperties containsObject:propertyName];
}

@end
