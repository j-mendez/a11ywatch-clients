#import "OAIAnalytics.h"

@implementation OAIAnalytics

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"_id": @"_id", @"domain": @"domain", @"pageUrl": @"pageUrl", @"userId": @"userId", @"accessScore": @"accessScore", @"possibleIssuesFixedByCdn": @"possibleIssuesFixedByCdn", @"totalIssues": @"totalIssues", @"issuesFixedByCdn": @"issuesFixedByCdn", @"errorCount": @"errorCount", @"warningCount": @"warningCount", @"noticeCount": @"noticeCount" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"_id", @"domain", @"pageUrl", @"userId", @"accessScore", @"possibleIssuesFixedByCdn", @"totalIssues", @"issuesFixedByCdn", @"errorCount", @"warningCount", @"noticeCount"];
  return [optionalProperties containsObject:propertyName];
}

@end
