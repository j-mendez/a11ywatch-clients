#import "OAIWebsites.h"

@implementation OAIWebsites

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"_id": @"_id", @"userId": @"userId", @"url": @"url", @"domain": @"domain", @"crawlDuration": @"crawlDuration", @"cdnConnected": @"cdnConnected", @"pageInsights": @"pageInsights", @"online": @"online", @"mobile": @"mobile", @"robots": @"robots", @"insight": @"insight", @"pageHeaders": @"pageHeaders", @"pageLoadTime": @"pageLoadTime", @"issuesInfo": @"issuesInfo" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"_id", @"userId", @"url", @"domain", @"crawlDuration", @"cdnConnected", @"pageInsights", @"online", @"mobile", @"robots", @"insight", @"pageHeaders", @"pageLoadTime", @"issuesInfo"];
  return [optionalProperties containsObject:propertyName];
}

@end
