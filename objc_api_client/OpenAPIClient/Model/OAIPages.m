#import "OAIPages.h"

@implementation OAIPages

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"_id": @"_id", @"userId": @"userId", @"domain": @"domain", @"url": @"url", @"cdnConnected": @"cdnConnected", @"online": @"online", @"pageLoadTime": @"pageLoadTime", @"insight": @"insight", @"issuesInfo": @"issuesInfo", @"lastScanDate": @"lastScanDate" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"_id", @"userId", @"domain", @"url", @"cdnConnected", @"online", @"pageLoadTime", @"insight", @"issuesInfo", @"lastScanDate"];
  return [optionalProperties containsObject:propertyName];
}

@end
