#import <Foundation/Foundation.h>
#import "OAIObject.h"

/**
* A11ywatch Client
* The web accessibility tool built for scale.
*
* The version of the OpenAPI document: 0.7.66
* Contact: support@a11ywatch.com
*
* NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
* https://openapi-generator.tech
* Do not edit the class manually.
*/


#import "OAIAnalytics.h"
@protocol OAIAnalytics;
@class OAIAnalytics;



@protocol OAIGetAnalytics200Response
@end

@interface OAIGetAnalytics200Response : OAIObject


@property(nonatomic) NSArray<OAIAnalytics>* data;

@end
