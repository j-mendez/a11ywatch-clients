#import "OAIPageIssue.h"

@implementation OAIPageIssue

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"recurrence": @"recurrence", @"typeCode": @"typeCode", @"code": @"code", @"message": @"message", @"context": @"context", @"selector": @"selector", @"runner": @"runner", @"type": @"type" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"recurrence", @"typeCode", @"code", @"message", @"context", @"selector", @"runner", @"type"];
  return [optionalProperties containsObject:propertyName];
}

@end
