# WebsiteInput

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Url** | Pointer to **string** |  | [optional] 
**Mobile** | Pointer to **bool** |  | [optional] 
**PageInsights** | Pointer to **bool** |  | [optional] 
**Ua** | Pointer to **string** |  | [optional] 
**Standard** | Pointer to **string** |  | [optional] 
**Robots** | Pointer to **string** |  | [optional] 
**Subdomains** | Pointer to **bool** |  | [optional] 
**Tld** | Pointer to **bool** |  | [optional] 
**CustomHeaders** | Pointer to **[]string** |  | [optional] 

## Methods

### NewWebsiteInput

`func NewWebsiteInput() *WebsiteInput`

NewWebsiteInput instantiates a new WebsiteInput object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewWebsiteInputWithDefaults

`func NewWebsiteInputWithDefaults() *WebsiteInput`

NewWebsiteInputWithDefaults instantiates a new WebsiteInput object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetUrl

`func (o *WebsiteInput) GetUrl() string`

GetUrl returns the Url field if non-nil, zero value otherwise.

### GetUrlOk

`func (o *WebsiteInput) GetUrlOk() (*string, bool)`

GetUrlOk returns a tuple with the Url field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUrl

`func (o *WebsiteInput) SetUrl(v string)`

SetUrl sets Url field to given value.

### HasUrl

`func (o *WebsiteInput) HasUrl() bool`

HasUrl returns a boolean if a field has been set.

### GetMobile

`func (o *WebsiteInput) GetMobile() bool`

GetMobile returns the Mobile field if non-nil, zero value otherwise.

### GetMobileOk

`func (o *WebsiteInput) GetMobileOk() (*bool, bool)`

GetMobileOk returns a tuple with the Mobile field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMobile

`func (o *WebsiteInput) SetMobile(v bool)`

SetMobile sets Mobile field to given value.

### HasMobile

`func (o *WebsiteInput) HasMobile() bool`

HasMobile returns a boolean if a field has been set.

### GetPageInsights

`func (o *WebsiteInput) GetPageInsights() bool`

GetPageInsights returns the PageInsights field if non-nil, zero value otherwise.

### GetPageInsightsOk

`func (o *WebsiteInput) GetPageInsightsOk() (*bool, bool)`

GetPageInsightsOk returns a tuple with the PageInsights field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPageInsights

`func (o *WebsiteInput) SetPageInsights(v bool)`

SetPageInsights sets PageInsights field to given value.

### HasPageInsights

`func (o *WebsiteInput) HasPageInsights() bool`

HasPageInsights returns a boolean if a field has been set.

### GetUa

`func (o *WebsiteInput) GetUa() string`

GetUa returns the Ua field if non-nil, zero value otherwise.

### GetUaOk

`func (o *WebsiteInput) GetUaOk() (*string, bool)`

GetUaOk returns a tuple with the Ua field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUa

`func (o *WebsiteInput) SetUa(v string)`

SetUa sets Ua field to given value.

### HasUa

`func (o *WebsiteInput) HasUa() bool`

HasUa returns a boolean if a field has been set.

### GetStandard

`func (o *WebsiteInput) GetStandard() string`

GetStandard returns the Standard field if non-nil, zero value otherwise.

### GetStandardOk

`func (o *WebsiteInput) GetStandardOk() (*string, bool)`

GetStandardOk returns a tuple with the Standard field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStandard

`func (o *WebsiteInput) SetStandard(v string)`

SetStandard sets Standard field to given value.

### HasStandard

`func (o *WebsiteInput) HasStandard() bool`

HasStandard returns a boolean if a field has been set.

### GetRobots

`func (o *WebsiteInput) GetRobots() string`

GetRobots returns the Robots field if non-nil, zero value otherwise.

### GetRobotsOk

`func (o *WebsiteInput) GetRobotsOk() (*string, bool)`

GetRobotsOk returns a tuple with the Robots field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRobots

`func (o *WebsiteInput) SetRobots(v string)`

SetRobots sets Robots field to given value.

### HasRobots

`func (o *WebsiteInput) HasRobots() bool`

HasRobots returns a boolean if a field has been set.

### GetSubdomains

`func (o *WebsiteInput) GetSubdomains() bool`

GetSubdomains returns the Subdomains field if non-nil, zero value otherwise.

### GetSubdomainsOk

`func (o *WebsiteInput) GetSubdomainsOk() (*bool, bool)`

GetSubdomainsOk returns a tuple with the Subdomains field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSubdomains

`func (o *WebsiteInput) SetSubdomains(v bool)`

SetSubdomains sets Subdomains field to given value.

### HasSubdomains

`func (o *WebsiteInput) HasSubdomains() bool`

HasSubdomains returns a boolean if a field has been set.

### GetTld

`func (o *WebsiteInput) GetTld() bool`

GetTld returns the Tld field if non-nil, zero value otherwise.

### GetTldOk

`func (o *WebsiteInput) GetTldOk() (*bool, bool)`

GetTldOk returns a tuple with the Tld field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTld

`func (o *WebsiteInput) SetTld(v bool)`

SetTld sets Tld field to given value.

### HasTld

`func (o *WebsiteInput) HasTld() bool`

HasTld returns a boolean if a field has been set.

### GetCustomHeaders

`func (o *WebsiteInput) GetCustomHeaders() []string`

GetCustomHeaders returns the CustomHeaders field if non-nil, zero value otherwise.

### GetCustomHeadersOk

`func (o *WebsiteInput) GetCustomHeadersOk() (*[]string, bool)`

GetCustomHeadersOk returns a tuple with the CustomHeaders field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCustomHeaders

`func (o *WebsiteInput) SetCustomHeaders(v []string)`

SetCustomHeaders sets CustomHeaders field to given value.

### HasCustomHeaders

`func (o *WebsiteInput) HasCustomHeaders() bool`

HasCustomHeaders returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


