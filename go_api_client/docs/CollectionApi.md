# \CollectionApi

All URIs are relative to *https://api.a11ywatch.com/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**GetAnalytics**](CollectionApi.md#GetAnalytics) | **Get** /list/analytics | Get the analytics for a website
[**GetIssues**](CollectionApi.md#GetIssues) | **Get** /list/issue | List the issues for a website
[**GetPageSpeed**](CollectionApi.md#GetPageSpeed) | **Get** /list/pagespeed | Get the pagespeed for a website
[**GetPages**](CollectionApi.md#GetPages) | **Get** /list/pages | List the pages in order for a website
[**GetWebsites**](CollectionApi.md#GetWebsites) | **Get** /list/website | Returns websites for the user in alphabetical order



## GetAnalytics

> interface{} GetAnalytics(ctx).Offset(offset).Domain(domain).Execute()

Get the analytics for a website

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    offset := "offset_example" // string | The page offset for the next set (optional)
    domain := "domain_example" // string | Domain of website that needs to be fetched (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.CollectionApi.GetAnalytics(context.Background()).Offset(offset).Domain(domain).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `CollectionApi.GetAnalytics``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetAnalytics`: interface{}
    fmt.Fprintf(os.Stdout, "Response from `CollectionApi.GetAnalytics`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiGetAnalyticsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **offset** | **string** | The page offset for the next set | 
 **domain** | **string** | Domain of website that needs to be fetched | 

### Return type

**interface{}**

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetIssues

> interface{} GetIssues(ctx).Offset(offset).Domain(domain).Execute()

List the issues for a website

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    offset := "offset_example" // string | The page offset for the next set (optional)
    domain := "domain_example" // string | Domain of website that needs to be fetched (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.CollectionApi.GetIssues(context.Background()).Offset(offset).Domain(domain).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `CollectionApi.GetIssues``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetIssues`: interface{}
    fmt.Fprintf(os.Stdout, "Response from `CollectionApi.GetIssues`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiGetIssuesRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **offset** | **string** | The page offset for the next set | 
 **domain** | **string** | Domain of website that needs to be fetched | 

### Return type

**interface{}**

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetPageSpeed

> interface{} GetPageSpeed(ctx).Offset(offset).Domain(domain).Execute()

Get the pagespeed for a website

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    offset := "offset_example" // string | The page offset for the next set (optional)
    domain := "domain_example" // string | Domain of website that needs to be fetched (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.CollectionApi.GetPageSpeed(context.Background()).Offset(offset).Domain(domain).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `CollectionApi.GetPageSpeed``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetPageSpeed`: interface{}
    fmt.Fprintf(os.Stdout, "Response from `CollectionApi.GetPageSpeed`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiGetPageSpeedRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **offset** | **string** | The page offset for the next set | 
 **domain** | **string** | Domain of website that needs to be fetched | 

### Return type

**interface{}**

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetPages

> interface{} GetPages(ctx).Offset(offset).Domain(domain).Execute()

List the pages in order for a website

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    offset := "offset_example" // string | The page offset for the next set (optional)
    domain := "domain_example" // string | Domain of website that needs to be fetched (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.CollectionApi.GetPages(context.Background()).Offset(offset).Domain(domain).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `CollectionApi.GetPages``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetPages`: interface{}
    fmt.Fprintf(os.Stdout, "Response from `CollectionApi.GetPages`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiGetPagesRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **offset** | **string** | The page offset for the next set | 
 **domain** | **string** | Domain of website that needs to be fetched | 

### Return type

**interface{}**

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetWebsites

> interface{} GetWebsites(ctx).Offset(offset).Execute()

Returns websites for the user in alphabetical order



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    offset := "offset_example" // string | The page offset for the next set (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.CollectionApi.GetWebsites(context.Background()).Offset(offset).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `CollectionApi.GetWebsites``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetWebsites`: interface{}
    fmt.Fprintf(os.Stdout, "Response from `CollectionApi.GetWebsites`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiGetWebsitesRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **offset** | **string** | The page offset for the next set | 

### Return type

**interface{}**

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

