# CrawlWebsitesSync200Response

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Data** | Pointer to **bool** |  | [optional] 
**Message** | Pointer to **string** |  | [optional] 

## Methods

### NewCrawlWebsitesSync200Response

`func NewCrawlWebsitesSync200Response() *CrawlWebsitesSync200Response`

NewCrawlWebsitesSync200Response instantiates a new CrawlWebsitesSync200Response object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCrawlWebsitesSync200ResponseWithDefaults

`func NewCrawlWebsitesSync200ResponseWithDefaults() *CrawlWebsitesSync200Response`

NewCrawlWebsitesSync200ResponseWithDefaults instantiates a new CrawlWebsitesSync200Response object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetData

`func (o *CrawlWebsitesSync200Response) GetData() bool`

GetData returns the Data field if non-nil, zero value otherwise.

### GetDataOk

`func (o *CrawlWebsitesSync200Response) GetDataOk() (*bool, bool)`

GetDataOk returns a tuple with the Data field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetData

`func (o *CrawlWebsitesSync200Response) SetData(v bool)`

SetData sets Data field to given value.

### HasData

`func (o *CrawlWebsitesSync200Response) HasData() bool`

HasData returns a boolean if a field has been set.

### GetMessage

`func (o *CrawlWebsitesSync200Response) GetMessage() string`

GetMessage returns the Message field if non-nil, zero value otherwise.

### GetMessageOk

`func (o *CrawlWebsitesSync200Response) GetMessageOk() (*string, bool)`

GetMessageOk returns a tuple with the Message field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMessage

`func (o *CrawlWebsitesSync200Response) SetMessage(v string)`

SetMessage sets Message field to given value.

### HasMessage

`func (o *CrawlWebsitesSync200Response) HasMessage() bool`

HasMessage returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


