# GetWebsiteByDomain200Response

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Data** | Pointer to [**Websites**](Websites.md) |  | [optional] 

## Methods

### NewGetWebsiteByDomain200Response

`func NewGetWebsiteByDomain200Response() *GetWebsiteByDomain200Response`

NewGetWebsiteByDomain200Response instantiates a new GetWebsiteByDomain200Response object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewGetWebsiteByDomain200ResponseWithDefaults

`func NewGetWebsiteByDomain200ResponseWithDefaults() *GetWebsiteByDomain200Response`

NewGetWebsiteByDomain200ResponseWithDefaults instantiates a new GetWebsiteByDomain200Response object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetData

`func (o *GetWebsiteByDomain200Response) GetData() Websites`

GetData returns the Data field if non-nil, zero value otherwise.

### GetDataOk

`func (o *GetWebsiteByDomain200Response) GetDataOk() (*Websites, bool)`

GetDataOk returns a tuple with the Data field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetData

`func (o *GetWebsiteByDomain200Response) SetData(v Websites)`

SetData sets Data field to given value.

### HasData

`func (o *GetWebsiteByDomain200Response) HasData() bool`

HasData returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


