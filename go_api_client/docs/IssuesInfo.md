# IssuesInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**AccessScoreAverage** | Pointer to **int64** |  | [optional] 
**PossibleIssuesFixedByCdn** | Pointer to **int64** |  | [optional] 
**TotalIssues** | Pointer to **int64** |  | [optional] 
**IssuesFixedByCdn** | Pointer to **int64** |  | [optional] 
**ErrorCount** | Pointer to **int64** |  | [optional] 
**WarningCount** | Pointer to **int64** |  | [optional] 
**NoticeCount** | Pointer to **int64** |  | [optional] 
**PageCount** | Pointer to **int64** |  | [optional] 

## Methods

### NewIssuesInfo

`func NewIssuesInfo() *IssuesInfo`

NewIssuesInfo instantiates a new IssuesInfo object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewIssuesInfoWithDefaults

`func NewIssuesInfoWithDefaults() *IssuesInfo`

NewIssuesInfoWithDefaults instantiates a new IssuesInfo object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetAccessScoreAverage

`func (o *IssuesInfo) GetAccessScoreAverage() int64`

GetAccessScoreAverage returns the AccessScoreAverage field if non-nil, zero value otherwise.

### GetAccessScoreAverageOk

`func (o *IssuesInfo) GetAccessScoreAverageOk() (*int64, bool)`

GetAccessScoreAverageOk returns a tuple with the AccessScoreAverage field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAccessScoreAverage

`func (o *IssuesInfo) SetAccessScoreAverage(v int64)`

SetAccessScoreAverage sets AccessScoreAverage field to given value.

### HasAccessScoreAverage

`func (o *IssuesInfo) HasAccessScoreAverage() bool`

HasAccessScoreAverage returns a boolean if a field has been set.

### GetPossibleIssuesFixedByCdn

`func (o *IssuesInfo) GetPossibleIssuesFixedByCdn() int64`

GetPossibleIssuesFixedByCdn returns the PossibleIssuesFixedByCdn field if non-nil, zero value otherwise.

### GetPossibleIssuesFixedByCdnOk

`func (o *IssuesInfo) GetPossibleIssuesFixedByCdnOk() (*int64, bool)`

GetPossibleIssuesFixedByCdnOk returns a tuple with the PossibleIssuesFixedByCdn field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPossibleIssuesFixedByCdn

`func (o *IssuesInfo) SetPossibleIssuesFixedByCdn(v int64)`

SetPossibleIssuesFixedByCdn sets PossibleIssuesFixedByCdn field to given value.

### HasPossibleIssuesFixedByCdn

`func (o *IssuesInfo) HasPossibleIssuesFixedByCdn() bool`

HasPossibleIssuesFixedByCdn returns a boolean if a field has been set.

### GetTotalIssues

`func (o *IssuesInfo) GetTotalIssues() int64`

GetTotalIssues returns the TotalIssues field if non-nil, zero value otherwise.

### GetTotalIssuesOk

`func (o *IssuesInfo) GetTotalIssuesOk() (*int64, bool)`

GetTotalIssuesOk returns a tuple with the TotalIssues field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTotalIssues

`func (o *IssuesInfo) SetTotalIssues(v int64)`

SetTotalIssues sets TotalIssues field to given value.

### HasTotalIssues

`func (o *IssuesInfo) HasTotalIssues() bool`

HasTotalIssues returns a boolean if a field has been set.

### GetIssuesFixedByCdn

`func (o *IssuesInfo) GetIssuesFixedByCdn() int64`

GetIssuesFixedByCdn returns the IssuesFixedByCdn field if non-nil, zero value otherwise.

### GetIssuesFixedByCdnOk

`func (o *IssuesInfo) GetIssuesFixedByCdnOk() (*int64, bool)`

GetIssuesFixedByCdnOk returns a tuple with the IssuesFixedByCdn field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetIssuesFixedByCdn

`func (o *IssuesInfo) SetIssuesFixedByCdn(v int64)`

SetIssuesFixedByCdn sets IssuesFixedByCdn field to given value.

### HasIssuesFixedByCdn

`func (o *IssuesInfo) HasIssuesFixedByCdn() bool`

HasIssuesFixedByCdn returns a boolean if a field has been set.

### GetErrorCount

`func (o *IssuesInfo) GetErrorCount() int64`

GetErrorCount returns the ErrorCount field if non-nil, zero value otherwise.

### GetErrorCountOk

`func (o *IssuesInfo) GetErrorCountOk() (*int64, bool)`

GetErrorCountOk returns a tuple with the ErrorCount field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetErrorCount

`func (o *IssuesInfo) SetErrorCount(v int64)`

SetErrorCount sets ErrorCount field to given value.

### HasErrorCount

`func (o *IssuesInfo) HasErrorCount() bool`

HasErrorCount returns a boolean if a field has been set.

### GetWarningCount

`func (o *IssuesInfo) GetWarningCount() int64`

GetWarningCount returns the WarningCount field if non-nil, zero value otherwise.

### GetWarningCountOk

`func (o *IssuesInfo) GetWarningCountOk() (*int64, bool)`

GetWarningCountOk returns a tuple with the WarningCount field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetWarningCount

`func (o *IssuesInfo) SetWarningCount(v int64)`

SetWarningCount sets WarningCount field to given value.

### HasWarningCount

`func (o *IssuesInfo) HasWarningCount() bool`

HasWarningCount returns a boolean if a field has been set.

### GetNoticeCount

`func (o *IssuesInfo) GetNoticeCount() int64`

GetNoticeCount returns the NoticeCount field if non-nil, zero value otherwise.

### GetNoticeCountOk

`func (o *IssuesInfo) GetNoticeCountOk() (*int64, bool)`

GetNoticeCountOk returns a tuple with the NoticeCount field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNoticeCount

`func (o *IssuesInfo) SetNoticeCount(v int64)`

SetNoticeCount sets NoticeCount field to given value.

### HasNoticeCount

`func (o *IssuesInfo) HasNoticeCount() bool`

HasNoticeCount returns a boolean if a field has been set.

### GetPageCount

`func (o *IssuesInfo) GetPageCount() int64`

GetPageCount returns the PageCount field if non-nil, zero value otherwise.

### GetPageCountOk

`func (o *IssuesInfo) GetPageCountOk() (*int64, bool)`

GetPageCountOk returns a tuple with the PageCount field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPageCount

`func (o *IssuesInfo) SetPageCount(v int64)`

SetPageCount sets PageCount field to given value.

### HasPageCount

`func (o *IssuesInfo) HasPageCount() bool`

HasPageCount returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


