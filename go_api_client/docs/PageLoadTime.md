# PageLoadTime

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Duration** | Pointer to **float32** |  | [optional] 
**DurationFormated** | Pointer to **string** |  | [optional] 
**Color** | Pointer to **string** |  | [optional] 

## Methods

### NewPageLoadTime

`func NewPageLoadTime() *PageLoadTime`

NewPageLoadTime instantiates a new PageLoadTime object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewPageLoadTimeWithDefaults

`func NewPageLoadTimeWithDefaults() *PageLoadTime`

NewPageLoadTimeWithDefaults instantiates a new PageLoadTime object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetDuration

`func (o *PageLoadTime) GetDuration() float32`

GetDuration returns the Duration field if non-nil, zero value otherwise.

### GetDurationOk

`func (o *PageLoadTime) GetDurationOk() (*float32, bool)`

GetDurationOk returns a tuple with the Duration field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDuration

`func (o *PageLoadTime) SetDuration(v float32)`

SetDuration sets Duration field to given value.

### HasDuration

`func (o *PageLoadTime) HasDuration() bool`

HasDuration returns a boolean if a field has been set.

### GetDurationFormated

`func (o *PageLoadTime) GetDurationFormated() string`

GetDurationFormated returns the DurationFormated field if non-nil, zero value otherwise.

### GetDurationFormatedOk

`func (o *PageLoadTime) GetDurationFormatedOk() (*string, bool)`

GetDurationFormatedOk returns a tuple with the DurationFormated field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDurationFormated

`func (o *PageLoadTime) SetDurationFormated(v string)`

SetDurationFormated sets DurationFormated field to given value.

### HasDurationFormated

`func (o *PageLoadTime) HasDurationFormated() bool`

HasDurationFormated returns a boolean if a field has been set.

### GetColor

`func (o *PageLoadTime) GetColor() string`

GetColor returns the Color field if non-nil, zero value otherwise.

### GetColorOk

`func (o *PageLoadTime) GetColorOk() (*string, bool)`

GetColorOk returns a tuple with the Color field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetColor

`func (o *PageLoadTime) SetColor(v string)`

SetColor sets Color field to given value.

### HasColor

`func (o *PageLoadTime) HasColor() bool`

HasColor returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


