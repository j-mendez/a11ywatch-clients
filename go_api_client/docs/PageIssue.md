# PageIssue

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Recurrence** | Pointer to **int64** |  | [optional] 
**TypeCode** | Pointer to **int64** |  | [optional] 
**Code** | Pointer to **string** |  | [optional] 
**Message** | Pointer to **string** |  | [optional] 
**Context** | Pointer to **string** |  | [optional] 
**Selector** | Pointer to **string** |  | [optional] 
**Runner** | Pointer to **string** |  | [optional] 
**Type** | Pointer to **string** |  | [optional] 

## Methods

### NewPageIssue

`func NewPageIssue() *PageIssue`

NewPageIssue instantiates a new PageIssue object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewPageIssueWithDefaults

`func NewPageIssueWithDefaults() *PageIssue`

NewPageIssueWithDefaults instantiates a new PageIssue object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetRecurrence

`func (o *PageIssue) GetRecurrence() int64`

GetRecurrence returns the Recurrence field if non-nil, zero value otherwise.

### GetRecurrenceOk

`func (o *PageIssue) GetRecurrenceOk() (*int64, bool)`

GetRecurrenceOk returns a tuple with the Recurrence field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRecurrence

`func (o *PageIssue) SetRecurrence(v int64)`

SetRecurrence sets Recurrence field to given value.

### HasRecurrence

`func (o *PageIssue) HasRecurrence() bool`

HasRecurrence returns a boolean if a field has been set.

### GetTypeCode

`func (o *PageIssue) GetTypeCode() int64`

GetTypeCode returns the TypeCode field if non-nil, zero value otherwise.

### GetTypeCodeOk

`func (o *PageIssue) GetTypeCodeOk() (*int64, bool)`

GetTypeCodeOk returns a tuple with the TypeCode field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTypeCode

`func (o *PageIssue) SetTypeCode(v int64)`

SetTypeCode sets TypeCode field to given value.

### HasTypeCode

`func (o *PageIssue) HasTypeCode() bool`

HasTypeCode returns a boolean if a field has been set.

### GetCode

`func (o *PageIssue) GetCode() string`

GetCode returns the Code field if non-nil, zero value otherwise.

### GetCodeOk

`func (o *PageIssue) GetCodeOk() (*string, bool)`

GetCodeOk returns a tuple with the Code field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCode

`func (o *PageIssue) SetCode(v string)`

SetCode sets Code field to given value.

### HasCode

`func (o *PageIssue) HasCode() bool`

HasCode returns a boolean if a field has been set.

### GetMessage

`func (o *PageIssue) GetMessage() string`

GetMessage returns the Message field if non-nil, zero value otherwise.

### GetMessageOk

`func (o *PageIssue) GetMessageOk() (*string, bool)`

GetMessageOk returns a tuple with the Message field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMessage

`func (o *PageIssue) SetMessage(v string)`

SetMessage sets Message field to given value.

### HasMessage

`func (o *PageIssue) HasMessage() bool`

HasMessage returns a boolean if a field has been set.

### GetContext

`func (o *PageIssue) GetContext() string`

GetContext returns the Context field if non-nil, zero value otherwise.

### GetContextOk

`func (o *PageIssue) GetContextOk() (*string, bool)`

GetContextOk returns a tuple with the Context field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetContext

`func (o *PageIssue) SetContext(v string)`

SetContext sets Context field to given value.

### HasContext

`func (o *PageIssue) HasContext() bool`

HasContext returns a boolean if a field has been set.

### GetSelector

`func (o *PageIssue) GetSelector() string`

GetSelector returns the Selector field if non-nil, zero value otherwise.

### GetSelectorOk

`func (o *PageIssue) GetSelectorOk() (*string, bool)`

GetSelectorOk returns a tuple with the Selector field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSelector

`func (o *PageIssue) SetSelector(v string)`

SetSelector sets Selector field to given value.

### HasSelector

`func (o *PageIssue) HasSelector() bool`

HasSelector returns a boolean if a field has been set.

### GetRunner

`func (o *PageIssue) GetRunner() string`

GetRunner returns the Runner field if non-nil, zero value otherwise.

### GetRunnerOk

`func (o *PageIssue) GetRunnerOk() (*string, bool)`

GetRunnerOk returns a tuple with the Runner field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRunner

`func (o *PageIssue) SetRunner(v string)`

SetRunner sets Runner field to given value.

### HasRunner

`func (o *PageIssue) HasRunner() bool`

HasRunner returns a boolean if a field has been set.

### GetType

`func (o *PageIssue) GetType() string`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *PageIssue) GetTypeOk() (*string, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *PageIssue) SetType(v string)`

SetType sets Type field to given value.

### HasType

`func (o *PageIssue) HasType() bool`

HasType returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


