# GetPages200Response

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Data** | Pointer to [**[]Pages**](Pages.md) |  | [optional] 

## Methods

### NewGetPages200Response

`func NewGetPages200Response() *GetPages200Response`

NewGetPages200Response instantiates a new GetPages200Response object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewGetPages200ResponseWithDefaults

`func NewGetPages200ResponseWithDefaults() *GetPages200Response`

NewGetPages200ResponseWithDefaults instantiates a new GetPages200Response object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetData

`func (o *GetPages200Response) GetData() []Pages`

GetData returns the Data field if non-nil, zero value otherwise.

### GetDataOk

`func (o *GetPages200Response) GetDataOk() (*[]Pages, bool)`

GetDataOk returns a tuple with the Data field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetData

`func (o *GetPages200Response) SetData(v []Pages)`

SetData sets Data field to given value.

### HasData

`func (o *GetPages200Response) HasData() bool`

HasData returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


