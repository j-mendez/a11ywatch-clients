# \ReportsApi

All URIs are relative to *https://api.a11ywatch.com/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**CrawlWebsiteStream**](ReportsApi.md#CrawlWebsiteStream) | **Post** /crawl | Multi-page crawl a website streaming issues on found
[**CrawlWebsitesSync**](ReportsApi.md#CrawlWebsitesSync) | **Post** /websites-sync | Multi-page crawl all websites attached to account
[**GetReport**](ReportsApi.md#GetReport) | **Get** /report | Get the report from a previus scan
[**ScanWebsite**](ReportsApi.md#ScanWebsite) | **Post** /scan | Scan a website for issues
[**ScanWebsiteSimple**](ReportsApi.md#ScanWebsiteSimple) | **Post** /scan-simple | Scan a website for issues without storing data and limited responses.



## CrawlWebsiteStream

> interface{} CrawlWebsiteStream(ctx).TransferEncoding(transferEncoding).WebsiteInput(websiteInput).Execute()

Multi-page crawl a website streaming issues on found

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    transferEncoding := "transferEncoding_example" // string |  (default to "Chunked")
    websiteInput := *openapiclient.NewWebsiteInput() // WebsiteInput | The website standard body

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.ReportsApi.CrawlWebsiteStream(context.Background()).TransferEncoding(transferEncoding).WebsiteInput(websiteInput).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ReportsApi.CrawlWebsiteStream``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `CrawlWebsiteStream`: interface{}
    fmt.Fprintf(os.Stdout, "Response from `ReportsApi.CrawlWebsiteStream`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiCrawlWebsiteStreamRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **transferEncoding** | **string** |  | [default to &quot;Chunked&quot;]
 **websiteInput** | [**WebsiteInput**](WebsiteInput.md) | The website standard body | 

### Return type

**interface{}**

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## CrawlWebsitesSync

> interface{} CrawlWebsitesSync(ctx).Execute()

Multi-page crawl all websites attached to account

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.ReportsApi.CrawlWebsitesSync(context.Background()).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ReportsApi.CrawlWebsitesSync``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `CrawlWebsitesSync`: interface{}
    fmt.Fprintf(os.Stdout, "Response from `ReportsApi.CrawlWebsitesSync`: %v\n", resp)
}
```

### Path Parameters

This endpoint does not need any parameter.

### Other Parameters

Other parameters are passed through a pointer to a apiCrawlWebsitesSyncRequest struct via the builder pattern


### Return type

**interface{}**

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetReport

> interface{} GetReport(ctx).Url(url).Domain(domain).Execute()

Get the report from a previus scan

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    url := "url_example" // string | The page url or domain for the report (optional)
    domain := "domain_example" // string | Domain of website that needs to be fetched (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.ReportsApi.GetReport(context.Background()).Url(url).Domain(domain).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ReportsApi.GetReport``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetReport`: interface{}
    fmt.Fprintf(os.Stdout, "Response from `ReportsApi.GetReport`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiGetReportRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **url** | **string** | The page url or domain for the report | 
 **domain** | **string** | Domain of website that needs to be fetched | 

### Return type

**interface{}**

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ScanWebsite

> interface{} ScanWebsite(ctx).WebsiteInput(websiteInput).Execute()

Scan a website for issues

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    websiteInput := *openapiclient.NewWebsiteInput() // WebsiteInput | The website standard body

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.ReportsApi.ScanWebsite(context.Background()).WebsiteInput(websiteInput).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ReportsApi.ScanWebsite``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `ScanWebsite`: interface{}
    fmt.Fprintf(os.Stdout, "Response from `ReportsApi.ScanWebsite`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiScanWebsiteRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **websiteInput** | [**WebsiteInput**](WebsiteInput.md) | The website standard body | 

### Return type

**interface{}**

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ScanWebsiteSimple

> interface{} ScanWebsiteSimple(ctx).WebsiteInput(websiteInput).Execute()

Scan a website for issues without storing data and limited responses.

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    websiteInput := *openapiclient.NewWebsiteInput() // WebsiteInput | The website standard body

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.ReportsApi.ScanWebsiteSimple(context.Background()).WebsiteInput(websiteInput).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ReportsApi.ScanWebsiteSimple``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `ScanWebsiteSimple`: interface{}
    fmt.Fprintf(os.Stdout, "Response from `ReportsApi.ScanWebsiteSimple`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiScanWebsiteSimpleRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **websiteInput** | [**WebsiteInput**](WebsiteInput.md) | The website standard body | 

### Return type

**interface{}**

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

