# CrawlInput

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Domain** | Pointer to **string** |  | [optional] 
**Url** | Pointer to **string** |  | [optional] 

## Methods

### NewCrawlInput

`func NewCrawlInput() *CrawlInput`

NewCrawlInput instantiates a new CrawlInput object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCrawlInputWithDefaults

`func NewCrawlInputWithDefaults() *CrawlInput`

NewCrawlInputWithDefaults instantiates a new CrawlInput object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetDomain

`func (o *CrawlInput) GetDomain() string`

GetDomain returns the Domain field if non-nil, zero value otherwise.

### GetDomainOk

`func (o *CrawlInput) GetDomainOk() (*string, bool)`

GetDomainOk returns a tuple with the Domain field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDomain

`func (o *CrawlInput) SetDomain(v string)`

SetDomain sets Domain field to given value.

### HasDomain

`func (o *CrawlInput) HasDomain() bool`

HasDomain returns a boolean if a field has been set.

### GetUrl

`func (o *CrawlInput) GetUrl() string`

GetUrl returns the Url field if non-nil, zero value otherwise.

### GetUrlOk

`func (o *CrawlInput) GetUrlOk() (*string, bool)`

GetUrlOk returns a tuple with the Url field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUrl

`func (o *CrawlInput) SetUrl(v string)`

SetUrl sets Url field to given value.

### HasUrl

`func (o *CrawlInput) HasUrl() bool`

HasUrl returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


