# GetAnalytics200Response

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Data** | Pointer to [**[]Analytics**](Analytics.md) |  | [optional] 

## Methods

### NewGetAnalytics200Response

`func NewGetAnalytics200Response() *GetAnalytics200Response`

NewGetAnalytics200Response instantiates a new GetAnalytics200Response object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewGetAnalytics200ResponseWithDefaults

`func NewGetAnalytics200ResponseWithDefaults() *GetAnalytics200Response`

NewGetAnalytics200ResponseWithDefaults instantiates a new GetAnalytics200Response object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetData

`func (o *GetAnalytics200Response) GetData() []Analytics`

GetData returns the Data field if non-nil, zero value otherwise.

### GetDataOk

`func (o *GetAnalytics200Response) GetDataOk() (*[]Analytics, bool)`

GetDataOk returns a tuple with the Data field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetData

`func (o *GetAnalytics200Response) SetData(v []Analytics)`

SetData sets Data field to given value.

### HasData

`func (o *GetAnalytics200Response) HasData() bool`

HasData returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


