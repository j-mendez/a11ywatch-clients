# WebsitesInsight

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Json** | Pointer to **string** |  | [optional] 

## Methods

### NewWebsitesInsight

`func NewWebsitesInsight() *WebsitesInsight`

NewWebsitesInsight instantiates a new WebsitesInsight object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewWebsitesInsightWithDefaults

`func NewWebsitesInsightWithDefaults() *WebsitesInsight`

NewWebsitesInsightWithDefaults instantiates a new WebsitesInsight object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetJson

`func (o *WebsitesInsight) GetJson() string`

GetJson returns the Json field if non-nil, zero value otherwise.

### GetJsonOk

`func (o *WebsitesInsight) GetJsonOk() (*string, bool)`

GetJsonOk returns a tuple with the Json field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetJson

`func (o *WebsitesInsight) SetJson(v string)`

SetJson sets Json field to given value.

### HasJson

`func (o *WebsitesInsight) HasJson() bool`

HasJson returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


