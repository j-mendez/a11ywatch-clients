#include <stdlib.h>
#include <stdio.h>
#include "../include/apiClient.h"
#include "../include/list.h"
#include "../external/cJSON.h"
#include "../include/keyValuePair.h"
#include "../include/binary.h"
#include "../model/object.h"
#include "../model/website_input.h"


// Multi-page crawl a website streaming issues on found
//
object_t*
ReportsAPI_crawlWebsiteStream(apiClient_t *apiClient, char * Transfer_Encoding , website_input_t * website_input );


// Multi-page crawl all websites attached to account
//
object_t*
ReportsAPI_crawlWebsitesSync(apiClient_t *apiClient);


// Get the report from a previus scan
//
object_t*
ReportsAPI_getReport(apiClient_t *apiClient, char * url , char * domain );


// Scan a website for issues
//
object_t*
ReportsAPI_scanWebsite(apiClient_t *apiClient, website_input_t * website_input );


// Scan a website for issues without storing data and limited responses.
//
object_t*
ReportsAPI_scanWebsiteSimple(apiClient_t *apiClient, website_input_t * website_input );


