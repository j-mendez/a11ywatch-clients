#include <stdlib.h>
#include <stdio.h>
#include "../include/apiClient.h"
#include "../include/list.h"
#include "../external/cJSON.h"
#include "../include/keyValuePair.h"
#include "../include/binary.h"
#include "../model/auth_input.h"
#include "../model/users.h"


// Register user into the system
//
// Adds a new user not created yet into the system
//
users_t*
UserAPI_createUser(apiClient_t *apiClient, auth_input_t * auth_input );


// Get user
//
// Retrieve the current user.
//
void
UserAPI_getUsers(apiClient_t *apiClient);


// Logs user into the system
//
users_t*
UserAPI_loginUser(apiClient_t *apiClient, auth_input_t * auth_input );


// Logs out current logged in user session
//
void
UserAPI_logoutUser(apiClient_t *apiClient);


