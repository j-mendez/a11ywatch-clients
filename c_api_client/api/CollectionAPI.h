#include <stdlib.h>
#include <stdio.h>
#include "../include/apiClient.h"
#include "../include/list.h"
#include "../external/cJSON.h"
#include "../include/keyValuePair.h"
#include "../include/binary.h"
#include "../model/object.h"


// Get the analytics for a website
//
object_t*
CollectionAPI_getAnalytics(apiClient_t *apiClient, char * offset , char * domain );


// List the issues for a website
//
object_t*
CollectionAPI_getIssues(apiClient_t *apiClient, char * offset , char * domain );


// Get the pagespeed for a website
//
object_t*
CollectionAPI_getPageSpeed(apiClient_t *apiClient, char * offset , char * domain );


// List the pages in order for a website
//
object_t*
CollectionAPI_getPages(apiClient_t *apiClient, char * offset , char * domain );


// Returns websites for the user in alphabetical order
//
// Returns a map of websites
//
object_t*
CollectionAPI_getWebsites(apiClient_t *apiClient, char * offset );


