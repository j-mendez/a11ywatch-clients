#include <stdlib.h>
#include <stdio.h>
#include "../include/apiClient.h"
#include "../include/list.h"
#include "../external/cJSON.h"
#include "../include/keyValuePair.h"
#include "../include/binary.h"
#include "../model/object.h"
#include "../model/website_input.h"


// Add a website in the collection with form data
//
object_t*
WebsitesAPI_addWebsite(apiClient_t *apiClient, website_input_t * website_input );


// Deletes a website
//
object_t*
WebsitesAPI_deleteWebsite(apiClient_t *apiClient, char * domain );


// Find website by Domain
//
// Returns a website when DOMAIN == website.domain.  Empty strings will simulate API error conditions
//
object_t*
WebsitesAPI_getWebsiteByDomain(apiClient_t *apiClient, char * domain );


