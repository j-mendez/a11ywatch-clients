#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include "ReportsAPI.h"

#define MAX_NUMBER_LENGTH 16
#define MAX_BUFFER_LENGTH 4096
#define intToStr(dst, src) \
    do {\
    char dst[256];\
    snprintf(dst, 256, "%ld", (long int)(src));\
}while(0)


// Multi-page crawl a website streaming issues on found
//
object_t*
ReportsAPI_crawlWebsiteStream(apiClient_t *apiClient, char * Transfer_Encoding , website_input_t * website_input )
{
    list_t    *localVarQueryParameters = NULL;
    list_t    *localVarHeaderParameters = list_create();
    list_t    *localVarFormParameters = NULL;
    list_t *localVarHeaderType = list_create();
    list_t *localVarContentType = list_create();
    char      *localVarBodyParameters = NULL;

    // create the path
    long sizeOfPath = strlen("/crawl")+1;
    char *localVarPath = malloc(sizeOfPath);
    snprintf(localVarPath, sizeOfPath, "/crawl");




    // header parameters
    char *keyHeader_Transfer_Encoding = NULL;
    char * valueHeader_Transfer_Encoding = 0;
    keyValuePair_t *keyPairHeader_Transfer_Encoding = 0;
    if (Transfer_Encoding) {
        keyHeader_Transfer_Encoding = strdup("Transfer-Encoding");
        valueHeader_Transfer_Encoding = strdup((Transfer_Encoding));
        keyPairHeader_Transfer_Encoding = keyValuePair_create(keyHeader_Transfer_Encoding, valueHeader_Transfer_Encoding);
        list_addElement(localVarHeaderParameters,keyPairHeader_Transfer_Encoding);
    }


    // Body Param
    cJSON *localVarSingleItemJSON_website_input = NULL;
    if (website_input != NULL)
    {
        //string
        localVarSingleItemJSON_website_input = website_input_convertToJSON(website_input);
        localVarBodyParameters = cJSON_Print(localVarSingleItemJSON_website_input);
    }
    list_addElement(localVarHeaderType,"application/json"); //produces
    list_addElement(localVarContentType,"application/json"); //consumes
    apiClient_invoke(apiClient,
                    localVarPath,
                    localVarQueryParameters,
                    localVarHeaderParameters,
                    localVarFormParameters,
                    localVarHeaderType,
                    localVarContentType,
                    localVarBodyParameters,
                    "POST");

    if (apiClient->response_code == 200) {
        printf("%s\n","successful operation");
    }
    if (apiClient->response_code == 405) {
        printf("%s\n","Invalid input");
    }
    //nonprimitive not container
    cJSON *ReportsAPIlocalVarJSON = cJSON_Parse(apiClient->dataReceived);
    object_t *elementToReturn = object_parseFromJSON(ReportsAPIlocalVarJSON);
    cJSON_Delete(ReportsAPIlocalVarJSON);
    if(elementToReturn == NULL) {
        // return 0;
    }

    //return type
    if (apiClient->dataReceived) {
        free(apiClient->dataReceived);
        apiClient->dataReceived = NULL;
        apiClient->dataReceivedLen = 0;
    }
    
    list_free(localVarHeaderParameters);
    
    list_free(localVarHeaderType);
    list_free(localVarContentType);
    free(localVarPath);
    if (keyHeader_Transfer_Encoding) {
        free(keyHeader_Transfer_Encoding);
        keyHeader_Transfer_Encoding = NULL;
    }
    if (valueHeader_Transfer_Encoding) {
        free(valueHeader_Transfer_Encoding);
        valueHeader_Transfer_Encoding = NULL;
    }
    free(keyPairHeader_Transfer_Encoding);
    if (localVarSingleItemJSON_website_input) {
        cJSON_Delete(localVarSingleItemJSON_website_input);
        localVarSingleItemJSON_website_input = NULL;
    }
    free(localVarBodyParameters);
    return elementToReturn;
end:
    free(localVarPath);
    return NULL;

}

// Multi-page crawl all websites attached to account
//
object_t*
ReportsAPI_crawlWebsitesSync(apiClient_t *apiClient)
{
    list_t    *localVarQueryParameters = NULL;
    list_t    *localVarHeaderParameters = NULL;
    list_t    *localVarFormParameters = NULL;
    list_t *localVarHeaderType = list_create();
    list_t *localVarContentType = NULL;
    char      *localVarBodyParameters = NULL;

    // create the path
    long sizeOfPath = strlen("/websites-sync")+1;
    char *localVarPath = malloc(sizeOfPath);
    snprintf(localVarPath, sizeOfPath, "/websites-sync");



    list_addElement(localVarHeaderType,"application/json"); //produces
    apiClient_invoke(apiClient,
                    localVarPath,
                    localVarQueryParameters,
                    localVarHeaderParameters,
                    localVarFormParameters,
                    localVarHeaderType,
                    localVarContentType,
                    localVarBodyParameters,
                    "POST");

    if (apiClient->response_code == 200) {
        printf("%s\n","successful operation");
    }
    if (apiClient->response_code == 405) {
        printf("%s\n","Invalid input");
    }
    //nonprimitive not container
    cJSON *ReportsAPIlocalVarJSON = cJSON_Parse(apiClient->dataReceived);
    object_t *elementToReturn = object_parseFromJSON(ReportsAPIlocalVarJSON);
    cJSON_Delete(ReportsAPIlocalVarJSON);
    if(elementToReturn == NULL) {
        // return 0;
    }

    //return type
    if (apiClient->dataReceived) {
        free(apiClient->dataReceived);
        apiClient->dataReceived = NULL;
        apiClient->dataReceivedLen = 0;
    }
    
    
    
    list_free(localVarHeaderType);
    
    free(localVarPath);
    return elementToReturn;
end:
    free(localVarPath);
    return NULL;

}

// Get the report from a previus scan
//
object_t*
ReportsAPI_getReport(apiClient_t *apiClient, char * url , char * domain )
{
    list_t    *localVarQueryParameters = list_create();
    list_t    *localVarHeaderParameters = NULL;
    list_t    *localVarFormParameters = NULL;
    list_t *localVarHeaderType = list_create();
    list_t *localVarContentType = NULL;
    char      *localVarBodyParameters = NULL;

    // create the path
    long sizeOfPath = strlen("/report")+1;
    char *localVarPath = malloc(sizeOfPath);
    snprintf(localVarPath, sizeOfPath, "/report");




    // query parameters
    char *keyQuery_url = NULL;
    char * valueQuery_url = NULL;
    keyValuePair_t *keyPairQuery_url = 0;
    if (url)
    {
        keyQuery_url = strdup("url");
        valueQuery_url = strdup((url));
        keyPairQuery_url = keyValuePair_create(keyQuery_url, valueQuery_url);
        list_addElement(localVarQueryParameters,keyPairQuery_url);
    }

    // query parameters
    char *keyQuery_domain = NULL;
    char * valueQuery_domain = NULL;
    keyValuePair_t *keyPairQuery_domain = 0;
    if (domain)
    {
        keyQuery_domain = strdup("domain");
        valueQuery_domain = strdup((domain));
        keyPairQuery_domain = keyValuePair_create(keyQuery_domain, valueQuery_domain);
        list_addElement(localVarQueryParameters,keyPairQuery_domain);
    }
    list_addElement(localVarHeaderType,"application/json"); //produces
    apiClient_invoke(apiClient,
                    localVarPath,
                    localVarQueryParameters,
                    localVarHeaderParameters,
                    localVarFormParameters,
                    localVarHeaderType,
                    localVarContentType,
                    localVarBodyParameters,
                    "GET");

    if (apiClient->response_code == 200) {
        printf("%s\n","successful operation");
    }
    if (apiClient->response_code == 405) {
        printf("%s\n","Invalid input");
    }
    //nonprimitive not container
    cJSON *ReportsAPIlocalVarJSON = cJSON_Parse(apiClient->dataReceived);
    object_t *elementToReturn = object_parseFromJSON(ReportsAPIlocalVarJSON);
    cJSON_Delete(ReportsAPIlocalVarJSON);
    if(elementToReturn == NULL) {
        // return 0;
    }

    //return type
    if (apiClient->dataReceived) {
        free(apiClient->dataReceived);
        apiClient->dataReceived = NULL;
        apiClient->dataReceivedLen = 0;
    }
    list_free(localVarQueryParameters);
    
    
    list_free(localVarHeaderType);
    
    free(localVarPath);
    if(keyQuery_url){
        free(keyQuery_url);
        keyQuery_url = NULL;
    }
    if(valueQuery_url){
        free(valueQuery_url);
        valueQuery_url = NULL;
    }
    if(keyPairQuery_url){
        keyValuePair_free(keyPairQuery_url);
        keyPairQuery_url = NULL;
    }
    if(keyQuery_domain){
        free(keyQuery_domain);
        keyQuery_domain = NULL;
    }
    if(valueQuery_domain){
        free(valueQuery_domain);
        valueQuery_domain = NULL;
    }
    if(keyPairQuery_domain){
        keyValuePair_free(keyPairQuery_domain);
        keyPairQuery_domain = NULL;
    }
    return elementToReturn;
end:
    free(localVarPath);
    return NULL;

}

// Scan a website for issues
//
object_t*
ReportsAPI_scanWebsite(apiClient_t *apiClient, website_input_t * website_input )
{
    list_t    *localVarQueryParameters = NULL;
    list_t    *localVarHeaderParameters = NULL;
    list_t    *localVarFormParameters = NULL;
    list_t *localVarHeaderType = list_create();
    list_t *localVarContentType = list_create();
    char      *localVarBodyParameters = NULL;

    // create the path
    long sizeOfPath = strlen("/scan")+1;
    char *localVarPath = malloc(sizeOfPath);
    snprintf(localVarPath, sizeOfPath, "/scan");




    // Body Param
    cJSON *localVarSingleItemJSON_website_input = NULL;
    if (website_input != NULL)
    {
        //string
        localVarSingleItemJSON_website_input = website_input_convertToJSON(website_input);
        localVarBodyParameters = cJSON_Print(localVarSingleItemJSON_website_input);
    }
    list_addElement(localVarHeaderType,"application/json"); //produces
    list_addElement(localVarContentType,"application/json"); //consumes
    apiClient_invoke(apiClient,
                    localVarPath,
                    localVarQueryParameters,
                    localVarHeaderParameters,
                    localVarFormParameters,
                    localVarHeaderType,
                    localVarContentType,
                    localVarBodyParameters,
                    "POST");

    if (apiClient->response_code == 200) {
        printf("%s\n","successful operation");
    }
    if (apiClient->response_code == 405) {
        printf("%s\n","Invalid input");
    }
    //nonprimitive not container
    cJSON *ReportsAPIlocalVarJSON = cJSON_Parse(apiClient->dataReceived);
    object_t *elementToReturn = object_parseFromJSON(ReportsAPIlocalVarJSON);
    cJSON_Delete(ReportsAPIlocalVarJSON);
    if(elementToReturn == NULL) {
        // return 0;
    }

    //return type
    if (apiClient->dataReceived) {
        free(apiClient->dataReceived);
        apiClient->dataReceived = NULL;
        apiClient->dataReceivedLen = 0;
    }
    
    
    
    list_free(localVarHeaderType);
    list_free(localVarContentType);
    free(localVarPath);
    if (localVarSingleItemJSON_website_input) {
        cJSON_Delete(localVarSingleItemJSON_website_input);
        localVarSingleItemJSON_website_input = NULL;
    }
    free(localVarBodyParameters);
    return elementToReturn;
end:
    free(localVarPath);
    return NULL;

}

// Scan a website for issues without storing data and limited responses.
//
object_t*
ReportsAPI_scanWebsiteSimple(apiClient_t *apiClient, website_input_t * website_input )
{
    list_t    *localVarQueryParameters = NULL;
    list_t    *localVarHeaderParameters = NULL;
    list_t    *localVarFormParameters = NULL;
    list_t *localVarHeaderType = list_create();
    list_t *localVarContentType = list_create();
    char      *localVarBodyParameters = NULL;

    // create the path
    long sizeOfPath = strlen("/scan-simple")+1;
    char *localVarPath = malloc(sizeOfPath);
    snprintf(localVarPath, sizeOfPath, "/scan-simple");




    // Body Param
    cJSON *localVarSingleItemJSON_website_input = NULL;
    if (website_input != NULL)
    {
        //string
        localVarSingleItemJSON_website_input = website_input_convertToJSON(website_input);
        localVarBodyParameters = cJSON_Print(localVarSingleItemJSON_website_input);
    }
    list_addElement(localVarHeaderType,"application/json"); //produces
    list_addElement(localVarContentType,"application/json"); //consumes
    apiClient_invoke(apiClient,
                    localVarPath,
                    localVarQueryParameters,
                    localVarHeaderParameters,
                    localVarFormParameters,
                    localVarHeaderType,
                    localVarContentType,
                    localVarBodyParameters,
                    "POST");

    if (apiClient->response_code == 200) {
        printf("%s\n","successful operation");
    }
    if (apiClient->response_code == 405) {
        printf("%s\n","Invalid input");
    }
    //nonprimitive not container
    cJSON *ReportsAPIlocalVarJSON = cJSON_Parse(apiClient->dataReceived);
    object_t *elementToReturn = object_parseFromJSON(ReportsAPIlocalVarJSON);
    cJSON_Delete(ReportsAPIlocalVarJSON);
    if(elementToReturn == NULL) {
        // return 0;
    }

    //return type
    if (apiClient->dataReceived) {
        free(apiClient->dataReceived);
        apiClient->dataReceived = NULL;
        apiClient->dataReceivedLen = 0;
    }
    
    
    
    list_free(localVarHeaderType);
    list_free(localVarContentType);
    free(localVarPath);
    if (localVarSingleItemJSON_website_input) {
        cJSON_Delete(localVarSingleItemJSON_website_input);
        localVarSingleItemJSON_website_input = NULL;
    }
    free(localVarBodyParameters);
    return elementToReturn;
end:
    free(localVarPath);
    return NULL;

}

