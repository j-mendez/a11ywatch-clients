/*
 * history.h
 *
 * 
 */

#ifndef _history_H_
#define _history_H_

#include <string.h>
#include "../external/cJSON.h"
#include "../include/list.h"
#include "../include/keyValuePair.h"
#include "../include/binary.h"

typedef struct history_t history_t;

#include "issues_info.h"
#include "websites_insight.h"



typedef struct history_t {
    long _id; //numeric
    char *domain; // string
    struct websites_insight_t *insight; //model
    list_t *page_headers; //primitive container
    struct issues_info_t *issues_info; //model

} history_t;

history_t *history_create(
    long _id,
    char *domain,
    websites_insight_t *insight,
    list_t *page_headers,
    issues_info_t *issues_info
);

void history_free(history_t *history);

history_t *history_parseFromJSON(cJSON *historyJSON);

cJSON *history_convertToJSON(history_t *history);

#endif /* _history_H_ */

