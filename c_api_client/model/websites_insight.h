/*
 * websites_insight.h
 *
 * 
 */

#ifndef _websites_insight_H_
#define _websites_insight_H_

#include <string.h>
#include "../external/cJSON.h"
#include "../include/list.h"
#include "../include/keyValuePair.h"
#include "../include/binary.h"

typedef struct websites_insight_t websites_insight_t;




typedef struct websites_insight_t {
    char *json; // string

} websites_insight_t;

websites_insight_t *websites_insight_create(
    char *json
);

void websites_insight_free(websites_insight_t *websites_insight);

websites_insight_t *websites_insight_parseFromJSON(cJSON *websites_insightJSON);

cJSON *websites_insight_convertToJSON(websites_insight_t *websites_insight);

#endif /* _websites_insight_H_ */

