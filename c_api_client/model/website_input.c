#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "website_input.h"



website_input_t *website_input_create(
    char *url,
    int mobile,
    int page_insights,
    char *ua,
    char *standard,
    char *robots,
    int subdomains,
    int tld,
    list_t *custom_headers
    ) {
    website_input_t *website_input_local_var = malloc(sizeof(website_input_t));
    if (!website_input_local_var) {
        return NULL;
    }
    website_input_local_var->url = url;
    website_input_local_var->mobile = mobile;
    website_input_local_var->page_insights = page_insights;
    website_input_local_var->ua = ua;
    website_input_local_var->standard = standard;
    website_input_local_var->robots = robots;
    website_input_local_var->subdomains = subdomains;
    website_input_local_var->tld = tld;
    website_input_local_var->custom_headers = custom_headers;

    return website_input_local_var;
}


void website_input_free(website_input_t *website_input) {
    if(NULL == website_input){
        return ;
    }
    listEntry_t *listEntry;
    if (website_input->url) {
        free(website_input->url);
        website_input->url = NULL;
    }
    if (website_input->ua) {
        free(website_input->ua);
        website_input->ua = NULL;
    }
    if (website_input->standard) {
        free(website_input->standard);
        website_input->standard = NULL;
    }
    if (website_input->robots) {
        free(website_input->robots);
        website_input->robots = NULL;
    }
    if (website_input->custom_headers) {
        list_ForEach(listEntry, website_input->custom_headers) {
            free(listEntry->data);
        }
        list_free(website_input->custom_headers);
        website_input->custom_headers = NULL;
    }
    free(website_input);
}

cJSON *website_input_convertToJSON(website_input_t *website_input) {
    cJSON *item = cJSON_CreateObject();

    // website_input->url
    if(website_input->url) { 
    if(cJSON_AddStringToObject(item, "url", website_input->url) == NULL) {
    goto fail; //String
    }
     } 


    // website_input->mobile
    if(website_input->mobile) { 
    if(cJSON_AddBoolToObject(item, "mobile", website_input->mobile) == NULL) {
    goto fail; //Bool
    }
     } 


    // website_input->page_insights
    if(website_input->page_insights) { 
    if(cJSON_AddBoolToObject(item, "pageInsights", website_input->page_insights) == NULL) {
    goto fail; //Bool
    }
     } 


    // website_input->ua
    if(website_input->ua) { 
    if(cJSON_AddStringToObject(item, "ua", website_input->ua) == NULL) {
    goto fail; //String
    }
     } 


    // website_input->standard
    if(website_input->standard) { 
    if(cJSON_AddStringToObject(item, "standard", website_input->standard) == NULL) {
    goto fail; //String
    }
     } 


    // website_input->robots
    if(website_input->robots) { 
    if(cJSON_AddStringToObject(item, "robots", website_input->robots) == NULL) {
    goto fail; //String
    }
     } 


    // website_input->subdomains
    if(website_input->subdomains) { 
    if(cJSON_AddBoolToObject(item, "subdomains", website_input->subdomains) == NULL) {
    goto fail; //Bool
    }
     } 


    // website_input->tld
    if(website_input->tld) { 
    if(cJSON_AddBoolToObject(item, "tld", website_input->tld) == NULL) {
    goto fail; //Bool
    }
     } 


    // website_input->custom_headers
    if(website_input->custom_headers) { 
    cJSON *custom_headers = cJSON_AddArrayToObject(item, "customHeaders");
    if(custom_headers == NULL) {
        goto fail; //primitive container
    }

    listEntry_t *custom_headersListEntry;
    list_ForEach(custom_headersListEntry, website_input->custom_headers) {
    if(cJSON_AddStringToObject(custom_headers, "", (char*)custom_headersListEntry->data) == NULL)
    {
        goto fail;
    }
    }
     } 

    return item;
fail:
    if (item) {
        cJSON_Delete(item);
    }
    return NULL;
}

website_input_t *website_input_parseFromJSON(cJSON *website_inputJSON){

    website_input_t *website_input_local_var = NULL;

    // website_input->url
    cJSON *url = cJSON_GetObjectItemCaseSensitive(website_inputJSON, "url");
    if (url) { 
    if(!cJSON_IsString(url))
    {
    goto end; //String
    }
    }

    // website_input->mobile
    cJSON *mobile = cJSON_GetObjectItemCaseSensitive(website_inputJSON, "mobile");
    if (mobile) { 
    if(!cJSON_IsBool(mobile))
    {
    goto end; //Bool
    }
    }

    // website_input->page_insights
    cJSON *page_insights = cJSON_GetObjectItemCaseSensitive(website_inputJSON, "pageInsights");
    if (page_insights) { 
    if(!cJSON_IsBool(page_insights))
    {
    goto end; //Bool
    }
    }

    // website_input->ua
    cJSON *ua = cJSON_GetObjectItemCaseSensitive(website_inputJSON, "ua");
    if (ua) { 
    if(!cJSON_IsString(ua))
    {
    goto end; //String
    }
    }

    // website_input->standard
    cJSON *standard = cJSON_GetObjectItemCaseSensitive(website_inputJSON, "standard");
    if (standard) { 
    if(!cJSON_IsString(standard))
    {
    goto end; //String
    }
    }

    // website_input->robots
    cJSON *robots = cJSON_GetObjectItemCaseSensitive(website_inputJSON, "robots");
    if (robots) { 
    if(!cJSON_IsString(robots))
    {
    goto end; //String
    }
    }

    // website_input->subdomains
    cJSON *subdomains = cJSON_GetObjectItemCaseSensitive(website_inputJSON, "subdomains");
    if (subdomains) { 
    if(!cJSON_IsBool(subdomains))
    {
    goto end; //Bool
    }
    }

    // website_input->tld
    cJSON *tld = cJSON_GetObjectItemCaseSensitive(website_inputJSON, "tld");
    if (tld) { 
    if(!cJSON_IsBool(tld))
    {
    goto end; //Bool
    }
    }

    // website_input->custom_headers
    cJSON *custom_headers = cJSON_GetObjectItemCaseSensitive(website_inputJSON, "customHeaders");
    list_t *custom_headersList;
    if (custom_headers) { 
    cJSON *custom_headers_local;
    if(!cJSON_IsArray(custom_headers)) {
        goto end;//primitive container
    }
    custom_headersList = list_create();

    cJSON_ArrayForEach(custom_headers_local, custom_headers)
    {
        if(!cJSON_IsString(custom_headers_local))
        {
            goto end;
        }
        list_addElement(custom_headersList , strdup(custom_headers_local->valuestring));
    }
    }


    website_input_local_var = website_input_create (
        url ? strdup(url->valuestring) : NULL,
        mobile ? mobile->valueint : 0,
        page_insights ? page_insights->valueint : 0,
        ua ? strdup(ua->valuestring) : NULL,
        standard ? strdup(standard->valuestring) : NULL,
        robots ? strdup(robots->valuestring) : NULL,
        subdomains ? subdomains->valueint : 0,
        tld ? tld->valueint : 0,
        custom_headers ? custom_headersList : NULL
        );

    return website_input_local_var;
end:
    return NULL;

}
