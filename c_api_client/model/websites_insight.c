#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "websites_insight.h"



websites_insight_t *websites_insight_create(
    char *json
    ) {
    websites_insight_t *websites_insight_local_var = malloc(sizeof(websites_insight_t));
    if (!websites_insight_local_var) {
        return NULL;
    }
    websites_insight_local_var->json = json;

    return websites_insight_local_var;
}


void websites_insight_free(websites_insight_t *websites_insight) {
    if(NULL == websites_insight){
        return ;
    }
    listEntry_t *listEntry;
    if (websites_insight->json) {
        free(websites_insight->json);
        websites_insight->json = NULL;
    }
    free(websites_insight);
}

cJSON *websites_insight_convertToJSON(websites_insight_t *websites_insight) {
    cJSON *item = cJSON_CreateObject();

    // websites_insight->json
    if(websites_insight->json) { 
    if(cJSON_AddStringToObject(item, "json", websites_insight->json) == NULL) {
    goto fail; //String
    }
     } 

    return item;
fail:
    if (item) {
        cJSON_Delete(item);
    }
    return NULL;
}

websites_insight_t *websites_insight_parseFromJSON(cJSON *websites_insightJSON){

    websites_insight_t *websites_insight_local_var = NULL;

    // websites_insight->json
    cJSON *json = cJSON_GetObjectItemCaseSensitive(websites_insightJSON, "json");
    if (json) { 
    if(!cJSON_IsString(json))
    {
    goto end; //String
    }
    }


    websites_insight_local_var = websites_insight_create (
        json ? strdup(json->valuestring) : NULL
        );

    return websites_insight_local_var;
end:
    return NULL;

}
