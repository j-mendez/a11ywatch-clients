/*
 * crawl_websites_sync_200_response.h
 *
 * 
 */

#ifndef _crawl_websites_sync_200_response_H_
#define _crawl_websites_sync_200_response_H_

#include <string.h>
#include "../external/cJSON.h"
#include "../include/list.h"
#include "../include/keyValuePair.h"
#include "../include/binary.h"

typedef struct crawl_websites_sync_200_response_t crawl_websites_sync_200_response_t;




typedef struct crawl_websites_sync_200_response_t {
    int data; //boolean
    char *message; // string

} crawl_websites_sync_200_response_t;

crawl_websites_sync_200_response_t *crawl_websites_sync_200_response_create(
    int data,
    char *message
);

void crawl_websites_sync_200_response_free(crawl_websites_sync_200_response_t *crawl_websites_sync_200_response);

crawl_websites_sync_200_response_t *crawl_websites_sync_200_response_parseFromJSON(cJSON *crawl_websites_sync_200_responseJSON);

cJSON *crawl_websites_sync_200_response_convertToJSON(crawl_websites_sync_200_response_t *crawl_websites_sync_200_response);

#endif /* _crawl_websites_sync_200_response_H_ */

