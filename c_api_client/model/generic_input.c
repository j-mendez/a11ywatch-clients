#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "generic_input.h"



generic_input_t *generic_input_create(
    char *domain
    ) {
    generic_input_t *generic_input_local_var = malloc(sizeof(generic_input_t));
    if (!generic_input_local_var) {
        return NULL;
    }
    generic_input_local_var->domain = domain;

    return generic_input_local_var;
}


void generic_input_free(generic_input_t *generic_input) {
    if(NULL == generic_input){
        return ;
    }
    listEntry_t *listEntry;
    if (generic_input->domain) {
        free(generic_input->domain);
        generic_input->domain = NULL;
    }
    free(generic_input);
}

cJSON *generic_input_convertToJSON(generic_input_t *generic_input) {
    cJSON *item = cJSON_CreateObject();

    // generic_input->domain
    if(generic_input->domain) { 
    if(cJSON_AddStringToObject(item, "domain", generic_input->domain) == NULL) {
    goto fail; //String
    }
     } 

    return item;
fail:
    if (item) {
        cJSON_Delete(item);
    }
    return NULL;
}

generic_input_t *generic_input_parseFromJSON(cJSON *generic_inputJSON){

    generic_input_t *generic_input_local_var = NULL;

    // generic_input->domain
    cJSON *domain = cJSON_GetObjectItemCaseSensitive(generic_inputJSON, "domain");
    if (domain) { 
    if(!cJSON_IsString(domain))
    {
    goto end; //String
    }
    }


    generic_input_local_var = generic_input_create (
        domain ? strdup(domain->valuestring) : NULL
        );

    return generic_input_local_var;
end:
    return NULL;

}
