#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "scan_website_simple_200_response.h"



scan_website_simple_200_response_t *scan_website_simple_200_response_create(
    report_t *data
    ) {
    scan_website_simple_200_response_t *scan_website_simple_200_response_local_var = malloc(sizeof(scan_website_simple_200_response_t));
    if (!scan_website_simple_200_response_local_var) {
        return NULL;
    }
    scan_website_simple_200_response_local_var->data = data;

    return scan_website_simple_200_response_local_var;
}


void scan_website_simple_200_response_free(scan_website_simple_200_response_t *scan_website_simple_200_response) {
    if(NULL == scan_website_simple_200_response){
        return ;
    }
    listEntry_t *listEntry;
    if (scan_website_simple_200_response->data) {
        report_free(scan_website_simple_200_response->data);
        scan_website_simple_200_response->data = NULL;
    }
    free(scan_website_simple_200_response);
}

cJSON *scan_website_simple_200_response_convertToJSON(scan_website_simple_200_response_t *scan_website_simple_200_response) {
    cJSON *item = cJSON_CreateObject();

    // scan_website_simple_200_response->data
    if(scan_website_simple_200_response->data) {
    cJSON *data_local_JSON = report_convertToJSON(scan_website_simple_200_response->data);
    if(data_local_JSON == NULL) {
    goto fail; //model
    }
    cJSON_AddItemToObject(item, "data", data_local_JSON);
    if(item->child == NULL) {
    goto fail;
    }
    }

    return item;
fail:
    if (item) {
        cJSON_Delete(item);
    }
    return NULL;
}

scan_website_simple_200_response_t *scan_website_simple_200_response_parseFromJSON(cJSON *scan_website_simple_200_responseJSON){

    scan_website_simple_200_response_t *scan_website_simple_200_response_local_var = NULL;

    // define the local variable for scan_website_simple_200_response->data
    report_t *data_local_nonprim = NULL;

    // scan_website_simple_200_response->data
    cJSON *data = cJSON_GetObjectItemCaseSensitive(scan_website_simple_200_responseJSON, "data");
    if (data) { 
    data_local_nonprim = report_parseFromJSON(data); //nonprimitive
    }


    scan_website_simple_200_response_local_var = scan_website_simple_200_response_create (
        data ? data_local_nonprim : NULL
        );

    return scan_website_simple_200_response_local_var;
end:
    if (data_local_nonprim) {
        report_free(data_local_nonprim);
        data_local_nonprim = NULL;
    }
    return NULL;

}
