/*
 * generic_input.h
 *
 * 
 */

#ifndef _generic_input_H_
#define _generic_input_H_

#include <string.h>
#include "../external/cJSON.h"
#include "../include/list.h"
#include "../include/keyValuePair.h"
#include "../include/binary.h"

typedef struct generic_input_t generic_input_t;




typedef struct generic_input_t {
    char *domain; // string

} generic_input_t;

generic_input_t *generic_input_create(
    char *domain
);

void generic_input_free(generic_input_t *generic_input);

generic_input_t *generic_input_parseFromJSON(cJSON *generic_inputJSON);

cJSON *generic_input_convertToJSON(generic_input_t *generic_input);

#endif /* _generic_input_H_ */

