/*
 * analytics.h
 *
 * 
 */

#ifndef _analytics_H_
#define _analytics_H_

#include <string.h>
#include "../external/cJSON.h"
#include "../include/list.h"
#include "../include/keyValuePair.h"
#include "../include/binary.h"

typedef struct analytics_t analytics_t;




typedef struct analytics_t {
    long _id; //numeric
    char *domain; // string
    char *page_url; // string
    long user_id; //numeric
    long access_score; //numeric
    long possible_issues_fixed_by_cdn; //numeric
    long total_issues; //numeric
    long issues_fixed_by_cdn; //numeric
    long error_count; //numeric
    long warning_count; //numeric
    long notice_count; //numeric

} analytics_t;

analytics_t *analytics_create(
    long _id,
    char *domain,
    char *page_url,
    long user_id,
    long access_score,
    long possible_issues_fixed_by_cdn,
    long total_issues,
    long issues_fixed_by_cdn,
    long error_count,
    long warning_count,
    long notice_count
);

void analytics_free(analytics_t *analytics);

analytics_t *analytics_parseFromJSON(cJSON *analyticsJSON);

cJSON *analytics_convertToJSON(analytics_t *analytics);

#endif /* _analytics_H_ */

