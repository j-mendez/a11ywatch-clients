#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "auth_input.h"



auth_input_t *auth_input_create(
    char *email,
    char *password
    ) {
    auth_input_t *auth_input_local_var = malloc(sizeof(auth_input_t));
    if (!auth_input_local_var) {
        return NULL;
    }
    auth_input_local_var->email = email;
    auth_input_local_var->password = password;

    return auth_input_local_var;
}


void auth_input_free(auth_input_t *auth_input) {
    if(NULL == auth_input){
        return ;
    }
    listEntry_t *listEntry;
    if (auth_input->email) {
        free(auth_input->email);
        auth_input->email = NULL;
    }
    if (auth_input->password) {
        free(auth_input->password);
        auth_input->password = NULL;
    }
    free(auth_input);
}

cJSON *auth_input_convertToJSON(auth_input_t *auth_input) {
    cJSON *item = cJSON_CreateObject();

    // auth_input->email
    if (!auth_input->email) {
        goto fail;
    }
    
    if(cJSON_AddStringToObject(item, "email", auth_input->email) == NULL) {
    goto fail; //String
    }


    // auth_input->password
    if (!auth_input->password) {
        goto fail;
    }
    
    if(cJSON_AddStringToObject(item, "password", auth_input->password) == NULL) {
    goto fail; //String
    }

    return item;
fail:
    if (item) {
        cJSON_Delete(item);
    }
    return NULL;
}

auth_input_t *auth_input_parseFromJSON(cJSON *auth_inputJSON){

    auth_input_t *auth_input_local_var = NULL;

    // auth_input->email
    cJSON *email = cJSON_GetObjectItemCaseSensitive(auth_inputJSON, "email");
    if (!email) {
        goto end;
    }

    
    if(!cJSON_IsString(email))
    {
    goto end; //String
    }

    // auth_input->password
    cJSON *password = cJSON_GetObjectItemCaseSensitive(auth_inputJSON, "password");
    if (!password) {
        goto end;
    }

    
    if(!cJSON_IsString(password))
    {
    goto end; //String
    }


    auth_input_local_var = auth_input_create (
        strdup(email->valuestring),
        strdup(password->valuestring)
        );

    return auth_input_local_var;
end:
    return NULL;

}
