#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "crawl_website_stream_200_response.h"



crawl_website_stream_200_response_t *crawl_website_stream_200_response_create(
    list_t *data
    ) {
    crawl_website_stream_200_response_t *crawl_website_stream_200_response_local_var = malloc(sizeof(crawl_website_stream_200_response_t));
    if (!crawl_website_stream_200_response_local_var) {
        return NULL;
    }
    crawl_website_stream_200_response_local_var->data = data;

    return crawl_website_stream_200_response_local_var;
}


void crawl_website_stream_200_response_free(crawl_website_stream_200_response_t *crawl_website_stream_200_response) {
    if(NULL == crawl_website_stream_200_response){
        return ;
    }
    listEntry_t *listEntry;
    if (crawl_website_stream_200_response->data) {
        list_ForEach(listEntry, crawl_website_stream_200_response->data) {
            report_free(listEntry->data);
        }
        list_freeList(crawl_website_stream_200_response->data);
        crawl_website_stream_200_response->data = NULL;
    }
    free(crawl_website_stream_200_response);
}

cJSON *crawl_website_stream_200_response_convertToJSON(crawl_website_stream_200_response_t *crawl_website_stream_200_response) {
    cJSON *item = cJSON_CreateObject();

    // crawl_website_stream_200_response->data
    if(crawl_website_stream_200_response->data) {
    cJSON *data = cJSON_AddArrayToObject(item, "data");
    if(data == NULL) {
    goto fail; //nonprimitive container
    }

    listEntry_t *dataListEntry;
    if (crawl_website_stream_200_response->data) {
    list_ForEach(dataListEntry, crawl_website_stream_200_response->data) {
    cJSON *itemLocal = report_convertToJSON(dataListEntry->data);
    if(itemLocal == NULL) {
    goto fail;
    }
    cJSON_AddItemToArray(data, itemLocal);
    }
    }
    }

    return item;
fail:
    if (item) {
        cJSON_Delete(item);
    }
    return NULL;
}

crawl_website_stream_200_response_t *crawl_website_stream_200_response_parseFromJSON(cJSON *crawl_website_stream_200_responseJSON){

    crawl_website_stream_200_response_t *crawl_website_stream_200_response_local_var = NULL;

    // define the local list for crawl_website_stream_200_response->data
    list_t *dataList = NULL;

    // crawl_website_stream_200_response->data
    cJSON *data = cJSON_GetObjectItemCaseSensitive(crawl_website_stream_200_responseJSON, "data");
    if (data) { 
    cJSON *data_local_nonprimitive = NULL;
    if(!cJSON_IsArray(data)){
        goto end; //nonprimitive container
    }

    dataList = list_createList();

    cJSON_ArrayForEach(data_local_nonprimitive,data )
    {
        if(!cJSON_IsObject(data_local_nonprimitive)){
            goto end;
        }
        report_t *dataItem = report_parseFromJSON(data_local_nonprimitive);

        list_addElement(dataList, dataItem);
    }
    }


    crawl_website_stream_200_response_local_var = crawl_website_stream_200_response_create (
        data ? dataList : NULL
        );

    return crawl_website_stream_200_response_local_var;
end:
    if (dataList) {
        listEntry_t *listEntry = NULL;
        list_ForEach(listEntry, dataList) {
            report_free(listEntry->data);
            listEntry->data = NULL;
        }
        list_freeList(dataList);
        dataList = NULL;
    }
    return NULL;

}
