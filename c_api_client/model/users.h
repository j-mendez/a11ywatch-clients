/*
 * users.h
 *
 * 
 */

#ifndef _users_H_
#define _users_H_

#include <string.h>
#include "../external/cJSON.h"
#include "../include/list.h"
#include "../include/keyValuePair.h"
#include "../include/binary.h"

typedef struct users_t users_t;




typedef struct users_t {
    long _id; //numeric
    char *email; // string
    char *jwt; // string
    char *role; // string
    char *password; // string
    int alert_enabled; //boolean

} users_t;

users_t *users_create(
    long _id,
    char *email,
    char *jwt,
    char *role,
    char *password,
    int alert_enabled
);

void users_free(users_t *users);

users_t *users_parseFromJSON(cJSON *usersJSON);

cJSON *users_convertToJSON(users_t *users);

#endif /* _users_H_ */

