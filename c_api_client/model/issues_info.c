#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "issues_info.h"



issues_info_t *issues_info_create(
    long access_score_average,
    long possible_issues_fixed_by_cdn,
    long total_issues,
    long issues_fixed_by_cdn,
    long error_count,
    long warning_count,
    long notice_count,
    long page_count
    ) {
    issues_info_t *issues_info_local_var = malloc(sizeof(issues_info_t));
    if (!issues_info_local_var) {
        return NULL;
    }
    issues_info_local_var->access_score_average = access_score_average;
    issues_info_local_var->possible_issues_fixed_by_cdn = possible_issues_fixed_by_cdn;
    issues_info_local_var->total_issues = total_issues;
    issues_info_local_var->issues_fixed_by_cdn = issues_fixed_by_cdn;
    issues_info_local_var->error_count = error_count;
    issues_info_local_var->warning_count = warning_count;
    issues_info_local_var->notice_count = notice_count;
    issues_info_local_var->page_count = page_count;

    return issues_info_local_var;
}


void issues_info_free(issues_info_t *issues_info) {
    if(NULL == issues_info){
        return ;
    }
    listEntry_t *listEntry;
    free(issues_info);
}

cJSON *issues_info_convertToJSON(issues_info_t *issues_info) {
    cJSON *item = cJSON_CreateObject();

    // issues_info->access_score_average
    if(issues_info->access_score_average) { 
    if(cJSON_AddNumberToObject(item, "accessScoreAverage", issues_info->access_score_average) == NULL) {
    goto fail; //Numeric
    }
     } 


    // issues_info->possible_issues_fixed_by_cdn
    if(issues_info->possible_issues_fixed_by_cdn) { 
    if(cJSON_AddNumberToObject(item, "possibleIssuesFixedByCdn", issues_info->possible_issues_fixed_by_cdn) == NULL) {
    goto fail; //Numeric
    }
     } 


    // issues_info->total_issues
    if(issues_info->total_issues) { 
    if(cJSON_AddNumberToObject(item, "totalIssues", issues_info->total_issues) == NULL) {
    goto fail; //Numeric
    }
     } 


    // issues_info->issues_fixed_by_cdn
    if(issues_info->issues_fixed_by_cdn) { 
    if(cJSON_AddNumberToObject(item, "issuesFixedByCdn", issues_info->issues_fixed_by_cdn) == NULL) {
    goto fail; //Numeric
    }
     } 


    // issues_info->error_count
    if(issues_info->error_count) { 
    if(cJSON_AddNumberToObject(item, "errorCount", issues_info->error_count) == NULL) {
    goto fail; //Numeric
    }
     } 


    // issues_info->warning_count
    if(issues_info->warning_count) { 
    if(cJSON_AddNumberToObject(item, "warningCount", issues_info->warning_count) == NULL) {
    goto fail; //Numeric
    }
     } 


    // issues_info->notice_count
    if(issues_info->notice_count) { 
    if(cJSON_AddNumberToObject(item, "noticeCount", issues_info->notice_count) == NULL) {
    goto fail; //Numeric
    }
     } 


    // issues_info->page_count
    if(issues_info->page_count) { 
    if(cJSON_AddNumberToObject(item, "pageCount", issues_info->page_count) == NULL) {
    goto fail; //Numeric
    }
     } 

    return item;
fail:
    if (item) {
        cJSON_Delete(item);
    }
    return NULL;
}

issues_info_t *issues_info_parseFromJSON(cJSON *issues_infoJSON){

    issues_info_t *issues_info_local_var = NULL;

    // issues_info->access_score_average
    cJSON *access_score_average = cJSON_GetObjectItemCaseSensitive(issues_infoJSON, "accessScoreAverage");
    if (access_score_average) { 
    if(!cJSON_IsNumber(access_score_average))
    {
    goto end; //Numeric
    }
    }

    // issues_info->possible_issues_fixed_by_cdn
    cJSON *possible_issues_fixed_by_cdn = cJSON_GetObjectItemCaseSensitive(issues_infoJSON, "possibleIssuesFixedByCdn");
    if (possible_issues_fixed_by_cdn) { 
    if(!cJSON_IsNumber(possible_issues_fixed_by_cdn))
    {
    goto end; //Numeric
    }
    }

    // issues_info->total_issues
    cJSON *total_issues = cJSON_GetObjectItemCaseSensitive(issues_infoJSON, "totalIssues");
    if (total_issues) { 
    if(!cJSON_IsNumber(total_issues))
    {
    goto end; //Numeric
    }
    }

    // issues_info->issues_fixed_by_cdn
    cJSON *issues_fixed_by_cdn = cJSON_GetObjectItemCaseSensitive(issues_infoJSON, "issuesFixedByCdn");
    if (issues_fixed_by_cdn) { 
    if(!cJSON_IsNumber(issues_fixed_by_cdn))
    {
    goto end; //Numeric
    }
    }

    // issues_info->error_count
    cJSON *error_count = cJSON_GetObjectItemCaseSensitive(issues_infoJSON, "errorCount");
    if (error_count) { 
    if(!cJSON_IsNumber(error_count))
    {
    goto end; //Numeric
    }
    }

    // issues_info->warning_count
    cJSON *warning_count = cJSON_GetObjectItemCaseSensitive(issues_infoJSON, "warningCount");
    if (warning_count) { 
    if(!cJSON_IsNumber(warning_count))
    {
    goto end; //Numeric
    }
    }

    // issues_info->notice_count
    cJSON *notice_count = cJSON_GetObjectItemCaseSensitive(issues_infoJSON, "noticeCount");
    if (notice_count) { 
    if(!cJSON_IsNumber(notice_count))
    {
    goto end; //Numeric
    }
    }

    // issues_info->page_count
    cJSON *page_count = cJSON_GetObjectItemCaseSensitive(issues_infoJSON, "pageCount");
    if (page_count) { 
    if(!cJSON_IsNumber(page_count))
    {
    goto end; //Numeric
    }
    }


    issues_info_local_var = issues_info_create (
        access_score_average ? access_score_average->valuedouble : 0,
        possible_issues_fixed_by_cdn ? possible_issues_fixed_by_cdn->valuedouble : 0,
        total_issues ? total_issues->valuedouble : 0,
        issues_fixed_by_cdn ? issues_fixed_by_cdn->valuedouble : 0,
        error_count ? error_count->valuedouble : 0,
        warning_count ? warning_count->valuedouble : 0,
        notice_count ? notice_count->valuedouble : 0,
        page_count ? page_count->valuedouble : 0
        );

    return issues_info_local_var;
end:
    return NULL;

}
