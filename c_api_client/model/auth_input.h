/*
 * auth_input.h
 *
 * 
 */

#ifndef _auth_input_H_
#define _auth_input_H_

#include <string.h>
#include "../external/cJSON.h"
#include "../include/list.h"
#include "../include/keyValuePair.h"
#include "../include/binary.h"

typedef struct auth_input_t auth_input_t;




typedef struct auth_input_t {
    char *email; // string
    char *password; // string

} auth_input_t;

auth_input_t *auth_input_create(
    char *email,
    char *password
);

void auth_input_free(auth_input_t *auth_input);

auth_input_t *auth_input_parseFromJSON(cJSON *auth_inputJSON);

cJSON *auth_input_convertToJSON(auth_input_t *auth_input);

#endif /* _auth_input_H_ */

