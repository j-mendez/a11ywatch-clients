#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "users.h"



users_t *users_create(
    long _id,
    char *email,
    char *jwt,
    char *role,
    char *password,
    int alert_enabled
    ) {
    users_t *users_local_var = malloc(sizeof(users_t));
    if (!users_local_var) {
        return NULL;
    }
    users_local_var->_id = _id;
    users_local_var->email = email;
    users_local_var->jwt = jwt;
    users_local_var->role = role;
    users_local_var->password = password;
    users_local_var->alert_enabled = alert_enabled;

    return users_local_var;
}


void users_free(users_t *users) {
    if(NULL == users){
        return ;
    }
    listEntry_t *listEntry;
    if (users->email) {
        free(users->email);
        users->email = NULL;
    }
    if (users->jwt) {
        free(users->jwt);
        users->jwt = NULL;
    }
    if (users->role) {
        free(users->role);
        users->role = NULL;
    }
    if (users->password) {
        free(users->password);
        users->password = NULL;
    }
    free(users);
}

cJSON *users_convertToJSON(users_t *users) {
    cJSON *item = cJSON_CreateObject();

    // users->_id
    if(users->_id) { 
    if(cJSON_AddNumberToObject(item, "_id", users->_id) == NULL) {
    goto fail; //Numeric
    }
     } 


    // users->email
    if(users->email) { 
    if(cJSON_AddStringToObject(item, "email", users->email) == NULL) {
    goto fail; //String
    }
     } 


    // users->jwt
    if(users->jwt) { 
    if(cJSON_AddStringToObject(item, "jwt", users->jwt) == NULL) {
    goto fail; //String
    }
     } 


    // users->role
    if(users->role) { 
    if(cJSON_AddStringToObject(item, "role", users->role) == NULL) {
    goto fail; //String
    }
     } 


    // users->password
    if(users->password) { 
    if(cJSON_AddStringToObject(item, "password", users->password) == NULL) {
    goto fail; //String
    }
     } 


    // users->alert_enabled
    if(users->alert_enabled) { 
    if(cJSON_AddBoolToObject(item, "alertEnabled", users->alert_enabled) == NULL) {
    goto fail; //Bool
    }
     } 

    return item;
fail:
    if (item) {
        cJSON_Delete(item);
    }
    return NULL;
}

users_t *users_parseFromJSON(cJSON *usersJSON){

    users_t *users_local_var = NULL;

    // users->_id
    cJSON *_id = cJSON_GetObjectItemCaseSensitive(usersJSON, "_id");
    if (_id) { 
    if(!cJSON_IsNumber(_id))
    {
    goto end; //Numeric
    }
    }

    // users->email
    cJSON *email = cJSON_GetObjectItemCaseSensitive(usersJSON, "email");
    if (email) { 
    if(!cJSON_IsString(email))
    {
    goto end; //String
    }
    }

    // users->jwt
    cJSON *jwt = cJSON_GetObjectItemCaseSensitive(usersJSON, "jwt");
    if (jwt) { 
    if(!cJSON_IsString(jwt))
    {
    goto end; //String
    }
    }

    // users->role
    cJSON *role = cJSON_GetObjectItemCaseSensitive(usersJSON, "role");
    if (role) { 
    if(!cJSON_IsString(role))
    {
    goto end; //String
    }
    }

    // users->password
    cJSON *password = cJSON_GetObjectItemCaseSensitive(usersJSON, "password");
    if (password) { 
    if(!cJSON_IsString(password))
    {
    goto end; //String
    }
    }

    // users->alert_enabled
    cJSON *alert_enabled = cJSON_GetObjectItemCaseSensitive(usersJSON, "alertEnabled");
    if (alert_enabled) { 
    if(!cJSON_IsBool(alert_enabled))
    {
    goto end; //Bool
    }
    }


    users_local_var = users_create (
        _id ? _id->valuedouble : 0,
        email ? strdup(email->valuestring) : NULL,
        jwt ? strdup(jwt->valuestring) : NULL,
        role ? strdup(role->valuestring) : NULL,
        password ? strdup(password->valuestring) : NULL,
        alert_enabled ? alert_enabled->valueint : 0
        );

    return users_local_var;
end:
    return NULL;

}
