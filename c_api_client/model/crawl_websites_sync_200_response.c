#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "crawl_websites_sync_200_response.h"



crawl_websites_sync_200_response_t *crawl_websites_sync_200_response_create(
    int data,
    char *message
    ) {
    crawl_websites_sync_200_response_t *crawl_websites_sync_200_response_local_var = malloc(sizeof(crawl_websites_sync_200_response_t));
    if (!crawl_websites_sync_200_response_local_var) {
        return NULL;
    }
    crawl_websites_sync_200_response_local_var->data = data;
    crawl_websites_sync_200_response_local_var->message = message;

    return crawl_websites_sync_200_response_local_var;
}


void crawl_websites_sync_200_response_free(crawl_websites_sync_200_response_t *crawl_websites_sync_200_response) {
    if(NULL == crawl_websites_sync_200_response){
        return ;
    }
    listEntry_t *listEntry;
    if (crawl_websites_sync_200_response->message) {
        free(crawl_websites_sync_200_response->message);
        crawl_websites_sync_200_response->message = NULL;
    }
    free(crawl_websites_sync_200_response);
}

cJSON *crawl_websites_sync_200_response_convertToJSON(crawl_websites_sync_200_response_t *crawl_websites_sync_200_response) {
    cJSON *item = cJSON_CreateObject();

    // crawl_websites_sync_200_response->data
    if(crawl_websites_sync_200_response->data) {
    if(cJSON_AddBoolToObject(item, "data", crawl_websites_sync_200_response->data) == NULL) {
    goto fail; //Bool
    }
    }


    // crawl_websites_sync_200_response->message
    if(crawl_websites_sync_200_response->message) {
    if(cJSON_AddStringToObject(item, "message", crawl_websites_sync_200_response->message) == NULL) {
    goto fail; //String
    }
    }

    return item;
fail:
    if (item) {
        cJSON_Delete(item);
    }
    return NULL;
}

crawl_websites_sync_200_response_t *crawl_websites_sync_200_response_parseFromJSON(cJSON *crawl_websites_sync_200_responseJSON){

    crawl_websites_sync_200_response_t *crawl_websites_sync_200_response_local_var = NULL;

    // crawl_websites_sync_200_response->data
    cJSON *data = cJSON_GetObjectItemCaseSensitive(crawl_websites_sync_200_responseJSON, "data");
    if (data) { 
    if(!cJSON_IsBool(data))
    {
    goto end; //Bool
    }
    }

    // crawl_websites_sync_200_response->message
    cJSON *message = cJSON_GetObjectItemCaseSensitive(crawl_websites_sync_200_responseJSON, "message");
    if (message) { 
    if(!cJSON_IsString(message))
    {
    goto end; //String
    }
    }


    crawl_websites_sync_200_response_local_var = crawl_websites_sync_200_response_create (
        data ? data->valueint : 0,
        message ? strdup(message->valuestring) : NULL
        );

    return crawl_websites_sync_200_response_local_var;
end:
    return NULL;

}
