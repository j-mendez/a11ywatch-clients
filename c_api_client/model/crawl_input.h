/*
 * crawl_input.h
 *
 * 
 */

#ifndef _crawl_input_H_
#define _crawl_input_H_

#include <string.h>
#include "../external/cJSON.h"
#include "../include/list.h"
#include "../include/keyValuePair.h"
#include "../include/binary.h"

typedef struct crawl_input_t crawl_input_t;




typedef struct crawl_input_t {
    char *domain; // string
    char *url; // string

} crawl_input_t;

crawl_input_t *crawl_input_create(
    char *domain,
    char *url
);

void crawl_input_free(crawl_input_t *crawl_input);

crawl_input_t *crawl_input_parseFromJSON(cJSON *crawl_inputJSON);

cJSON *crawl_input_convertToJSON(crawl_input_t *crawl_input);

#endif /* _crawl_input_H_ */

