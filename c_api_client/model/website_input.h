/*
 * website_input.h
 *
 * 
 */

#ifndef _website_input_H_
#define _website_input_H_

#include <string.h>
#include "../external/cJSON.h"
#include "../include/list.h"
#include "../include/keyValuePair.h"
#include "../include/binary.h"

typedef struct website_input_t website_input_t;




typedef struct website_input_t {
    char *url; // string
    int mobile; //boolean
    int page_insights; //boolean
    char *ua; // string
    char *standard; // string
    char *robots; // string
    int subdomains; //boolean
    int tld; //boolean
    list_t *custom_headers; //primitive container

} website_input_t;

website_input_t *website_input_create(
    char *url,
    int mobile,
    int page_insights,
    char *ua,
    char *standard,
    char *robots,
    int subdomains,
    int tld,
    list_t *custom_headers
);

void website_input_free(website_input_t *website_input);

website_input_t *website_input_parseFromJSON(cJSON *website_inputJSON);

cJSON *website_input_convertToJSON(website_input_t *website_input);

#endif /* _website_input_H_ */

