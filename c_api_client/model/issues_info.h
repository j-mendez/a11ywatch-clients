/*
 * issues_info.h
 *
 * 
 */

#ifndef _issues_info_H_
#define _issues_info_H_

#include <string.h>
#include "../external/cJSON.h"
#include "../include/list.h"
#include "../include/keyValuePair.h"
#include "../include/binary.h"

typedef struct issues_info_t issues_info_t;




typedef struct issues_info_t {
    long access_score_average; //numeric
    long possible_issues_fixed_by_cdn; //numeric
    long total_issues; //numeric
    long issues_fixed_by_cdn; //numeric
    long error_count; //numeric
    long warning_count; //numeric
    long notice_count; //numeric
    long page_count; //numeric

} issues_info_t;

issues_info_t *issues_info_create(
    long access_score_average,
    long possible_issues_fixed_by_cdn,
    long total_issues,
    long issues_fixed_by_cdn,
    long error_count,
    long warning_count,
    long notice_count,
    long page_count
);

void issues_info_free(issues_info_t *issues_info);

issues_info_t *issues_info_parseFromJSON(cJSON *issues_infoJSON);

cJSON *issues_info_convertToJSON(issues_info_t *issues_info);

#endif /* _issues_info_H_ */

