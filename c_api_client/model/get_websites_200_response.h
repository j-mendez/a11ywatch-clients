/*
 * get_websites_200_response.h
 *
 * 
 */

#ifndef _get_websites_200_response_H_
#define _get_websites_200_response_H_

#include <string.h>
#include "../external/cJSON.h"
#include "../include/list.h"
#include "../include/keyValuePair.h"
#include "../include/binary.h"

typedef struct get_websites_200_response_t get_websites_200_response_t;

#include "websites.h"



typedef struct get_websites_200_response_t {
    list_t *data; //nonprimitive container

} get_websites_200_response_t;

get_websites_200_response_t *get_websites_200_response_create(
    list_t *data
);

void get_websites_200_response_free(get_websites_200_response_t *get_websites_200_response);

get_websites_200_response_t *get_websites_200_response_parseFromJSON(cJSON *get_websites_200_responseJSON);

cJSON *get_websites_200_response_convertToJSON(get_websites_200_response_t *get_websites_200_response);

#endif /* _get_websites_200_response_H_ */

