#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "report.h"



report_t *report_create(
    long _id,
    char *domain,
    char *page_url,
    list_t *issues,
    issues_info_t *issues_info
    ) {
    report_t *report_local_var = malloc(sizeof(report_t));
    if (!report_local_var) {
        return NULL;
    }
    report_local_var->_id = _id;
    report_local_var->domain = domain;
    report_local_var->page_url = page_url;
    report_local_var->issues = issues;
    report_local_var->issues_info = issues_info;

    return report_local_var;
}


void report_free(report_t *report) {
    if(NULL == report){
        return ;
    }
    listEntry_t *listEntry;
    if (report->domain) {
        free(report->domain);
        report->domain = NULL;
    }
    if (report->page_url) {
        free(report->page_url);
        report->page_url = NULL;
    }
    if (report->issues) {
        list_ForEach(listEntry, report->issues) {
            page_issue_free(listEntry->data);
        }
        list_free(report->issues);
        report->issues = NULL;
    }
    if (report->issues_info) {
        issues_info_free(report->issues_info);
        report->issues_info = NULL;
    }
    free(report);
}

cJSON *report_convertToJSON(report_t *report) {
    cJSON *item = cJSON_CreateObject();

    // report->_id
    if(report->_id) { 
    if(cJSON_AddNumberToObject(item, "_id", report->_id) == NULL) {
    goto fail; //Numeric
    }
     } 


    // report->domain
    if(report->domain) { 
    if(cJSON_AddStringToObject(item, "domain", report->domain) == NULL) {
    goto fail; //String
    }
     } 


    // report->page_url
    if(report->page_url) { 
    if(cJSON_AddStringToObject(item, "pageUrl", report->page_url) == NULL) {
    goto fail; //String
    }
     } 


    // report->issues
    if(report->issues) { 
    cJSON *issues = cJSON_AddArrayToObject(item, "issues");
    if(issues == NULL) {
    goto fail; //nonprimitive container
    }

    listEntry_t *issuesListEntry;
    if (report->issues) {
    list_ForEach(issuesListEntry, report->issues) {
    cJSON *itemLocal = page_issue_convertToJSON(issuesListEntry->data);
    if(itemLocal == NULL) {
    goto fail;
    }
    cJSON_AddItemToArray(issues, itemLocal);
    }
    }
     } 


    // report->issues_info
    if(report->issues_info) { 
    cJSON *issues_info_local_JSON = issues_info_convertToJSON(report->issues_info);
    if(issues_info_local_JSON == NULL) {
    goto fail; //model
    }
    cJSON_AddItemToObject(item, "issuesInfo", issues_info_local_JSON);
    if(item->child == NULL) {
    goto fail;
    }
     } 

    return item;
fail:
    if (item) {
        cJSON_Delete(item);
    }
    return NULL;
}

report_t *report_parseFromJSON(cJSON *reportJSON){

    report_t *report_local_var = NULL;

    // report->_id
    cJSON *_id = cJSON_GetObjectItemCaseSensitive(reportJSON, "_id");
    if (_id) { 
    if(!cJSON_IsNumber(_id))
    {
    goto end; //Numeric
    }
    }

    // report->domain
    cJSON *domain = cJSON_GetObjectItemCaseSensitive(reportJSON, "domain");
    if (domain) { 
    if(!cJSON_IsString(domain))
    {
    goto end; //String
    }
    }

    // report->page_url
    cJSON *page_url = cJSON_GetObjectItemCaseSensitive(reportJSON, "pageUrl");
    if (page_url) { 
    if(!cJSON_IsString(page_url))
    {
    goto end; //String
    }
    }

    // report->issues
    cJSON *issues = cJSON_GetObjectItemCaseSensitive(reportJSON, "issues");
    list_t *issuesList;
    if (issues) { 
    cJSON *issues_local_nonprimitive;
    if(!cJSON_IsArray(issues)){
        goto end; //nonprimitive container
    }

    issuesList = list_create();

    cJSON_ArrayForEach(issues_local_nonprimitive,issues )
    {
        if(!cJSON_IsObject(issues_local_nonprimitive)){
            goto end;
        }
        page_issue_t *issuesItem = page_issue_parseFromJSON(issues_local_nonprimitive);

        list_addElement(issuesList, issuesItem);
    }
    }

    // report->issues_info
    cJSON *issues_info = cJSON_GetObjectItemCaseSensitive(reportJSON, "issuesInfo");
    issues_info_t *issues_info_local_nonprim = NULL;
    if (issues_info) { 
    issues_info_local_nonprim = issues_info_parseFromJSON(issues_info); //nonprimitive
    }


    report_local_var = report_create (
        _id ? _id->valuedouble : 0,
        domain ? strdup(domain->valuestring) : NULL,
        page_url ? strdup(page_url->valuestring) : NULL,
        issues ? issuesList : NULL,
        issues_info ? issues_info_local_nonprim : NULL
        );

    return report_local_var;
end:
    if (issues_info_local_nonprim) {
        issues_info_free(issues_info_local_nonprim);
        issues_info_local_nonprim = NULL;
    }
    return NULL;

}
