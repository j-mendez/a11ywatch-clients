#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "get_website_by_domain_200_response.h"



get_website_by_domain_200_response_t *get_website_by_domain_200_response_create(
    websites_t *data
    ) {
    get_website_by_domain_200_response_t *get_website_by_domain_200_response_local_var = malloc(sizeof(get_website_by_domain_200_response_t));
    if (!get_website_by_domain_200_response_local_var) {
        return NULL;
    }
    get_website_by_domain_200_response_local_var->data = data;

    return get_website_by_domain_200_response_local_var;
}


void get_website_by_domain_200_response_free(get_website_by_domain_200_response_t *get_website_by_domain_200_response) {
    if(NULL == get_website_by_domain_200_response){
        return ;
    }
    listEntry_t *listEntry;
    if (get_website_by_domain_200_response->data) {
        websites_free(get_website_by_domain_200_response->data);
        get_website_by_domain_200_response->data = NULL;
    }
    free(get_website_by_domain_200_response);
}

cJSON *get_website_by_domain_200_response_convertToJSON(get_website_by_domain_200_response_t *get_website_by_domain_200_response) {
    cJSON *item = cJSON_CreateObject();

    // get_website_by_domain_200_response->data
    if(get_website_by_domain_200_response->data) {
    cJSON *data_local_JSON = websites_convertToJSON(get_website_by_domain_200_response->data);
    if(data_local_JSON == NULL) {
    goto fail; //model
    }
    cJSON_AddItemToObject(item, "data", data_local_JSON);
    if(item->child == NULL) {
    goto fail;
    }
    }

    return item;
fail:
    if (item) {
        cJSON_Delete(item);
    }
    return NULL;
}

get_website_by_domain_200_response_t *get_website_by_domain_200_response_parseFromJSON(cJSON *get_website_by_domain_200_responseJSON){

    get_website_by_domain_200_response_t *get_website_by_domain_200_response_local_var = NULL;

    // define the local variable for get_website_by_domain_200_response->data
    websites_t *data_local_nonprim = NULL;

    // get_website_by_domain_200_response->data
    cJSON *data = cJSON_GetObjectItemCaseSensitive(get_website_by_domain_200_responseJSON, "data");
    if (data) { 
    data_local_nonprim = websites_parseFromJSON(data); //nonprimitive
    }


    get_website_by_domain_200_response_local_var = get_website_by_domain_200_response_create (
        data ? data_local_nonprim : NULL
        );

    return get_website_by_domain_200_response_local_var;
end:
    if (data_local_nonprim) {
        websites_free(data_local_nonprim);
        data_local_nonprim = NULL;
    }
    return NULL;

}
