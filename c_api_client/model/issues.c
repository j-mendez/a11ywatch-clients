#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "issues.h"



issues_t *issues_create(
    long _id,
    long user_id,
    char *domain,
    char *page_url,
    page_issue_t *issues
    ) {
    issues_t *issues_local_var = malloc(sizeof(issues_t));
    if (!issues_local_var) {
        return NULL;
    }
    issues_local_var->_id = _id;
    issues_local_var->user_id = user_id;
    issues_local_var->domain = domain;
    issues_local_var->page_url = page_url;
    issues_local_var->issues = issues;

    return issues_local_var;
}


void issues_free(issues_t *issues) {
    if(NULL == issues){
        return ;
    }
    listEntry_t *listEntry;
    if (issues->domain) {
        free(issues->domain);
        issues->domain = NULL;
    }
    if (issues->page_url) {
        free(issues->page_url);
        issues->page_url = NULL;
    }
    if (issues->issues) {
        page_issue_free(issues->issues);
        issues->issues = NULL;
    }
    free(issues);
}

cJSON *issues_convertToJSON(issues_t *issues) {
    cJSON *item = cJSON_CreateObject();

    // issues->_id
    if(issues->_id) { 
    if(cJSON_AddNumberToObject(item, "_id", issues->_id) == NULL) {
    goto fail; //Numeric
    }
     } 


    // issues->user_id
    if(issues->user_id) { 
    if(cJSON_AddNumberToObject(item, "userId", issues->user_id) == NULL) {
    goto fail; //Numeric
    }
     } 


    // issues->domain
    if(issues->domain) { 
    if(cJSON_AddStringToObject(item, "domain", issues->domain) == NULL) {
    goto fail; //String
    }
     } 


    // issues->page_url
    if(issues->page_url) { 
    if(cJSON_AddStringToObject(item, "pageUrl", issues->page_url) == NULL) {
    goto fail; //String
    }
     } 


    // issues->issues
    if(issues->issues) { 
    cJSON *issues_local_JSON = page_issue_convertToJSON(issues->issues);
    if(issues_local_JSON == NULL) {
    goto fail; //model
    }
    cJSON_AddItemToObject(item, "issues", issues_local_JSON);
    if(item->child == NULL) {
    goto fail;
    }
     } 

    return item;
fail:
    if (item) {
        cJSON_Delete(item);
    }
    return NULL;
}

issues_t *issues_parseFromJSON(cJSON *issuesJSON){

    issues_t *issues_local_var = NULL;

    // issues->_id
    cJSON *_id = cJSON_GetObjectItemCaseSensitive(issuesJSON, "_id");
    if (_id) { 
    if(!cJSON_IsNumber(_id))
    {
    goto end; //Numeric
    }
    }

    // issues->user_id
    cJSON *user_id = cJSON_GetObjectItemCaseSensitive(issuesJSON, "userId");
    if (user_id) { 
    if(!cJSON_IsNumber(user_id))
    {
    goto end; //Numeric
    }
    }

    // issues->domain
    cJSON *domain = cJSON_GetObjectItemCaseSensitive(issuesJSON, "domain");
    if (domain) { 
    if(!cJSON_IsString(domain))
    {
    goto end; //String
    }
    }

    // issues->page_url
    cJSON *page_url = cJSON_GetObjectItemCaseSensitive(issuesJSON, "pageUrl");
    if (page_url) { 
    if(!cJSON_IsString(page_url))
    {
    goto end; //String
    }
    }

    // issues->issues
    cJSON *issues = cJSON_GetObjectItemCaseSensitive(issuesJSON, "issues");
    page_issue_t *issues_local_nonprim = NULL;
    if (issues) { 
    issues_local_nonprim = page_issue_parseFromJSON(issues); //nonprimitive
    }


    issues_local_var = issues_create (
        _id ? _id->valuedouble : 0,
        user_id ? user_id->valuedouble : 0,
        domain ? strdup(domain->valuestring) : NULL,
        page_url ? strdup(page_url->valuestring) : NULL,
        issues ? issues_local_nonprim : NULL
        );

    return issues_local_var;
end:
    if (issues_local_nonprim) {
        page_issue_free(issues_local_nonprim);
        issues_local_nonprim = NULL;
    }
    return NULL;

}
