/*
 * page_issue.h
 *
 * 
 */

#ifndef _page_issue_H_
#define _page_issue_H_

#include <string.h>
#include "../external/cJSON.h"
#include "../include/list.h"
#include "../include/keyValuePair.h"
#include "../include/binary.h"

typedef struct page_issue_t page_issue_t;




typedef struct page_issue_t {
    long recurrence; //numeric
    long type_code; //numeric
    char *code; // string
    char *message; // string
    char *context; // string
    char *selector; // string
    char *runner; // string
    char *type; // string

} page_issue_t;

page_issue_t *page_issue_create(
    long recurrence,
    long type_code,
    char *code,
    char *message,
    char *context,
    char *selector,
    char *runner,
    char *type
);

void page_issue_free(page_issue_t *page_issue);

page_issue_t *page_issue_parseFromJSON(cJSON *page_issueJSON);

cJSON *page_issue_convertToJSON(page_issue_t *page_issue);

#endif /* _page_issue_H_ */

