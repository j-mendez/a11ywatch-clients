#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "get_pages_200_response.h"



get_pages_200_response_t *get_pages_200_response_create(
    list_t *data
    ) {
    get_pages_200_response_t *get_pages_200_response_local_var = malloc(sizeof(get_pages_200_response_t));
    if (!get_pages_200_response_local_var) {
        return NULL;
    }
    get_pages_200_response_local_var->data = data;

    return get_pages_200_response_local_var;
}


void get_pages_200_response_free(get_pages_200_response_t *get_pages_200_response) {
    if(NULL == get_pages_200_response){
        return ;
    }
    listEntry_t *listEntry;
    if (get_pages_200_response->data) {
        list_ForEach(listEntry, get_pages_200_response->data) {
            pages_free(listEntry->data);
        }
        list_freeList(get_pages_200_response->data);
        get_pages_200_response->data = NULL;
    }
    free(get_pages_200_response);
}

cJSON *get_pages_200_response_convertToJSON(get_pages_200_response_t *get_pages_200_response) {
    cJSON *item = cJSON_CreateObject();

    // get_pages_200_response->data
    if(get_pages_200_response->data) {
    cJSON *data = cJSON_AddArrayToObject(item, "data");
    if(data == NULL) {
    goto fail; //nonprimitive container
    }

    listEntry_t *dataListEntry;
    if (get_pages_200_response->data) {
    list_ForEach(dataListEntry, get_pages_200_response->data) {
    cJSON *itemLocal = pages_convertToJSON(dataListEntry->data);
    if(itemLocal == NULL) {
    goto fail;
    }
    cJSON_AddItemToArray(data, itemLocal);
    }
    }
    }

    return item;
fail:
    if (item) {
        cJSON_Delete(item);
    }
    return NULL;
}

get_pages_200_response_t *get_pages_200_response_parseFromJSON(cJSON *get_pages_200_responseJSON){

    get_pages_200_response_t *get_pages_200_response_local_var = NULL;

    // define the local list for get_pages_200_response->data
    list_t *dataList = NULL;

    // get_pages_200_response->data
    cJSON *data = cJSON_GetObjectItemCaseSensitive(get_pages_200_responseJSON, "data");
    if (data) { 
    cJSON *data_local_nonprimitive = NULL;
    if(!cJSON_IsArray(data)){
        goto end; //nonprimitive container
    }

    dataList = list_createList();

    cJSON_ArrayForEach(data_local_nonprimitive,data )
    {
        if(!cJSON_IsObject(data_local_nonprimitive)){
            goto end;
        }
        pages_t *dataItem = pages_parseFromJSON(data_local_nonprimitive);

        list_addElement(dataList, dataItem);
    }
    }


    get_pages_200_response_local_var = get_pages_200_response_create (
        data ? dataList : NULL
        );

    return get_pages_200_response_local_var;
end:
    if (dataList) {
        listEntry_t *listEntry = NULL;
        list_ForEach(listEntry, dataList) {
            pages_free(listEntry->data);
            listEntry->data = NULL;
        }
        list_freeList(dataList);
        dataList = NULL;
    }
    return NULL;

}
