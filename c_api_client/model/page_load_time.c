#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "page_load_time.h"



page_load_time_t *page_load_time_create(
    double duration,
    char *duration_formated,
    char *color
    ) {
    page_load_time_t *page_load_time_local_var = malloc(sizeof(page_load_time_t));
    if (!page_load_time_local_var) {
        return NULL;
    }
    page_load_time_local_var->duration = duration;
    page_load_time_local_var->duration_formated = duration_formated;
    page_load_time_local_var->color = color;

    return page_load_time_local_var;
}


void page_load_time_free(page_load_time_t *page_load_time) {
    if(NULL == page_load_time){
        return ;
    }
    listEntry_t *listEntry;
    if (page_load_time->duration_formated) {
        free(page_load_time->duration_formated);
        page_load_time->duration_formated = NULL;
    }
    if (page_load_time->color) {
        free(page_load_time->color);
        page_load_time->color = NULL;
    }
    free(page_load_time);
}

cJSON *page_load_time_convertToJSON(page_load_time_t *page_load_time) {
    cJSON *item = cJSON_CreateObject();

    // page_load_time->duration
    if(page_load_time->duration) { 
    if(cJSON_AddNumberToObject(item, "duration", page_load_time->duration) == NULL) {
    goto fail; //Numeric
    }
     } 


    // page_load_time->duration_formated
    if(page_load_time->duration_formated) { 
    if(cJSON_AddStringToObject(item, "durationFormated", page_load_time->duration_formated) == NULL) {
    goto fail; //String
    }
     } 


    // page_load_time->color
    if(page_load_time->color) { 
    if(cJSON_AddStringToObject(item, "color", page_load_time->color) == NULL) {
    goto fail; //String
    }
     } 

    return item;
fail:
    if (item) {
        cJSON_Delete(item);
    }
    return NULL;
}

page_load_time_t *page_load_time_parseFromJSON(cJSON *page_load_timeJSON){

    page_load_time_t *page_load_time_local_var = NULL;

    // page_load_time->duration
    cJSON *duration = cJSON_GetObjectItemCaseSensitive(page_load_timeJSON, "duration");
    if (duration) { 
    if(!cJSON_IsNumber(duration))
    {
    goto end; //Numeric
    }
    }

    // page_load_time->duration_formated
    cJSON *duration_formated = cJSON_GetObjectItemCaseSensitive(page_load_timeJSON, "durationFormated");
    if (duration_formated) { 
    if(!cJSON_IsString(duration_formated))
    {
    goto end; //String
    }
    }

    // page_load_time->color
    cJSON *color = cJSON_GetObjectItemCaseSensitive(page_load_timeJSON, "color");
    if (color) { 
    if(!cJSON_IsString(color))
    {
    goto end; //String
    }
    }


    page_load_time_local_var = page_load_time_create (
        duration ? duration->valuedouble : 0,
        duration_formated ? strdup(duration_formated->valuestring) : NULL,
        color ? strdup(color->valuestring) : NULL
        );

    return page_load_time_local_var;
end:
    return NULL;

}
