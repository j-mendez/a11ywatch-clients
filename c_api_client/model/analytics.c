#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "analytics.h"



analytics_t *analytics_create(
    long _id,
    char *domain,
    char *page_url,
    long user_id,
    long access_score,
    long possible_issues_fixed_by_cdn,
    long total_issues,
    long issues_fixed_by_cdn,
    long error_count,
    long warning_count,
    long notice_count
    ) {
    analytics_t *analytics_local_var = malloc(sizeof(analytics_t));
    if (!analytics_local_var) {
        return NULL;
    }
    analytics_local_var->_id = _id;
    analytics_local_var->domain = domain;
    analytics_local_var->page_url = page_url;
    analytics_local_var->user_id = user_id;
    analytics_local_var->access_score = access_score;
    analytics_local_var->possible_issues_fixed_by_cdn = possible_issues_fixed_by_cdn;
    analytics_local_var->total_issues = total_issues;
    analytics_local_var->issues_fixed_by_cdn = issues_fixed_by_cdn;
    analytics_local_var->error_count = error_count;
    analytics_local_var->warning_count = warning_count;
    analytics_local_var->notice_count = notice_count;

    return analytics_local_var;
}


void analytics_free(analytics_t *analytics) {
    if(NULL == analytics){
        return ;
    }
    listEntry_t *listEntry;
    if (analytics->domain) {
        free(analytics->domain);
        analytics->domain = NULL;
    }
    if (analytics->page_url) {
        free(analytics->page_url);
        analytics->page_url = NULL;
    }
    free(analytics);
}

cJSON *analytics_convertToJSON(analytics_t *analytics) {
    cJSON *item = cJSON_CreateObject();

    // analytics->_id
    if(analytics->_id) { 
    if(cJSON_AddNumberToObject(item, "_id", analytics->_id) == NULL) {
    goto fail; //Numeric
    }
     } 


    // analytics->domain
    if(analytics->domain) { 
    if(cJSON_AddStringToObject(item, "domain", analytics->domain) == NULL) {
    goto fail; //String
    }
     } 


    // analytics->page_url
    if(analytics->page_url) { 
    if(cJSON_AddStringToObject(item, "pageUrl", analytics->page_url) == NULL) {
    goto fail; //String
    }
     } 


    // analytics->user_id
    if(analytics->user_id) { 
    if(cJSON_AddNumberToObject(item, "userId", analytics->user_id) == NULL) {
    goto fail; //Numeric
    }
     } 


    // analytics->access_score
    if(analytics->access_score) { 
    if(cJSON_AddNumberToObject(item, "accessScore", analytics->access_score) == NULL) {
    goto fail; //Numeric
    }
     } 


    // analytics->possible_issues_fixed_by_cdn
    if(analytics->possible_issues_fixed_by_cdn) { 
    if(cJSON_AddNumberToObject(item, "possibleIssuesFixedByCdn", analytics->possible_issues_fixed_by_cdn) == NULL) {
    goto fail; //Numeric
    }
     } 


    // analytics->total_issues
    if(analytics->total_issues) { 
    if(cJSON_AddNumberToObject(item, "totalIssues", analytics->total_issues) == NULL) {
    goto fail; //Numeric
    }
     } 


    // analytics->issues_fixed_by_cdn
    if(analytics->issues_fixed_by_cdn) { 
    if(cJSON_AddNumberToObject(item, "issuesFixedByCdn", analytics->issues_fixed_by_cdn) == NULL) {
    goto fail; //Numeric
    }
     } 


    // analytics->error_count
    if(analytics->error_count) { 
    if(cJSON_AddNumberToObject(item, "errorCount", analytics->error_count) == NULL) {
    goto fail; //Numeric
    }
     } 


    // analytics->warning_count
    if(analytics->warning_count) { 
    if(cJSON_AddNumberToObject(item, "warningCount", analytics->warning_count) == NULL) {
    goto fail; //Numeric
    }
     } 


    // analytics->notice_count
    if(analytics->notice_count) { 
    if(cJSON_AddNumberToObject(item, "noticeCount", analytics->notice_count) == NULL) {
    goto fail; //Numeric
    }
     } 

    return item;
fail:
    if (item) {
        cJSON_Delete(item);
    }
    return NULL;
}

analytics_t *analytics_parseFromJSON(cJSON *analyticsJSON){

    analytics_t *analytics_local_var = NULL;

    // analytics->_id
    cJSON *_id = cJSON_GetObjectItemCaseSensitive(analyticsJSON, "_id");
    if (_id) { 
    if(!cJSON_IsNumber(_id))
    {
    goto end; //Numeric
    }
    }

    // analytics->domain
    cJSON *domain = cJSON_GetObjectItemCaseSensitive(analyticsJSON, "domain");
    if (domain) { 
    if(!cJSON_IsString(domain))
    {
    goto end; //String
    }
    }

    // analytics->page_url
    cJSON *page_url = cJSON_GetObjectItemCaseSensitive(analyticsJSON, "pageUrl");
    if (page_url) { 
    if(!cJSON_IsString(page_url))
    {
    goto end; //String
    }
    }

    // analytics->user_id
    cJSON *user_id = cJSON_GetObjectItemCaseSensitive(analyticsJSON, "userId");
    if (user_id) { 
    if(!cJSON_IsNumber(user_id))
    {
    goto end; //Numeric
    }
    }

    // analytics->access_score
    cJSON *access_score = cJSON_GetObjectItemCaseSensitive(analyticsJSON, "accessScore");
    if (access_score) { 
    if(!cJSON_IsNumber(access_score))
    {
    goto end; //Numeric
    }
    }

    // analytics->possible_issues_fixed_by_cdn
    cJSON *possible_issues_fixed_by_cdn = cJSON_GetObjectItemCaseSensitive(analyticsJSON, "possibleIssuesFixedByCdn");
    if (possible_issues_fixed_by_cdn) { 
    if(!cJSON_IsNumber(possible_issues_fixed_by_cdn))
    {
    goto end; //Numeric
    }
    }

    // analytics->total_issues
    cJSON *total_issues = cJSON_GetObjectItemCaseSensitive(analyticsJSON, "totalIssues");
    if (total_issues) { 
    if(!cJSON_IsNumber(total_issues))
    {
    goto end; //Numeric
    }
    }

    // analytics->issues_fixed_by_cdn
    cJSON *issues_fixed_by_cdn = cJSON_GetObjectItemCaseSensitive(analyticsJSON, "issuesFixedByCdn");
    if (issues_fixed_by_cdn) { 
    if(!cJSON_IsNumber(issues_fixed_by_cdn))
    {
    goto end; //Numeric
    }
    }

    // analytics->error_count
    cJSON *error_count = cJSON_GetObjectItemCaseSensitive(analyticsJSON, "errorCount");
    if (error_count) { 
    if(!cJSON_IsNumber(error_count))
    {
    goto end; //Numeric
    }
    }

    // analytics->warning_count
    cJSON *warning_count = cJSON_GetObjectItemCaseSensitive(analyticsJSON, "warningCount");
    if (warning_count) { 
    if(!cJSON_IsNumber(warning_count))
    {
    goto end; //Numeric
    }
    }

    // analytics->notice_count
    cJSON *notice_count = cJSON_GetObjectItemCaseSensitive(analyticsJSON, "noticeCount");
    if (notice_count) { 
    if(!cJSON_IsNumber(notice_count))
    {
    goto end; //Numeric
    }
    }


    analytics_local_var = analytics_create (
        _id ? _id->valuedouble : 0,
        domain ? strdup(domain->valuestring) : NULL,
        page_url ? strdup(page_url->valuestring) : NULL,
        user_id ? user_id->valuedouble : 0,
        access_score ? access_score->valuedouble : 0,
        possible_issues_fixed_by_cdn ? possible_issues_fixed_by_cdn->valuedouble : 0,
        total_issues ? total_issues->valuedouble : 0,
        issues_fixed_by_cdn ? issues_fixed_by_cdn->valuedouble : 0,
        error_count ? error_count->valuedouble : 0,
        warning_count ? warning_count->valuedouble : 0,
        notice_count ? notice_count->valuedouble : 0
        );

    return analytics_local_var;
end:
    return NULL;

}
