/*
 * scan_website_simple_200_response.h
 *
 * 
 */

#ifndef _scan_website_simple_200_response_H_
#define _scan_website_simple_200_response_H_

#include <string.h>
#include "../external/cJSON.h"
#include "../include/list.h"
#include "../include/keyValuePair.h"
#include "../include/binary.h"

typedef struct scan_website_simple_200_response_t scan_website_simple_200_response_t;

#include "report.h"



typedef struct scan_website_simple_200_response_t {
    struct report_t *data; //model

} scan_website_simple_200_response_t;

scan_website_simple_200_response_t *scan_website_simple_200_response_create(
    report_t *data
);

void scan_website_simple_200_response_free(scan_website_simple_200_response_t *scan_website_simple_200_response);

scan_website_simple_200_response_t *scan_website_simple_200_response_parseFromJSON(cJSON *scan_website_simple_200_responseJSON);

cJSON *scan_website_simple_200_response_convertToJSON(scan_website_simple_200_response_t *scan_website_simple_200_response);

#endif /* _scan_website_simple_200_response_H_ */

