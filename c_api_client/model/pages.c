#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "pages.h"



pages_t *pages_create(
    long _id,
    long user_id,
    char *domain,
    char *url,
    int cdn_connected,
    int online,
    page_load_time_t *page_load_time,
    websites_insight_t *insight,
    issues_info_t *issues_info,
    char *last_scan_date
    ) {
    pages_t *pages_local_var = malloc(sizeof(pages_t));
    if (!pages_local_var) {
        return NULL;
    }
    pages_local_var->_id = _id;
    pages_local_var->user_id = user_id;
    pages_local_var->domain = domain;
    pages_local_var->url = url;
    pages_local_var->cdn_connected = cdn_connected;
    pages_local_var->online = online;
    pages_local_var->page_load_time = page_load_time;
    pages_local_var->insight = insight;
    pages_local_var->issues_info = issues_info;
    pages_local_var->last_scan_date = last_scan_date;

    return pages_local_var;
}


void pages_free(pages_t *pages) {
    if(NULL == pages){
        return ;
    }
    listEntry_t *listEntry;
    if (pages->domain) {
        free(pages->domain);
        pages->domain = NULL;
    }
    if (pages->url) {
        free(pages->url);
        pages->url = NULL;
    }
    if (pages->page_load_time) {
        page_load_time_free(pages->page_load_time);
        pages->page_load_time = NULL;
    }
    if (pages->insight) {
        websites_insight_free(pages->insight);
        pages->insight = NULL;
    }
    if (pages->issues_info) {
        issues_info_free(pages->issues_info);
        pages->issues_info = NULL;
    }
    if (pages->last_scan_date) {
        free(pages->last_scan_date);
        pages->last_scan_date = NULL;
    }
    free(pages);
}

cJSON *pages_convertToJSON(pages_t *pages) {
    cJSON *item = cJSON_CreateObject();

    // pages->_id
    if(pages->_id) { 
    if(cJSON_AddNumberToObject(item, "_id", pages->_id) == NULL) {
    goto fail; //Numeric
    }
     } 


    // pages->user_id
    if(pages->user_id) { 
    if(cJSON_AddNumberToObject(item, "userId", pages->user_id) == NULL) {
    goto fail; //Numeric
    }
     } 


    // pages->domain
    if(pages->domain) { 
    if(cJSON_AddStringToObject(item, "domain", pages->domain) == NULL) {
    goto fail; //String
    }
     } 


    // pages->url
    if(pages->url) { 
    if(cJSON_AddStringToObject(item, "url", pages->url) == NULL) {
    goto fail; //String
    }
     } 


    // pages->cdn_connected
    if(pages->cdn_connected) { 
    if(cJSON_AddBoolToObject(item, "cdnConnected", pages->cdn_connected) == NULL) {
    goto fail; //Bool
    }
     } 


    // pages->online
    if(pages->online) { 
    if(cJSON_AddBoolToObject(item, "online", pages->online) == NULL) {
    goto fail; //Bool
    }
     } 


    // pages->page_load_time
    if(pages->page_load_time) { 
    cJSON *page_load_time_local_JSON = page_load_time_convertToJSON(pages->page_load_time);
    if(page_load_time_local_JSON == NULL) {
    goto fail; //model
    }
    cJSON_AddItemToObject(item, "pageLoadTime", page_load_time_local_JSON);
    if(item->child == NULL) {
    goto fail;
    }
     } 


    // pages->insight
    if(pages->insight) { 
    cJSON *insight_local_JSON = websites_insight_convertToJSON(pages->insight);
    if(insight_local_JSON == NULL) {
    goto fail; //model
    }
    cJSON_AddItemToObject(item, "insight", insight_local_JSON);
    if(item->child == NULL) {
    goto fail;
    }
     } 


    // pages->issues_info
    if(pages->issues_info) { 
    cJSON *issues_info_local_JSON = issues_info_convertToJSON(pages->issues_info);
    if(issues_info_local_JSON == NULL) {
    goto fail; //model
    }
    cJSON_AddItemToObject(item, "issuesInfo", issues_info_local_JSON);
    if(item->child == NULL) {
    goto fail;
    }
     } 


    // pages->last_scan_date
    if(pages->last_scan_date) { 
    if(cJSON_AddStringToObject(item, "lastScanDate", pages->last_scan_date) == NULL) {
    goto fail; //String
    }
     } 

    return item;
fail:
    if (item) {
        cJSON_Delete(item);
    }
    return NULL;
}

pages_t *pages_parseFromJSON(cJSON *pagesJSON){

    pages_t *pages_local_var = NULL;

    // pages->_id
    cJSON *_id = cJSON_GetObjectItemCaseSensitive(pagesJSON, "_id");
    if (_id) { 
    if(!cJSON_IsNumber(_id))
    {
    goto end; //Numeric
    }
    }

    // pages->user_id
    cJSON *user_id = cJSON_GetObjectItemCaseSensitive(pagesJSON, "userId");
    if (user_id) { 
    if(!cJSON_IsNumber(user_id))
    {
    goto end; //Numeric
    }
    }

    // pages->domain
    cJSON *domain = cJSON_GetObjectItemCaseSensitive(pagesJSON, "domain");
    if (domain) { 
    if(!cJSON_IsString(domain))
    {
    goto end; //String
    }
    }

    // pages->url
    cJSON *url = cJSON_GetObjectItemCaseSensitive(pagesJSON, "url");
    if (url) { 
    if(!cJSON_IsString(url))
    {
    goto end; //String
    }
    }

    // pages->cdn_connected
    cJSON *cdn_connected = cJSON_GetObjectItemCaseSensitive(pagesJSON, "cdnConnected");
    if (cdn_connected) { 
    if(!cJSON_IsBool(cdn_connected))
    {
    goto end; //Bool
    }
    }

    // pages->online
    cJSON *online = cJSON_GetObjectItemCaseSensitive(pagesJSON, "online");
    if (online) { 
    if(!cJSON_IsBool(online))
    {
    goto end; //Bool
    }
    }

    // pages->page_load_time
    cJSON *page_load_time = cJSON_GetObjectItemCaseSensitive(pagesJSON, "pageLoadTime");
    page_load_time_t *page_load_time_local_nonprim = NULL;
    if (page_load_time) { 
    page_load_time_local_nonprim = page_load_time_parseFromJSON(page_load_time); //nonprimitive
    }

    // pages->insight
    cJSON *insight = cJSON_GetObjectItemCaseSensitive(pagesJSON, "insight");
    websites_insight_t *insight_local_nonprim = NULL;
    if (insight) { 
    insight_local_nonprim = websites_insight_parseFromJSON(insight); //nonprimitive
    }

    // pages->issues_info
    cJSON *issues_info = cJSON_GetObjectItemCaseSensitive(pagesJSON, "issuesInfo");
    issues_info_t *issues_info_local_nonprim = NULL;
    if (issues_info) { 
    issues_info_local_nonprim = issues_info_parseFromJSON(issues_info); //nonprimitive
    }

    // pages->last_scan_date
    cJSON *last_scan_date = cJSON_GetObjectItemCaseSensitive(pagesJSON, "lastScanDate");
    if (last_scan_date) { 
    if(!cJSON_IsString(last_scan_date))
    {
    goto end; //String
    }
    }


    pages_local_var = pages_create (
        _id ? _id->valuedouble : 0,
        user_id ? user_id->valuedouble : 0,
        domain ? strdup(domain->valuestring) : NULL,
        url ? strdup(url->valuestring) : NULL,
        cdn_connected ? cdn_connected->valueint : 0,
        online ? online->valueint : 0,
        page_load_time ? page_load_time_local_nonprim : NULL,
        insight ? insight_local_nonprim : NULL,
        issues_info ? issues_info_local_nonprim : NULL,
        last_scan_date ? strdup(last_scan_date->valuestring) : NULL
        );

    return pages_local_var;
end:
    if (page_load_time_local_nonprim) {
        page_load_time_free(page_load_time_local_nonprim);
        page_load_time_local_nonprim = NULL;
    }
    if (insight_local_nonprim) {
        websites_insight_free(insight_local_nonprim);
        insight_local_nonprim = NULL;
    }
    if (issues_info_local_nonprim) {
        issues_info_free(issues_info_local_nonprim);
        issues_info_local_nonprim = NULL;
    }
    return NULL;

}
