#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "page_issue.h"



page_issue_t *page_issue_create(
    long recurrence,
    long type_code,
    char *code,
    char *message,
    char *context,
    char *selector,
    char *runner,
    char *type
    ) {
    page_issue_t *page_issue_local_var = malloc(sizeof(page_issue_t));
    if (!page_issue_local_var) {
        return NULL;
    }
    page_issue_local_var->recurrence = recurrence;
    page_issue_local_var->type_code = type_code;
    page_issue_local_var->code = code;
    page_issue_local_var->message = message;
    page_issue_local_var->context = context;
    page_issue_local_var->selector = selector;
    page_issue_local_var->runner = runner;
    page_issue_local_var->type = type;

    return page_issue_local_var;
}


void page_issue_free(page_issue_t *page_issue) {
    if(NULL == page_issue){
        return ;
    }
    listEntry_t *listEntry;
    if (page_issue->code) {
        free(page_issue->code);
        page_issue->code = NULL;
    }
    if (page_issue->message) {
        free(page_issue->message);
        page_issue->message = NULL;
    }
    if (page_issue->context) {
        free(page_issue->context);
        page_issue->context = NULL;
    }
    if (page_issue->selector) {
        free(page_issue->selector);
        page_issue->selector = NULL;
    }
    if (page_issue->runner) {
        free(page_issue->runner);
        page_issue->runner = NULL;
    }
    if (page_issue->type) {
        free(page_issue->type);
        page_issue->type = NULL;
    }
    free(page_issue);
}

cJSON *page_issue_convertToJSON(page_issue_t *page_issue) {
    cJSON *item = cJSON_CreateObject();

    // page_issue->recurrence
    if(page_issue->recurrence) { 
    if(cJSON_AddNumberToObject(item, "recurrence", page_issue->recurrence) == NULL) {
    goto fail; //Numeric
    }
     } 


    // page_issue->type_code
    if(page_issue->type_code) { 
    if(cJSON_AddNumberToObject(item, "typeCode", page_issue->type_code) == NULL) {
    goto fail; //Numeric
    }
     } 


    // page_issue->code
    if(page_issue->code) { 
    if(cJSON_AddStringToObject(item, "code", page_issue->code) == NULL) {
    goto fail; //String
    }
     } 


    // page_issue->message
    if(page_issue->message) { 
    if(cJSON_AddStringToObject(item, "message", page_issue->message) == NULL) {
    goto fail; //String
    }
     } 


    // page_issue->context
    if(page_issue->context) { 
    if(cJSON_AddStringToObject(item, "context", page_issue->context) == NULL) {
    goto fail; //String
    }
     } 


    // page_issue->selector
    if(page_issue->selector) { 
    if(cJSON_AddStringToObject(item, "selector", page_issue->selector) == NULL) {
    goto fail; //String
    }
     } 


    // page_issue->runner
    if(page_issue->runner) { 
    if(cJSON_AddStringToObject(item, "runner", page_issue->runner) == NULL) {
    goto fail; //String
    }
     } 


    // page_issue->type
    if(page_issue->type) { 
    if(cJSON_AddStringToObject(item, "type", page_issue->type) == NULL) {
    goto fail; //String
    }
     } 

    return item;
fail:
    if (item) {
        cJSON_Delete(item);
    }
    return NULL;
}

page_issue_t *page_issue_parseFromJSON(cJSON *page_issueJSON){

    page_issue_t *page_issue_local_var = NULL;

    // page_issue->recurrence
    cJSON *recurrence = cJSON_GetObjectItemCaseSensitive(page_issueJSON, "recurrence");
    if (recurrence) { 
    if(!cJSON_IsNumber(recurrence))
    {
    goto end; //Numeric
    }
    }

    // page_issue->type_code
    cJSON *type_code = cJSON_GetObjectItemCaseSensitive(page_issueJSON, "typeCode");
    if (type_code) { 
    if(!cJSON_IsNumber(type_code))
    {
    goto end; //Numeric
    }
    }

    // page_issue->code
    cJSON *code = cJSON_GetObjectItemCaseSensitive(page_issueJSON, "code");
    if (code) { 
    if(!cJSON_IsString(code))
    {
    goto end; //String
    }
    }

    // page_issue->message
    cJSON *message = cJSON_GetObjectItemCaseSensitive(page_issueJSON, "message");
    if (message) { 
    if(!cJSON_IsString(message))
    {
    goto end; //String
    }
    }

    // page_issue->context
    cJSON *context = cJSON_GetObjectItemCaseSensitive(page_issueJSON, "context");
    if (context) { 
    if(!cJSON_IsString(context))
    {
    goto end; //String
    }
    }

    // page_issue->selector
    cJSON *selector = cJSON_GetObjectItemCaseSensitive(page_issueJSON, "selector");
    if (selector) { 
    if(!cJSON_IsString(selector))
    {
    goto end; //String
    }
    }

    // page_issue->runner
    cJSON *runner = cJSON_GetObjectItemCaseSensitive(page_issueJSON, "runner");
    if (runner) { 
    if(!cJSON_IsString(runner))
    {
    goto end; //String
    }
    }

    // page_issue->type
    cJSON *type = cJSON_GetObjectItemCaseSensitive(page_issueJSON, "type");
    if (type) { 
    if(!cJSON_IsString(type))
    {
    goto end; //String
    }
    }


    page_issue_local_var = page_issue_create (
        recurrence ? recurrence->valuedouble : 0,
        type_code ? type_code->valuedouble : 0,
        code ? strdup(code->valuestring) : NULL,
        message ? strdup(message->valuestring) : NULL,
        context ? strdup(context->valuestring) : NULL,
        selector ? strdup(selector->valuestring) : NULL,
        runner ? strdup(runner->valuestring) : NULL,
        type ? strdup(type->valuestring) : NULL
        );

    return page_issue_local_var;
end:
    return NULL;

}
