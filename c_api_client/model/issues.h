/*
 * issues.h
 *
 * 
 */

#ifndef _issues_H_
#define _issues_H_

#include <string.h>
#include "../external/cJSON.h"
#include "../include/list.h"
#include "../include/keyValuePair.h"
#include "../include/binary.h"

typedef struct issues_t issues_t;

#include "page_issue.h"



typedef struct issues_t {
    long _id; //numeric
    long user_id; //numeric
    char *domain; // string
    char *page_url; // string
    struct page_issue_t *issues; //model

} issues_t;

issues_t *issues_create(
    long _id,
    long user_id,
    char *domain,
    char *page_url,
    page_issue_t *issues
);

void issues_free(issues_t *issues);

issues_t *issues_parseFromJSON(cJSON *issuesJSON);

cJSON *issues_convertToJSON(issues_t *issues);

#endif /* _issues_H_ */

