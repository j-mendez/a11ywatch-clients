/*
 * report.h
 *
 * 
 */

#ifndef _report_H_
#define _report_H_

#include <string.h>
#include "../external/cJSON.h"
#include "../include/list.h"
#include "../include/keyValuePair.h"
#include "../include/binary.h"

typedef struct report_t report_t;

#include "issues_info.h"
#include "page_issue.h"



typedef struct report_t {
    long _id; //numeric
    char *domain; // string
    char *page_url; // string
    list_t *issues; //nonprimitive container
    struct issues_info_t *issues_info; //model

} report_t;

report_t *report_create(
    long _id,
    char *domain,
    char *page_url,
    list_t *issues,
    issues_info_t *issues_info
);

void report_free(report_t *report);

report_t *report_parseFromJSON(cJSON *reportJSON);

cJSON *report_convertToJSON(report_t *report);

#endif /* _report_H_ */

