/*
 * page_load_time.h
 *
 * 
 */

#ifndef _page_load_time_H_
#define _page_load_time_H_

#include <string.h>
#include "../external/cJSON.h"
#include "../include/list.h"
#include "../include/keyValuePair.h"
#include "../include/binary.h"

typedef struct page_load_time_t page_load_time_t;




typedef struct page_load_time_t {
    double duration; //numeric
    char *duration_formated; // string
    char *color; // string

} page_load_time_t;

page_load_time_t *page_load_time_create(
    double duration,
    char *duration_formated,
    char *color
);

void page_load_time_free(page_load_time_t *page_load_time);

page_load_time_t *page_load_time_parseFromJSON(cJSON *page_load_timeJSON);

cJSON *page_load_time_convertToJSON(page_load_time_t *page_load_time);

#endif /* _page_load_time_H_ */

