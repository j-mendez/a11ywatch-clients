/*
 * get_website_by_domain_200_response.h
 *
 * 
 */

#ifndef _get_website_by_domain_200_response_H_
#define _get_website_by_domain_200_response_H_

#include <string.h>
#include "../external/cJSON.h"
#include "../include/list.h"
#include "../include/keyValuePair.h"
#include "../include/binary.h"

typedef struct get_website_by_domain_200_response_t get_website_by_domain_200_response_t;

#include "websites.h"



typedef struct get_website_by_domain_200_response_t {
    struct websites_t *data; //model

} get_website_by_domain_200_response_t;

get_website_by_domain_200_response_t *get_website_by_domain_200_response_create(
    websites_t *data
);

void get_website_by_domain_200_response_free(get_website_by_domain_200_response_t *get_website_by_domain_200_response);

get_website_by_domain_200_response_t *get_website_by_domain_200_response_parseFromJSON(cJSON *get_website_by_domain_200_responseJSON);

cJSON *get_website_by_domain_200_response_convertToJSON(get_website_by_domain_200_response_t *get_website_by_domain_200_response);

#endif /* _get_website_by_domain_200_response_H_ */

