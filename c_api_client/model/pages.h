/*
 * pages.h
 *
 * 
 */

#ifndef _pages_H_
#define _pages_H_

#include <string.h>
#include "../external/cJSON.h"
#include "../include/list.h"
#include "../include/keyValuePair.h"
#include "../include/binary.h"

typedef struct pages_t pages_t;

#include "issues_info.h"
#include "page_load_time.h"
#include "websites_insight.h"



typedef struct pages_t {
    long _id; //numeric
    long user_id; //numeric
    char *domain; // string
    char *url; // string
    int cdn_connected; //boolean
    int online; //boolean
    struct page_load_time_t *page_load_time; //model
    struct websites_insight_t *insight; //model
    struct issues_info_t *issues_info; //model
    char *last_scan_date; // string

} pages_t;

pages_t *pages_create(
    long _id,
    long user_id,
    char *domain,
    char *url,
    int cdn_connected,
    int online,
    page_load_time_t *page_load_time,
    websites_insight_t *insight,
    issues_info_t *issues_info,
    char *last_scan_date
);

void pages_free(pages_t *pages);

pages_t *pages_parseFromJSON(cJSON *pagesJSON);

cJSON *pages_convertToJSON(pages_t *pages);

#endif /* _pages_H_ */

