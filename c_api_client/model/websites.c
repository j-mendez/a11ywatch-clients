#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "websites.h"



websites_t *websites_create(
    long _id,
    long user_id,
    char *url,
    char *domain,
    long crawl_duration,
    int cdn_connected,
    int page_insights,
    int online,
    int mobile,
    int robots,
    websites_insight_t *insight,
    list_t *page_headers,
    page_load_time_t *page_load_time,
    issues_info_t *issues_info
    ) {
    websites_t *websites_local_var = malloc(sizeof(websites_t));
    if (!websites_local_var) {
        return NULL;
    }
    websites_local_var->_id = _id;
    websites_local_var->user_id = user_id;
    websites_local_var->url = url;
    websites_local_var->domain = domain;
    websites_local_var->crawl_duration = crawl_duration;
    websites_local_var->cdn_connected = cdn_connected;
    websites_local_var->page_insights = page_insights;
    websites_local_var->online = online;
    websites_local_var->mobile = mobile;
    websites_local_var->robots = robots;
    websites_local_var->insight = insight;
    websites_local_var->page_headers = page_headers;
    websites_local_var->page_load_time = page_load_time;
    websites_local_var->issues_info = issues_info;

    return websites_local_var;
}


void websites_free(websites_t *websites) {
    if(NULL == websites){
        return ;
    }
    listEntry_t *listEntry;
    if (websites->url) {
        free(websites->url);
        websites->url = NULL;
    }
    if (websites->domain) {
        free(websites->domain);
        websites->domain = NULL;
    }
    if (websites->insight) {
        websites_insight_free(websites->insight);
        websites->insight = NULL;
    }
    if (websites->page_headers) {
        list_ForEach(listEntry, websites->page_headers) {
            free(listEntry->data);
        }
        list_free(websites->page_headers);
        websites->page_headers = NULL;
    }
    if (websites->page_load_time) {
        page_load_time_free(websites->page_load_time);
        websites->page_load_time = NULL;
    }
    if (websites->issues_info) {
        issues_info_free(websites->issues_info);
        websites->issues_info = NULL;
    }
    free(websites);
}

cJSON *websites_convertToJSON(websites_t *websites) {
    cJSON *item = cJSON_CreateObject();

    // websites->_id
    if(websites->_id) { 
    if(cJSON_AddNumberToObject(item, "_id", websites->_id) == NULL) {
    goto fail; //Numeric
    }
     } 


    // websites->user_id
    if(websites->user_id) { 
    if(cJSON_AddNumberToObject(item, "userId", websites->user_id) == NULL) {
    goto fail; //Numeric
    }
     } 


    // websites->url
    if(websites->url) { 
    if(cJSON_AddStringToObject(item, "url", websites->url) == NULL) {
    goto fail; //String
    }
     } 


    // websites->domain
    if(websites->domain) { 
    if(cJSON_AddStringToObject(item, "domain", websites->domain) == NULL) {
    goto fail; //String
    }
     } 


    // websites->crawl_duration
    if(websites->crawl_duration) { 
    if(cJSON_AddNumberToObject(item, "crawlDuration", websites->crawl_duration) == NULL) {
    goto fail; //Numeric
    }
     } 


    // websites->cdn_connected
    if(websites->cdn_connected) { 
    if(cJSON_AddBoolToObject(item, "cdnConnected", websites->cdn_connected) == NULL) {
    goto fail; //Bool
    }
     } 


    // websites->page_insights
    if(websites->page_insights) { 
    if(cJSON_AddBoolToObject(item, "pageInsights", websites->page_insights) == NULL) {
    goto fail; //Bool
    }
     } 


    // websites->online
    if(websites->online) { 
    if(cJSON_AddBoolToObject(item, "online", websites->online) == NULL) {
    goto fail; //Bool
    }
     } 


    // websites->mobile
    if(websites->mobile) { 
    if(cJSON_AddBoolToObject(item, "mobile", websites->mobile) == NULL) {
    goto fail; //Bool
    }
     } 


    // websites->robots
    if(websites->robots) { 
    if(cJSON_AddBoolToObject(item, "robots", websites->robots) == NULL) {
    goto fail; //Bool
    }
     } 


    // websites->insight
    if(websites->insight) { 
    cJSON *insight_local_JSON = websites_insight_convertToJSON(websites->insight);
    if(insight_local_JSON == NULL) {
    goto fail; //model
    }
    cJSON_AddItemToObject(item, "insight", insight_local_JSON);
    if(item->child == NULL) {
    goto fail;
    }
     } 


    // websites->page_headers
    if(websites->page_headers) { 
    cJSON *page_headers = cJSON_AddArrayToObject(item, "pageHeaders");
    if(page_headers == NULL) {
        goto fail; //primitive container
    }

    listEntry_t *page_headersListEntry;
    list_ForEach(page_headersListEntry, websites->page_headers) {
    if(cJSON_AddStringToObject(page_headers, "", (char*)page_headersListEntry->data) == NULL)
    {
        goto fail;
    }
    }
     } 


    // websites->page_load_time
    if(websites->page_load_time) { 
    cJSON *page_load_time_local_JSON = page_load_time_convertToJSON(websites->page_load_time);
    if(page_load_time_local_JSON == NULL) {
    goto fail; //model
    }
    cJSON_AddItemToObject(item, "pageLoadTime", page_load_time_local_JSON);
    if(item->child == NULL) {
    goto fail;
    }
     } 


    // websites->issues_info
    if(websites->issues_info) { 
    cJSON *issues_info_local_JSON = issues_info_convertToJSON(websites->issues_info);
    if(issues_info_local_JSON == NULL) {
    goto fail; //model
    }
    cJSON_AddItemToObject(item, "issuesInfo", issues_info_local_JSON);
    if(item->child == NULL) {
    goto fail;
    }
     } 

    return item;
fail:
    if (item) {
        cJSON_Delete(item);
    }
    return NULL;
}

websites_t *websites_parseFromJSON(cJSON *websitesJSON){

    websites_t *websites_local_var = NULL;

    // websites->_id
    cJSON *_id = cJSON_GetObjectItemCaseSensitive(websitesJSON, "_id");
    if (_id) { 
    if(!cJSON_IsNumber(_id))
    {
    goto end; //Numeric
    }
    }

    // websites->user_id
    cJSON *user_id = cJSON_GetObjectItemCaseSensitive(websitesJSON, "userId");
    if (user_id) { 
    if(!cJSON_IsNumber(user_id))
    {
    goto end; //Numeric
    }
    }

    // websites->url
    cJSON *url = cJSON_GetObjectItemCaseSensitive(websitesJSON, "url");
    if (url) { 
    if(!cJSON_IsString(url))
    {
    goto end; //String
    }
    }

    // websites->domain
    cJSON *domain = cJSON_GetObjectItemCaseSensitive(websitesJSON, "domain");
    if (domain) { 
    if(!cJSON_IsString(domain))
    {
    goto end; //String
    }
    }

    // websites->crawl_duration
    cJSON *crawl_duration = cJSON_GetObjectItemCaseSensitive(websitesJSON, "crawlDuration");
    if (crawl_duration) { 
    if(!cJSON_IsNumber(crawl_duration))
    {
    goto end; //Numeric
    }
    }

    // websites->cdn_connected
    cJSON *cdn_connected = cJSON_GetObjectItemCaseSensitive(websitesJSON, "cdnConnected");
    if (cdn_connected) { 
    if(!cJSON_IsBool(cdn_connected))
    {
    goto end; //Bool
    }
    }

    // websites->page_insights
    cJSON *page_insights = cJSON_GetObjectItemCaseSensitive(websitesJSON, "pageInsights");
    if (page_insights) { 
    if(!cJSON_IsBool(page_insights))
    {
    goto end; //Bool
    }
    }

    // websites->online
    cJSON *online = cJSON_GetObjectItemCaseSensitive(websitesJSON, "online");
    if (online) { 
    if(!cJSON_IsBool(online))
    {
    goto end; //Bool
    }
    }

    // websites->mobile
    cJSON *mobile = cJSON_GetObjectItemCaseSensitive(websitesJSON, "mobile");
    if (mobile) { 
    if(!cJSON_IsBool(mobile))
    {
    goto end; //Bool
    }
    }

    // websites->robots
    cJSON *robots = cJSON_GetObjectItemCaseSensitive(websitesJSON, "robots");
    if (robots) { 
    if(!cJSON_IsBool(robots))
    {
    goto end; //Bool
    }
    }

    // websites->insight
    cJSON *insight = cJSON_GetObjectItemCaseSensitive(websitesJSON, "insight");
    websites_insight_t *insight_local_nonprim = NULL;
    if (insight) { 
    insight_local_nonprim = websites_insight_parseFromJSON(insight); //nonprimitive
    }

    // websites->page_headers
    cJSON *page_headers = cJSON_GetObjectItemCaseSensitive(websitesJSON, "pageHeaders");
    list_t *page_headersList;
    if (page_headers) { 
    cJSON *page_headers_local;
    if(!cJSON_IsArray(page_headers)) {
        goto end;//primitive container
    }
    page_headersList = list_create();

    cJSON_ArrayForEach(page_headers_local, page_headers)
    {
        if(!cJSON_IsString(page_headers_local))
        {
            goto end;
        }
        list_addElement(page_headersList , strdup(page_headers_local->valuestring));
    }
    }

    // websites->page_load_time
    cJSON *page_load_time = cJSON_GetObjectItemCaseSensitive(websitesJSON, "pageLoadTime");
    page_load_time_t *page_load_time_local_nonprim = NULL;
    if (page_load_time) { 
    page_load_time_local_nonprim = page_load_time_parseFromJSON(page_load_time); //nonprimitive
    }

    // websites->issues_info
    cJSON *issues_info = cJSON_GetObjectItemCaseSensitive(websitesJSON, "issuesInfo");
    issues_info_t *issues_info_local_nonprim = NULL;
    if (issues_info) { 
    issues_info_local_nonprim = issues_info_parseFromJSON(issues_info); //nonprimitive
    }


    websites_local_var = websites_create (
        _id ? _id->valuedouble : 0,
        user_id ? user_id->valuedouble : 0,
        url ? strdup(url->valuestring) : NULL,
        domain ? strdup(domain->valuestring) : NULL,
        crawl_duration ? crawl_duration->valuedouble : 0,
        cdn_connected ? cdn_connected->valueint : 0,
        page_insights ? page_insights->valueint : 0,
        online ? online->valueint : 0,
        mobile ? mobile->valueint : 0,
        robots ? robots->valueint : 0,
        insight ? insight_local_nonprim : NULL,
        page_headers ? page_headersList : NULL,
        page_load_time ? page_load_time_local_nonprim : NULL,
        issues_info ? issues_info_local_nonprim : NULL
        );

    return websites_local_var;
end:
    if (insight_local_nonprim) {
        websites_insight_free(insight_local_nonprim);
        insight_local_nonprim = NULL;
    }
    if (page_load_time_local_nonprim) {
        page_load_time_free(page_load_time_local_nonprim);
        page_load_time_local_nonprim = NULL;
    }
    if (issues_info_local_nonprim) {
        issues_info_free(issues_info_local_nonprim);
        issues_info_local_nonprim = NULL;
    }
    return NULL;

}
