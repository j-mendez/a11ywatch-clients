#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "history.h"



history_t *history_create(
    long _id,
    char *domain,
    websites_insight_t *insight,
    list_t *page_headers,
    issues_info_t *issues_info
    ) {
    history_t *history_local_var = malloc(sizeof(history_t));
    if (!history_local_var) {
        return NULL;
    }
    history_local_var->_id = _id;
    history_local_var->domain = domain;
    history_local_var->insight = insight;
    history_local_var->page_headers = page_headers;
    history_local_var->issues_info = issues_info;

    return history_local_var;
}


void history_free(history_t *history) {
    if(NULL == history){
        return ;
    }
    listEntry_t *listEntry;
    if (history->domain) {
        free(history->domain);
        history->domain = NULL;
    }
    if (history->insight) {
        websites_insight_free(history->insight);
        history->insight = NULL;
    }
    if (history->page_headers) {
        list_ForEach(listEntry, history->page_headers) {
            free(listEntry->data);
        }
        list_free(history->page_headers);
        history->page_headers = NULL;
    }
    if (history->issues_info) {
        issues_info_free(history->issues_info);
        history->issues_info = NULL;
    }
    free(history);
}

cJSON *history_convertToJSON(history_t *history) {
    cJSON *item = cJSON_CreateObject();

    // history->_id
    if(history->_id) { 
    if(cJSON_AddNumberToObject(item, "_id", history->_id) == NULL) {
    goto fail; //Numeric
    }
     } 


    // history->domain
    if(history->domain) { 
    if(cJSON_AddStringToObject(item, "domain", history->domain) == NULL) {
    goto fail; //String
    }
     } 


    // history->insight
    if(history->insight) { 
    cJSON *insight_local_JSON = websites_insight_convertToJSON(history->insight);
    if(insight_local_JSON == NULL) {
    goto fail; //model
    }
    cJSON_AddItemToObject(item, "insight", insight_local_JSON);
    if(item->child == NULL) {
    goto fail;
    }
     } 


    // history->page_headers
    if(history->page_headers) { 
    cJSON *page_headers = cJSON_AddArrayToObject(item, "pageHeaders");
    if(page_headers == NULL) {
        goto fail; //primitive container
    }

    listEntry_t *page_headersListEntry;
    list_ForEach(page_headersListEntry, history->page_headers) {
    if(cJSON_AddStringToObject(page_headers, "", (char*)page_headersListEntry->data) == NULL)
    {
        goto fail;
    }
    }
     } 


    // history->issues_info
    if(history->issues_info) { 
    cJSON *issues_info_local_JSON = issues_info_convertToJSON(history->issues_info);
    if(issues_info_local_JSON == NULL) {
    goto fail; //model
    }
    cJSON_AddItemToObject(item, "issuesInfo", issues_info_local_JSON);
    if(item->child == NULL) {
    goto fail;
    }
     } 

    return item;
fail:
    if (item) {
        cJSON_Delete(item);
    }
    return NULL;
}

history_t *history_parseFromJSON(cJSON *historyJSON){

    history_t *history_local_var = NULL;

    // history->_id
    cJSON *_id = cJSON_GetObjectItemCaseSensitive(historyJSON, "_id");
    if (_id) { 
    if(!cJSON_IsNumber(_id))
    {
    goto end; //Numeric
    }
    }

    // history->domain
    cJSON *domain = cJSON_GetObjectItemCaseSensitive(historyJSON, "domain");
    if (domain) { 
    if(!cJSON_IsString(domain))
    {
    goto end; //String
    }
    }

    // history->insight
    cJSON *insight = cJSON_GetObjectItemCaseSensitive(historyJSON, "insight");
    websites_insight_t *insight_local_nonprim = NULL;
    if (insight) { 
    insight_local_nonprim = websites_insight_parseFromJSON(insight); //nonprimitive
    }

    // history->page_headers
    cJSON *page_headers = cJSON_GetObjectItemCaseSensitive(historyJSON, "pageHeaders");
    list_t *page_headersList;
    if (page_headers) { 
    cJSON *page_headers_local;
    if(!cJSON_IsArray(page_headers)) {
        goto end;//primitive container
    }
    page_headersList = list_create();

    cJSON_ArrayForEach(page_headers_local, page_headers)
    {
        if(!cJSON_IsString(page_headers_local))
        {
            goto end;
        }
        list_addElement(page_headersList , strdup(page_headers_local->valuestring));
    }
    }

    // history->issues_info
    cJSON *issues_info = cJSON_GetObjectItemCaseSensitive(historyJSON, "issuesInfo");
    issues_info_t *issues_info_local_nonprim = NULL;
    if (issues_info) { 
    issues_info_local_nonprim = issues_info_parseFromJSON(issues_info); //nonprimitive
    }


    history_local_var = history_create (
        _id ? _id->valuedouble : 0,
        domain ? strdup(domain->valuestring) : NULL,
        insight ? insight_local_nonprim : NULL,
        page_headers ? page_headersList : NULL,
        issues_info ? issues_info_local_nonprim : NULL
        );

    return history_local_var;
end:
    if (insight_local_nonprim) {
        websites_insight_free(insight_local_nonprim);
        insight_local_nonprim = NULL;
    }
    if (issues_info_local_nonprim) {
        issues_info_free(issues_info_local_nonprim);
        issues_info_local_nonprim = NULL;
    }
    return NULL;

}
