/*
 * websites.h
 *
 * 
 */

#ifndef _websites_H_
#define _websites_H_

#include <string.h>
#include "../external/cJSON.h"
#include "../include/list.h"
#include "../include/keyValuePair.h"
#include "../include/binary.h"

typedef struct websites_t websites_t;

#include "issues_info.h"
#include "page_load_time.h"
#include "websites_insight.h"



typedef struct websites_t {
    long _id; //numeric
    long user_id; //numeric
    char *url; // string
    char *domain; // string
    long crawl_duration; //numeric
    int cdn_connected; //boolean
    int page_insights; //boolean
    int online; //boolean
    int mobile; //boolean
    int robots; //boolean
    struct websites_insight_t *insight; //model
    list_t *page_headers; //primitive container
    struct page_load_time_t *page_load_time; //model
    struct issues_info_t *issues_info; //model

} websites_t;

websites_t *websites_create(
    long _id,
    long user_id,
    char *url,
    char *domain,
    long crawl_duration,
    int cdn_connected,
    int page_insights,
    int online,
    int mobile,
    int robots,
    websites_insight_t *insight,
    list_t *page_headers,
    page_load_time_t *page_load_time,
    issues_info_t *issues_info
);

void websites_free(websites_t *websites);

websites_t *websites_parseFromJSON(cJSON *websitesJSON);

cJSON *websites_convertToJSON(websites_t *websites);

#endif /* _websites_H_ */

