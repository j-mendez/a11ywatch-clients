#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "crawl_input.h"



crawl_input_t *crawl_input_create(
    char *domain,
    char *url
    ) {
    crawl_input_t *crawl_input_local_var = malloc(sizeof(crawl_input_t));
    if (!crawl_input_local_var) {
        return NULL;
    }
    crawl_input_local_var->domain = domain;
    crawl_input_local_var->url = url;

    return crawl_input_local_var;
}


void crawl_input_free(crawl_input_t *crawl_input) {
    if(NULL == crawl_input){
        return ;
    }
    listEntry_t *listEntry;
    if (crawl_input->domain) {
        free(crawl_input->domain);
        crawl_input->domain = NULL;
    }
    if (crawl_input->url) {
        free(crawl_input->url);
        crawl_input->url = NULL;
    }
    free(crawl_input);
}

cJSON *crawl_input_convertToJSON(crawl_input_t *crawl_input) {
    cJSON *item = cJSON_CreateObject();

    // crawl_input->domain
    if(crawl_input->domain) { 
    if(cJSON_AddStringToObject(item, "domain", crawl_input->domain) == NULL) {
    goto fail; //String
    }
     } 


    // crawl_input->url
    if(crawl_input->url) { 
    if(cJSON_AddStringToObject(item, "url", crawl_input->url) == NULL) {
    goto fail; //String
    }
     } 

    return item;
fail:
    if (item) {
        cJSON_Delete(item);
    }
    return NULL;
}

crawl_input_t *crawl_input_parseFromJSON(cJSON *crawl_inputJSON){

    crawl_input_t *crawl_input_local_var = NULL;

    // crawl_input->domain
    cJSON *domain = cJSON_GetObjectItemCaseSensitive(crawl_inputJSON, "domain");
    if (domain) { 
    if(!cJSON_IsString(domain))
    {
    goto end; //String
    }
    }

    // crawl_input->url
    cJSON *url = cJSON_GetObjectItemCaseSensitive(crawl_inputJSON, "url");
    if (url) { 
    if(!cJSON_IsString(url))
    {
    goto end; //String
    }
    }


    crawl_input_local_var = crawl_input_create (
        domain ? strdup(domain->valuestring) : NULL,
        url ? strdup(url->valuestring) : NULL
        );

    return crawl_input_local_var;
end:
    return NULL;

}
