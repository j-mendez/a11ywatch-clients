/*
 * crawl_website_stream_200_response.h
 *
 * 
 */

#ifndef _crawl_website_stream_200_response_H_
#define _crawl_website_stream_200_response_H_

#include <string.h>
#include "../external/cJSON.h"
#include "../include/list.h"
#include "../include/keyValuePair.h"
#include "../include/binary.h"

typedef struct crawl_website_stream_200_response_t crawl_website_stream_200_response_t;

#include "report.h"



typedef struct crawl_website_stream_200_response_t {
    list_t *data; //nonprimitive container

} crawl_website_stream_200_response_t;

crawl_website_stream_200_response_t *crawl_website_stream_200_response_create(
    list_t *data
);

void crawl_website_stream_200_response_free(crawl_website_stream_200_response_t *crawl_website_stream_200_response);

crawl_website_stream_200_response_t *crawl_website_stream_200_response_parseFromJSON(cJSON *crawl_website_stream_200_responseJSON);

cJSON *crawl_website_stream_200_response_convertToJSON(crawl_website_stream_200_response_t *crawl_website_stream_200_response);

#endif /* _crawl_website_stream_200_response_H_ */

