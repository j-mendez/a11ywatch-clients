# users_t

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_id** | **long** |  | [optional] 
**email** | **char \*** |  | [optional] 
**jwt** | **char \*** |  | [optional] 
**role** | **char \*** |  | [optional] 
**password** | **char \*** |  | [optional] 
**alert_enabled** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


