# issues_info_t

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**access_score_average** | **long** |  | [optional] 
**possible_issues_fixed_by_cdn** | **long** |  | [optional] 
**total_issues** | **long** |  | [optional] 
**issues_fixed_by_cdn** | **long** |  | [optional] 
**error_count** | **long** |  | [optional] 
**warning_count** | **long** |  | [optional] 
**notice_count** | **long** |  | [optional] 
**page_count** | **long** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


