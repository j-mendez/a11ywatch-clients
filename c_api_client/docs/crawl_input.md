# crawl_input_t

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**domain** | **char \*** |  | [optional] 
**url** | **char \*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


