# websites_t

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_id** | **long** |  | [optional] 
**user_id** | **long** |  | [optional] 
**url** | **char \*** |  | [optional] 
**domain** | **char \*** |  | [optional] 
**crawl_duration** | **long** |  | [optional] 
**cdn_connected** | **int** |  | [optional] 
**page_insights** | **int** |  | [optional] 
**online** | **int** |  | [optional] 
**mobile** | **int** |  | [optional] 
**robots** | **int** |  | [optional] 
**insight** | [**websites_insight_t**](websites_insight.md) \* |  | [optional] 
**page_headers** | **list_t \*** |  | [optional] 
**page_load_time** | [**page_load_time_t**](page_load_time.md) \* |  | [optional] 
**issues_info** | [**issues_info_t**](issues_info.md) \* |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


