# issues_t

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_id** | **long** |  | [optional] 
**user_id** | **long** |  | [optional] 
**domain** | **char \*** |  | [optional] 
**page_url** | **char \*** |  | [optional] 
**issues** | [**page_issue_t**](page_issue.md) \* |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


