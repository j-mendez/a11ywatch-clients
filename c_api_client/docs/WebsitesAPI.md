# WebsitesAPI

All URIs are relative to *https://api.a11ywatch.com/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**WebsitesAPI_addWebsite**](WebsitesAPI.md#WebsitesAPI_addWebsite) | **POST** /website | Add a website in the collection with form data
[**WebsitesAPI_deleteWebsite**](WebsitesAPI.md#WebsitesAPI_deleteWebsite) | **DELETE** /website | Deletes a website
[**WebsitesAPI_getWebsiteByDomain**](WebsitesAPI.md#WebsitesAPI_getWebsiteByDomain) | **GET** /website | Find website by Domain


# **WebsitesAPI_addWebsite**
```c
// Add a website in the collection with form data
//
object_t* WebsitesAPI_addWebsite(apiClient_t *apiClient, website_input_t * website_input);
```

### Parameters
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**apiClient** | **apiClient_t \*** | context containing the client configuration |
**website_input** | **[website_input_t](website_input.md) \*** | The website standard body | 

### Return type

[object_t](object.md) *


### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **WebsitesAPI_deleteWebsite**
```c
// Deletes a website
//
object_t* WebsitesAPI_deleteWebsite(apiClient_t *apiClient, char * domain);
```

### Parameters
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**apiClient** | **apiClient_t \*** | context containing the client configuration |
**domain** | **char \*** | Websites domain to delete | 

### Return type

[object_t](object.md) *


### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **WebsitesAPI_getWebsiteByDomain**
```c
// Find website by Domain
//
// Returns a website when DOMAIN == website.domain.  Empty strings will simulate API error conditions
//
object_t* WebsitesAPI_getWebsiteByDomain(apiClient_t *apiClient, char * domain);
```

### Parameters
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**apiClient** | **apiClient_t \*** | context containing the client configuration |
**domain** | **char \*** | Domain of website that needs to be fetched | 

### Return type

[object_t](object.md) *


### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

