# CollectionAPI

All URIs are relative to *https://api.a11ywatch.com/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**CollectionAPI_getAnalytics**](CollectionAPI.md#CollectionAPI_getAnalytics) | **GET** /list/analytics | Get the analytics for a website
[**CollectionAPI_getIssues**](CollectionAPI.md#CollectionAPI_getIssues) | **GET** /list/issue | List the issues for a website
[**CollectionAPI_getPageSpeed**](CollectionAPI.md#CollectionAPI_getPageSpeed) | **GET** /list/pagespeed | Get the pagespeed for a website
[**CollectionAPI_getPages**](CollectionAPI.md#CollectionAPI_getPages) | **GET** /list/pages | List the pages in order for a website
[**CollectionAPI_getWebsites**](CollectionAPI.md#CollectionAPI_getWebsites) | **GET** /list/website | Returns websites for the user in alphabetical order


# **CollectionAPI_getAnalytics**
```c
// Get the analytics for a website
//
object_t* CollectionAPI_getAnalytics(apiClient_t *apiClient, char * offset, char * domain);
```

### Parameters
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**apiClient** | **apiClient_t \*** | context containing the client configuration |
**offset** | **char \*** | The page offset for the next set | [optional] 
**domain** | **char \*** | Domain of website that needs to be fetched | [optional] 

### Return type

[object_t](object.md) *


### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **CollectionAPI_getIssues**
```c
// List the issues for a website
//
object_t* CollectionAPI_getIssues(apiClient_t *apiClient, char * offset, char * domain);
```

### Parameters
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**apiClient** | **apiClient_t \*** | context containing the client configuration |
**offset** | **char \*** | The page offset for the next set | [optional] 
**domain** | **char \*** | Domain of website that needs to be fetched | [optional] 

### Return type

[object_t](object.md) *


### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **CollectionAPI_getPageSpeed**
```c
// Get the pagespeed for a website
//
object_t* CollectionAPI_getPageSpeed(apiClient_t *apiClient, char * offset, char * domain);
```

### Parameters
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**apiClient** | **apiClient_t \*** | context containing the client configuration |
**offset** | **char \*** | The page offset for the next set | [optional] 
**domain** | **char \*** | Domain of website that needs to be fetched | [optional] 

### Return type

[object_t](object.md) *


### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **CollectionAPI_getPages**
```c
// List the pages in order for a website
//
object_t* CollectionAPI_getPages(apiClient_t *apiClient, char * offset, char * domain);
```

### Parameters
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**apiClient** | **apiClient_t \*** | context containing the client configuration |
**offset** | **char \*** | The page offset for the next set | [optional] 
**domain** | **char \*** | Domain of website that needs to be fetched | [optional] 

### Return type

[object_t](object.md) *


### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **CollectionAPI_getWebsites**
```c
// Returns websites for the user in alphabetical order
//
// Returns a map of websites
//
object_t* CollectionAPI_getWebsites(apiClient_t *apiClient, char * offset);
```

### Parameters
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**apiClient** | **apiClient_t \*** | context containing the client configuration |
**offset** | **char \*** | The page offset for the next set | [optional] 

### Return type

[object_t](object.md) *


### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

