# page_issue_t

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**recurrence** | **long** |  | [optional] 
**type_code** | **long** |  | [optional] 
**code** | **char \*** |  | [optional] 
**message** | **char \*** |  | [optional] 
**context** | **char \*** |  | [optional] 
**selector** | **char \*** |  | [optional] 
**runner** | **char \*** |  | [optional] 
**type** | **char \*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


