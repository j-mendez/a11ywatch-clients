# crawl_websites_sync_200_response_t

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | **int** |  | [optional] 
**message** | **char \*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


