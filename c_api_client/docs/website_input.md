# website_input_t

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**url** | **char \*** |  | [optional] 
**mobile** | **int** |  | [optional] 
**page_insights** | **int** |  | [optional] 
**ua** | **char \*** |  | [optional] 
**standard** | **char \*** |  | [optional] 
**robots** | **char \*** |  | [optional] 
**subdomains** | **int** |  | [optional] 
**tld** | **int** |  | [optional] 
**custom_headers** | **list_t \*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


