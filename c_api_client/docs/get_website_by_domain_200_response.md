# get_website_by_domain_200_response_t

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**websites_t**](websites.md) \* |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


