# report_t

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_id** | **long** |  | [optional] 
**domain** | **char \*** |  | [optional] 
**page_url** | **char \*** |  | [optional] 
**issues** | [**list_t**](page_issue.md) \* |  | [optional] 
**issues_info** | [**issues_info_t**](issues_info.md) \* |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


