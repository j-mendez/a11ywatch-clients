# ReportsAPI

All URIs are relative to *https://api.a11ywatch.com/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**ReportsAPI_crawlWebsiteStream**](ReportsAPI.md#ReportsAPI_crawlWebsiteStream) | **POST** /crawl | Multi-page crawl a website streaming issues on found
[**ReportsAPI_crawlWebsitesSync**](ReportsAPI.md#ReportsAPI_crawlWebsitesSync) | **POST** /websites-sync | Multi-page crawl all websites attached to account
[**ReportsAPI_getReport**](ReportsAPI.md#ReportsAPI_getReport) | **GET** /report | Get the report from a previus scan
[**ReportsAPI_scanWebsite**](ReportsAPI.md#ReportsAPI_scanWebsite) | **POST** /scan | Scan a website for issues
[**ReportsAPI_scanWebsiteSimple**](ReportsAPI.md#ReportsAPI_scanWebsiteSimple) | **POST** /scan-simple | Scan a website for issues without storing data and limited responses.


# **ReportsAPI_crawlWebsiteStream**
```c
// Multi-page crawl a website streaming issues on found
//
object_t* ReportsAPI_crawlWebsiteStream(apiClient_t *apiClient, char * Transfer_Encoding, website_input_t * website_input);
```

### Parameters
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**apiClient** | **apiClient_t \*** | context containing the client configuration |
**Transfer_Encoding** | **char \*** |  | [default to &#39;Chunked&#39;]
**website_input** | **[website_input_t](website_input.md) \*** | The website standard body | 

### Return type

[object_t](object.md) *


### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **ReportsAPI_crawlWebsitesSync**
```c
// Multi-page crawl all websites attached to account
//
object_t* ReportsAPI_crawlWebsitesSync(apiClient_t *apiClient);
```

### Parameters
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**apiClient** | **apiClient_t \*** | context containing the client configuration |

### Return type

[object_t](object.md) *


### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **ReportsAPI_getReport**
```c
// Get the report from a previus scan
//
object_t* ReportsAPI_getReport(apiClient_t *apiClient, char * url, char * domain);
```

### Parameters
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**apiClient** | **apiClient_t \*** | context containing the client configuration |
**url** | **char \*** | The page url or domain for the report | [optional] 
**domain** | **char \*** | Domain of website that needs to be fetched | [optional] 

### Return type

[object_t](object.md) *


### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **ReportsAPI_scanWebsite**
```c
// Scan a website for issues
//
object_t* ReportsAPI_scanWebsite(apiClient_t *apiClient, website_input_t * website_input);
```

### Parameters
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**apiClient** | **apiClient_t \*** | context containing the client configuration |
**website_input** | **[website_input_t](website_input.md) \*** | The website standard body | 

### Return type

[object_t](object.md) *


### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **ReportsAPI_scanWebsiteSimple**
```c
// Scan a website for issues without storing data and limited responses.
//
object_t* ReportsAPI_scanWebsiteSimple(apiClient_t *apiClient, website_input_t * website_input);
```

### Parameters
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**apiClient** | **apiClient_t \*** | context containing the client configuration |
**website_input** | **[website_input_t](website_input.md) \*** | The website standard body | 

### Return type

[object_t](object.md) *


### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

