# UserAPI

All URIs are relative to *https://api.a11ywatch.com/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**UserAPI_createUser**](UserAPI.md#UserAPI_createUser) | **POST** /register | Register user into the system
[**UserAPI_getUsers**](UserAPI.md#UserAPI_getUsers) | **GET** /user | Get user
[**UserAPI_loginUser**](UserAPI.md#UserAPI_loginUser) | **POST** /login | Logs user into the system
[**UserAPI_logoutUser**](UserAPI.md#UserAPI_logoutUser) | **POST** /logout | Logs out current logged in user session


# **UserAPI_createUser**
```c
// Register user into the system
//
// Adds a new user not created yet into the system
//
users_t* UserAPI_createUser(apiClient_t *apiClient, auth_input_t * auth_input);
```

### Parameters
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**apiClient** | **apiClient_t \*** | context containing the client configuration |
**auth_input** | **[auth_input_t](auth_input.md) \*** | The auth standard body | 

### Return type

[users_t](users.md) *


### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **UserAPI_getUsers**
```c
// Get user
//
// Retrieve the current user.
//
void UserAPI_getUsers(apiClient_t *apiClient);
```

### Parameters
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**apiClient** | **apiClient_t \*** | context containing the client configuration |

### Return type

void

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **UserAPI_loginUser**
```c
// Logs user into the system
//
users_t* UserAPI_loginUser(apiClient_t *apiClient, auth_input_t * auth_input);
```

### Parameters
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**apiClient** | **apiClient_t \*** | context containing the client configuration |
**auth_input** | **[auth_input_t](auth_input.md) \*** | The auth standard body | 

### Return type

[users_t](users.md) *


### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **UserAPI_logoutUser**
```c
// Logs out current logged in user session
//
void UserAPI_logoutUser(apiClient_t *apiClient);
```

### Parameters
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**apiClient** | **apiClient_t \*** | context containing the client configuration |

### Return type

void

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

