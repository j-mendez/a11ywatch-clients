# crawl_website_stream_200_response_t

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**list_t**](report.md) \* |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


