# analytics_t

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_id** | **long** |  | [optional] 
**domain** | **char \*** |  | [optional] 
**page_url** | **char \*** |  | [optional] 
**user_id** | **long** |  | [optional] 
**access_score** | **long** |  | [optional] 
**possible_issues_fixed_by_cdn** | **long** |  | [optional] 
**total_issues** | **long** |  | [optional] 
**issues_fixed_by_cdn** | **long** |  | [optional] 
**error_count** | **long** |  | [optional] 
**warning_count** | **long** |  | [optional] 
**notice_count** | **long** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


