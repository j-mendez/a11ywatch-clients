# history_t

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_id** | **long** |  | [optional] 
**domain** | **char \*** |  | [optional] 
**insight** | [**websites_insight_t**](websites_insight.md) \* |  | [optional] 
**page_headers** | **list_t \*** |  | [optional] 
**issues_info** | [**issues_info_t**](issues_info.md) \* |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


