# page_load_time_t

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**duration** | **double** |  | [optional] 
**duration_formated** | **char \*** |  | [optional] 
**color** | **char \*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


