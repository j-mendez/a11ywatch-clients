#ifndef generic_input_TEST
#define generic_input_TEST

// the following is to include only the main from the first c file
#ifndef TEST_MAIN
#define TEST_MAIN
#define generic_input_MAIN
#endif // TEST_MAIN

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdbool.h>
#include "../external/cJSON.h"

#include "../model/generic_input.h"
generic_input_t* instantiate_generic_input(int include_optional);



generic_input_t* instantiate_generic_input(int include_optional) {
  generic_input_t* generic_input = NULL;
  if (include_optional) {
    generic_input = generic_input_create(
      "0"
    );
  } else {
    generic_input = generic_input_create(
      "0"
    );
  }

  return generic_input;
}


#ifdef generic_input_MAIN

void test_generic_input(int include_optional) {
    generic_input_t* generic_input_1 = instantiate_generic_input(include_optional);

	cJSON* jsongeneric_input_1 = generic_input_convertToJSON(generic_input_1);
	printf("generic_input :\n%s\n", cJSON_Print(jsongeneric_input_1));
	generic_input_t* generic_input_2 = generic_input_parseFromJSON(jsongeneric_input_1);
	cJSON* jsongeneric_input_2 = generic_input_convertToJSON(generic_input_2);
	printf("repeating generic_input:\n%s\n", cJSON_Print(jsongeneric_input_2));
}

int main() {
  test_generic_input(1);
  test_generic_input(0);

  printf("Hello world \n");
  return 0;
}

#endif // generic_input_MAIN
#endif // generic_input_TEST
