#ifndef website_input_TEST
#define website_input_TEST

// the following is to include only the main from the first c file
#ifndef TEST_MAIN
#define TEST_MAIN
#define website_input_MAIN
#endif // TEST_MAIN

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdbool.h>
#include "../external/cJSON.h"

#include "../model/website_input.h"
website_input_t* instantiate_website_input(int include_optional);



website_input_t* instantiate_website_input(int include_optional) {
  website_input_t* website_input = NULL;
  if (include_optional) {
    website_input = website_input_create(
      "0",
      1,
      1,
      "0",
      "0",
      "0",
      1,
      1,
      list_createList()
    );
  } else {
    website_input = website_input_create(
      "0",
      1,
      1,
      "0",
      "0",
      "0",
      1,
      1,
      list_createList()
    );
  }

  return website_input;
}


#ifdef website_input_MAIN

void test_website_input(int include_optional) {
    website_input_t* website_input_1 = instantiate_website_input(include_optional);

	cJSON* jsonwebsite_input_1 = website_input_convertToJSON(website_input_1);
	printf("website_input :\n%s\n", cJSON_Print(jsonwebsite_input_1));
	website_input_t* website_input_2 = website_input_parseFromJSON(jsonwebsite_input_1);
	cJSON* jsonwebsite_input_2 = website_input_convertToJSON(website_input_2);
	printf("repeating website_input:\n%s\n", cJSON_Print(jsonwebsite_input_2));
}

int main() {
  test_website_input(1);
  test_website_input(0);

  printf("Hello world \n");
  return 0;
}

#endif // website_input_MAIN
#endif // website_input_TEST
