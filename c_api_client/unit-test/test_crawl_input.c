#ifndef crawl_input_TEST
#define crawl_input_TEST

// the following is to include only the main from the first c file
#ifndef TEST_MAIN
#define TEST_MAIN
#define crawl_input_MAIN
#endif // TEST_MAIN

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdbool.h>
#include "../external/cJSON.h"

#include "../model/crawl_input.h"
crawl_input_t* instantiate_crawl_input(int include_optional);



crawl_input_t* instantiate_crawl_input(int include_optional) {
  crawl_input_t* crawl_input = NULL;
  if (include_optional) {
    crawl_input = crawl_input_create(
      "0",
      "0"
    );
  } else {
    crawl_input = crawl_input_create(
      "0",
      "0"
    );
  }

  return crawl_input;
}


#ifdef crawl_input_MAIN

void test_crawl_input(int include_optional) {
    crawl_input_t* crawl_input_1 = instantiate_crawl_input(include_optional);

	cJSON* jsoncrawl_input_1 = crawl_input_convertToJSON(crawl_input_1);
	printf("crawl_input :\n%s\n", cJSON_Print(jsoncrawl_input_1));
	crawl_input_t* crawl_input_2 = crawl_input_parseFromJSON(jsoncrawl_input_1);
	cJSON* jsoncrawl_input_2 = crawl_input_convertToJSON(crawl_input_2);
	printf("repeating crawl_input:\n%s\n", cJSON_Print(jsoncrawl_input_2));
}

int main() {
  test_crawl_input(1);
  test_crawl_input(0);

  printf("Hello world \n");
  return 0;
}

#endif // crawl_input_MAIN
#endif // crawl_input_TEST
