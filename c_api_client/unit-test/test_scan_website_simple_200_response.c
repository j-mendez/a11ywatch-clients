#ifndef scan_website_simple_200_response_TEST
#define scan_website_simple_200_response_TEST

// the following is to include only the main from the first c file
#ifndef TEST_MAIN
#define TEST_MAIN
#define scan_website_simple_200_response_MAIN
#endif // TEST_MAIN

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdbool.h>
#include "../external/cJSON.h"

#include "../model/scan_website_simple_200_response.h"
scan_website_simple_200_response_t* instantiate_scan_website_simple_200_response(int include_optional);

#include "test_report.c"


scan_website_simple_200_response_t* instantiate_scan_website_simple_200_response(int include_optional) {
  scan_website_simple_200_response_t* scan_website_simple_200_response = NULL;
  if (include_optional) {
    scan_website_simple_200_response = scan_website_simple_200_response_create(
       // false, not to have infinite recursion
      instantiate_report(0)
    );
  } else {
    scan_website_simple_200_response = scan_website_simple_200_response_create(
      NULL
    );
  }

  return scan_website_simple_200_response;
}


#ifdef scan_website_simple_200_response_MAIN

void test_scan_website_simple_200_response(int include_optional) {
    scan_website_simple_200_response_t* scan_website_simple_200_response_1 = instantiate_scan_website_simple_200_response(include_optional);

	cJSON* jsonscan_website_simple_200_response_1 = scan_website_simple_200_response_convertToJSON(scan_website_simple_200_response_1);
	printf("scan_website_simple_200_response :\n%s\n", cJSON_Print(jsonscan_website_simple_200_response_1));
	scan_website_simple_200_response_t* scan_website_simple_200_response_2 = scan_website_simple_200_response_parseFromJSON(jsonscan_website_simple_200_response_1);
	cJSON* jsonscan_website_simple_200_response_2 = scan_website_simple_200_response_convertToJSON(scan_website_simple_200_response_2);
	printf("repeating scan_website_simple_200_response:\n%s\n", cJSON_Print(jsonscan_website_simple_200_response_2));
}

int main() {
  test_scan_website_simple_200_response(1);
  test_scan_website_simple_200_response(0);

  printf("Hello world \n");
  return 0;
}

#endif // scan_website_simple_200_response_MAIN
#endif // scan_website_simple_200_response_TEST
