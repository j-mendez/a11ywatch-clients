#ifndef page_load_time_TEST
#define page_load_time_TEST

// the following is to include only the main from the first c file
#ifndef TEST_MAIN
#define TEST_MAIN
#define page_load_time_MAIN
#endif // TEST_MAIN

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdbool.h>
#include "../external/cJSON.h"

#include "../model/page_load_time.h"
page_load_time_t* instantiate_page_load_time(int include_optional);



page_load_time_t* instantiate_page_load_time(int include_optional) {
  page_load_time_t* page_load_time = NULL;
  if (include_optional) {
    page_load_time = page_load_time_create(
      null,
      "0",
      "0"
    );
  } else {
    page_load_time = page_load_time_create(
      null,
      "0",
      "0"
    );
  }

  return page_load_time;
}


#ifdef page_load_time_MAIN

void test_page_load_time(int include_optional) {
    page_load_time_t* page_load_time_1 = instantiate_page_load_time(include_optional);

	cJSON* jsonpage_load_time_1 = page_load_time_convertToJSON(page_load_time_1);
	printf("page_load_time :\n%s\n", cJSON_Print(jsonpage_load_time_1));
	page_load_time_t* page_load_time_2 = page_load_time_parseFromJSON(jsonpage_load_time_1);
	cJSON* jsonpage_load_time_2 = page_load_time_convertToJSON(page_load_time_2);
	printf("repeating page_load_time:\n%s\n", cJSON_Print(jsonpage_load_time_2));
}

int main() {
  test_page_load_time(1);
  test_page_load_time(0);

  printf("Hello world \n");
  return 0;
}

#endif // page_load_time_MAIN
#endif // page_load_time_TEST
