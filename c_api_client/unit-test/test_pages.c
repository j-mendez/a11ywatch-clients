#ifndef pages_TEST
#define pages_TEST

// the following is to include only the main from the first c file
#ifndef TEST_MAIN
#define TEST_MAIN
#define pages_MAIN
#endif // TEST_MAIN

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdbool.h>
#include "../external/cJSON.h"

#include "../model/pages.h"
pages_t* instantiate_pages(int include_optional);

#include "test_page_load_time.c"
#include "test_websites_insight.c"
#include "test_issues_info.c"


pages_t* instantiate_pages(int include_optional) {
  pages_t* pages = NULL;
  if (include_optional) {
    pages = pages_create(
      56,
      56,
      "0",
      "0",
      1,
      1,
       // false, not to have infinite recursion
      instantiate_page_load_time(0),
       // false, not to have infinite recursion
      instantiate_websites_insight(0),
       // false, not to have infinite recursion
      instantiate_issues_info(0),
      "0"
    );
  } else {
    pages = pages_create(
      56,
      56,
      "0",
      "0",
      1,
      1,
      NULL,
      NULL,
      NULL,
      "0"
    );
  }

  return pages;
}


#ifdef pages_MAIN

void test_pages(int include_optional) {
    pages_t* pages_1 = instantiate_pages(include_optional);

	cJSON* jsonpages_1 = pages_convertToJSON(pages_1);
	printf("pages :\n%s\n", cJSON_Print(jsonpages_1));
	pages_t* pages_2 = pages_parseFromJSON(jsonpages_1);
	cJSON* jsonpages_2 = pages_convertToJSON(pages_2);
	printf("repeating pages:\n%s\n", cJSON_Print(jsonpages_2));
}

int main() {
  test_pages(1);
  test_pages(0);

  printf("Hello world \n");
  return 0;
}

#endif // pages_MAIN
#endif // pages_TEST
