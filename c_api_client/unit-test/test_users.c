#ifndef users_TEST
#define users_TEST

// the following is to include only the main from the first c file
#ifndef TEST_MAIN
#define TEST_MAIN
#define users_MAIN
#endif // TEST_MAIN

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdbool.h>
#include "../external/cJSON.h"

#include "../model/users.h"
users_t* instantiate_users(int include_optional);



users_t* instantiate_users(int include_optional) {
  users_t* users = NULL;
  if (include_optional) {
    users = users_create(
      56,
      "0",
      "0",
      "0",
      "0",
      1
    );
  } else {
    users = users_create(
      56,
      "0",
      "0",
      "0",
      "0",
      1
    );
  }

  return users;
}


#ifdef users_MAIN

void test_users(int include_optional) {
    users_t* users_1 = instantiate_users(include_optional);

	cJSON* jsonusers_1 = users_convertToJSON(users_1);
	printf("users :\n%s\n", cJSON_Print(jsonusers_1));
	users_t* users_2 = users_parseFromJSON(jsonusers_1);
	cJSON* jsonusers_2 = users_convertToJSON(users_2);
	printf("repeating users:\n%s\n", cJSON_Print(jsonusers_2));
}

int main() {
  test_users(1);
  test_users(0);

  printf("Hello world \n");
  return 0;
}

#endif // users_MAIN
#endif // users_TEST
