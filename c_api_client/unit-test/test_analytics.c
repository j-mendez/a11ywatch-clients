#ifndef analytics_TEST
#define analytics_TEST

// the following is to include only the main from the first c file
#ifndef TEST_MAIN
#define TEST_MAIN
#define analytics_MAIN
#endif // TEST_MAIN

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdbool.h>
#include "../external/cJSON.h"

#include "../model/analytics.h"
analytics_t* instantiate_analytics(int include_optional);



analytics_t* instantiate_analytics(int include_optional) {
  analytics_t* analytics = NULL;
  if (include_optional) {
    analytics = analytics_create(
      56,
      "0",
      "0",
      56,
      56,
      56,
      56,
      56,
      56,
      56,
      56
    );
  } else {
    analytics = analytics_create(
      56,
      "0",
      "0",
      56,
      56,
      56,
      56,
      56,
      56,
      56,
      56
    );
  }

  return analytics;
}


#ifdef analytics_MAIN

void test_analytics(int include_optional) {
    analytics_t* analytics_1 = instantiate_analytics(include_optional);

	cJSON* jsonanalytics_1 = analytics_convertToJSON(analytics_1);
	printf("analytics :\n%s\n", cJSON_Print(jsonanalytics_1));
	analytics_t* analytics_2 = analytics_parseFromJSON(jsonanalytics_1);
	cJSON* jsonanalytics_2 = analytics_convertToJSON(analytics_2);
	printf("repeating analytics:\n%s\n", cJSON_Print(jsonanalytics_2));
}

int main() {
  test_analytics(1);
  test_analytics(0);

  printf("Hello world \n");
  return 0;
}

#endif // analytics_MAIN
#endif // analytics_TEST
