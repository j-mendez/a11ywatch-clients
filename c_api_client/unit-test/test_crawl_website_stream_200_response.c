#ifndef crawl_website_stream_200_response_TEST
#define crawl_website_stream_200_response_TEST

// the following is to include only the main from the first c file
#ifndef TEST_MAIN
#define TEST_MAIN
#define crawl_website_stream_200_response_MAIN
#endif // TEST_MAIN

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdbool.h>
#include "../external/cJSON.h"

#include "../model/crawl_website_stream_200_response.h"
crawl_website_stream_200_response_t* instantiate_crawl_website_stream_200_response(int include_optional);



crawl_website_stream_200_response_t* instantiate_crawl_website_stream_200_response(int include_optional) {
  crawl_website_stream_200_response_t* crawl_website_stream_200_response = NULL;
  if (include_optional) {
    crawl_website_stream_200_response = crawl_website_stream_200_response_create(
      list_createList()
    );
  } else {
    crawl_website_stream_200_response = crawl_website_stream_200_response_create(
      list_createList()
    );
  }

  return crawl_website_stream_200_response;
}


#ifdef crawl_website_stream_200_response_MAIN

void test_crawl_website_stream_200_response(int include_optional) {
    crawl_website_stream_200_response_t* crawl_website_stream_200_response_1 = instantiate_crawl_website_stream_200_response(include_optional);

	cJSON* jsoncrawl_website_stream_200_response_1 = crawl_website_stream_200_response_convertToJSON(crawl_website_stream_200_response_1);
	printf("crawl_website_stream_200_response :\n%s\n", cJSON_Print(jsoncrawl_website_stream_200_response_1));
	crawl_website_stream_200_response_t* crawl_website_stream_200_response_2 = crawl_website_stream_200_response_parseFromJSON(jsoncrawl_website_stream_200_response_1);
	cJSON* jsoncrawl_website_stream_200_response_2 = crawl_website_stream_200_response_convertToJSON(crawl_website_stream_200_response_2);
	printf("repeating crawl_website_stream_200_response:\n%s\n", cJSON_Print(jsoncrawl_website_stream_200_response_2));
}

int main() {
  test_crawl_website_stream_200_response(1);
  test_crawl_website_stream_200_response(0);

  printf("Hello world \n");
  return 0;
}

#endif // crawl_website_stream_200_response_MAIN
#endif // crawl_website_stream_200_response_TEST
