#ifndef page_issue_TEST
#define page_issue_TEST

// the following is to include only the main from the first c file
#ifndef TEST_MAIN
#define TEST_MAIN
#define page_issue_MAIN
#endif // TEST_MAIN

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdbool.h>
#include "../external/cJSON.h"

#include "../model/page_issue.h"
page_issue_t* instantiate_page_issue(int include_optional);



page_issue_t* instantiate_page_issue(int include_optional) {
  page_issue_t* page_issue = NULL;
  if (include_optional) {
    page_issue = page_issue_create(
      56,
      56,
      "0",
      "0",
      "0",
      "0",
      "0",
      "0"
    );
  } else {
    page_issue = page_issue_create(
      56,
      56,
      "0",
      "0",
      "0",
      "0",
      "0",
      "0"
    );
  }

  return page_issue;
}


#ifdef page_issue_MAIN

void test_page_issue(int include_optional) {
    page_issue_t* page_issue_1 = instantiate_page_issue(include_optional);

	cJSON* jsonpage_issue_1 = page_issue_convertToJSON(page_issue_1);
	printf("page_issue :\n%s\n", cJSON_Print(jsonpage_issue_1));
	page_issue_t* page_issue_2 = page_issue_parseFromJSON(jsonpage_issue_1);
	cJSON* jsonpage_issue_2 = page_issue_convertToJSON(page_issue_2);
	printf("repeating page_issue:\n%s\n", cJSON_Print(jsonpage_issue_2));
}

int main() {
  test_page_issue(1);
  test_page_issue(0);

  printf("Hello world \n");
  return 0;
}

#endif // page_issue_MAIN
#endif // page_issue_TEST
