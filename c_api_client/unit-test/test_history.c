#ifndef history_TEST
#define history_TEST

// the following is to include only the main from the first c file
#ifndef TEST_MAIN
#define TEST_MAIN
#define history_MAIN
#endif // TEST_MAIN

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdbool.h>
#include "../external/cJSON.h"

#include "../model/history.h"
history_t* instantiate_history(int include_optional);

#include "test_websites_insight.c"
#include "test_issues_info.c"


history_t* instantiate_history(int include_optional) {
  history_t* history = NULL;
  if (include_optional) {
    history = history_create(
      56,
      "0",
       // false, not to have infinite recursion
      instantiate_websites_insight(0),
      list_createList(),
       // false, not to have infinite recursion
      instantiate_issues_info(0)
    );
  } else {
    history = history_create(
      56,
      "0",
      NULL,
      list_createList(),
      NULL
    );
  }

  return history;
}


#ifdef history_MAIN

void test_history(int include_optional) {
    history_t* history_1 = instantiate_history(include_optional);

	cJSON* jsonhistory_1 = history_convertToJSON(history_1);
	printf("history :\n%s\n", cJSON_Print(jsonhistory_1));
	history_t* history_2 = history_parseFromJSON(jsonhistory_1);
	cJSON* jsonhistory_2 = history_convertToJSON(history_2);
	printf("repeating history:\n%s\n", cJSON_Print(jsonhistory_2));
}

int main() {
  test_history(1);
  test_history(0);

  printf("Hello world \n");
  return 0;
}

#endif // history_MAIN
#endif // history_TEST
