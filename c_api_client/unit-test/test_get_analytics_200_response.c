#ifndef get_analytics_200_response_TEST
#define get_analytics_200_response_TEST

// the following is to include only the main from the first c file
#ifndef TEST_MAIN
#define TEST_MAIN
#define get_analytics_200_response_MAIN
#endif // TEST_MAIN

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdbool.h>
#include "../external/cJSON.h"

#include "../model/get_analytics_200_response.h"
get_analytics_200_response_t* instantiate_get_analytics_200_response(int include_optional);



get_analytics_200_response_t* instantiate_get_analytics_200_response(int include_optional) {
  get_analytics_200_response_t* get_analytics_200_response = NULL;
  if (include_optional) {
    get_analytics_200_response = get_analytics_200_response_create(
      list_createList()
    );
  } else {
    get_analytics_200_response = get_analytics_200_response_create(
      list_createList()
    );
  }

  return get_analytics_200_response;
}


#ifdef get_analytics_200_response_MAIN

void test_get_analytics_200_response(int include_optional) {
    get_analytics_200_response_t* get_analytics_200_response_1 = instantiate_get_analytics_200_response(include_optional);

	cJSON* jsonget_analytics_200_response_1 = get_analytics_200_response_convertToJSON(get_analytics_200_response_1);
	printf("get_analytics_200_response :\n%s\n", cJSON_Print(jsonget_analytics_200_response_1));
	get_analytics_200_response_t* get_analytics_200_response_2 = get_analytics_200_response_parseFromJSON(jsonget_analytics_200_response_1);
	cJSON* jsonget_analytics_200_response_2 = get_analytics_200_response_convertToJSON(get_analytics_200_response_2);
	printf("repeating get_analytics_200_response:\n%s\n", cJSON_Print(jsonget_analytics_200_response_2));
}

int main() {
  test_get_analytics_200_response(1);
  test_get_analytics_200_response(0);

  printf("Hello world \n");
  return 0;
}

#endif // get_analytics_200_response_MAIN
#endif // get_analytics_200_response_TEST
