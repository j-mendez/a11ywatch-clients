#ifndef websites_insight_TEST
#define websites_insight_TEST

// the following is to include only the main from the first c file
#ifndef TEST_MAIN
#define TEST_MAIN
#define websites_insight_MAIN
#endif // TEST_MAIN

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdbool.h>
#include "../external/cJSON.h"

#include "../model/websites_insight.h"
websites_insight_t* instantiate_websites_insight(int include_optional);



websites_insight_t* instantiate_websites_insight(int include_optional) {
  websites_insight_t* websites_insight = NULL;
  if (include_optional) {
    websites_insight = websites_insight_create(
      "0"
    );
  } else {
    websites_insight = websites_insight_create(
      "0"
    );
  }

  return websites_insight;
}


#ifdef websites_insight_MAIN

void test_websites_insight(int include_optional) {
    websites_insight_t* websites_insight_1 = instantiate_websites_insight(include_optional);

	cJSON* jsonwebsites_insight_1 = websites_insight_convertToJSON(websites_insight_1);
	printf("websites_insight :\n%s\n", cJSON_Print(jsonwebsites_insight_1));
	websites_insight_t* websites_insight_2 = websites_insight_parseFromJSON(jsonwebsites_insight_1);
	cJSON* jsonwebsites_insight_2 = websites_insight_convertToJSON(websites_insight_2);
	printf("repeating websites_insight:\n%s\n", cJSON_Print(jsonwebsites_insight_2));
}

int main() {
  test_websites_insight(1);
  test_websites_insight(0);

  printf("Hello world \n");
  return 0;
}

#endif // websites_insight_MAIN
#endif // websites_insight_TEST
