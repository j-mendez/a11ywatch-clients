#ifndef report_TEST
#define report_TEST

// the following is to include only the main from the first c file
#ifndef TEST_MAIN
#define TEST_MAIN
#define report_MAIN
#endif // TEST_MAIN

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdbool.h>
#include "../external/cJSON.h"

#include "../model/report.h"
report_t* instantiate_report(int include_optional);

#include "test_page_issue.c"
#include "test_issues_info.c"


report_t* instantiate_report(int include_optional) {
  report_t* report = NULL;
  if (include_optional) {
    report = report_create(
      56,
      "0",
      "0",
       // false, not to have infinite recursion
      instantiate_page_issue(0),
       // false, not to have infinite recursion
      instantiate_issues_info(0)
    );
  } else {
    report = report_create(
      56,
      "0",
      "0",
      NULL,
      NULL
    );
  }

  return report;
}


#ifdef report_MAIN

void test_report(int include_optional) {
    report_t* report_1 = instantiate_report(include_optional);

	cJSON* jsonreport_1 = report_convertToJSON(report_1);
	printf("report :\n%s\n", cJSON_Print(jsonreport_1));
	report_t* report_2 = report_parseFromJSON(jsonreport_1);
	cJSON* jsonreport_2 = report_convertToJSON(report_2);
	printf("repeating report:\n%s\n", cJSON_Print(jsonreport_2));
}

int main() {
  test_report(1);
  test_report(0);

  printf("Hello world \n");
  return 0;
}

#endif // report_MAIN
#endif // report_TEST
