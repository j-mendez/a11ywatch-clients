#ifndef issues_info_TEST
#define issues_info_TEST

// the following is to include only the main from the first c file
#ifndef TEST_MAIN
#define TEST_MAIN
#define issues_info_MAIN
#endif // TEST_MAIN

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdbool.h>
#include "../external/cJSON.h"

#include "../model/issues_info.h"
issues_info_t* instantiate_issues_info(int include_optional);



issues_info_t* instantiate_issues_info(int include_optional) {
  issues_info_t* issues_info = NULL;
  if (include_optional) {
    issues_info = issues_info_create(
      56,
      56,
      56,
      56,
      56,
      56,
      56,
      56
    );
  } else {
    issues_info = issues_info_create(
      56,
      56,
      56,
      56,
      56,
      56,
      56,
      56
    );
  }

  return issues_info;
}


#ifdef issues_info_MAIN

void test_issues_info(int include_optional) {
    issues_info_t* issues_info_1 = instantiate_issues_info(include_optional);

	cJSON* jsonissues_info_1 = issues_info_convertToJSON(issues_info_1);
	printf("issues_info :\n%s\n", cJSON_Print(jsonissues_info_1));
	issues_info_t* issues_info_2 = issues_info_parseFromJSON(jsonissues_info_1);
	cJSON* jsonissues_info_2 = issues_info_convertToJSON(issues_info_2);
	printf("repeating issues_info:\n%s\n", cJSON_Print(jsonissues_info_2));
}

int main() {
  test_issues_info(1);
  test_issues_info(0);

  printf("Hello world \n");
  return 0;
}

#endif // issues_info_MAIN
#endif // issues_info_TEST
