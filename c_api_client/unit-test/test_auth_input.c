#ifndef auth_input_TEST
#define auth_input_TEST

// the following is to include only the main from the first c file
#ifndef TEST_MAIN
#define TEST_MAIN
#define auth_input_MAIN
#endif // TEST_MAIN

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdbool.h>
#include "../external/cJSON.h"

#include "../model/auth_input.h"
auth_input_t* instantiate_auth_input(int include_optional);



auth_input_t* instantiate_auth_input(int include_optional) {
  auth_input_t* auth_input = NULL;
  if (include_optional) {
    auth_input = auth_input_create(
      "0",
      "0"
    );
  } else {
    auth_input = auth_input_create(
      "0",
      "0"
    );
  }

  return auth_input;
}


#ifdef auth_input_MAIN

void test_auth_input(int include_optional) {
    auth_input_t* auth_input_1 = instantiate_auth_input(include_optional);

	cJSON* jsonauth_input_1 = auth_input_convertToJSON(auth_input_1);
	printf("auth_input :\n%s\n", cJSON_Print(jsonauth_input_1));
	auth_input_t* auth_input_2 = auth_input_parseFromJSON(jsonauth_input_1);
	cJSON* jsonauth_input_2 = auth_input_convertToJSON(auth_input_2);
	printf("repeating auth_input:\n%s\n", cJSON_Print(jsonauth_input_2));
}

int main() {
  test_auth_input(1);
  test_auth_input(0);

  printf("Hello world \n");
  return 0;
}

#endif // auth_input_MAIN
#endif // auth_input_TEST
