#ifndef issues_TEST
#define issues_TEST

// the following is to include only the main from the first c file
#ifndef TEST_MAIN
#define TEST_MAIN
#define issues_MAIN
#endif // TEST_MAIN

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdbool.h>
#include "../external/cJSON.h"

#include "../model/issues.h"
issues_t* instantiate_issues(int include_optional);

#include "test_page_issue.c"


issues_t* instantiate_issues(int include_optional) {
  issues_t* issues = NULL;
  if (include_optional) {
    issues = issues_create(
      56,
      56,
      "0",
      "0",
       // false, not to have infinite recursion
      instantiate_page_issue(0)
    );
  } else {
    issues = issues_create(
      56,
      56,
      "0",
      "0",
      NULL
    );
  }

  return issues;
}


#ifdef issues_MAIN

void test_issues(int include_optional) {
    issues_t* issues_1 = instantiate_issues(include_optional);

	cJSON* jsonissues_1 = issues_convertToJSON(issues_1);
	printf("issues :\n%s\n", cJSON_Print(jsonissues_1));
	issues_t* issues_2 = issues_parseFromJSON(jsonissues_1);
	cJSON* jsonissues_2 = issues_convertToJSON(issues_2);
	printf("repeating issues:\n%s\n", cJSON_Print(jsonissues_2));
}

int main() {
  test_issues(1);
  test_issues(0);

  printf("Hello world \n");
  return 0;
}

#endif // issues_MAIN
#endif // issues_TEST
