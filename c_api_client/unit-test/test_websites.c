#ifndef websites_TEST
#define websites_TEST

// the following is to include only the main from the first c file
#ifndef TEST_MAIN
#define TEST_MAIN
#define websites_MAIN
#endif // TEST_MAIN

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdbool.h>
#include "../external/cJSON.h"

#include "../model/websites.h"
websites_t* instantiate_websites(int include_optional);

#include "test_websites_insight.c"
#include "test_page_load_time.c"
#include "test_issues_info.c"


websites_t* instantiate_websites(int include_optional) {
  websites_t* websites = NULL;
  if (include_optional) {
    websites = websites_create(
      56,
      56,
      "0",
      "0",
      56,
      1,
      1,
      1,
      1,
      1,
       // false, not to have infinite recursion
      instantiate_websites_insight(0),
      list_createList(),
       // false, not to have infinite recursion
      instantiate_page_load_time(0),
       // false, not to have infinite recursion
      instantiate_issues_info(0)
    );
  } else {
    websites = websites_create(
      56,
      56,
      "0",
      "0",
      56,
      1,
      1,
      1,
      1,
      1,
      NULL,
      list_createList(),
      NULL,
      NULL
    );
  }

  return websites;
}


#ifdef websites_MAIN

void test_websites(int include_optional) {
    websites_t* websites_1 = instantiate_websites(include_optional);

	cJSON* jsonwebsites_1 = websites_convertToJSON(websites_1);
	printf("websites :\n%s\n", cJSON_Print(jsonwebsites_1));
	websites_t* websites_2 = websites_parseFromJSON(jsonwebsites_1);
	cJSON* jsonwebsites_2 = websites_convertToJSON(websites_2);
	printf("repeating websites:\n%s\n", cJSON_Print(jsonwebsites_2));
}

int main() {
  test_websites(1);
  test_websites(0);

  printf("Hello world \n");
  return 0;
}

#endif // websites_MAIN
#endif // websites_TEST
