#ifndef get_website_by_domain_200_response_TEST
#define get_website_by_domain_200_response_TEST

// the following is to include only the main from the first c file
#ifndef TEST_MAIN
#define TEST_MAIN
#define get_website_by_domain_200_response_MAIN
#endif // TEST_MAIN

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdbool.h>
#include "../external/cJSON.h"

#include "../model/get_website_by_domain_200_response.h"
get_website_by_domain_200_response_t* instantiate_get_website_by_domain_200_response(int include_optional);

#include "test_websites.c"


get_website_by_domain_200_response_t* instantiate_get_website_by_domain_200_response(int include_optional) {
  get_website_by_domain_200_response_t* get_website_by_domain_200_response = NULL;
  if (include_optional) {
    get_website_by_domain_200_response = get_website_by_domain_200_response_create(
       // false, not to have infinite recursion
      instantiate_websites(0)
    );
  } else {
    get_website_by_domain_200_response = get_website_by_domain_200_response_create(
      NULL
    );
  }

  return get_website_by_domain_200_response;
}


#ifdef get_website_by_domain_200_response_MAIN

void test_get_website_by_domain_200_response(int include_optional) {
    get_website_by_domain_200_response_t* get_website_by_domain_200_response_1 = instantiate_get_website_by_domain_200_response(include_optional);

	cJSON* jsonget_website_by_domain_200_response_1 = get_website_by_domain_200_response_convertToJSON(get_website_by_domain_200_response_1);
	printf("get_website_by_domain_200_response :\n%s\n", cJSON_Print(jsonget_website_by_domain_200_response_1));
	get_website_by_domain_200_response_t* get_website_by_domain_200_response_2 = get_website_by_domain_200_response_parseFromJSON(jsonget_website_by_domain_200_response_1);
	cJSON* jsonget_website_by_domain_200_response_2 = get_website_by_domain_200_response_convertToJSON(get_website_by_domain_200_response_2);
	printf("repeating get_website_by_domain_200_response:\n%s\n", cJSON_Print(jsonget_website_by_domain_200_response_2));
}

int main() {
  test_get_website_by_domain_200_response(1);
  test_get_website_by_domain_200_response(0);

  printf("Hello world \n");
  return 0;
}

#endif // get_website_by_domain_200_response_MAIN
#endif // get_website_by_domain_200_response_TEST
