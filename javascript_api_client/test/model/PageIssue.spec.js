/**
 * A11ywatch Client
 * The web accessibility tool built for scale.
 *
 * The version of the OpenAPI document: 0.7.66
 * Contact: support@a11ywatch.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD.
    define(['expect.js', process.cwd()+'/src/index'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    factory(require('expect.js'), require(process.cwd()+'/src/index'));
  } else {
    // Browser globals (root is window)
    factory(root.expect, root.A11ywatchClient);
  }
}(this, function(expect, A11ywatchClient) {
  'use strict';

  var instance;

  beforeEach(function() {
    instance = new A11ywatchClient.PageIssue();
  });

  var getProperty = function(object, getter, property) {
    // Use getter method if present; otherwise, get the property directly.
    if (typeof object[getter] === 'function')
      return object[getter]();
    else
      return object[property];
  }

  var setProperty = function(object, setter, property, value) {
    // Use setter method if present; otherwise, set the property directly.
    if (typeof object[setter] === 'function')
      object[setter](value);
    else
      object[property] = value;
  }

  describe('PageIssue', function() {
    it('should create an instance of PageIssue', function() {
      // uncomment below and update the code to test PageIssue
      //var instance = new A11ywatchClient.PageIssue();
      //expect(instance).to.be.a(A11ywatchClient.PageIssue);
    });

    it('should have the property recurrence (base name: "recurrence")', function() {
      // uncomment below and update the code to test the property recurrence
      //var instance = new A11ywatchClient.PageIssue();
      //expect(instance).to.be();
    });

    it('should have the property typeCode (base name: "typeCode")', function() {
      // uncomment below and update the code to test the property typeCode
      //var instance = new A11ywatchClient.PageIssue();
      //expect(instance).to.be();
    });

    it('should have the property code (base name: "code")', function() {
      // uncomment below and update the code to test the property code
      //var instance = new A11ywatchClient.PageIssue();
      //expect(instance).to.be();
    });

    it('should have the property message (base name: "message")', function() {
      // uncomment below and update the code to test the property message
      //var instance = new A11ywatchClient.PageIssue();
      //expect(instance).to.be();
    });

    it('should have the property context (base name: "context")', function() {
      // uncomment below and update the code to test the property context
      //var instance = new A11ywatchClient.PageIssue();
      //expect(instance).to.be();
    });

    it('should have the property selector (base name: "selector")', function() {
      // uncomment below and update the code to test the property selector
      //var instance = new A11ywatchClient.PageIssue();
      //expect(instance).to.be();
    });

    it('should have the property runner (base name: "runner")', function() {
      // uncomment below and update the code to test the property runner
      //var instance = new A11ywatchClient.PageIssue();
      //expect(instance).to.be();
    });

    it('should have the property type (base name: "type")', function() {
      // uncomment below and update the code to test the property type
      //var instance = new A11ywatchClient.PageIssue();
      //expect(instance).to.be();
    });

  });

}));
