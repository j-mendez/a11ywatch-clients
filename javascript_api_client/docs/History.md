# A11ywatchClient.History

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** |  | [optional] 
**domain** | **String** |  | [optional] 
**insight** | [**WebsitesInsight**](WebsitesInsight.md) |  | [optional] 
**pageHeaders** | **[String]** |  | [optional] 
**issuesInfo** | [**IssuesInfo**](IssuesInfo.md) |  | [optional] 


