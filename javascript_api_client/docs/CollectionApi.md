# A11ywatchClient.CollectionApi

All URIs are relative to *https://api.a11ywatch.com/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getAnalytics**](CollectionApi.md#getAnalytics) | **GET** /list/analytics | Get the analytics for a website
[**getIssues**](CollectionApi.md#getIssues) | **GET** /list/issue | List the issues for a website
[**getPageSpeed**](CollectionApi.md#getPageSpeed) | **GET** /list/pagespeed | Get the pagespeed for a website
[**getPages**](CollectionApi.md#getPages) | **GET** /list/pages | List the pages in order for a website
[**getWebsites**](CollectionApi.md#getWebsites) | **GET** /list/website | Returns websites for the user in alphabetical order



## getAnalytics

> Object getAnalytics(opts)

Get the analytics for a website

### Example

```javascript
import A11ywatchClient from 'a11ywatch_client';
let defaultClient = A11ywatchClient.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new A11ywatchClient.CollectionApi();
let opts = {
  'offset': "offset_example", // String | The page offset for the next set
  'domain': "domain_example" // String | Domain of website that needs to be fetched
};
apiInstance.getAnalytics(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **offset** | **String**| The page offset for the next set | [optional] 
 **domain** | **String**| Domain of website that needs to be fetched | [optional] 

### Return type

**Object**

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getIssues

> Object getIssues(opts)

List the issues for a website

### Example

```javascript
import A11ywatchClient from 'a11ywatch_client';
let defaultClient = A11ywatchClient.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new A11ywatchClient.CollectionApi();
let opts = {
  'offset': "offset_example", // String | The page offset for the next set
  'domain': "domain_example" // String | Domain of website that needs to be fetched
};
apiInstance.getIssues(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **offset** | **String**| The page offset for the next set | [optional] 
 **domain** | **String**| Domain of website that needs to be fetched | [optional] 

### Return type

**Object**

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getPageSpeed

> Object getPageSpeed(opts)

Get the pagespeed for a website

### Example

```javascript
import A11ywatchClient from 'a11ywatch_client';
let defaultClient = A11ywatchClient.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new A11ywatchClient.CollectionApi();
let opts = {
  'offset': "offset_example", // String | The page offset for the next set
  'domain': "domain_example" // String | Domain of website that needs to be fetched
};
apiInstance.getPageSpeed(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **offset** | **String**| The page offset for the next set | [optional] 
 **domain** | **String**| Domain of website that needs to be fetched | [optional] 

### Return type

**Object**

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getPages

> Object getPages(opts)

List the pages in order for a website

### Example

```javascript
import A11ywatchClient from 'a11ywatch_client';
let defaultClient = A11ywatchClient.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new A11ywatchClient.CollectionApi();
let opts = {
  'offset': "offset_example", // String | The page offset for the next set
  'domain': "domain_example" // String | Domain of website that needs to be fetched
};
apiInstance.getPages(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **offset** | **String**| The page offset for the next set | [optional] 
 **domain** | **String**| Domain of website that needs to be fetched | [optional] 

### Return type

**Object**

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getWebsites

> Object getWebsites(opts)

Returns websites for the user in alphabetical order

Returns a map of websites

### Example

```javascript
import A11ywatchClient from 'a11ywatch_client';
let defaultClient = A11ywatchClient.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new A11ywatchClient.CollectionApi();
let opts = {
  'offset': "offset_example" // String | The page offset for the next set
};
apiInstance.getWebsites(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **offset** | **String**| The page offset for the next set | [optional] 

### Return type

**Object**

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

