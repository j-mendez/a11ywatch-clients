# A11ywatchClient.WebsiteInput

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**url** | **String** |  | [optional] 
**mobile** | **Boolean** |  | [optional] 
**pageInsights** | **Boolean** |  | [optional] 
**ua** | **String** |  | [optional] 
**standard** | **String** |  | [optional] 
**robots** | **String** |  | [optional] 
**subdomains** | **Boolean** |  | [optional] 
**tld** | **Boolean** |  | [optional] 
**customHeaders** | **[String]** |  | [optional] 


