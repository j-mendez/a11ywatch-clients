# A11ywatchClient.Report

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** |  | [optional] 
**domain** | **String** |  | [optional] 
**pageUrl** | **String** |  | [optional] 
**issues** | [**[PageIssue]**](PageIssue.md) |  | [optional] 
**issuesInfo** | [**IssuesInfo**](IssuesInfo.md) |  | [optional] 


