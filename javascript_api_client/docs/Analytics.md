# A11ywatchClient.Analytics

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** |  | [optional] 
**domain** | **String** |  | [optional] 
**pageUrl** | **String** |  | [optional] 
**userId** | **Number** |  | [optional] 
**accessScore** | **Number** |  | [optional] 
**possibleIssuesFixedByCdn** | **Number** |  | [optional] 
**totalIssues** | **Number** |  | [optional] 
**issuesFixedByCdn** | **Number** |  | [optional] 
**errorCount** | **Number** |  | [optional] 
**warningCount** | **Number** |  | [optional] 
**noticeCount** | **Number** |  | [optional] 


