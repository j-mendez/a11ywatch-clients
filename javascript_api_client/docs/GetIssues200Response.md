# A11ywatchClient.GetIssues200Response

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**[Issues]**](Issues.md) |  | [optional] 


