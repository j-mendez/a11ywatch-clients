# A11ywatchClient.IssuesInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**accessScoreAverage** | **Number** |  | [optional] 
**possibleIssuesFixedByCdn** | **Number** |  | [optional] 
**totalIssues** | **Number** |  | [optional] 
**issuesFixedByCdn** | **Number** |  | [optional] 
**errorCount** | **Number** |  | [optional] 
**warningCount** | **Number** |  | [optional] 
**noticeCount** | **Number** |  | [optional] 
**pageCount** | **Number** |  | [optional] 


