# A11ywatchClient.Websites

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** |  | [optional] 
**userId** | **Number** |  | [optional] 
**url** | **String** |  | [optional] 
**domain** | **String** |  | [optional] 
**crawlDuration** | **Number** |  | [optional] 
**cdnConnected** | **Boolean** |  | [optional] 
**pageInsights** | **Boolean** |  | [optional] 
**online** | **Boolean** |  | [optional] 
**mobile** | **Boolean** |  | [optional] 
**robots** | **Boolean** |  | [optional] 
**insight** | [**WebsitesInsight**](WebsitesInsight.md) |  | [optional] 
**pageHeaders** | **[String]** |  | [optional] 
**pageLoadTime** | [**PageLoadTime**](PageLoadTime.md) |  | [optional] 
**issuesInfo** | [**IssuesInfo**](IssuesInfo.md) |  | [optional] 


