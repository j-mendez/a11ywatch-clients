# A11ywatchClient.CrawlInput

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**domain** | **String** |  | [optional] 
**url** | **String** |  | [optional] 


