# A11ywatchClient.GetPages200Response

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**[Pages]**](Pages.md) |  | [optional] 


