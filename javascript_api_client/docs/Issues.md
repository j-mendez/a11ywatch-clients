# A11ywatchClient.Issues

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** |  | [optional] 
**userId** | **Number** |  | [optional] 
**domain** | **String** |  | [optional] 
**pageUrl** | **String** |  | [optional] 
**issues** | [**PageIssue**](PageIssue.md) |  | [optional] 


