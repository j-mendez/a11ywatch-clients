# A11ywatchClient.PageLoadTime

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**duration** | **Number** |  | [optional] 
**durationFormated** | **String** |  | [optional] 
**color** | **String** |  | [optional] 


