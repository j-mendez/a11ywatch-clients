# A11ywatchClient.PageIssue

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**recurrence** | **Number** |  | [optional] 
**typeCode** | **Number** |  | [optional] 
**code** | **String** |  | [optional] 
**message** | **String** |  | [optional] 
**context** | **String** |  | [optional] 
**selector** | **String** |  | [optional] 
**runner** | **String** |  | [optional] 
**type** | **String** |  | [optional] 


