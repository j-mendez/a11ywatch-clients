# A11ywatchClient.CrawlWebsitesSync200Response

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | **Boolean** |  | [optional] 
**message** | **String** |  | [optional] 


