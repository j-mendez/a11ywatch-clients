# A11ywatchClient.GetAnalytics200Response

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**[Analytics]**](Analytics.md) |  | [optional] 


