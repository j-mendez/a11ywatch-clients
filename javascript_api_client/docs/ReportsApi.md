# A11ywatchClient.ReportsApi

All URIs are relative to *https://api.a11ywatch.com/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**crawlWebsiteStream**](ReportsApi.md#crawlWebsiteStream) | **POST** /crawl | Multi-page crawl a website streaming issues on found
[**crawlWebsitesSync**](ReportsApi.md#crawlWebsitesSync) | **POST** /websites-sync | Multi-page crawl all websites attached to account
[**getReport**](ReportsApi.md#getReport) | **GET** /report | Get the report from a previus scan
[**scanWebsite**](ReportsApi.md#scanWebsite) | **POST** /scan | Scan a website for issues
[**scanWebsiteSimple**](ReportsApi.md#scanWebsiteSimple) | **POST** /scan-simple | Scan a website for issues without storing data and limited responses.



## crawlWebsiteStream

> Object crawlWebsiteStream(transferEncoding, websiteInput)

Multi-page crawl a website streaming issues on found

### Example

```javascript
import A11ywatchClient from 'a11ywatch_client';
let defaultClient = A11ywatchClient.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new A11ywatchClient.ReportsApi();
let transferEncoding = "'Chunked'"; // String | 
let websiteInput = new A11ywatchClient.WebsiteInput(); // WebsiteInput | The website standard body
apiInstance.crawlWebsiteStream(transferEncoding, websiteInput, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **transferEncoding** | **String**|  | [default to &#39;Chunked&#39;]
 **websiteInput** | [**WebsiteInput**](WebsiteInput.md)| The website standard body | 

### Return type

**Object**

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## crawlWebsitesSync

> Object crawlWebsitesSync()

Multi-page crawl all websites attached to account

### Example

```javascript
import A11ywatchClient from 'a11ywatch_client';
let defaultClient = A11ywatchClient.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new A11ywatchClient.ReportsApi();
apiInstance.crawlWebsitesSync((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

This endpoint does not need any parameter.

### Return type

**Object**

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getReport

> Object getReport(opts)

Get the report from a previus scan

### Example

```javascript
import A11ywatchClient from 'a11ywatch_client';
let defaultClient = A11ywatchClient.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new A11ywatchClient.ReportsApi();
let opts = {
  'url': "url_example", // String | The page url or domain for the report
  'domain': "domain_example" // String | Domain of website that needs to be fetched
};
apiInstance.getReport(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **url** | **String**| The page url or domain for the report | [optional] 
 **domain** | **String**| Domain of website that needs to be fetched | [optional] 

### Return type

**Object**

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## scanWebsite

> Object scanWebsite(websiteInput)

Scan a website for issues

### Example

```javascript
import A11ywatchClient from 'a11ywatch_client';
let defaultClient = A11ywatchClient.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new A11ywatchClient.ReportsApi();
let websiteInput = new A11ywatchClient.WebsiteInput(); // WebsiteInput | The website standard body
apiInstance.scanWebsite(websiteInput, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **websiteInput** | [**WebsiteInput**](WebsiteInput.md)| The website standard body | 

### Return type

**Object**

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## scanWebsiteSimple

> Object scanWebsiteSimple(websiteInput)

Scan a website for issues without storing data and limited responses.

### Example

```javascript
import A11ywatchClient from 'a11ywatch_client';
let defaultClient = A11ywatchClient.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new A11ywatchClient.ReportsApi();
let websiteInput = new A11ywatchClient.WebsiteInput(); // WebsiteInput | The website standard body
apiInstance.scanWebsiteSimple(websiteInput, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **websiteInput** | [**WebsiteInput**](WebsiteInput.md)| The website standard body | 

### Return type

**Object**

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

