# A11ywatchClient.Pages

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** |  | [optional] 
**userId** | **Number** |  | [optional] 
**domain** | **String** |  | [optional] 
**url** | **String** |  | [optional] 
**cdnConnected** | **Boolean** |  | [optional] 
**online** | **Boolean** |  | [optional] 
**pageLoadTime** | [**PageLoadTime**](PageLoadTime.md) |  | [optional] 
**insight** | [**WebsitesInsight**](WebsitesInsight.md) |  | [optional] 
**issuesInfo** | [**IssuesInfo**](IssuesInfo.md) |  | [optional] 
**lastScanDate** | **String** |  | [optional] 


