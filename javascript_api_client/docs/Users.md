# A11ywatchClient.Users

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** |  | [optional] 
**email** | **String** |  | [optional] 
**jwt** | **String** |  | [optional] 
**role** | **String** |  | [optional] 
**password** | **String** |  | [optional] 
**alertEnabled** | **Boolean** |  | [optional] 


