/**
 * A11ywatch Client
 * The web accessibility tool built for scale.
 *
 * The version of the OpenAPI document: 0.7.66
 * Contact: support@a11ywatch.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';
import PageIssue from './PageIssue';

/**
 * The Issues model module.
 * @module model/Issues
 * @version 0.7.66
 */
class Issues {
    /**
     * Constructs a new <code>Issues</code>.
     * @alias module:model/Issues
     */
    constructor() { 
        
        Issues.initialize(this);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj) { 
    }

    /**
     * Constructs a <code>Issues</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/Issues} obj Optional instance to populate.
     * @return {module:model/Issues} The populated <code>Issues</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new Issues();

            if (data.hasOwnProperty('_id')) {
                obj['_id'] = ApiClient.convertToType(data['_id'], 'Number');
            }
            if (data.hasOwnProperty('userId')) {
                obj['userId'] = ApiClient.convertToType(data['userId'], 'Number');
            }
            if (data.hasOwnProperty('domain')) {
                obj['domain'] = ApiClient.convertToType(data['domain'], 'String');
            }
            if (data.hasOwnProperty('pageUrl')) {
                obj['pageUrl'] = ApiClient.convertToType(data['pageUrl'], 'String');
            }
            if (data.hasOwnProperty('issues')) {
                obj['issues'] = PageIssue.constructFromObject(data['issues']);
            }
        }
        return obj;
    }


}

/**
 * @member {Number} _id
 */
Issues.prototype['_id'] = undefined;

/**
 * @member {Number} userId
 */
Issues.prototype['userId'] = undefined;

/**
 * @member {String} domain
 */
Issues.prototype['domain'] = undefined;

/**
 * @member {String} pageUrl
 */
Issues.prototype['pageUrl'] = undefined;

/**
 * @member {module:model/PageIssue} issues
 */
Issues.prototype['issues'] = undefined;






export default Issues;

