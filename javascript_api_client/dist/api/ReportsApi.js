"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
var _ApiClient = _interopRequireDefault(require("../ApiClient"));
var _WebsiteInput = _interopRequireDefault(require("../model/WebsiteInput"));
function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }
function _typeof(obj) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (obj) { return typeof obj; } : function (obj) { return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }, _typeof(obj); }
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, _toPropertyKey(descriptor.key), descriptor); } }
function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }
function _toPropertyKey(arg) { var key = _toPrimitive(arg, "string"); return _typeof(key) === "symbol" ? key : String(key); }
function _toPrimitive(input, hint) { if (_typeof(input) !== "object" || input === null) return input; var prim = input[Symbol.toPrimitive]; if (prim !== undefined) { var res = prim.call(input, hint || "default"); if (_typeof(res) !== "object") return res; throw new TypeError("@@toPrimitive must return a primitive value."); } return (hint === "string" ? String : Number)(input); } /**
                                                                                                                                                                                                                                                                                                                                                                                               * A11ywatch Client
                                                                                                                                                                                                                                                                                                                                                                                               * The web accessibility tool built for scale.
                                                                                                                                                                                                                                                                                                                                                                                               *
                                                                                                                                                                                                                                                                                                                                                                                               * The version of the OpenAPI document: 0.7.66
                                                                                                                                                                                                                                                                                                                                                                                               * Contact: support@a11ywatch.com
                                                                                                                                                                                                                                                                                                                                                                                               *
                                                                                                                                                                                                                                                                                                                                                                                               * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
                                                                                                                                                                                                                                                                                                                                                                                               * https://openapi-generator.tech
                                                                                                                                                                                                                                                                                                                                                                                               * Do not edit the class manually.
                                                                                                                                                                                                                                                                                                                                                                                               *
                                                                                                                                                                                                                                                                                                                                                                                               */
/**
* Reports service.
* @module api/ReportsApi
* @version 0.7.66
*/
var ReportsApi = /*#__PURE__*/function () {
  /**
  * Constructs a new ReportsApi. 
  * @alias module:api/ReportsApi
  * @class
  * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
  * default to {@link module:ApiClient#instance} if unspecified.
  */
  function ReportsApi(apiClient) {
    _classCallCheck(this, ReportsApi);
    this.apiClient = apiClient || _ApiClient["default"].instance;
  }

  /**
   * Callback function to receive the result of the crawlWebsiteStream operation.
   * @callback module:api/ReportsApi~crawlWebsiteStreamCallback
   * @param {String} error Error message, if any.
   * @param {Object} data The data returned by the service call.
   * @param {String} response The complete HTTP response.
   */

  /**
   * Multi-page crawl a website streaming issues on found
   * @param {String} transferEncoding 
   * @param {module:model/WebsiteInput} websiteInput The website standard body
   * @param {module:api/ReportsApi~crawlWebsiteStreamCallback} callback The callback function, accepting three arguments: error, data, response
   * data is of type: {@link Object}
   */
  _createClass(ReportsApi, [{
    key: "crawlWebsiteStream",
    value: function crawlWebsiteStream(transferEncoding, websiteInput, callback) {
      var postBody = websiteInput;
      // verify the required parameter 'transferEncoding' is set
      if (transferEncoding === undefined || transferEncoding === null) {
        throw new Error("Missing the required parameter 'transferEncoding' when calling crawlWebsiteStream");
      }
      // verify the required parameter 'websiteInput' is set
      if (websiteInput === undefined || websiteInput === null) {
        throw new Error("Missing the required parameter 'websiteInput' when calling crawlWebsiteStream");
      }
      var pathParams = {};
      var queryParams = {};
      var headerParams = {
        'Transfer-Encoding': transferEncoding
      };
      var formParams = {};
      var authNames = ['bearerAuth'];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/crawl', 'POST', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }

    /**
     * Callback function to receive the result of the crawlWebsitesSync operation.
     * @callback module:api/ReportsApi~crawlWebsitesSyncCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Multi-page crawl all websites attached to account
     * @param {module:api/ReportsApi~crawlWebsitesSyncCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */
  }, {
    key: "crawlWebsitesSync",
    value: function crawlWebsitesSync(callback) {
      var postBody = null;
      var pathParams = {};
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['bearerAuth'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/websites-sync', 'POST', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }

    /**
     * Callback function to receive the result of the getReport operation.
     * @callback module:api/ReportsApi~getReportCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get the report from a previus scan
     * @param {Object} opts Optional parameters
     * @param {String} opts.url The page url or domain for the report
     * @param {String} opts.domain Domain of website that needs to be fetched
     * @param {module:api/ReportsApi~getReportCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */
  }, {
    key: "getReport",
    value: function getReport(opts, callback) {
      opts = opts || {};
      var postBody = null;
      var pathParams = {};
      var queryParams = {
        'url': opts['url'],
        'domain': opts['domain']
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['bearerAuth'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/report', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }

    /**
     * Callback function to receive the result of the scanWebsite operation.
     * @callback module:api/ReportsApi~scanWebsiteCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Scan a website for issues
     * @param {module:model/WebsiteInput} websiteInput The website standard body
     * @param {module:api/ReportsApi~scanWebsiteCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */
  }, {
    key: "scanWebsite",
    value: function scanWebsite(websiteInput, callback) {
      var postBody = websiteInput;
      // verify the required parameter 'websiteInput' is set
      if (websiteInput === undefined || websiteInput === null) {
        throw new Error("Missing the required parameter 'websiteInput' when calling scanWebsite");
      }
      var pathParams = {};
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['bearerAuth'];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/scan', 'POST', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }

    /**
     * Callback function to receive the result of the scanWebsiteSimple operation.
     * @callback module:api/ReportsApi~scanWebsiteSimpleCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Scan a website for issues without storing data and limited responses.
     * @param {module:model/WebsiteInput} websiteInput The website standard body
     * @param {module:api/ReportsApi~scanWebsiteSimpleCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */
  }, {
    key: "scanWebsiteSimple",
    value: function scanWebsiteSimple(websiteInput, callback) {
      var postBody = websiteInput;
      // verify the required parameter 'websiteInput' is set
      if (websiteInput === undefined || websiteInput === null) {
        throw new Error("Missing the required parameter 'websiteInput' when calling scanWebsiteSimple");
      }
      var pathParams = {};
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['bearerAuth'];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/scan-simple', 'POST', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
  }]);
  return ReportsApi;
}();
exports["default"] = ReportsApi;