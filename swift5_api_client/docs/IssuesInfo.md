# IssuesInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**accessScoreAverage** | **Int64** |  | [optional] 
**possibleIssuesFixedByCdn** | **Int64** |  | [optional] 
**totalIssues** | **Int64** |  | [optional] 
**issuesFixedByCdn** | **Int64** |  | [optional] 
**errorCount** | **Int64** |  | [optional] 
**warningCount** | **Int64** |  | [optional] 
**noticeCount** | **Int64** |  | [optional] 
**pageCount** | **Int64** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


