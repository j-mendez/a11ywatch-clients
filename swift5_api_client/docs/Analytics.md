# Analytics

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Int64** |  | [optional] 
**domain** | **String** |  | [optional] 
**pageUrl** | **String** |  | [optional] 
**userId** | **Int64** |  | [optional] 
**accessScore** | **Int64** |  | [optional] 
**possibleIssuesFixedByCdn** | **Int64** |  | [optional] 
**totalIssues** | **Int64** |  | [optional] 
**issuesFixedByCdn** | **Int64** |  | [optional] 
**errorCount** | **Int64** |  | [optional] 
**warningCount** | **Int64** |  | [optional] 
**noticeCount** | **Int64** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


