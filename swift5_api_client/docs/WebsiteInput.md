# WebsiteInput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**url** | **String** |  | [optional] 
**mobile** | **Bool** |  | [optional] 
**pageInsights** | **Bool** |  | [optional] 
**ua** | **String** |  | [optional] 
**standard** | **String** |  | [optional] 
**robots** | **String** |  | [optional] 
**subdomains** | **Bool** |  | [optional] 
**tld** | **Bool** |  | [optional] 
**customHeaders** | **[String]** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


