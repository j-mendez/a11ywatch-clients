# Users

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Int64** |  | [optional] 
**email** | **String** |  | [optional] 
**jwt** | **String** |  | [optional] 
**role** | **String** |  | [optional] 
**password** | **String** |  | [optional] 
**alertEnabled** | **Bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


