# PageIssue

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**recurrence** | **Int64** |  | [optional] 
**typeCode** | **Int64** |  | [optional] 
**code** | **String** |  | [optional] 
**message** | **String** |  | [optional] 
**context** | **String** |  | [optional] 
**selector** | **String** |  | [optional] 
**runner** | **String** |  | [optional] 
**type** | **String** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


