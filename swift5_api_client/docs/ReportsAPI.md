# ReportsAPI

All URIs are relative to *https://api.a11ywatch.com/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**crawlWebsiteStream**](ReportsAPI.md#crawlwebsitestream) | **POST** /crawl | Multi-page crawl a website streaming issues on found
[**crawlWebsitesSync**](ReportsAPI.md#crawlwebsitessync) | **POST** /websites-sync | Multi-page crawl all websites attached to account
[**getReport**](ReportsAPI.md#getreport) | **GET** /report | Get the report from a previus scan
[**scanWebsite**](ReportsAPI.md#scanwebsite) | **POST** /scan | Scan a website for issues
[**scanWebsiteSimple**](ReportsAPI.md#scanwebsitesimple) | **POST** /scan-simple | Scan a website for issues without storing data and limited responses.


# **crawlWebsiteStream**
```swift
    open class func crawlWebsiteStream(transferEncoding: String, websiteInput: WebsiteInput, completion: @escaping (_ data: AnyCodable?, _ error: Error?) -> Void)
```

Multi-page crawl a website streaming issues on found

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let transferEncoding = "transferEncoding_example" // String |  (default to "Chunked")
let websiteInput = WebsiteInput(url: "url_example", mobile: false, pageInsights: false, ua: "ua_example", standard: "standard_example", robots: "robots_example", subdomains: false, tld: false, customHeaders: ["customHeaders_example"]) // WebsiteInput | The website standard body

// Multi-page crawl a website streaming issues on found
ReportsAPI.crawlWebsiteStream(transferEncoding: transferEncoding, websiteInput: websiteInput) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **transferEncoding** | **String** |  | [default to &quot;Chunked&quot;]
 **websiteInput** | [**WebsiteInput**](WebsiteInput.md) | The website standard body | 

### Return type

[**AnyCodable**](AnyCodable.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **crawlWebsitesSync**
```swift
    open class func crawlWebsitesSync(completion: @escaping (_ data: AnyCodable?, _ error: Error?) -> Void)
```

Multi-page crawl all websites attached to account

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient


// Multi-page crawl all websites attached to account
ReportsAPI.crawlWebsitesSync() { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**AnyCodable**](AnyCodable.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getReport**
```swift
    open class func getReport(url: String? = nil, domain: String? = nil, completion: @escaping (_ data: AnyCodable?, _ error: Error?) -> Void)
```

Get the report from a previus scan

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let url = "url_example" // String | The page url or domain for the report (optional)
let domain = "domain_example" // String | Domain of website that needs to be fetched (optional)

// Get the report from a previus scan
ReportsAPI.getReport(url: url, domain: domain) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **url** | **String** | The page url or domain for the report | [optional] 
 **domain** | **String** | Domain of website that needs to be fetched | [optional] 

### Return type

[**AnyCodable**](AnyCodable.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **scanWebsite**
```swift
    open class func scanWebsite(websiteInput: WebsiteInput, completion: @escaping (_ data: AnyCodable?, _ error: Error?) -> Void)
```

Scan a website for issues

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let websiteInput = WebsiteInput(url: "url_example", mobile: false, pageInsights: false, ua: "ua_example", standard: "standard_example", robots: "robots_example", subdomains: false, tld: false, customHeaders: ["customHeaders_example"]) // WebsiteInput | The website standard body

// Scan a website for issues
ReportsAPI.scanWebsite(websiteInput: websiteInput) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **websiteInput** | [**WebsiteInput**](WebsiteInput.md) | The website standard body | 

### Return type

[**AnyCodable**](AnyCodable.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **scanWebsiteSimple**
```swift
    open class func scanWebsiteSimple(websiteInput: WebsiteInput, completion: @escaping (_ data: AnyCodable?, _ error: Error?) -> Void)
```

Scan a website for issues without storing data and limited responses.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let websiteInput = WebsiteInput(url: "url_example", mobile: false, pageInsights: false, ua: "ua_example", standard: "standard_example", robots: "robots_example", subdomains: false, tld: false, customHeaders: ["customHeaders_example"]) // WebsiteInput | The website standard body

// Scan a website for issues without storing data and limited responses.
ReportsAPI.scanWebsiteSimple(websiteInput: websiteInput) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **websiteInput** | [**WebsiteInput**](WebsiteInput.md) | The website standard body | 

### Return type

[**AnyCodable**](AnyCodable.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

