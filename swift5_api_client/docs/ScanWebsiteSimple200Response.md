# ScanWebsiteSimple200Response

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**Report**](Report.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


