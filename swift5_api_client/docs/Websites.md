# Websites

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Int64** |  | [optional] 
**userId** | **Int64** |  | [optional] 
**url** | **String** |  | [optional] 
**domain** | **String** |  | [optional] 
**crawlDuration** | **Int64** |  | [optional] 
**cdnConnected** | **Bool** |  | [optional] 
**pageInsights** | **Bool** |  | [optional] 
**online** | **Bool** |  | [optional] 
**mobile** | **Bool** |  | [optional] 
**robots** | **Bool** |  | [optional] 
**insight** | [**WebsitesInsight**](WebsitesInsight.md) |  | [optional] 
**pageHeaders** | **[String]** |  | [optional] 
**pageLoadTime** | [**PageLoadTime**](PageLoadTime.md) |  | [optional] 
**issuesInfo** | [**IssuesInfo**](IssuesInfo.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


