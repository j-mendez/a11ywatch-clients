# Pages

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Int64** |  | [optional] 
**userId** | **Int64** |  | [optional] 
**domain** | **String** |  | [optional] 
**url** | **String** |  | [optional] 
**cdnConnected** | **Bool** |  | [optional] 
**online** | **Bool** |  | [optional] 
**pageLoadTime** | [**PageLoadTime**](PageLoadTime.md) |  | [optional] 
**insight** | [**WebsitesInsight**](WebsitesInsight.md) |  | [optional] 
**issuesInfo** | [**IssuesInfo**](IssuesInfo.md) |  | [optional] 
**lastScanDate** | **String** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


