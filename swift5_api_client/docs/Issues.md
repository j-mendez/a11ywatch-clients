# Issues

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Int64** |  | [optional] 
**userId** | **Int64** |  | [optional] 
**domain** | **String** |  | [optional] 
**pageUrl** | **String** |  | [optional] 
**issues** | [**PageIssue**](PageIssue.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


