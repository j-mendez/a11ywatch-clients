# History

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Int64** |  | [optional] 
**domain** | **String** |  | [optional] 
**insight** | [**WebsitesInsight**](WebsitesInsight.md) |  | [optional] 
**pageHeaders** | **[String]** |  | [optional] 
**issuesInfo** | [**IssuesInfo**](IssuesInfo.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


