//
// CrawlInput.swift
//
// Generated by openapi-generator
// https://openapi-generator.tech
//

import Foundation
#if canImport(AnyCodable)
import AnyCodable
#endif

public struct CrawlInput: Codable, Hashable {

    public var domain: String?
    public var url: String?

    public init(domain: String? = nil, url: String? = nil) {
        self.domain = domain
        self.url = url
    }

    public enum CodingKeys: String, CodingKey, CaseIterable {
        case domain
        case url
    }

    // Encodable protocol methods

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encodeIfPresent(domain, forKey: .domain)
        try container.encodeIfPresent(url, forKey: .url)
    }
}

