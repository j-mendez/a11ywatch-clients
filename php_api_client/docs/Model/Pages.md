# # Pages

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_id** | **int** |  | [optional]
**user_id** | **int** |  | [optional]
**domain** | **string** |  | [optional]
**url** | **string** |  | [optional]
**cdn_connected** | **bool** |  | [optional]
**online** | **bool** |  | [optional]
**page_load_time** | [**\OpenAPI\Client\Model\PageLoadTime**](PageLoadTime.md) |  | [optional]
**insight** | [**\OpenAPI\Client\Model\WebsitesInsight**](WebsitesInsight.md) |  | [optional]
**issues_info** | [**\OpenAPI\Client\Model\IssuesInfo**](IssuesInfo.md) |  | [optional]
**last_scan_date** | **string** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
