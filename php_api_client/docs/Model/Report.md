# # Report

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_id** | **int** |  | [optional]
**domain** | **string** |  | [optional]
**page_url** | **string** |  | [optional]
**issues** | [**\OpenAPI\Client\Model\PageIssue[]**](PageIssue.md) |  | [optional]
**issues_info** | [**\OpenAPI\Client\Model\IssuesInfo**](IssuesInfo.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
