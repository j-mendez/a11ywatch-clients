# # Websites

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_id** | **int** |  | [optional]
**user_id** | **int** |  | [optional]
**url** | **string** |  | [optional]
**domain** | **string** |  | [optional]
**crawl_duration** | **int** |  | [optional]
**cdn_connected** | **bool** |  | [optional]
**page_insights** | **bool** |  | [optional]
**online** | **bool** |  | [optional]
**mobile** | **bool** |  | [optional]
**robots** | **bool** |  | [optional]
**insight** | [**\OpenAPI\Client\Model\WebsitesInsight**](WebsitesInsight.md) |  | [optional]
**page_headers** | **string[]** |  | [optional]
**page_load_time** | [**\OpenAPI\Client\Model\PageLoadTime**](PageLoadTime.md) |  | [optional]
**issues_info** | [**\OpenAPI\Client\Model\IssuesInfo**](IssuesInfo.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
