# # IssuesInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**access_score_average** | **int** |  | [optional]
**possible_issues_fixed_by_cdn** | **int** |  | [optional]
**total_issues** | **int** |  | [optional]
**issues_fixed_by_cdn** | **int** |  | [optional]
**error_count** | **int** |  | [optional]
**warning_count** | **int** |  | [optional]
**notice_count** | **int** |  | [optional]
**page_count** | **int** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
