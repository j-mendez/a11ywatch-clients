# # PageIssue

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**recurrence** | **int** |  | [optional]
**type_code** | **int** |  | [optional]
**code** | **string** |  | [optional]
**message** | **string** |  | [optional]
**context** | **string** |  | [optional]
**selector** | **string** |  | [optional]
**runner** | **string** |  | [optional]
**type** | **string** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
