# # WebsiteInput

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**url** | **string** |  | [optional]
**mobile** | **bool** |  | [optional]
**page_insights** | **bool** |  | [optional]
**ua** | **string** |  | [optional]
**standard** | **string** |  | [optional]
**robots** | **string** |  | [optional]
**subdomains** | **bool** |  | [optional]
**tld** | **bool** |  | [optional]
**custom_headers** | **string[]** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
