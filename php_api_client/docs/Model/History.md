# # History

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_id** | **int** |  | [optional]
**domain** | **string** |  | [optional]
**insight** | [**\OpenAPI\Client\Model\WebsitesInsight**](WebsitesInsight.md) |  | [optional]
**page_headers** | **string[]** |  | [optional]
**issues_info** | [**\OpenAPI\Client\Model\IssuesInfo**](IssuesInfo.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
