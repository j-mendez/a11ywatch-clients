# # Analytics

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_id** | **int** |  | [optional]
**domain** | **string** |  | [optional]
**page_url** | **string** |  | [optional]
**user_id** | **int** |  | [optional]
**access_score** | **int** |  | [optional]
**possible_issues_fixed_by_cdn** | **int** |  | [optional]
**total_issues** | **int** |  | [optional]
**issues_fixed_by_cdn** | **int** |  | [optional]
**error_count** | **int** |  | [optional]
**warning_count** | **int** |  | [optional]
**notice_count** | **int** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
