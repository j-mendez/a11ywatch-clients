# # Users

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_id** | **int** |  | [optional]
**email** | **string** |  | [optional]
**jwt** | **string** |  | [optional]
**role** | **string** |  | [optional]
**password** | **string** |  | [optional]
**alert_enabled** | **bool** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
