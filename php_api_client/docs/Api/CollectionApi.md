# OpenAPI\Client\CollectionApi

All URIs are relative to https://api.a11ywatch.com/api.

Method | HTTP request | Description
------------- | ------------- | -------------
[**getAnalytics()**](CollectionApi.md#getAnalytics) | **GET** /list/analytics | Get the analytics for a website
[**getIssues()**](CollectionApi.md#getIssues) | **GET** /list/issue | List the issues for a website
[**getPageSpeed()**](CollectionApi.md#getPageSpeed) | **GET** /list/pagespeed | Get the pagespeed for a website
[**getPages()**](CollectionApi.md#getPages) | **GET** /list/pages | List the pages in order for a website
[**getWebsites()**](CollectionApi.md#getWebsites) | **GET** /list/website | Returns websites for the user in alphabetical order


## `getAnalytics()`

```php
getAnalytics($offset, $domain): object
```

Get the analytics for a website

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (JWT) authorization: bearerAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\CollectionApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$offset = 'offset_example'; // string | The page offset for the next set
$domain = 'domain_example'; // string | Domain of website that needs to be fetched

try {
    $result = $apiInstance->getAnalytics($offset, $domain);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CollectionApi->getAnalytics: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **offset** | **string**| The page offset for the next set | [optional]
 **domain** | **string**| Domain of website that needs to be fetched | [optional]

### Return type

**object**

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `getIssues()`

```php
getIssues($offset, $domain): object
```

List the issues for a website

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (JWT) authorization: bearerAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\CollectionApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$offset = 'offset_example'; // string | The page offset for the next set
$domain = 'domain_example'; // string | Domain of website that needs to be fetched

try {
    $result = $apiInstance->getIssues($offset, $domain);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CollectionApi->getIssues: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **offset** | **string**| The page offset for the next set | [optional]
 **domain** | **string**| Domain of website that needs to be fetched | [optional]

### Return type

**object**

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `getPageSpeed()`

```php
getPageSpeed($offset, $domain): object
```

Get the pagespeed for a website

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (JWT) authorization: bearerAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\CollectionApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$offset = 'offset_example'; // string | The page offset for the next set
$domain = 'domain_example'; // string | Domain of website that needs to be fetched

try {
    $result = $apiInstance->getPageSpeed($offset, $domain);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CollectionApi->getPageSpeed: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **offset** | **string**| The page offset for the next set | [optional]
 **domain** | **string**| Domain of website that needs to be fetched | [optional]

### Return type

**object**

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `getPages()`

```php
getPages($offset, $domain): object
```

List the pages in order for a website

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (JWT) authorization: bearerAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\CollectionApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$offset = 'offset_example'; // string | The page offset for the next set
$domain = 'domain_example'; // string | Domain of website that needs to be fetched

try {
    $result = $apiInstance->getPages($offset, $domain);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CollectionApi->getPages: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **offset** | **string**| The page offset for the next set | [optional]
 **domain** | **string**| Domain of website that needs to be fetched | [optional]

### Return type

**object**

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `getWebsites()`

```php
getWebsites($offset): object
```

Returns websites for the user in alphabetical order

Returns a map of websites

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (JWT) authorization: bearerAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\CollectionApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$offset = 'offset_example'; // string | The page offset for the next set

try {
    $result = $apiInstance->getWebsites($offset);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CollectionApi->getWebsites: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **offset** | **string**| The page offset for the next set | [optional]

### Return type

**object**

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
