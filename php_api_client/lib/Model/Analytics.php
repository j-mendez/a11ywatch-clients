<?php
/**
 * Analytics
 *
 * PHP version 7.3
 *
 * @category Class
 * @package  OpenAPI\Client
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * A11ywatch Client
 *
 * The web accessibility tool built for scale.
 *
 * The version of the OpenAPI document: 0.7.66
 * Contact: support@a11ywatch.com
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 5.3.0
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

namespace OpenAPI\Client\Model;

use \ArrayAccess;
use \OpenAPI\Client\ObjectSerializer;

/**
 * Analytics Class Doc Comment
 *
 * @category Class
 * @package  OpenAPI\Client
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 * @implements \ArrayAccess<TKey, TValue>
 * @template TKey int|null
 * @template TValue mixed|null
 */
class Analytics implements ModelInterface, ArrayAccess, \JsonSerializable
{
    public const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $openAPIModelName = 'Analytics';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $openAPITypes = [
        '_id' => 'int',
        'domain' => 'string',
        'page_url' => 'string',
        'user_id' => 'int',
        'access_score' => 'int',
        'possible_issues_fixed_by_cdn' => 'int',
        'total_issues' => 'int',
        'issues_fixed_by_cdn' => 'int',
        'error_count' => 'int',
        'warning_count' => 'int',
        'notice_count' => 'int'
    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      * @phpstan-var array<string, string|null>
      * @psalm-var array<string, string|null>
      */
    protected static $openAPIFormats = [
        '_id' => 'int64',
        'domain' => null,
        'page_url' => null,
        'user_id' => 'int64',
        'access_score' => 'int64',
        'possible_issues_fixed_by_cdn' => 'int64',
        'total_issues' => 'int64',
        'issues_fixed_by_cdn' => 'int64',
        'error_count' => 'int64',
        'warning_count' => 'int64',
        'notice_count' => 'int64'
    ];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function openAPITypes()
    {
        return self::$openAPITypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function openAPIFormats()
    {
        return self::$openAPIFormats;
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        '_id' => '_id',
        'domain' => 'domain',
        'page_url' => 'pageUrl',
        'user_id' => 'userId',
        'access_score' => 'accessScore',
        'possible_issues_fixed_by_cdn' => 'possibleIssuesFixedByCdn',
        'total_issues' => 'totalIssues',
        'issues_fixed_by_cdn' => 'issuesFixedByCdn',
        'error_count' => 'errorCount',
        'warning_count' => 'warningCount',
        'notice_count' => 'noticeCount'
    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        '_id' => 'setId',
        'domain' => 'setDomain',
        'page_url' => 'setPageUrl',
        'user_id' => 'setUserId',
        'access_score' => 'setAccessScore',
        'possible_issues_fixed_by_cdn' => 'setPossibleIssuesFixedByCdn',
        'total_issues' => 'setTotalIssues',
        'issues_fixed_by_cdn' => 'setIssuesFixedByCdn',
        'error_count' => 'setErrorCount',
        'warning_count' => 'setWarningCount',
        'notice_count' => 'setNoticeCount'
    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        '_id' => 'getId',
        'domain' => 'getDomain',
        'page_url' => 'getPageUrl',
        'user_id' => 'getUserId',
        'access_score' => 'getAccessScore',
        'possible_issues_fixed_by_cdn' => 'getPossibleIssuesFixedByCdn',
        'total_issues' => 'getTotalIssues',
        'issues_fixed_by_cdn' => 'getIssuesFixedByCdn',
        'error_count' => 'getErrorCount',
        'warning_count' => 'getWarningCount',
        'notice_count' => 'getNoticeCount'
    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$openAPIModelName;
    }


    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['_id'] = $data['_id'] ?? null;
        $this->container['domain'] = $data['domain'] ?? null;
        $this->container['page_url'] = $data['page_url'] ?? null;
        $this->container['user_id'] = $data['user_id'] ?? null;
        $this->container['access_score'] = $data['access_score'] ?? null;
        $this->container['possible_issues_fixed_by_cdn'] = $data['possible_issues_fixed_by_cdn'] ?? null;
        $this->container['total_issues'] = $data['total_issues'] ?? null;
        $this->container['issues_fixed_by_cdn'] = $data['issues_fixed_by_cdn'] ?? null;
        $this->container['error_count'] = $data['error_count'] ?? null;
        $this->container['warning_count'] = $data['warning_count'] ?? null;
        $this->container['notice_count'] = $data['notice_count'] ?? null;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {
        return count($this->listInvalidProperties()) === 0;
    }


    /**
     * Gets _id
     *
     * @return int|null
     */
    public function getId()
    {
        return $this->container['_id'];
    }

    /**
     * Sets _id
     *
     * @param int|null $_id _id
     *
     * @return self
     */
    public function setId($_id)
    {
        $this->container['_id'] = $_id;

        return $this;
    }

    /**
     * Gets domain
     *
     * @return string|null
     */
    public function getDomain()
    {
        return $this->container['domain'];
    }

    /**
     * Sets domain
     *
     * @param string|null $domain domain
     *
     * @return self
     */
    public function setDomain($domain)
    {
        $this->container['domain'] = $domain;

        return $this;
    }

    /**
     * Gets page_url
     *
     * @return string|null
     */
    public function getPageUrl()
    {
        return $this->container['page_url'];
    }

    /**
     * Sets page_url
     *
     * @param string|null $page_url page_url
     *
     * @return self
     */
    public function setPageUrl($page_url)
    {
        $this->container['page_url'] = $page_url;

        return $this;
    }

    /**
     * Gets user_id
     *
     * @return int|null
     */
    public function getUserId()
    {
        return $this->container['user_id'];
    }

    /**
     * Sets user_id
     *
     * @param int|null $user_id user_id
     *
     * @return self
     */
    public function setUserId($user_id)
    {
        $this->container['user_id'] = $user_id;

        return $this;
    }

    /**
     * Gets access_score
     *
     * @return int|null
     */
    public function getAccessScore()
    {
        return $this->container['access_score'];
    }

    /**
     * Sets access_score
     *
     * @param int|null $access_score access_score
     *
     * @return self
     */
    public function setAccessScore($access_score)
    {
        $this->container['access_score'] = $access_score;

        return $this;
    }

    /**
     * Gets possible_issues_fixed_by_cdn
     *
     * @return int|null
     */
    public function getPossibleIssuesFixedByCdn()
    {
        return $this->container['possible_issues_fixed_by_cdn'];
    }

    /**
     * Sets possible_issues_fixed_by_cdn
     *
     * @param int|null $possible_issues_fixed_by_cdn possible_issues_fixed_by_cdn
     *
     * @return self
     */
    public function setPossibleIssuesFixedByCdn($possible_issues_fixed_by_cdn)
    {
        $this->container['possible_issues_fixed_by_cdn'] = $possible_issues_fixed_by_cdn;

        return $this;
    }

    /**
     * Gets total_issues
     *
     * @return int|null
     */
    public function getTotalIssues()
    {
        return $this->container['total_issues'];
    }

    /**
     * Sets total_issues
     *
     * @param int|null $total_issues total_issues
     *
     * @return self
     */
    public function setTotalIssues($total_issues)
    {
        $this->container['total_issues'] = $total_issues;

        return $this;
    }

    /**
     * Gets issues_fixed_by_cdn
     *
     * @return int|null
     */
    public function getIssuesFixedByCdn()
    {
        return $this->container['issues_fixed_by_cdn'];
    }

    /**
     * Sets issues_fixed_by_cdn
     *
     * @param int|null $issues_fixed_by_cdn issues_fixed_by_cdn
     *
     * @return self
     */
    public function setIssuesFixedByCdn($issues_fixed_by_cdn)
    {
        $this->container['issues_fixed_by_cdn'] = $issues_fixed_by_cdn;

        return $this;
    }

    /**
     * Gets error_count
     *
     * @return int|null
     */
    public function getErrorCount()
    {
        return $this->container['error_count'];
    }

    /**
     * Sets error_count
     *
     * @param int|null $error_count error_count
     *
     * @return self
     */
    public function setErrorCount($error_count)
    {
        $this->container['error_count'] = $error_count;

        return $this;
    }

    /**
     * Gets warning_count
     *
     * @return int|null
     */
    public function getWarningCount()
    {
        return $this->container['warning_count'];
    }

    /**
     * Sets warning_count
     *
     * @param int|null $warning_count warning_count
     *
     * @return self
     */
    public function setWarningCount($warning_count)
    {
        $this->container['warning_count'] = $warning_count;

        return $this;
    }

    /**
     * Gets notice_count
     *
     * @return int|null
     */
    public function getNoticeCount()
    {
        return $this->container['notice_count'];
    }

    /**
     * Sets notice_count
     *
     * @param int|null $notice_count notice_count
     *
     * @return self
     */
    public function setNoticeCount($notice_count)
    {
        $this->container['notice_count'] = $notice_count;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed|null
     */
    public function offsetGet($offset)
    {
        return $this->container[$offset] ?? null;
    }

    /**
     * Sets value based on offset.
     *
     * @param int|null $offset Offset
     * @param mixed    $value  Value to be set
     *
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Serializes the object to a value that can be serialized natively by json_encode().
     * @link https://www.php.net/manual/en/jsonserializable.jsonserialize.php
     *
     * @return mixed Returns data which can be serialized by json_encode(), which is a value
     * of any type other than a resource.
     */
    public function jsonSerialize()
    {
       return ObjectSerializer::sanitizeForSerialization($this);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        return json_encode(
            ObjectSerializer::sanitizeForSerialization($this),
            JSON_PRETTY_PRINT
        );
    }

    /**
     * Gets a header-safe presentation of the object
     *
     * @return string
     */
    public function toHeaderValue()
    {
        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}


