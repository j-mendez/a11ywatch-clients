<?php
/**
 * WebsiteInput
 *
 * PHP version 7.3
 *
 * @category Class
 * @package  OpenAPI\Client
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * A11ywatch Client
 *
 * The web accessibility tool built for scale.
 *
 * The version of the OpenAPI document: 0.7.66
 * Contact: support@a11ywatch.com
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 5.3.0
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

namespace OpenAPI\Client\Model;

use \ArrayAccess;
use \OpenAPI\Client\ObjectSerializer;

/**
 * WebsiteInput Class Doc Comment
 *
 * @category Class
 * @package  OpenAPI\Client
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 * @implements \ArrayAccess<TKey, TValue>
 * @template TKey int|null
 * @template TValue mixed|null
 */
class WebsiteInput implements ModelInterface, ArrayAccess, \JsonSerializable
{
    public const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $openAPIModelName = 'WebsiteInput';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $openAPITypes = [
        'url' => 'string',
        'mobile' => 'bool',
        'page_insights' => 'bool',
        'ua' => 'string',
        'standard' => 'string',
        'robots' => 'string',
        'subdomains' => 'bool',
        'tld' => 'bool',
        'custom_headers' => 'string[]'
    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      * @phpstan-var array<string, string|null>
      * @psalm-var array<string, string|null>
      */
    protected static $openAPIFormats = [
        'url' => null,
        'mobile' => null,
        'page_insights' => null,
        'ua' => null,
        'standard' => null,
        'robots' => null,
        'subdomains' => null,
        'tld' => null,
        'custom_headers' => null
    ];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function openAPITypes()
    {
        return self::$openAPITypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function openAPIFormats()
    {
        return self::$openAPIFormats;
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'url' => 'url',
        'mobile' => 'mobile',
        'page_insights' => 'pageInsights',
        'ua' => 'ua',
        'standard' => 'standard',
        'robots' => 'robots',
        'subdomains' => 'subdomains',
        'tld' => 'tld',
        'custom_headers' => 'customHeaders'
    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'url' => 'setUrl',
        'mobile' => 'setMobile',
        'page_insights' => 'setPageInsights',
        'ua' => 'setUa',
        'standard' => 'setStandard',
        'robots' => 'setRobots',
        'subdomains' => 'setSubdomains',
        'tld' => 'setTld',
        'custom_headers' => 'setCustomHeaders'
    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'url' => 'getUrl',
        'mobile' => 'getMobile',
        'page_insights' => 'getPageInsights',
        'ua' => 'getUa',
        'standard' => 'getStandard',
        'robots' => 'getRobots',
        'subdomains' => 'getSubdomains',
        'tld' => 'getTld',
        'custom_headers' => 'getCustomHeaders'
    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$openAPIModelName;
    }


    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['url'] = $data['url'] ?? null;
        $this->container['mobile'] = $data['mobile'] ?? null;
        $this->container['page_insights'] = $data['page_insights'] ?? null;
        $this->container['ua'] = $data['ua'] ?? null;
        $this->container['standard'] = $data['standard'] ?? null;
        $this->container['robots'] = $data['robots'] ?? null;
        $this->container['subdomains'] = $data['subdomains'] ?? null;
        $this->container['tld'] = $data['tld'] ?? null;
        $this->container['custom_headers'] = $data['custom_headers'] ?? null;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {
        return count($this->listInvalidProperties()) === 0;
    }


    /**
     * Gets url
     *
     * @return string|null
     */
    public function getUrl()
    {
        return $this->container['url'];
    }

    /**
     * Sets url
     *
     * @param string|null $url url
     *
     * @return self
     */
    public function setUrl($url)
    {
        $this->container['url'] = $url;

        return $this;
    }

    /**
     * Gets mobile
     *
     * @return bool|null
     */
    public function getMobile()
    {
        return $this->container['mobile'];
    }

    /**
     * Sets mobile
     *
     * @param bool|null $mobile mobile
     *
     * @return self
     */
    public function setMobile($mobile)
    {
        $this->container['mobile'] = $mobile;

        return $this;
    }

    /**
     * Gets page_insights
     *
     * @return bool|null
     */
    public function getPageInsights()
    {
        return $this->container['page_insights'];
    }

    /**
     * Sets page_insights
     *
     * @param bool|null $page_insights page_insights
     *
     * @return self
     */
    public function setPageInsights($page_insights)
    {
        $this->container['page_insights'] = $page_insights;

        return $this;
    }

    /**
     * Gets ua
     *
     * @return string|null
     */
    public function getUa()
    {
        return $this->container['ua'];
    }

    /**
     * Sets ua
     *
     * @param string|null $ua ua
     *
     * @return self
     */
    public function setUa($ua)
    {
        $this->container['ua'] = $ua;

        return $this;
    }

    /**
     * Gets standard
     *
     * @return string|null
     */
    public function getStandard()
    {
        return $this->container['standard'];
    }

    /**
     * Sets standard
     *
     * @param string|null $standard standard
     *
     * @return self
     */
    public function setStandard($standard)
    {
        $this->container['standard'] = $standard;

        return $this;
    }

    /**
     * Gets robots
     *
     * @return string|null
     */
    public function getRobots()
    {
        return $this->container['robots'];
    }

    /**
     * Sets robots
     *
     * @param string|null $robots robots
     *
     * @return self
     */
    public function setRobots($robots)
    {
        $this->container['robots'] = $robots;

        return $this;
    }

    /**
     * Gets subdomains
     *
     * @return bool|null
     */
    public function getSubdomains()
    {
        return $this->container['subdomains'];
    }

    /**
     * Sets subdomains
     *
     * @param bool|null $subdomains subdomains
     *
     * @return self
     */
    public function setSubdomains($subdomains)
    {
        $this->container['subdomains'] = $subdomains;

        return $this;
    }

    /**
     * Gets tld
     *
     * @return bool|null
     */
    public function getTld()
    {
        return $this->container['tld'];
    }

    /**
     * Sets tld
     *
     * @param bool|null $tld tld
     *
     * @return self
     */
    public function setTld($tld)
    {
        $this->container['tld'] = $tld;

        return $this;
    }

    /**
     * Gets custom_headers
     *
     * @return string[]|null
     */
    public function getCustomHeaders()
    {
        return $this->container['custom_headers'];
    }

    /**
     * Sets custom_headers
     *
     * @param string[]|null $custom_headers custom_headers
     *
     * @return self
     */
    public function setCustomHeaders($custom_headers)
    {
        $this->container['custom_headers'] = $custom_headers;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed|null
     */
    public function offsetGet($offset)
    {
        return $this->container[$offset] ?? null;
    }

    /**
     * Sets value based on offset.
     *
     * @param int|null $offset Offset
     * @param mixed    $value  Value to be set
     *
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Serializes the object to a value that can be serialized natively by json_encode().
     * @link https://www.php.net/manual/en/jsonserializable.jsonserialize.php
     *
     * @return mixed Returns data which can be serialized by json_encode(), which is a value
     * of any type other than a resource.
     */
    public function jsonSerialize()
    {
       return ObjectSerializer::sanitizeForSerialization($this);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        return json_encode(
            ObjectSerializer::sanitizeForSerialization($this),
            JSON_PRETTY_PRINT
        );
    }

    /**
     * Gets a header-safe presentation of the object
     *
     * @return string
     */
    public function toHeaderValue()
    {
        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}


