# OpenapiClient::GetWebsites200Response

## Properties

| Name | Type | Description | Notes |
| ---- | ---- | ----------- | ----- |
| **data** | [**Array&lt;Websites&gt;**](Websites.md) |  | [optional] |

## Example

```ruby
require 'openapi_client'

instance = OpenapiClient::GetWebsites200Response.new(
  data: null
)
```

