# OpenapiClient::GetIssues200Response

## Properties

| Name | Type | Description | Notes |
| ---- | ---- | ----------- | ----- |
| **data** | [**Array&lt;Issues&gt;**](Issues.md) |  | [optional] |

## Example

```ruby
require 'openapi_client'

instance = OpenapiClient::GetIssues200Response.new(
  data: null
)
```

