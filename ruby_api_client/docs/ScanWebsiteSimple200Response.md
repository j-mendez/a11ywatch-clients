# OpenapiClient::ScanWebsiteSimple200Response

## Properties

| Name | Type | Description | Notes |
| ---- | ---- | ----------- | ----- |
| **data** | [**Report**](Report.md) |  | [optional] |

## Example

```ruby
require 'openapi_client'

instance = OpenapiClient::ScanWebsiteSimple200Response.new(
  data: null
)
```

