# OpenapiClient::PageIssue

## Properties

| Name | Type | Description | Notes |
| ---- | ---- | ----------- | ----- |
| **recurrence** | **Integer** |  | [optional] |
| **type_code** | **Integer** |  | [optional] |
| **code** | **String** |  | [optional] |
| **message** | **String** |  | [optional] |
| **context** | **String** |  | [optional] |
| **selector** | **String** |  | [optional] |
| **runner** | **String** |  | [optional] |
| **type** | **String** |  | [optional] |

## Example

```ruby
require 'openapi_client'

instance = OpenapiClient::PageIssue.new(
  recurrence: null,
  type_code: null,
  code: null,
  message: null,
  context: null,
  selector: null,
  runner: null,
  type: null
)
```

