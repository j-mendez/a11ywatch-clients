# OpenapiClient::IssuesInfo

## Properties

| Name | Type | Description | Notes |
| ---- | ---- | ----------- | ----- |
| **access_score_average** | **Integer** |  | [optional] |
| **possible_issues_fixed_by_cdn** | **Integer** |  | [optional] |
| **total_issues** | **Integer** |  | [optional] |
| **issues_fixed_by_cdn** | **Integer** |  | [optional] |
| **error_count** | **Integer** |  | [optional] |
| **warning_count** | **Integer** |  | [optional] |
| **notice_count** | **Integer** |  | [optional] |
| **page_count** | **Integer** |  | [optional] |

## Example

```ruby
require 'openapi_client'

instance = OpenapiClient::IssuesInfo.new(
  access_score_average: null,
  possible_issues_fixed_by_cdn: null,
  total_issues: null,
  issues_fixed_by_cdn: null,
  error_count: null,
  warning_count: null,
  notice_count: null,
  page_count: null
)
```

