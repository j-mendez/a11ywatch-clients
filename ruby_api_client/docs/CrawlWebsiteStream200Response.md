# OpenapiClient::CrawlWebsiteStream200Response

## Properties

| Name | Type | Description | Notes |
| ---- | ---- | ----------- | ----- |
| **data** | [**Array&lt;Report&gt;**](Report.md) |  | [optional] |

## Example

```ruby
require 'openapi_client'

instance = OpenapiClient::CrawlWebsiteStream200Response.new(
  data: null
)
```

