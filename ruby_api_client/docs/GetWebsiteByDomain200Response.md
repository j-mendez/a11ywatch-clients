# OpenapiClient::GetWebsiteByDomain200Response

## Properties

| Name | Type | Description | Notes |
| ---- | ---- | ----------- | ----- |
| **data** | [**Websites**](Websites.md) |  | [optional] |

## Example

```ruby
require 'openapi_client'

instance = OpenapiClient::GetWebsiteByDomain200Response.new(
  data: null
)
```

