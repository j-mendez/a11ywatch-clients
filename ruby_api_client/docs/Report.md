# OpenapiClient::Report

## Properties

| Name | Type | Description | Notes |
| ---- | ---- | ----------- | ----- |
| **_id** | **Integer** |  | [optional] |
| **domain** | **String** |  | [optional] |
| **page_url** | **String** |  | [optional] |
| **issues** | [**Array&lt;PageIssue&gt;**](PageIssue.md) |  | [optional] |
| **issues_info** | [**IssuesInfo**](IssuesInfo.md) |  | [optional] |

## Example

```ruby
require 'openapi_client'

instance = OpenapiClient::Report.new(
  _id: null,
  domain: null,
  page_url: null,
  issues: null,
  issues_info: null
)
```

