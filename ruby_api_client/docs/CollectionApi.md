# OpenapiClient::CollectionApi

All URIs are relative to *https://api.a11ywatch.com/api*

| Method | HTTP request | Description |
| ------ | ------------ | ----------- |
| [**get_analytics**](CollectionApi.md#get_analytics) | **GET** /list/analytics | Get the analytics for a website |
| [**get_issues**](CollectionApi.md#get_issues) | **GET** /list/issue | List the issues for a website |
| [**get_page_speed**](CollectionApi.md#get_page_speed) | **GET** /list/pagespeed | Get the pagespeed for a website |
| [**get_pages**](CollectionApi.md#get_pages) | **GET** /list/pages | List the pages in order for a website |
| [**get_websites**](CollectionApi.md#get_websites) | **GET** /list/website | Returns websites for the user in alphabetical order |


## get_analytics

> Object get_analytics(opts)

Get the analytics for a website

### Examples

```ruby
require 'time'
require 'openapi_client'
# setup authorization
OpenapiClient.configure do |config|
  # Configure Bearer authorization (JWT): bearerAuth
  config.access_token = 'YOUR_BEARER_TOKEN'
end

api_instance = OpenapiClient::CollectionApi.new
opts = {
  offset: 'offset_example', # String | The page offset for the next set
  domain: 'domain_example' # String | Domain of website that needs to be fetched
}

begin
  # Get the analytics for a website
  result = api_instance.get_analytics(opts)
  p result
rescue OpenapiClient::ApiError => e
  puts "Error when calling CollectionApi->get_analytics: #{e}"
end
```

#### Using the get_analytics_with_http_info variant

This returns an Array which contains the response data, status code and headers.

> <Array(Object, Integer, Hash)> get_analytics_with_http_info(opts)

```ruby
begin
  # Get the analytics for a website
  data, status_code, headers = api_instance.get_analytics_with_http_info(opts)
  p status_code # => 2xx
  p headers # => { ... }
  p data # => Object
rescue OpenapiClient::ApiError => e
  puts "Error when calling CollectionApi->get_analytics_with_http_info: #{e}"
end
```

### Parameters

| Name | Type | Description | Notes |
| ---- | ---- | ----------- | ----- |
| **offset** | **String** | The page offset for the next set | [optional] |
| **domain** | **String** | Domain of website that needs to be fetched | [optional] |

### Return type

**Object**

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## get_issues

> Object get_issues(opts)

List the issues for a website

### Examples

```ruby
require 'time'
require 'openapi_client'
# setup authorization
OpenapiClient.configure do |config|
  # Configure Bearer authorization (JWT): bearerAuth
  config.access_token = 'YOUR_BEARER_TOKEN'
end

api_instance = OpenapiClient::CollectionApi.new
opts = {
  offset: 'offset_example', # String | The page offset for the next set
  domain: 'domain_example' # String | Domain of website that needs to be fetched
}

begin
  # List the issues for a website
  result = api_instance.get_issues(opts)
  p result
rescue OpenapiClient::ApiError => e
  puts "Error when calling CollectionApi->get_issues: #{e}"
end
```

#### Using the get_issues_with_http_info variant

This returns an Array which contains the response data, status code and headers.

> <Array(Object, Integer, Hash)> get_issues_with_http_info(opts)

```ruby
begin
  # List the issues for a website
  data, status_code, headers = api_instance.get_issues_with_http_info(opts)
  p status_code # => 2xx
  p headers # => { ... }
  p data # => Object
rescue OpenapiClient::ApiError => e
  puts "Error when calling CollectionApi->get_issues_with_http_info: #{e}"
end
```

### Parameters

| Name | Type | Description | Notes |
| ---- | ---- | ----------- | ----- |
| **offset** | **String** | The page offset for the next set | [optional] |
| **domain** | **String** | Domain of website that needs to be fetched | [optional] |

### Return type

**Object**

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## get_page_speed

> Object get_page_speed(opts)

Get the pagespeed for a website

### Examples

```ruby
require 'time'
require 'openapi_client'
# setup authorization
OpenapiClient.configure do |config|
  # Configure Bearer authorization (JWT): bearerAuth
  config.access_token = 'YOUR_BEARER_TOKEN'
end

api_instance = OpenapiClient::CollectionApi.new
opts = {
  offset: 'offset_example', # String | The page offset for the next set
  domain: 'domain_example' # String | Domain of website that needs to be fetched
}

begin
  # Get the pagespeed for a website
  result = api_instance.get_page_speed(opts)
  p result
rescue OpenapiClient::ApiError => e
  puts "Error when calling CollectionApi->get_page_speed: #{e}"
end
```

#### Using the get_page_speed_with_http_info variant

This returns an Array which contains the response data, status code and headers.

> <Array(Object, Integer, Hash)> get_page_speed_with_http_info(opts)

```ruby
begin
  # Get the pagespeed for a website
  data, status_code, headers = api_instance.get_page_speed_with_http_info(opts)
  p status_code # => 2xx
  p headers # => { ... }
  p data # => Object
rescue OpenapiClient::ApiError => e
  puts "Error when calling CollectionApi->get_page_speed_with_http_info: #{e}"
end
```

### Parameters

| Name | Type | Description | Notes |
| ---- | ---- | ----------- | ----- |
| **offset** | **String** | The page offset for the next set | [optional] |
| **domain** | **String** | Domain of website that needs to be fetched | [optional] |

### Return type

**Object**

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## get_pages

> Object get_pages(opts)

List the pages in order for a website

### Examples

```ruby
require 'time'
require 'openapi_client'
# setup authorization
OpenapiClient.configure do |config|
  # Configure Bearer authorization (JWT): bearerAuth
  config.access_token = 'YOUR_BEARER_TOKEN'
end

api_instance = OpenapiClient::CollectionApi.new
opts = {
  offset: 'offset_example', # String | The page offset for the next set
  domain: 'domain_example' # String | Domain of website that needs to be fetched
}

begin
  # List the pages in order for a website
  result = api_instance.get_pages(opts)
  p result
rescue OpenapiClient::ApiError => e
  puts "Error when calling CollectionApi->get_pages: #{e}"
end
```

#### Using the get_pages_with_http_info variant

This returns an Array which contains the response data, status code and headers.

> <Array(Object, Integer, Hash)> get_pages_with_http_info(opts)

```ruby
begin
  # List the pages in order for a website
  data, status_code, headers = api_instance.get_pages_with_http_info(opts)
  p status_code # => 2xx
  p headers # => { ... }
  p data # => Object
rescue OpenapiClient::ApiError => e
  puts "Error when calling CollectionApi->get_pages_with_http_info: #{e}"
end
```

### Parameters

| Name | Type | Description | Notes |
| ---- | ---- | ----------- | ----- |
| **offset** | **String** | The page offset for the next set | [optional] |
| **domain** | **String** | Domain of website that needs to be fetched | [optional] |

### Return type

**Object**

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## get_websites

> Object get_websites(opts)

Returns websites for the user in alphabetical order

Returns a map of websites

### Examples

```ruby
require 'time'
require 'openapi_client'
# setup authorization
OpenapiClient.configure do |config|
  # Configure Bearer authorization (JWT): bearerAuth
  config.access_token = 'YOUR_BEARER_TOKEN'
end

api_instance = OpenapiClient::CollectionApi.new
opts = {
  offset: 'offset_example' # String | The page offset for the next set
}

begin
  # Returns websites for the user in alphabetical order
  result = api_instance.get_websites(opts)
  p result
rescue OpenapiClient::ApiError => e
  puts "Error when calling CollectionApi->get_websites: #{e}"
end
```

#### Using the get_websites_with_http_info variant

This returns an Array which contains the response data, status code and headers.

> <Array(Object, Integer, Hash)> get_websites_with_http_info(opts)

```ruby
begin
  # Returns websites for the user in alphabetical order
  data, status_code, headers = api_instance.get_websites_with_http_info(opts)
  p status_code # => 2xx
  p headers # => { ... }
  p data # => Object
rescue OpenapiClient::ApiError => e
  puts "Error when calling CollectionApi->get_websites_with_http_info: #{e}"
end
```

### Parameters

| Name | Type | Description | Notes |
| ---- | ---- | ----------- | ----- |
| **offset** | **String** | The page offset for the next set | [optional] |

### Return type

**Object**

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

