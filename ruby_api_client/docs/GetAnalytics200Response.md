# OpenapiClient::GetAnalytics200Response

## Properties

| Name | Type | Description | Notes |
| ---- | ---- | ----------- | ----- |
| **data** | [**Array&lt;Analytics&gt;**](Analytics.md) |  | [optional] |

## Example

```ruby
require 'openapi_client'

instance = OpenapiClient::GetAnalytics200Response.new(
  data: null
)
```

