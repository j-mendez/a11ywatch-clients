# OpenapiClient::PageLoadTime

## Properties

| Name | Type | Description | Notes |
| ---- | ---- | ----------- | ----- |
| **duration** | **Float** |  | [optional] |
| **duration_formated** | **String** |  | [optional] |
| **color** | **String** |  | [optional] |

## Example

```ruby
require 'openapi_client'

instance = OpenapiClient::PageLoadTime.new(
  duration: null,
  duration_formated: null,
  color: null
)
```

