# OpenapiClient::AuthInput

## Properties

| Name | Type | Description | Notes |
| ---- | ---- | ----------- | ----- |
| **email** | **String** |  |  |
| **password** | **String** |  |  |

## Example

```ruby
require 'openapi_client'

instance = OpenapiClient::AuthInput.new(
  email: null,
  password: null
)
```

