# OpenapiClient::CrawlWebsitesSync200Response

## Properties

| Name | Type | Description | Notes |
| ---- | ---- | ----------- | ----- |
| **data** | **Boolean** |  | [optional] |
| **message** | **String** |  | [optional] |

## Example

```ruby
require 'openapi_client'

instance = OpenapiClient::CrawlWebsitesSync200Response.new(
  data: null,
  message: null
)
```

