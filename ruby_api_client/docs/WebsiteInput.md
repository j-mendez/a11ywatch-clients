# OpenapiClient::WebsiteInput

## Properties

| Name | Type | Description | Notes |
| ---- | ---- | ----------- | ----- |
| **url** | **String** |  | [optional] |
| **mobile** | **Boolean** |  | [optional] |
| **page_insights** | **Boolean** |  | [optional] |
| **ua** | **String** |  | [optional] |
| **standard** | **String** |  | [optional] |
| **robots** | **String** |  | [optional] |
| **subdomains** | **Boolean** |  | [optional] |
| **tld** | **Boolean** |  | [optional] |
| **custom_headers** | **Array&lt;String&gt;** |  | [optional] |

## Example

```ruby
require 'openapi_client'

instance = OpenapiClient::WebsiteInput.new(
  url: null,
  mobile: null,
  page_insights: null,
  ua: null,
  standard: null,
  robots: null,
  subdomains: null,
  tld: null,
  custom_headers: null
)
```

