# OpenapiClient::ReportsApi

All URIs are relative to *https://api.a11ywatch.com/api*

| Method | HTTP request | Description |
| ------ | ------------ | ----------- |
| [**crawl_website_stream**](ReportsApi.md#crawl_website_stream) | **POST** /crawl | Multi-page crawl a website streaming issues on found |
| [**crawl_websites_sync**](ReportsApi.md#crawl_websites_sync) | **POST** /websites-sync | Multi-page crawl all websites attached to account |
| [**get_report**](ReportsApi.md#get_report) | **GET** /report | Get the report from a previus scan |
| [**scan_website**](ReportsApi.md#scan_website) | **POST** /scan | Scan a website for issues |
| [**scan_website_simple**](ReportsApi.md#scan_website_simple) | **POST** /scan-simple | Scan a website for issues without storing data and limited responses. |


## crawl_website_stream

> Object crawl_website_stream(transfer_encoding, website_input)

Multi-page crawl a website streaming issues on found

### Examples

```ruby
require 'time'
require 'openapi_client'
# setup authorization
OpenapiClient.configure do |config|
  # Configure Bearer authorization (JWT): bearerAuth
  config.access_token = 'YOUR_BEARER_TOKEN'
end

api_instance = OpenapiClient::ReportsApi.new
transfer_encoding = 'transfer_encoding_example' # String | 
website_input = OpenapiClient::WebsiteInput.new # WebsiteInput | The website standard body

begin
  # Multi-page crawl a website streaming issues on found
  result = api_instance.crawl_website_stream(transfer_encoding, website_input)
  p result
rescue OpenapiClient::ApiError => e
  puts "Error when calling ReportsApi->crawl_website_stream: #{e}"
end
```

#### Using the crawl_website_stream_with_http_info variant

This returns an Array which contains the response data, status code and headers.

> <Array(Object, Integer, Hash)> crawl_website_stream_with_http_info(transfer_encoding, website_input)

```ruby
begin
  # Multi-page crawl a website streaming issues on found
  data, status_code, headers = api_instance.crawl_website_stream_with_http_info(transfer_encoding, website_input)
  p status_code # => 2xx
  p headers # => { ... }
  p data # => Object
rescue OpenapiClient::ApiError => e
  puts "Error when calling ReportsApi->crawl_website_stream_with_http_info: #{e}"
end
```

### Parameters

| Name | Type | Description | Notes |
| ---- | ---- | ----------- | ----- |
| **transfer_encoding** | **String** |  | [default to &#39;Chunked&#39;] |
| **website_input** | [**WebsiteInput**](WebsiteInput.md) | The website standard body |  |

### Return type

**Object**

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## crawl_websites_sync

> Object crawl_websites_sync

Multi-page crawl all websites attached to account

### Examples

```ruby
require 'time'
require 'openapi_client'
# setup authorization
OpenapiClient.configure do |config|
  # Configure Bearer authorization (JWT): bearerAuth
  config.access_token = 'YOUR_BEARER_TOKEN'
end

api_instance = OpenapiClient::ReportsApi.new

begin
  # Multi-page crawl all websites attached to account
  result = api_instance.crawl_websites_sync
  p result
rescue OpenapiClient::ApiError => e
  puts "Error when calling ReportsApi->crawl_websites_sync: #{e}"
end
```

#### Using the crawl_websites_sync_with_http_info variant

This returns an Array which contains the response data, status code and headers.

> <Array(Object, Integer, Hash)> crawl_websites_sync_with_http_info

```ruby
begin
  # Multi-page crawl all websites attached to account
  data, status_code, headers = api_instance.crawl_websites_sync_with_http_info
  p status_code # => 2xx
  p headers # => { ... }
  p data # => Object
rescue OpenapiClient::ApiError => e
  puts "Error when calling ReportsApi->crawl_websites_sync_with_http_info: #{e}"
end
```

### Parameters

This endpoint does not need any parameter.

### Return type

**Object**

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## get_report

> Object get_report(opts)

Get the report from a previus scan

### Examples

```ruby
require 'time'
require 'openapi_client'
# setup authorization
OpenapiClient.configure do |config|
  # Configure Bearer authorization (JWT): bearerAuth
  config.access_token = 'YOUR_BEARER_TOKEN'
end

api_instance = OpenapiClient::ReportsApi.new
opts = {
  url: 'url_example', # String | The page url or domain for the report
  domain: 'domain_example' # String | Domain of website that needs to be fetched
}

begin
  # Get the report from a previus scan
  result = api_instance.get_report(opts)
  p result
rescue OpenapiClient::ApiError => e
  puts "Error when calling ReportsApi->get_report: #{e}"
end
```

#### Using the get_report_with_http_info variant

This returns an Array which contains the response data, status code and headers.

> <Array(Object, Integer, Hash)> get_report_with_http_info(opts)

```ruby
begin
  # Get the report from a previus scan
  data, status_code, headers = api_instance.get_report_with_http_info(opts)
  p status_code # => 2xx
  p headers # => { ... }
  p data # => Object
rescue OpenapiClient::ApiError => e
  puts "Error when calling ReportsApi->get_report_with_http_info: #{e}"
end
```

### Parameters

| Name | Type | Description | Notes |
| ---- | ---- | ----------- | ----- |
| **url** | **String** | The page url or domain for the report | [optional] |
| **domain** | **String** | Domain of website that needs to be fetched | [optional] |

### Return type

**Object**

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## scan_website

> Object scan_website(website_input)

Scan a website for issues

### Examples

```ruby
require 'time'
require 'openapi_client'
# setup authorization
OpenapiClient.configure do |config|
  # Configure Bearer authorization (JWT): bearerAuth
  config.access_token = 'YOUR_BEARER_TOKEN'
end

api_instance = OpenapiClient::ReportsApi.new
website_input = OpenapiClient::WebsiteInput.new # WebsiteInput | The website standard body

begin
  # Scan a website for issues
  result = api_instance.scan_website(website_input)
  p result
rescue OpenapiClient::ApiError => e
  puts "Error when calling ReportsApi->scan_website: #{e}"
end
```

#### Using the scan_website_with_http_info variant

This returns an Array which contains the response data, status code and headers.

> <Array(Object, Integer, Hash)> scan_website_with_http_info(website_input)

```ruby
begin
  # Scan a website for issues
  data, status_code, headers = api_instance.scan_website_with_http_info(website_input)
  p status_code # => 2xx
  p headers # => { ... }
  p data # => Object
rescue OpenapiClient::ApiError => e
  puts "Error when calling ReportsApi->scan_website_with_http_info: #{e}"
end
```

### Parameters

| Name | Type | Description | Notes |
| ---- | ---- | ----------- | ----- |
| **website_input** | [**WebsiteInput**](WebsiteInput.md) | The website standard body |  |

### Return type

**Object**

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## scan_website_simple

> Object scan_website_simple(website_input)

Scan a website for issues without storing data and limited responses.

### Examples

```ruby
require 'time'
require 'openapi_client'
# setup authorization
OpenapiClient.configure do |config|
  # Configure Bearer authorization (JWT): bearerAuth
  config.access_token = 'YOUR_BEARER_TOKEN'
end

api_instance = OpenapiClient::ReportsApi.new
website_input = OpenapiClient::WebsiteInput.new # WebsiteInput | The website standard body

begin
  # Scan a website for issues without storing data and limited responses.
  result = api_instance.scan_website_simple(website_input)
  p result
rescue OpenapiClient::ApiError => e
  puts "Error when calling ReportsApi->scan_website_simple: #{e}"
end
```

#### Using the scan_website_simple_with_http_info variant

This returns an Array which contains the response data, status code and headers.

> <Array(Object, Integer, Hash)> scan_website_simple_with_http_info(website_input)

```ruby
begin
  # Scan a website for issues without storing data and limited responses.
  data, status_code, headers = api_instance.scan_website_simple_with_http_info(website_input)
  p status_code # => 2xx
  p headers # => { ... }
  p data # => Object
rescue OpenapiClient::ApiError => e
  puts "Error when calling ReportsApi->scan_website_simple_with_http_info: #{e}"
end
```

### Parameters

| Name | Type | Description | Notes |
| ---- | ---- | ----------- | ----- |
| **website_input** | [**WebsiteInput**](WebsiteInput.md) | The website standard body |  |

### Return type

**Object**

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

