# OpenapiClient::Users

## Properties

| Name | Type | Description | Notes |
| ---- | ---- | ----------- | ----- |
| **_id** | **Integer** |  | [optional] |
| **email** | **String** |  | [optional] |
| **jwt** | **String** |  | [optional] |
| **role** | **String** |  | [optional] |
| **password** | **String** |  | [optional] |
| **alert_enabled** | **Boolean** |  | [optional] |

## Example

```ruby
require 'openapi_client'

instance = OpenapiClient::Users.new(
  _id: null,
  email: null,
  jwt: null,
  role: null,
  password: null,
  alert_enabled: null
)
```

