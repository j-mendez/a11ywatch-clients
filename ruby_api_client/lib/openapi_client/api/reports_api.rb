=begin
#A11ywatch Client

#The web accessibility tool built for scale.

The version of the OpenAPI document: 0.7.66
Contact: support@a11ywatch.com
Generated by: https://openapi-generator.tech
OpenAPI Generator version: 5.3.0

=end

require 'cgi'

module OpenapiClient
  class ReportsApi
    attr_accessor :api_client

    def initialize(api_client = ApiClient.default)
      @api_client = api_client
    end
    # Multi-page crawl a website streaming issues on found
    # @param transfer_encoding [String] 
    # @param website_input [WebsiteInput] The website standard body
    # @param [Hash] opts the optional parameters
    # @return [Object]
    def crawl_website_stream(transfer_encoding, website_input, opts = {})
      data, _status_code, _headers = crawl_website_stream_with_http_info(transfer_encoding, website_input, opts)
      data
    end

    # Multi-page crawl a website streaming issues on found
    # @param transfer_encoding [String] 
    # @param website_input [WebsiteInput] The website standard body
    # @param [Hash] opts the optional parameters
    # @return [Array<(Object, Integer, Hash)>] Object data, response status code and response headers
    def crawl_website_stream_with_http_info(transfer_encoding, website_input, opts = {})
      if @api_client.config.debugging
        @api_client.config.logger.debug 'Calling API: ReportsApi.crawl_website_stream ...'
      end
      # verify the required parameter 'transfer_encoding' is set
      if @api_client.config.client_side_validation && transfer_encoding.nil?
        fail ArgumentError, "Missing the required parameter 'transfer_encoding' when calling ReportsApi.crawl_website_stream"
      end
      # verify the required parameter 'website_input' is set
      if @api_client.config.client_side_validation && website_input.nil?
        fail ArgumentError, "Missing the required parameter 'website_input' when calling ReportsApi.crawl_website_stream"
      end
      # resource path
      local_var_path = '/crawl'

      # query parameters
      query_params = opts[:query_params] || {}

      # header parameters
      header_params = opts[:header_params] || {}
      # HTTP header 'Accept' (if needed)
      header_params['Accept'] = @api_client.select_header_accept(['application/json'])
      # HTTP header 'Content-Type'
      content_type = @api_client.select_header_content_type(['application/json'])
      if !content_type.nil?
          header_params['Content-Type'] = content_type
      end
      header_params[:'Transfer-Encoding'] = transfer_encoding

      # form parameters
      form_params = opts[:form_params] || {}

      # http body (model)
      post_body = opts[:debug_body] || @api_client.object_to_http_body(website_input)

      # return_type
      return_type = opts[:debug_return_type] || 'Object'

      # auth_names
      auth_names = opts[:debug_auth_names] || ['bearerAuth']

      new_options = opts.merge(
        :operation => :"ReportsApi.crawl_website_stream",
        :header_params => header_params,
        :query_params => query_params,
        :form_params => form_params,
        :body => post_body,
        :auth_names => auth_names,
        :return_type => return_type
      )

      data, status_code, headers = @api_client.call_api(:POST, local_var_path, new_options)
      if @api_client.config.debugging
        @api_client.config.logger.debug "API called: ReportsApi#crawl_website_stream\nData: #{data.inspect}\nStatus code: #{status_code}\nHeaders: #{headers}"
      end
      return data, status_code, headers
    end

    # Multi-page crawl all websites attached to account
    # @param [Hash] opts the optional parameters
    # @return [Object]
    def crawl_websites_sync(opts = {})
      data, _status_code, _headers = crawl_websites_sync_with_http_info(opts)
      data
    end

    # Multi-page crawl all websites attached to account
    # @param [Hash] opts the optional parameters
    # @return [Array<(Object, Integer, Hash)>] Object data, response status code and response headers
    def crawl_websites_sync_with_http_info(opts = {})
      if @api_client.config.debugging
        @api_client.config.logger.debug 'Calling API: ReportsApi.crawl_websites_sync ...'
      end
      # resource path
      local_var_path = '/websites-sync'

      # query parameters
      query_params = opts[:query_params] || {}

      # header parameters
      header_params = opts[:header_params] || {}
      # HTTP header 'Accept' (if needed)
      header_params['Accept'] = @api_client.select_header_accept(['application/json'])

      # form parameters
      form_params = opts[:form_params] || {}

      # http body (model)
      post_body = opts[:debug_body]

      # return_type
      return_type = opts[:debug_return_type] || 'Object'

      # auth_names
      auth_names = opts[:debug_auth_names] || ['bearerAuth']

      new_options = opts.merge(
        :operation => :"ReportsApi.crawl_websites_sync",
        :header_params => header_params,
        :query_params => query_params,
        :form_params => form_params,
        :body => post_body,
        :auth_names => auth_names,
        :return_type => return_type
      )

      data, status_code, headers = @api_client.call_api(:POST, local_var_path, new_options)
      if @api_client.config.debugging
        @api_client.config.logger.debug "API called: ReportsApi#crawl_websites_sync\nData: #{data.inspect}\nStatus code: #{status_code}\nHeaders: #{headers}"
      end
      return data, status_code, headers
    end

    # Get the report from a previus scan
    # @param [Hash] opts the optional parameters
    # @option opts [String] :url The page url or domain for the report
    # @option opts [String] :domain Domain of website that needs to be fetched
    # @return [Object]
    def get_report(opts = {})
      data, _status_code, _headers = get_report_with_http_info(opts)
      data
    end

    # Get the report from a previus scan
    # @param [Hash] opts the optional parameters
    # @option opts [String] :url The page url or domain for the report
    # @option opts [String] :domain Domain of website that needs to be fetched
    # @return [Array<(Object, Integer, Hash)>] Object data, response status code and response headers
    def get_report_with_http_info(opts = {})
      if @api_client.config.debugging
        @api_client.config.logger.debug 'Calling API: ReportsApi.get_report ...'
      end
      # resource path
      local_var_path = '/report'

      # query parameters
      query_params = opts[:query_params] || {}
      query_params[:'url'] = opts[:'url'] if !opts[:'url'].nil?
      query_params[:'domain'] = opts[:'domain'] if !opts[:'domain'].nil?

      # header parameters
      header_params = opts[:header_params] || {}
      # HTTP header 'Accept' (if needed)
      header_params['Accept'] = @api_client.select_header_accept(['application/json'])

      # form parameters
      form_params = opts[:form_params] || {}

      # http body (model)
      post_body = opts[:debug_body]

      # return_type
      return_type = opts[:debug_return_type] || 'Object'

      # auth_names
      auth_names = opts[:debug_auth_names] || ['bearerAuth']

      new_options = opts.merge(
        :operation => :"ReportsApi.get_report",
        :header_params => header_params,
        :query_params => query_params,
        :form_params => form_params,
        :body => post_body,
        :auth_names => auth_names,
        :return_type => return_type
      )

      data, status_code, headers = @api_client.call_api(:GET, local_var_path, new_options)
      if @api_client.config.debugging
        @api_client.config.logger.debug "API called: ReportsApi#get_report\nData: #{data.inspect}\nStatus code: #{status_code}\nHeaders: #{headers}"
      end
      return data, status_code, headers
    end

    # Scan a website for issues
    # @param website_input [WebsiteInput] The website standard body
    # @param [Hash] opts the optional parameters
    # @return [Object]
    def scan_website(website_input, opts = {})
      data, _status_code, _headers = scan_website_with_http_info(website_input, opts)
      data
    end

    # Scan a website for issues
    # @param website_input [WebsiteInput] The website standard body
    # @param [Hash] opts the optional parameters
    # @return [Array<(Object, Integer, Hash)>] Object data, response status code and response headers
    def scan_website_with_http_info(website_input, opts = {})
      if @api_client.config.debugging
        @api_client.config.logger.debug 'Calling API: ReportsApi.scan_website ...'
      end
      # verify the required parameter 'website_input' is set
      if @api_client.config.client_side_validation && website_input.nil?
        fail ArgumentError, "Missing the required parameter 'website_input' when calling ReportsApi.scan_website"
      end
      # resource path
      local_var_path = '/scan'

      # query parameters
      query_params = opts[:query_params] || {}

      # header parameters
      header_params = opts[:header_params] || {}
      # HTTP header 'Accept' (if needed)
      header_params['Accept'] = @api_client.select_header_accept(['application/json'])
      # HTTP header 'Content-Type'
      content_type = @api_client.select_header_content_type(['application/json'])
      if !content_type.nil?
          header_params['Content-Type'] = content_type
      end

      # form parameters
      form_params = opts[:form_params] || {}

      # http body (model)
      post_body = opts[:debug_body] || @api_client.object_to_http_body(website_input)

      # return_type
      return_type = opts[:debug_return_type] || 'Object'

      # auth_names
      auth_names = opts[:debug_auth_names] || ['bearerAuth']

      new_options = opts.merge(
        :operation => :"ReportsApi.scan_website",
        :header_params => header_params,
        :query_params => query_params,
        :form_params => form_params,
        :body => post_body,
        :auth_names => auth_names,
        :return_type => return_type
      )

      data, status_code, headers = @api_client.call_api(:POST, local_var_path, new_options)
      if @api_client.config.debugging
        @api_client.config.logger.debug "API called: ReportsApi#scan_website\nData: #{data.inspect}\nStatus code: #{status_code}\nHeaders: #{headers}"
      end
      return data, status_code, headers
    end

    # Scan a website for issues without storing data and limited responses.
    # @param website_input [WebsiteInput] The website standard body
    # @param [Hash] opts the optional parameters
    # @return [Object]
    def scan_website_simple(website_input, opts = {})
      data, _status_code, _headers = scan_website_simple_with_http_info(website_input, opts)
      data
    end

    # Scan a website for issues without storing data and limited responses.
    # @param website_input [WebsiteInput] The website standard body
    # @param [Hash] opts the optional parameters
    # @return [Array<(Object, Integer, Hash)>] Object data, response status code and response headers
    def scan_website_simple_with_http_info(website_input, opts = {})
      if @api_client.config.debugging
        @api_client.config.logger.debug 'Calling API: ReportsApi.scan_website_simple ...'
      end
      # verify the required parameter 'website_input' is set
      if @api_client.config.client_side_validation && website_input.nil?
        fail ArgumentError, "Missing the required parameter 'website_input' when calling ReportsApi.scan_website_simple"
      end
      # resource path
      local_var_path = '/scan-simple'

      # query parameters
      query_params = opts[:query_params] || {}

      # header parameters
      header_params = opts[:header_params] || {}
      # HTTP header 'Accept' (if needed)
      header_params['Accept'] = @api_client.select_header_accept(['application/json'])
      # HTTP header 'Content-Type'
      content_type = @api_client.select_header_content_type(['application/json'])
      if !content_type.nil?
          header_params['Content-Type'] = content_type
      end

      # form parameters
      form_params = opts[:form_params] || {}

      # http body (model)
      post_body = opts[:debug_body] || @api_client.object_to_http_body(website_input)

      # return_type
      return_type = opts[:debug_return_type] || 'Object'

      # auth_names
      auth_names = opts[:debug_auth_names] || ['bearerAuth']

      new_options = opts.merge(
        :operation => :"ReportsApi.scan_website_simple",
        :header_params => header_params,
        :query_params => query_params,
        :form_params => form_params,
        :body => post_body,
        :auth_names => auth_names,
        :return_type => return_type
      )

      data, status_code, headers = @api_client.call_api(:POST, local_var_path, new_options)
      if @api_client.config.debugging
        @api_client.config.logger.debug "API called: ReportsApi#scan_website_simple\nData: #{data.inspect}\nStatus code: #{status_code}\nHeaders: #{headers}"
      end
      return data, status_code, headers
    end
  end
end
