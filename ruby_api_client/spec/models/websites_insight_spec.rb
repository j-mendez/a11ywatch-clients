=begin
#A11ywatch Client

#The web accessibility tool built for scale.

The version of the OpenAPI document: 0.7.66
Contact: support@a11ywatch.com
Generated by: https://openapi-generator.tech
OpenAPI Generator version: 6.0.1

=end

require 'spec_helper'
require 'json'
require 'date'

# Unit tests for OpenapiClient::WebsitesInsight
# Automatically generated by openapi-generator (https://openapi-generator.tech)
# Please update as you see appropriate
describe OpenapiClient::WebsitesInsight do
  let(:instance) { OpenapiClient::WebsitesInsight.new }

  describe 'test an instance of WebsitesInsight' do
    it 'should create an instance of WebsitesInsight' do
      expect(instance).to be_instance_of(OpenapiClient::WebsitesInsight)
    end
  end
  describe 'test attribute "json"' do
    it 'should work' do
      # assertion here. ref: https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers
    end
  end

end
