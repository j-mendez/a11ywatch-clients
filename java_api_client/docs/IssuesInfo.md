

# IssuesInfo


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**accessScoreAverage** | **Long** |  |  [optional]
**possibleIssuesFixedByCdn** | **Long** |  |  [optional]
**totalIssues** | **Long** |  |  [optional]
**issuesFixedByCdn** | **Long** |  |  [optional]
**errorCount** | **Long** |  |  [optional]
**warningCount** | **Long** |  |  [optional]
**noticeCount** | **Long** |  |  [optional]
**pageCount** | **Long** |  |  [optional]



