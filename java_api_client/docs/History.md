

# History


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Long** |  |  [optional]
**domain** | **String** |  |  [optional]
**insight** | [**WebsitesInsight**](WebsitesInsight.md) |  |  [optional]
**pageHeaders** | **List&lt;String&gt;** |  |  [optional]
**issuesInfo** | [**IssuesInfo**](IssuesInfo.md) |  |  [optional]



