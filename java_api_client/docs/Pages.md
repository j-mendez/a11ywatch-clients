

# Pages


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Long** |  |  [optional]
**userId** | **Long** |  |  [optional]
**domain** | **String** |  |  [optional]
**url** | **String** |  |  [optional]
**cdnConnected** | **Boolean** |  |  [optional]
**online** | **Boolean** |  |  [optional]
**pageLoadTime** | [**PageLoadTime**](PageLoadTime.md) |  |  [optional]
**insight** | [**WebsitesInsight**](WebsitesInsight.md) |  |  [optional]
**issuesInfo** | [**IssuesInfo**](IssuesInfo.md) |  |  [optional]
**lastScanDate** | **String** |  |  [optional]



