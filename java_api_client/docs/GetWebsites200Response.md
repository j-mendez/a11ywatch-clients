

# GetWebsites200Response


## Properties

| Name | Type | Description | Notes |
|------------ | ------------- | ------------- | -------------|
|**data** | [**List&lt;Websites&gt;**](Websites.md) |  |  [optional] |



