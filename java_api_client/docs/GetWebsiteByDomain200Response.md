

# GetWebsiteByDomain200Response


## Properties

| Name | Type | Description | Notes |
|------------ | ------------- | ------------- | -------------|
|**data** | [**Websites**](Websites.md) |  |  [optional] |



