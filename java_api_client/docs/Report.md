

# Report


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Long** |  |  [optional]
**domain** | **String** |  |  [optional]
**pageUrl** | **String** |  |  [optional]
**issues** | [**List&lt;PageIssue&gt;**](PageIssue.md) |  |  [optional]
**issuesInfo** | [**IssuesInfo**](IssuesInfo.md) |  |  [optional]



