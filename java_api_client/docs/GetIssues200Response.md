

# GetIssues200Response


## Properties

| Name | Type | Description | Notes |
|------------ | ------------- | ------------- | -------------|
|**data** | [**List&lt;Issues&gt;**](Issues.md) |  |  [optional] |



