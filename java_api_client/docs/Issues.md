

# Issues


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Long** |  |  [optional]
**userId** | **Long** |  |  [optional]
**domain** | **String** |  |  [optional]
**pageUrl** | **String** |  |  [optional]
**issues** | [**PageIssue**](PageIssue.md) |  |  [optional]



