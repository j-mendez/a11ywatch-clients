

# Websites


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Long** |  |  [optional]
**userId** | **Long** |  |  [optional]
**url** | **String** |  |  [optional]
**domain** | **String** |  |  [optional]
**crawlDuration** | **Long** |  |  [optional]
**cdnConnected** | **Boolean** |  |  [optional]
**pageInsights** | **Boolean** |  |  [optional]
**online** | **Boolean** |  |  [optional]
**mobile** | **Boolean** |  |  [optional]
**robots** | **Boolean** |  |  [optional]
**insight** | [**WebsitesInsight**](WebsitesInsight.md) |  |  [optional]
**pageHeaders** | **List&lt;String&gt;** |  |  [optional]
**pageLoadTime** | [**PageLoadTime**](PageLoadTime.md) |  |  [optional]
**issuesInfo** | [**IssuesInfo**](IssuesInfo.md) |  |  [optional]



