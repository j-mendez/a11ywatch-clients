

# PageLoadTime


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**duration** | **BigDecimal** |  |  [optional]
**durationFormated** | **String** |  |  [optional]
**color** | **String** |  |  [optional]



