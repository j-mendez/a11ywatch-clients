

# GetAnalytics200Response


## Properties

| Name | Type | Description | Notes |
|------------ | ------------- | ------------- | -------------|
|**data** | [**List&lt;Analytics&gt;**](Analytics.md) |  |  [optional] |



