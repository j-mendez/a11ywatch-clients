

# CrawlWebsiteStream200Response


## Properties

| Name | Type | Description | Notes |
|------------ | ------------- | ------------- | -------------|
|**data** | [**List&lt;Report&gt;**](Report.md) |  |  [optional] |



