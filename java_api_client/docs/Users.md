

# Users


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Long** |  |  [optional]
**email** | **String** |  |  [optional]
**jwt** | **String** |  |  [optional]
**role** | **String** |  |  [optional]
**password** | **String** |  |  [optional]
**alertEnabled** | **Boolean** |  |  [optional]



