

# PageIssue


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**recurrence** | **Long** |  |  [optional]
**typeCode** | **Long** |  |  [optional]
**code** | **String** |  |  [optional]
**message** | **String** |  |  [optional]
**context** | **String** |  |  [optional]
**selector** | **String** |  |  [optional]
**runner** | **String** |  |  [optional]
**type** | **String** |  |  [optional]



