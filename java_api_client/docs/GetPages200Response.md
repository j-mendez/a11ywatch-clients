

# GetPages200Response


## Properties

| Name | Type | Description | Notes |
|------------ | ------------- | ------------- | -------------|
|**data** | [**List&lt;Pages&gt;**](Pages.md) |  |  [optional] |



