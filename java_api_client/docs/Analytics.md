

# Analytics


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Long** |  |  [optional]
**domain** | **String** |  |  [optional]
**pageUrl** | **String** |  |  [optional]
**userId** | **Long** |  |  [optional]
**accessScore** | **Long** |  |  [optional]
**possibleIssuesFixedByCdn** | **Long** |  |  [optional]
**totalIssues** | **Long** |  |  [optional]
**issuesFixedByCdn** | **Long** |  |  [optional]
**errorCount** | **Long** |  |  [optional]
**warningCount** | **Long** |  |  [optional]
**noticeCount** | **Long** |  |  [optional]



