

# ScanWebsiteSimple200Response


## Properties

| Name | Type | Description | Notes |
|------------ | ------------- | ------------- | -------------|
|**data** | [**Report**](Report.md) |  |  [optional] |



