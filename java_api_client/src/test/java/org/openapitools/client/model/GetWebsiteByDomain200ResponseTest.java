/*
 * A11ywatch Client
 * The web accessibility tool built for scale.
 *
 * The version of the OpenAPI document: 0.7.66
 * Contact: support@a11ywatch.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package org.openapitools.client.model;

import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;
import org.openapitools.client.model.Websites;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;


/**
 * Model tests for GetWebsiteByDomain200Response
 */
public class GetWebsiteByDomain200ResponseTest {
    private final GetWebsiteByDomain200Response model = new GetWebsiteByDomain200Response();

    /**
     * Model tests for GetWebsiteByDomain200Response
     */
    @Test
    public void testGetWebsiteByDomain200Response() {
        // TODO: test GetWebsiteByDomain200Response
    }

    /**
     * Test the property 'data'
     */
    @Test
    public void dataTest() {
        // TODO: test data
    }

}
