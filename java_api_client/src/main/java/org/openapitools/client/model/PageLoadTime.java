/*
 * A11ywatch Client
 * The web accessibility tool built for scale.
 *
 * The version of the OpenAPI document: 0.7.66
 * Contact: support@a11ywatch.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package org.openapitools.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;
import java.math.BigDecimal;

/**
 * PageLoadTime
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaClientCodegen", date = "2023-05-31T13:16:51.899277-04:00[America/New_York]")
public class PageLoadTime {
  public static final String SERIALIZED_NAME_DURATION = "duration";
  @SerializedName(SERIALIZED_NAME_DURATION)
  private BigDecimal duration;

  public static final String SERIALIZED_NAME_DURATION_FORMATED = "durationFormated";
  @SerializedName(SERIALIZED_NAME_DURATION_FORMATED)
  private String durationFormated;

  public static final String SERIALIZED_NAME_COLOR = "color";
  @SerializedName(SERIALIZED_NAME_COLOR)
  private String color;


  public PageLoadTime duration(BigDecimal duration) {
    
    this.duration = duration;
    return this;
  }

   /**
   * Get duration
   * @return duration
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public BigDecimal getDuration() {
    return duration;
  }


  public void setDuration(BigDecimal duration) {
    this.duration = duration;
  }


  public PageLoadTime durationFormated(String durationFormated) {
    
    this.durationFormated = durationFormated;
    return this;
  }

   /**
   * Get durationFormated
   * @return durationFormated
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public String getDurationFormated() {
    return durationFormated;
  }


  public void setDurationFormated(String durationFormated) {
    this.durationFormated = durationFormated;
  }


  public PageLoadTime color(String color) {
    
    this.color = color;
    return this;
  }

   /**
   * Get color
   * @return color
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public String getColor() {
    return color;
  }


  public void setColor(String color) {
    this.color = color;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    PageLoadTime pageLoadTime = (PageLoadTime) o;
    return Objects.equals(this.duration, pageLoadTime.duration) &&
        Objects.equals(this.durationFormated, pageLoadTime.durationFormated) &&
        Objects.equals(this.color, pageLoadTime.color);
  }

  @Override
  public int hashCode() {
    return Objects.hash(duration, durationFormated, color);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class PageLoadTime {\n");
    sb.append("    duration: ").append(toIndentedString(duration)).append("\n");
    sb.append("    durationFormated: ").append(toIndentedString(durationFormated)).append("\n");
    sb.append("    color: ").append(toIndentedString(color)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

